﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Core.Domain.Enums
{
    public enum AccountAccessEnum
    {
        Admin = 1,

        Mgmt,

        Head,

        User

    }
}
