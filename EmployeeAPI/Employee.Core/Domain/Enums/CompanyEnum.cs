﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Core.Domain.Enums
{
    public enum CompanyEnum
    {
        EarthStone = 1,
        Farnaz,
        JBWFloorcenterINC,
        LusterplusINC,
        SouthMilandiaINC,
        ArtemesiaCORP,
        VerducoTradingINC,
        FastbuildCORP,
        FCTilesDepotPhilsINC,
        FCFloorcenterINC,
    }
}
