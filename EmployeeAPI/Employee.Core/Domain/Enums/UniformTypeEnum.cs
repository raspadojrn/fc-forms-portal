﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Core.Domain.Enums
{
    public enum UniformTypeEnum
    {
        PoloShirtRed = 1,

        PoloShirtWhite,

        TShirtRed,

        TShirtBlack,

        Blouse,

        Slacks,

        Shoes,

        Apron
    }
}
