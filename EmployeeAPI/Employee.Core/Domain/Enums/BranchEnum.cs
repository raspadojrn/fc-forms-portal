﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Core.Domain.Enums
{
    public enum BranchEnum
    {
        _1252 = 1,
        Baguio,
        MarbelNewBranch,
        GeneralSantosCity,
        Lapasan,
        Tetuan,
        Logistics,
        WarehouseValenzuela,
        Libis,

        PCI,
        Davao,
        Buhangin,
        Bulua,
        WarehouseDavao,
        AllBranches,
        Obrero,

        Mandaue,

        CebuCityImus = 19,

        Tagum =21,

        ImusCebu,
        Ecoland,
        Iloilo,
        Matina,
        Marbel,
        Butuan,
        Cotabato,
        Veterans,
        ArtemesiaCagayan,
        Pagadian,
        Bajada,
        Lipa,
        VerducoWarehouse,
        Roxas1,
        SF2,
        MUNOZ,
        _1125,
        DIVERSION,
        ANGELES,
        Iligan,
        Binondo,
        Roxas2,
        SMIBranches,
        WarehouseCagayan,
        NewQuezonAve,
        QuezonAve1,
        QuezonAve2,
        SSD,
        VisayasAve,
        Sacop,
        Mexico,
        NewAngeles,
        Marikina,
        Kawit,
        OldCommonwealth,
        MolinoCavite,
        Antipolo,
        Molino1,
        Molino2,
        WarehouseVerducoCebu,

        Commonwealth,
        Cainta,
        Commonwealth2,
        EPCI,
        IMUS,
        Lemery,
        Makati,
        McArthur,
        ProjectRoxas,
        ProjectMunoz,
        GensanOffice,
        CebuProject
    }
}
