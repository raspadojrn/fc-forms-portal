﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Core.Domain.Enums
{
    public enum LeaveReqDatePeriodEnum

    {
        OneDayOrMore = 1,

        HalfDayMorning,

        HalfDayAfternoon,


    }
}
