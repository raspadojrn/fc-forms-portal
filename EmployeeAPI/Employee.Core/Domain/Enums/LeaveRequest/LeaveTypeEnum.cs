﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Core.Domain.Enums
{
    public enum LeaveTypeEnum
    {
        VacationLeave =1,

        SickLeave,

        MaternityLeave,

        PaternityLeave
    }
}
