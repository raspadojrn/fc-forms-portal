﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Core.Domain.Enums
{
    public enum LeaveFormEnum
    {
        LeaveRequest = 1,

        RequestForOvertime,

        OfficialBusiness,

        IncidentReport,

        SuggestionsBox

    }
}
