﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Core.Domain.Enums
{
    public enum PurchaseRequestTypesEnum
    {
        OfficeSupplies = 1,

        FurnitureAndFixtures,

        ComputerOrLandline,

        Others,
    }
}
