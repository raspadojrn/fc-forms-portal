﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Core.Domain.Enums
{

    public enum BookletManualTypeEnum
    {
        Booklets = 1,

        Boxes,

        Pads,
    }
}
