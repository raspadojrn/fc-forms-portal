﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Core.Domain.Enums
{
    public enum BookletRequestEnum
    {
        ManualSalesInvoice = 1,

        ManualDeliveryReceipt,

        ManualCollectionReceipt,

        POSSalesInvoice,

        POSDeliveryReceipt,

        POSCollectionReceipt,

        POSTapeReceipt,

        BlankContinuousForm,

        OrderSlip,

        PettyCashVoucher,

        ReceivingReceipt,

        ThermalPaper
    }
}
