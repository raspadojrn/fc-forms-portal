﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Core.Domain.Enums.Marketing
{
    public enum  MarketingGiveAwaysEnum
    {
        Pens = 1,

        Fans,

        USB,

        PostIt,
    }
}
