﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Core.Domain.Enums
{

    public enum MarketingRequestTypeEnum
    {
        BillboardOrSignageReplacement = 1,

        TileSpecificationSheet,

        WallDisplayUpdate,

        AcrylicStandPosterUpdate,

        PrintedCatalogue,

        PPTPresentation,

        Giveaways,

        //SampleSwatches,

        Others
    }
}
