﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Core.Domain.Enums.Marketing
{
    public enum MarketingSampleSwatchesEnum
    {
        TileBoard = 1,

        TileBox,

        PRKit,
    }
}
