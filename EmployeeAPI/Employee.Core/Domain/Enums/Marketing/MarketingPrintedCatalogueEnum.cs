﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Core.Domain.Enums.Marketing
{
    public enum MarketingPrintedCatalogueEnum
    {
        Vinyl = 1,

        StoneAndConcrete,

        Polished,

        Matte,

        MarbleLook,

        Eagle,

        CeramicWoodLook,

        CompanyProfile,


    }
}
