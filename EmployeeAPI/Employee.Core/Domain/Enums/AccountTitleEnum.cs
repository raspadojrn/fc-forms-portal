﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Core.Domain.Enums
{
    public enum AccountTitleEnum
    {

        CommissionsContractor = 8,

        CommunicationExpense = 11,

        ElectricityExpense =18, 

        GasolineOilAndLubricantsExpenseDelivery,

        GasolineOilAndLubricantsExpenseOffice,

        MealsAndAllowances = 27,

        OfficeSuppliesExpense = 30,

        PostageAndDeliveries = 34,

        PrintingAndPhotocopyExpenses = 35,

        RepairsAndMaintenanceMotorVehicle = 37,

        RepairsAndMaintenanceOthers,

        RepairsAndMaintenanceStoreFFE,

        RepairsAndMaintenanceOfficeFFE,

        RepairsAndMaintenance_LeaseholdImprovements,

        RepresentationExpense,

        SalariesAndWagesLaborer =43,

        StoreSuppliesExpense =50,

        TravelAndTransportationExpense=54,

        WaterDrinkingExpense,

        WaterExpense,

        OtherExpense = 9999,
    }
}
