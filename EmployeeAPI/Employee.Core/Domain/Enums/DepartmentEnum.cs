﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Core.Domain.Enums
{

    public enum DepartmentEnum
    {
        HumanResource = 1,

        InventoryHeadOffice,

        Accounting,

        Marketing,

        TaxAndCompliance,

        External1252,


        IT =8,

        SaleSupport,

        Administrator,

        HRLegalDepartment,

        OperationsDepartmentLuster,

        OperationsDepartmentVerduco
    }
}
