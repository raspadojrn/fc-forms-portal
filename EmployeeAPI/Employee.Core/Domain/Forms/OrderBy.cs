﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Core.Domain.Forms
{
    public enum OrderBy
    {

        Id = 1,

        Name = 2,

        Status = 3

    }
}
