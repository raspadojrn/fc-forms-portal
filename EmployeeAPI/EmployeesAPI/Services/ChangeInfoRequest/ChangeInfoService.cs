﻿using Employee.Core.Domain.Forms;
using EmployeesAPI.DTOs.LeaveRequest;
using EmployeesAPI.Helpers;
using EmployeesAPI.Helpers.Extensions;
using EmployeesAPI.Helpers.Responses;
using EmployeesAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesAPI.Services.ChangeInfoRequest
{
    public class ChangeInfoService : IChangeInfoService
    {
        private readonly dbhprofilingContext _context;

        public ChangeInfoService(dbhprofilingContext dataContext)
        {
            _context = dataContext;
        }

        public object GetAll(GetAll criteria, AppSettings appSettings)
        {

            var deptRec = _context.Tbldepartment.AsNoTracking();
            var branchRec = _context.Tblbranch.AsNoTracking();
            var records = _context.ChangeInfoRequest.AsNoTracking()
                      .Select(p => new
                      {

                          p.Id,
                          p.DateCreated,
                          p.EmpId,
                          p.FirstName,
                          p.LastName,
                          p.EmailAddress,

                          p.CurrentBranch,
                          p.CurrentDepartment,
                          p.CurrentPosition,
                          p.CurrentCompany,
                          p.CurrentBiometricId,
                          p.CurrentHiringDate,
                          p.CurrentDateStart,
                          p.CurrentEmpStatus,
                          p.CurrentEducAttain,
                          p.CurrentSchoolName,
                          p.CurrentCourse,
                          p.CurrentAttendYr,
                          p.CurrentSssNo,
                          p.CurrentPagibigNo,
                          p.CurrentTinNo,
                          p.CurrentPhilHealthNo,
                          p.Reason,

                          p.NewBranch,
                          p.NewDepartment,
                          p.NewPosition,
                          p.NewCompany,
                          p.NewbiometricId,
                          p.NewHiringDate,
                          p.NewDateStart,
                          p.NewEmpStatus,
                          p.NewEducAttain,
                          p.NewSchoolName,
                          p.NewCourse,
                          p.NewAttendYr,
                          p.NewSssNo,
                          p.NewPagibigNo,
                          p.NewTinNo,
                          p.NewPhilHealthNo,

                          p.AdminApproval,
                          p.DateApprove,
                          p.ApproveRemarks,
                          p.Status,
                          p.Approver,

                      });

            //  Check if keyword is specified

            if (criteria.Status.HasValue())
            {
                records = records.Where(p => p.Status.Equals(criteria.Status));
            }

            //  Check if orderby is defined
            if (criteria.OrderBy.HasValue)
            {
                if (criteria.OrderBy == OrderBy.Id)
                {
                    if (criteria.OrderType == OrderType.Descending)
                    {
                        records = records.OrderByDescending(p => p.Id);
                    }
                    else
                    {
                        records = records.OrderBy(p => p.Id);
                    }
                }
            }
            GetAllResponse response = null;

            //  Check if user don't want to show all records
            if (criteria.ShowAll == false)
            {
                response = new GetAllResponse(records.Count(), criteria.CurrentPage, appSettings.RecordDisplayPerPage);

                //  Check if CurrentPage is greater than TotalPage
                if (criteria.CurrentPage > response.TotalPage)
                {
                    var error = new ErrorResponse();
                    error.ErrorMessages.Add(MessageHelper.NoRecordFound);

                    //  Return no record found error
                    return error;
                }

                records = records.Skip((criteria.CurrentPage - 1) * appSettings.RecordDisplayPerPage)
                                    .Take(appSettings.RecordDisplayPerPage);
            }
            else
            {
                response = new GetAllResponse(records.Count());
            }

            response.List.AddRange(records);

            return response;
        }
    }
}
