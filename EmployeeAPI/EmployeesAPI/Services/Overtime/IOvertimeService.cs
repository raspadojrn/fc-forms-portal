﻿using EmployeesAPI.DTOs.LeaveRequest;
using EmployeesAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesAPI.Services.Overtime
{
    public interface IOvertimeService
    {
        object GetAll(GetAll criteria, AppSettings appSettings);
    }
}
