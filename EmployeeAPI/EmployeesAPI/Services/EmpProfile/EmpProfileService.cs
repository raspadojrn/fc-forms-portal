﻿using EmployeesAPI.Helpers;
using EmployeesAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesAPI.Services.EmpProfile
{
    public class EmpProfileService : IEmpProfileService
    {
        private readonly dbhprofilingContext _context;

        public EmpProfileService(dbhprofilingContext dataContext)
        {
            _context = dataContext;
        }

        public object GetAll(AppSettings appSettings)
        {
            var deptRec = _context.Tbldepartment.AsNoTracking();
            var branchRec = _context.Tblbranch.AsNoTracking();
            var positionRec = _context.Tblposition.AsNoTracking();
            var companyRec = _context.Tblcompany.AsNoTracking();
            var records = _context.Tblprofile.AsNoTracking()
                      .Select(p => new
                      {
                          p.Id,
                          p.Fname,
                          p.Lname,
                          p.Address,
                          p.Allowance,
                          Branch = branchRec.Where(x => x.Id.ToString().Equals(p.Branch)).AsNoTracking().Select(x => x.Branch).FirstOrDefault(),
                          Department = deptRec.Where(x => x.Id.ToString().Equals(p.Department)).AsNoTracking().Select(x => x.Department).FirstOrDefault(),
                          p.Attendyear,
                          p.Bday,
                          p.Bio,
                          p.Cola,
                          Company = companyRec.Where(x => x.Id.ToString().Equals(p.Company)).AsNoTracking().Select(x => x.Company).FirstOrDefault(),
                          p.Conemergency,
                          p.Conperson,
                          p.Conpersontelno,
                          p.Course,
                          p.Cstatus,
                          p.Dateeoc,
                          p.Datefinalinterview,
                          p.Dateinitialinterview,
                          p.Datereg,
                          p.Datesalrychange,
                          p.Datestart,
                          p.Dependents,
                          p.Educattain,
                          p.Emaladd,
                          p.Employststus,
                          p.Evaluationdate,
                          p.Feedback,
                          p.Gender,
                          p.Hiresource,
                          p.Hiringdate,
                          p.Idemployehistory,
                          p.Idno,
                          p.Inviteby,
                          p.Marks1,
                          p.Marks2,
                          p.Marks3,
                          p.Marks4,
                          p.Mname,
                          p.Nationalid,
                          p.Pagibigno,
                          p.Philhealthno,
                          Position = positionRec.Where(x => x.Id.ToString().Equals(p.Position)).AsNoTracking().Select(x => x.Position).FirstOrDefault(),
                          p.Regstatus,
                          p.Salary,
                          p.Schoolname,
                          p.Sssno,
                          p.Telno,
                          p.Tinno,
                          p.Warehouse,


                      });
            var response = records;
            return response;

        }

        public object GetDataById(int? id)
        {
            var deptRec = _context.Tbldepartment.AsNoTracking();
            var branchRec = _context.Tblbranch.AsNoTracking();
            var positionRec = _context.Tblposition.AsNoTracking();
            var companyRec = _context.Tblcompany.AsNoTracking();
            var records = _context.Tblprofile
                      .Select(p => new
                      {
                          p.Id,
                          p.Fname,
                          p.Lname,
                          p.Address,
                          p.Allowance,
                          p.Branch,
                          p.Department,
                          branchName = branchRec.Where(x => x.Id.ToString().Equals(p.Branch)).AsNoTracking().Select(x => x.Branch).FirstOrDefault(),
                          departmentName = deptRec.Where(x => x.Id.ToString().Equals(p.Department)).AsNoTracking().Select(x => x.Department).FirstOrDefault(),

                          Branch_Dept = deptRec.Where(x => x.Id.ToString().Equals(p.Department)).AsNoTracking().Select(x => x.Department).FirstOrDefault() == null ?
                                        branchRec.Where(x => x.Id.ToString().Equals(p.Branch)).AsNoTracking().Select(x => x.Branch).FirstOrDefault() + " / " + "Undefined" :
                                        branchRec.Where(x => x.Id.ToString().Equals(p.Branch)).AsNoTracking().Select(x => x.Branch).FirstOrDefault() + " / " +
                                        deptRec.Where(x => x.Id.ToString().Equals(p.Department)).AsNoTracking().Select(x => x.Department).FirstOrDefault(),
                          p.Attendyear,
                          p.Bday,
                          p.Bio,
                          p.Cola,
                          p.Company,
                          companyName = companyRec.Where(x => x.Id.ToString().Equals(p.Company)).AsNoTracking().Select(x => x.Company).FirstOrDefault(),
                          p.Conemergency,
                          p.Conperson,
                          p.Conpersontelno,
                          p.Course,
                          p.Cstatus,
                          p.Dateeoc,
                          p.Datefinalinterview,
                          p.Dateinitialinterview,
                          p.Datereg,
                          p.Datesalrychange,
                          p.Datestart,
                          p.Dependents,
                          p.Educattain,
                          p.Emaladd,
                          p.Employststus,
                          p.Evaluationdate,
                          p.Feedback,
                          p.Gender,
                          p.Hiresource,
                          p.Hiringdate,
                          p.Idemployehistory,
                          p.Idno,
                          p.Inviteby,
                          p.Marks1,
                          p.Marks2,
                          p.Marks3,
                          p.Marks4,
                          p.Mname,
                          p.Nationalid,
                          p.Pagibigno,
                          p.Philhealthno,
                          p.Position,
                          positionName = positionRec.Where(x => x.Id.ToString().Equals(p.Position)).AsNoTracking().Select(x => x.Position).FirstOrDefault(),
                          p.Regstatus,
                          p.Salary,
                          p.Schoolname,
                          p.Sssno,
                          p.Telno,
                          p.Tinno,
                          p.Warehouse,


                      }).Where(p=> p.Id.Equals(id)).FirstOrDefault();
            var list = records;
            return list;
        }


    }
}
