﻿using EmployeesAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesAPI.Services.EmpProfile
{
    public interface IEmpProfileService
    {
        object GetAll(AppSettings appSettings);

        object GetDataById(int? id);
    }
}
