﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeesAPI.Models;
using EmployeesAPI.Helpers;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using EmployeesAPI.Helpers.Extensions;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Helpers.Responses;
using EmployeesAPI.DTOs.LeaveRequest;
using Employee.Core.Domain.Forms;

namespace EmployeesAPI.Services
{
    public class UserService : IUserService
    {
        private readonly dbhprofilingContext _context;

        public UserService(dbhprofilingContext dataContext)
        {
            _context = dataContext;
        }


        public Tbuser Authenticate(AuthenticateUser user)
        {
            if (user.UserName.HasValue() == false || user.Password.HasValue() == false)
            {
                return null;
            }

            var authenticatedUser = _context.Tbuser
                                            .SingleOrDefault(x => x.Username.ToLower() == user.UserName.ToLower() &&
                                                                  x.Password.ToLower() == user.Password.ToLower());

            if (authenticatedUser == null)
            {
                return null;
            }

            // Authentication successful
            return authenticatedUser;
        }


        public object GenerateToken(Tbuser authenticatedUser, AppSettings appSettings)
        {
            var empRecords = _context.Tblprofile.AsNoTracking();
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, authenticatedUser.Username),
                new Claim(JwtRegisteredClaimNames.Jti, authenticatedUser.Id.ToString()),
            };

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddHours(appSettings.TokenExpiration),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            return new
            {

                authenticatedUser.Id,
                authenticatedUser.Empid,
                authenticatedUser.Password,
                authenticatedUser.Username,
                authenticatedUser.Access,
                authenticatedUser.Status,
                fullName = empRecords.Where(x=> x.Id.Equals(authenticatedUser.Empid)).AsNoTracking().Select(x=> x.Fname).FirstOrDefault() + " " +
                           empRecords.Where(x => x.Id.Equals(authenticatedUser.Empid)).AsNoTracking().Select(x => x.Lname).FirstOrDefault(),
                Token = tokenString
            };
        }

        public object GetAll(AppSettings appSettings)
        {
            var deptRec = _context.Tbldepartment.AsNoTracking();
            var empRecords = _context.Tblprofile.AsNoTracking();
            var records = _context.Tbuser.AsNoTracking()
                    .Select(p => new
                    {
                        p.Id,
                        p.Datecreated,
                        p.Empid,
                        p.Access,
                        p.Username,
                        p.Password,
                        p.Status,
                        department = deptRec.Where(y => y.Id.ToString().Equals(empRecords.Where(x => x.Id.Equals(p.Empid)).Select(x => x.Department).FirstOrDefault())).Select(y => y.Department).FirstOrDefault() ,
                        fullName = empRecords.Where(x => x.Id.Equals(p.Empid)).Select(x => x.Fname).FirstOrDefault() +" "+
                                   empRecords.Where(x => x.Id.Equals(p.Empid)).Select(x => x.Lname).FirstOrDefault(),

                    });
            var response = records;
            return response;
        }


        public object GetAllFilter(GetAll criteria, AppSettings appSettings)
        {

            var empRecords = _context.Tblprofile.AsNoTracking();
            var records = _context.Tbuser.AsNoTracking()
                    .Select(p => new
                    {
                        p.Id,
                        p.Datecreated,
                        p.Empid,
                        p.Access,
                        p.Username,
                        p.Password,
                        p.Status,
                        fullName = empRecords.Where(x => x.Id.Equals(p.Empid)).Select(x => x.Fname).FirstOrDefault() + " " +
                                   empRecords.Where(x => x.Id.Equals(p.Empid)).Select(x => x.Lname).FirstOrDefault(),

                    });

            //  Check if keyword is specified

            if (criteria.Status.HasValue())
            {
                records = records.Where(p => p.Status.Equals(criteria.Status));
            }

            //  Check if orderby is defined
            if (criteria.OrderBy.HasValue)
            {
                if (criteria.OrderBy == OrderBy.Id)
                {
                    if (criteria.OrderType == OrderType.Descending)
                    {
                        records = records.OrderByDescending(p => p.Id);
                    }
                    else
                    {
                        records = records.OrderBy(p => p.Id);
                    }
                }
            }
            GetAllResponse response = null;

            //  Check if user don't want to show all records
            if (criteria.ShowAll == false)
            {
                response = new GetAllResponse(records.Count(), criteria.CurrentPage, appSettings.RecordDisplayPerPage);

                //  Check if CurrentPage is greater than TotalPage
                if (criteria.CurrentPage > response.TotalPage)
                {
                    var error = new ErrorResponse();
                    error.ErrorMessages.Add(MessageHelper.NoRecordFound);

                    //  Return no record found error
                    return error;
                }

                records = records.Skip((criteria.CurrentPage - 1) * appSettings.RecordDisplayPerPage)
                                    .Take(appSettings.RecordDisplayPerPage);
            }
            else
            {
                response = new GetAllResponse(records.Count());
            }

            response.List.AddRange(records);

            return response;
        }
    }
}
