﻿
using EmployeesAPI.DTOs.LeaveRequest;
using EmployeesAPI.Helpers;
using EmployeesAPI.Models;

namespace EmployeesAPI.Services
{
    public interface IUserService
    {

        /// <summary>
        /// Authenticate User
        /// </summary>
        /// <param name="user">User</param>
        /// <returns></returns>
        Tbuser Authenticate(AuthenticateUser user);

        /// <summary>
        /// Generate Token
        /// </summary>
        /// <param name="authenticatedUser"></param>
        /// <param name="appSettings"></param>
        /// <returns></returns>
        object GenerateToken(Tbuser authenticatedUser, AppSettings appSettings);

        object GetAll( AppSettings appSettings);

        object GetAllFilter(GetAll criteria, AppSettings appSettings);
    }
}
