﻿using EmployeesAPI.Helpers;

namespace EmployeesAPI.Services.Profile
{
    public interface IProfileService
    {
        /// <summary>
        /// Get all Assets
        /// </summary>
        /// <param name="appSettings">AppSettings</param>
        /// <returns></returns>
        object GetAll( AppSettings appSettings);
    }
}
