﻿using EmployeesAPI.Helpers;
using EmployeesAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesAPI.Services.Profile
{
    public class ProfileService : IProfileService
    {

        private readonly dbhprofilingContext _context;

        public ProfileService(dbhprofilingContext dataContext)
        {
            _context = dataContext;
        }

        public object GetAll( AppSettings appSettings)
        {

            var records = _context.Tblprofile;
                                            
            return records;
        }

    }
}
