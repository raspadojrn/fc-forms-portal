﻿using EmployeesAPI.DTOs.LeaveRequest;
using EmployeesAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesAPI.Services.PurchaseForm.Booklet
{
    public interface IBookletService
    {
        /// <summary>
        /// Get all Models
        /// </summary>
        /// <param name="criteria">Search criterias</param>
        /// <param name="appSettings">AppSettings</param>
        /// <returns></returns>
        object GetAllFilter(GetAll criteria, AppSettings appSettings);
    }
}
