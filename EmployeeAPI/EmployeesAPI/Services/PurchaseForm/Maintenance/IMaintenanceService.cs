﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeesAPI.DTOs.LeaveRequest;
using EmployeesAPI.Helpers;

namespace EmployeesAPI.Services.PurchaseForm.Maintenance
{
    public interface IMaintenanceService
    {
        /// <summary>
        /// Get all Models
        /// </summary>
        /// <param name="criteria">Search criterias</param>
        /// <param name="appSettings">AppSettings</param>
        /// <returns></returns>
        object GetAllFilter(GetAll criteria, AppSettings appSettings);
    }
}

