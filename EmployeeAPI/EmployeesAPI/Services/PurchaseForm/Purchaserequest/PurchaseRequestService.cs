﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Employee.Core.Domain.Forms;
using EmployeesAPI.DTOs.LeaveRequest;
using EmployeesAPI.Helpers;
using EmployeesAPI.Helpers.Extensions;
using EmployeesAPI.Helpers.Responses;
using EmployeesAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace EmployeesAPI.Services.PurchaseForm.Purchaserequest
{
    public class PurchaseRequestService : IPurchaseRequestService
    {

        private readonly dbhprofilingContext _context;

        public PurchaseRequestService(dbhprofilingContext dataContext)
        {
            _context = dataContext;
        }

        public object GetAllFilter(GetAll criteria, AppSettings appSettings)
        {


            var OrderedItems = _context.PrOrderedItems.AsNoTracking();
            var records = _context.PurchaseRequest.AsNoTracking()
                      .Select(p => new
                      {

                          p.Id,
                          p.DateCreated,
                          p.EmpId,
                          p.FirstName,
                          p.LastName,
                          p.Branch,

                          p.Department,
                          p.EmailAddress,
                          p.ContactNo,
                          p.Purpose,
                          p.Comment,
                          p.PurchaseType,
                          p.RequestingParty,
                          p.Price,
                          p.Status,

                          p.ApproveRemarks,
                          p.AdminApproval,
                          p.PciApproval,
                          p.DateApprove,
                          p.Approver,

                          OrderedItems = OrderedItems.Select(x => new
                          {
                              x.Id,
                              x.PurchaseRequestId,
                              x.DateCreated,
                              x.EmpId,
                              x.BranchRequest,
                              x.ItemCode,
                              x.ItemDesc,
                              x.UnitMeasure,
                              x.Quantity,
                              x.ApprovedQuantity,
                              x.Status,
                          }).Where(x => x.PurchaseRequestId.Equals(p.Id)),

                      });

            //  Check if keyword is specified

            if (criteria.Status.HasValue())
            {
                records = records.Where(p => p.Status.Equals(criteria.Status));
            }
            if (criteria.Firstname.HasValue())
            {
                records = records.Where(p => p.FirstName.Equals(criteria.Firstname));
            }

            if (criteria.Lastname.HasValue())
            {
                records = records.Where(p => p.LastName.Equals(criteria.Lastname));
            }

            if (criteria.EmpID != null)
            {
                records = records.Where(p => p.EmpId.Equals(criteria.EmpID));
            }

            if (criteria.DateFrom.HasValue)
            {
                records = records.Where(p => criteria.DateFrom.Value <= p.DateCreated);
            }

            if (criteria.DateTo.HasValue)
            {
                records = records.Where(p => criteria.DateTo.Value >= p.DateCreated);
            }

            //  Check if orderby is defined
            if (criteria.OrderBy.HasValue)
            {
                if (criteria.OrderBy == OrderBy.Id)
                {
                    if (criteria.OrderType == OrderType.Descending)
                    {
                        records = records.OrderByDescending(p => p.Id);
                    }
                    else
                    {
                        records = records.OrderBy(p => p.Id);
                    }
                }
            }
            GetAllResponse response = null;

            //  Check if user don't want to show all records
            if (criteria.ShowAll == false)
            {
                response = new GetAllResponse(records.Count(), criteria.CurrentPage, appSettings.RecordDisplayPerPage);

                //  Check if CurrentPage is greater than TotalPage
                if (criteria.CurrentPage > response.TotalPage)
                {
                    var error = new ErrorResponse();
                    error.ErrorMessages.Add(MessageHelper.NoRecordFound);

                    //  Return no record found error
                    return error;
                }

                records = records.Skip((criteria.CurrentPage - 1) * appSettings.RecordDisplayPerPage)
                                    .Take(appSettings.RecordDisplayPerPage);
            }
            else
            {
                response = new GetAllResponse(records.Count());
            }

            response.List.AddRange(records);

            return response;
        }

    }
}
