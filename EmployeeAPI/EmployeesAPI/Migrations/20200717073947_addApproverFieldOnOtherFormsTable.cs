﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EmployeesAPI.Migrations
{
    public partial class addApproverFieldOnOtherFormsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Approver",
                table: "RequestOT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Approver",
                table: "OfficialBusiness",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Approver",
                table: "IncidentReport",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Approver",
                table: "RequestOT");

            migrationBuilder.DropColumn(
                name: "Approver",
                table: "OfficialBusiness");

            migrationBuilder.DropColumn(
                name: "Approver",
                table: "IncidentReport");
        }
    }
}
