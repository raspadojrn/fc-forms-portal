﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EmployeesAPI.Migrations
{
    public partial class addField_UnitMeasureToTheTAbleThatHasQuantityField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "unitMeasure",
                table: "uniformOrder",
                unicode: false,
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "unitMeasure",
                table: "PR_orderedItems",
                unicode: false,
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "unitMeasure",
                table: "orderedBooklet",
                unicode: false,
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "unitMeasure",
                table: "marketingOrder",
                unicode: false,
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "unitMeasure",
                table: "businessCardRequest",
                unicode: false,
                maxLength: 255,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "unitMeasure",
                table: "uniformOrder");

            migrationBuilder.DropColumn(
                name: "unitMeasure",
                table: "PR_orderedItems");

            migrationBuilder.DropColumn(
                name: "unitMeasure",
                table: "orderedBooklet");

            migrationBuilder.DropColumn(
                name: "unitMeasure",
                table: "marketingOrder");

            migrationBuilder.DropColumn(
                name: "unitMeasure",
                table: "businessCardRequest");
        }
    }
}
