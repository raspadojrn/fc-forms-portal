﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EmployeesAPI.Migrations
{
    public partial class add_requestTypeColumnToMaintenanceRequestTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "requestType ",
                table: "maintenanceRequest",
                unicode: false,
                maxLength: 255,
                nullable: true);

        
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
       
        }
    }
}
