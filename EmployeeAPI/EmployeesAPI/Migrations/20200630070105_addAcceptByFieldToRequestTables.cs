﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EmployeesAPI.Migrations
{
    public partial class addAcceptByFieldToRequestTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AcceptBy",
                table: "RequestOT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AcceptBy",
                table: "OfficialBusiness",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AcceptBy",
                table: "leaveRequest",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AcceptBy",
                table: "IncidentReport",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AcceptBy",
                table: "RequestOT");

            migrationBuilder.DropColumn(
                name: "AcceptBy",
                table: "OfficialBusiness");

            migrationBuilder.DropColumn(
                name: "AcceptBy",
                table: "leaveRequest");

            migrationBuilder.DropColumn(
                name: "AcceptBy",
                table: "IncidentReport");
        }
    }
}
