﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EmployeesAPI.Migrations
{
    public partial class add_approvedQuantity_FieldTo_OrderedBooklet_Table_AndRemoveBookletOrdered : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "unitMeasure",
                table: "orderedBooklet");

            migrationBuilder.AddColumn<int>(
                name: "approvedQuantity",
                table: "orderedBooklet",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "approvedQuantity",
                table: "orderedBooklet");

            migrationBuilder.AddColumn<string>(
                name: "unitMeasure",
                table: "orderedBooklet",
                unicode: false,
                maxLength: 255,
                nullable: true);
        }
    }
}
