﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EmployeesAPI.Migrations
{
    public partial class removedImportImageTableAndReplaceWithImageNameIn_MaintenanceAndMarketingTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "importImage",
                table: "marketingMaterialRequest");

            migrationBuilder.DropColumn(
                name: "importImage",
                table: "maintenanceRequest");

            migrationBuilder.AddColumn<string>(
                name: "imageName",
                table: "marketingMaterialRequest",
                unicode: false,
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "imageName",
                table: "maintenanceRequest",
                unicode: false,
                maxLength: 255,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "imageName",
                table: "marketingMaterialRequest");

            migrationBuilder.DropColumn(
                name: "imageName",
                table: "maintenanceRequest");

            migrationBuilder.AddColumn<byte[]>(
                name: "importImage",
                table: "marketingMaterialRequest",
                type: "image",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "importImage",
                table: "maintenanceRequest",
                type: "image",
                nullable: true);
        }
    }
}
