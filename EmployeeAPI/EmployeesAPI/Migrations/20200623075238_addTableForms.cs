﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EmployeesAPI.Migrations
{
    public partial class addTableForms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IncidentReport",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    empId = table.Column<int>(nullable: false),
                    lastName = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    firstName = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    dateCreated = table.Column<DateTime>(type: "date", nullable: true),
                    emailAddress = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Branch = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Department = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    contactNo = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    dateOfEvent = table.Column<DateTime>(type: "date", nullable: true),
                    timeOfEvent = table.Column<TimeSpan>(type: "time(0)", nullable: true),
                    descriptionOfHappening = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Supervisor = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    incidentLocation = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Witness = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    personReporting = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Status = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    ApproveRemarks = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    DateApproved = table.Column<DateTime>(type: "date", nullable: true),
                    ApprovedBy = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncidentReport", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "leaveRequest",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    empId = table.Column<int>(nullable: false),
                    lastName = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    firstName = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    dateCreated = table.Column<DateTime>(type: "date", nullable: false),
                    emailAddress = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    leaveType = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    Branch = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Department = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    contactNo = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Reason = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Period = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    biometricId = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    dateFrom = table.Column<DateTime>(type: "date", nullable: true),
                    dateTo = table.Column<DateTime>(type: "date", nullable: true),
                    Comment = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Status = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    ApproveRemarks = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    DateApproved = table.Column<DateTime>(type: "date", nullable: true),
                    ApprovedBy = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_leaveRequest", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "OfficialBusiness",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    empId = table.Column<int>(nullable: false),
                    firstName = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    dateCreated = table.Column<DateTime>(type: "date", nullable: true),
                    emailAddress = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Branch = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Department = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    contactNo = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    biometricId = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Purpose = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    dateOfActivity = table.Column<DateTime>(type: "date", nullable: true),
                    timeFrom = table.Column<TimeSpan>(type: "time(0)", nullable: true),
                    timeTo = table.Column<TimeSpan>(type: "time(0)", nullable: true),
                    Position = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Remarks = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    contactPerson = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    cntactNoOfContactPerson = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Status = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    ApproveRemarks = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    DateApproved = table.Column<DateTime>(type: "date", nullable: true),
                    ApprovedBy = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfficialBusiness", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "RequestOT",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    empId = table.Column<int>(nullable: false),
                    lastName = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    firstName = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    dateCreated = table.Column<DateTime>(type: "date", nullable: true),
                    emailAddress = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Branch = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Department = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    contactNo = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Reason = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    dateOfOT = table.Column<DateTime>(type: "date", nullable: true),
                    timeFrom = table.Column<TimeSpan>(type: "time(0)", nullable: true),
                    timeTo = table.Column<TimeSpan>(type: "time(0)", nullable: true),
                    position = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Status = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    ApproveRemarks = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    DateApproved = table.Column<DateTime>(type: "date", nullable: true),
                    ApprovedBy = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RequestOT", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "SuggestionBox",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    empId = table.Column<int>(nullable: false),
                    lastName = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    firstName = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    dateCreated = table.Column<DateTime>(type: "date", nullable: true),
                    emailAddress = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Branch = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Department = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    contactNo = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Suggestions = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuggestionBox", x => x.id);
                });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IncidentReport");

            migrationBuilder.DropTable(
                name: "leaveRequest");

            migrationBuilder.DropTable(
                name: "OfficialBusiness");

            migrationBuilder.DropTable(
                name: "RequestOT");

            migrationBuilder.DropTable(
                name: "SuggestionBox");


        }
    }
}
