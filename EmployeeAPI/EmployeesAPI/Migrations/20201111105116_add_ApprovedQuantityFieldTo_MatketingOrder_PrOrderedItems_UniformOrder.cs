﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EmployeesAPI.Migrations
{
    public partial class add_ApprovedQuantityFieldTo_MatketingOrder_PrOrderedItems_UniformOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "approvedQuantity",
                table: "uniformOrder",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "approvedQuantity",
                table: "PR_orderedItems",
                unicode: false,
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "approvedQuantity",
                table: "marketingOrder",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "approvedQuantity",
                table: "uniformOrder");

            migrationBuilder.DropColumn(
                name: "approvedQuantity",
                table: "PR_orderedItems");

            migrationBuilder.DropColumn(
                name: "approvedQuantity",
                table: "marketingOrder");
        }
    }
}
