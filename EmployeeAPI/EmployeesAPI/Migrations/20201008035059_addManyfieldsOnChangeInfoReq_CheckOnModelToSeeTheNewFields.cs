﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EmployeesAPI.Migrations
{
    public partial class addManyfieldsOnChangeInfoReq_CheckOnModelToSeeTheNewFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "currentAttendYr",
                table: "changeInfoRequest",
                unicode: false,
                maxLength: 225,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "currentCourse",
                table: "changeInfoRequest",
                unicode: false,
                maxLength: 225,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "currentDateStart",
                table: "changeInfoRequest",
                type: "date",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "currentEducAttain",
                table: "changeInfoRequest",
                unicode: false,
                maxLength: 225,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "currentEmpStatus",
                table: "changeInfoRequest",
                unicode: false,
                maxLength: 225,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "currentHiringDate",
                table: "changeInfoRequest",
                type: "date",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "currentPagibigNo",
                table: "changeInfoRequest",
                unicode: false,
                maxLength: 225,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "currentPhilHealthNo",
                table: "changeInfoRequest",
                unicode: false,
                maxLength: 225,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "currentSchoolName",
                table: "changeInfoRequest",
                unicode: false,
                maxLength: 225,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "currentSssNo",
                table: "changeInfoRequest",
                unicode: false,
                maxLength: 225,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "currentTinNo",
                table: "changeInfoRequest",
                unicode: false,
                maxLength: 225,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "newAttendYr",
                table: "changeInfoRequest",
                unicode: false,
                maxLength: 225,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "newCourse",
                table: "changeInfoRequest",
                unicode: false,
                maxLength: 225,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "newDateStart",
                table: "changeInfoRequest",
                type: "date",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "newEducAttain",
                table: "changeInfoRequest",
                unicode: false,
                maxLength: 225,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "newEmpStatus",
                table: "changeInfoRequest",
                unicode: false,
                maxLength: 225,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "newHiringDate",
                table: "changeInfoRequest",
                type: "date",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "newPagibigNo",
                table: "changeInfoRequest",
                unicode: false,
                maxLength: 225,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "newPhilHealthNo",
                table: "changeInfoRequest",
                unicode: false,
                maxLength: 225,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "newSchoolName",
                table: "changeInfoRequest",
                unicode: false,
                maxLength: 225,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "newSssNo",
                table: "changeInfoRequest",
                unicode: false,
                maxLength: 225,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "newTinNo",
                table: "changeInfoRequest",
                unicode: false,
                maxLength: 225,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "currentAttendYr",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "currentCourse",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "currentDateStart",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "currentEducAttain",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "currentEmpStatus",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "currentHiringDate",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "currentPagibigNo",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "currentPhilHealthNo",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "currentSchoolName",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "currentSssNo",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "currentTinNo",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "newAttendYr",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "newCourse",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "newDateStart",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "newEducAttain",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "newEmpStatus",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "newHiringDate",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "newPagibigNo",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "newPhilHealthNo",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "newSchoolName",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "newSssNo",
                table: "changeInfoRequest");

            migrationBuilder.DropColumn(
                name: "newTinNo",
                table: "changeInfoRequest");
        }
    }
}
