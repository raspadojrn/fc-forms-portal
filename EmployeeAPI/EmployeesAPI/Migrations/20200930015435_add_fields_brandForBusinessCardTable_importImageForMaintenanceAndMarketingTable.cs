﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EmployeesAPI.Migrations
{
    public partial class add_fields_brandForBusinessCardTable_importImageForMaintenanceAndMarketingTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "importImage",
                table: "marketingMaterialRequest",
                type: "image",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "importImage",
                table: "maintenanceRequest",
                type: "image",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "brand",
                table: "businessCardRequest",
                unicode: false,
                maxLength: 255,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "importImage",
                table: "marketingMaterialRequest");

            migrationBuilder.DropColumn(
                name: "importImage",
                table: "maintenanceRequest");

            migrationBuilder.DropColumn(
                name: "brand",
                table: "businessCardRequest");
        }
    }
}
