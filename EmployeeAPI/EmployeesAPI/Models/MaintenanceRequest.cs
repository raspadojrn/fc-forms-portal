﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeesAPI.Models
{
    public partial class MaintenanceRequest
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime DateCreated { get; set; }
        public string EmailAddress { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string Company { get; set; }
        public string ContactNo { get; set; } 
        public string ImageName { get; set; }
        public string RequestType { get; set; }
        public string NatureOfWork { get; set; }
        public DateTime? DateNeeded { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public string ApproveRemarks { get; set; }
        public string AdminApproval { get; set; }
        public DateTime? DateApprove { get; set; }
        public string Approver { get; set; }
    }
}
