﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class PettyCash
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Payee { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime PettyCashDate { get; set; }
        public string EmailAddress { get; set; }
        public int TotalAmount { get; set; }
        public string AmountInWord { get; set; }
        public string Classification { get; set; }
        public string DivourcherId { get; set; }
        public string Company { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public string ContactNo { get; set; }
        public string Reason { get; set; }
        public string PreparedBy { get; set; }
        public string CheckedBy { get; set; }
        public string ApprovedBy { get; set; }
        public string AuditBy { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
    }
}
