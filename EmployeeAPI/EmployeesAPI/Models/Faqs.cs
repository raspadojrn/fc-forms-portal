﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class Faqs
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string Questions { get; set; }
        public string Answers { get; set; }
    }
}
