﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class PurchaseRequest
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime DateCreated { get; set; }
        public string EmailAddress { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public string ContactNo { get; set; }
        public string Purpose { get; set; }
        public string Comment { get; set; }
        public string PurchaseType { get; set; }
        public string RequestingParty { get; set; }
        public double? Price { get; set; }
        public string Status { get; set; }
        public string ApproveRemarks { get; set; }
        public string AdminApproval { get; set; }
        public string PciApproval { get; set; }
        public DateTime? DateApprove { get; set; }
        public string Approver { get; set; }
    }
}
