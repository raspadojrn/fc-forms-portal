﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class LeaveRequest
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime DateCreated { get; set; }
        public string EmailAddress { get; set; }
        public string LeaveType { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public string ContactNo { get; set; }
        public string Reason { get; set; }
        public string Period { get; set; }
        public string BiometricId { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }

        public string ApproveRemarks { get; set; }
        public DateTime DateApproved { get; set; }
        public string AcceptBy { get; set; }
        public string ApprovedBy { get; set; }

        public string Approver { get; set; }
    }
}
