﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class Tblwarehouse
    {
        public int Id { get; set; }
        public string Warehouse { get; set; }
        public string Wareadd { get; set; }
        public DateTime? Datecreated { get; set; }
    }
}
