﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class Tblpayee
    {
        public int Id { get; set; }
        public string Payee { get; set; }
        public string Accountname { get; set; }
        public string Accountnumber { get; set; }
        public string Address { get; set; }
        public DateTime? Datecreated { get; set; }
        public string Tin { get; set; }
        public string Vat { get; set; }
        public string Branch { get; set; }
    }
}
