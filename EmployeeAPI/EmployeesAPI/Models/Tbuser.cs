﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class Tbuser
    {
        public int Id { get; set; }
        public int? Empid { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime? Datecreated { get; set; }
        public string Status { get; set; }
        public string Access { get; set; }

        //public ICollection<Tblprofile> EmpDetails { get; set; }
    }
}
