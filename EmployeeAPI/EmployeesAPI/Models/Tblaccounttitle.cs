﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class Tblaccounttitle
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public string Class { get; set; }
        public string Particulars { get; set; }
        public decimal? Percentage { get; set; }
        public DateTime? Datecreated { get; set; }
        public string Category { get; set; }
    }
}
