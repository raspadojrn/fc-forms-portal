﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class Tblbranch
    {
        public int Id { get; set; }
        public string Branch { get; set; }
        public string Branchadd { get; set; }
        public int? Idcompany { get; set; }
        public DateTime? Datecreated { get; set; }
    }
}
