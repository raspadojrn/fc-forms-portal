﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class UniformOrder
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public DateTime DateCreated { get; set; }
        public string Gender { get; set; }
        public string UniformType { get; set; }
        public string Size { get; set; }
        public string UnitMeasure { get; set; }
        public int? Quantity { get; set; }
        public int? ApprovedQuantity { get; set; }
        public int UniformRequestId { get; set; }
        public string Status { get; set; }
    }
}
