﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class PettyCashOrders
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string Amount { get; set; }
        public string Particulars { get; set; }
        public string ImageName { get; set; }
        public string AccountTitle { get; set; }
        public string AccountTitleId { get; set; }
        public int PettyCashId { get; set; }
    }
}
