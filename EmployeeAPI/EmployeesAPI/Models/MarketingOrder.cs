﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class MarketingOrder
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public DateTime DateCreated { get; set; }
        public string RequestType { get; set; }
        public string Item { get; set; }
        public string Description { get; set; }
        public string UnitMeasure { get; set; }
        public int? Quantity { get; set; }
        public int? ApprovedQuantity { get; set; }
        public int MarketingRequestId { get; set; }
        public string Status { get; set; }
    }
}
