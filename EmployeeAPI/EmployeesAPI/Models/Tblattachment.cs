﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class Tblattachment
    {
        public int Id { get; set; }
        public int? Attidemp { get; set; }
        public string Description { get; set; }
        public DateTime? Datecreated { get; set; }
        public DateTime? Datedocs { get; set; }
        public string Filelocation { get; set; }
    }
}
