﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class ChangeInfoRequest
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime DateCreated { get; set; }
        public string EmailAddress { get; set; }
        public string CurrentBranch { get; set; }
        public string CurrentDepartment { get; set; }
        public string CurrentPosition { get; set; }
        public string CurrentCompany { get; set; }
        public string CurrentBiometricId { get; set; }

        //New Fields
        public DateTime CurrentHiringDate { get; set; }
        public DateTime CurrentDateStart { get; set; }
        public string CurrentEmpStatus { get; set; }
        public string CurrentEducAttain { get; set; }
        public string CurrentSchoolName { get; set; }
        public string CurrentCourse { get; set; }
        public string CurrentAttendYr { get; set; }
        public string CurrentSssNo { get; set; }
        public string CurrentPagibigNo { get; set; }
        public string CurrentTinNo { get; set; }
        public string CurrentPhilHealthNo { get; set; }

        public string Reason { get; set; }
        public string NewBranch { get; set; }
        public string NewDepartment { get; set; }
        public string NewPosition { get; set; }
        public string NewCompany { get; set; }
        public string NewbiometricId { get; set; }

        //New Fields
        public DateTime NewHiringDate { get; set; }
        public DateTime NewDateStart { get; set; }
        public string NewEmpStatus { get; set; }
        public string NewEducAttain { get; set; }
        public string NewSchoolName { get; set; }
        public string NewCourse { get; set; }
        public string NewAttendYr { get; set; }
        public string NewSssNo { get; set; }
        public string NewPagibigNo { get; set; }
        public string NewTinNo { get; set; }
        public string NewPhilHealthNo { get; set; }

        public string Status { get; set; }
        public string ApproveRemarks { get; set; }
        public string AdminApproval { get; set; }
        public DateTime? DateApprove { get; set; }
        public string Approver { get; set; }
    }
}
