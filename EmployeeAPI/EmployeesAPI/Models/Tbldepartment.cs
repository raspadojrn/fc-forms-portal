﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class Tbldepartment
    {
        public int Id { get; set; }
        public string Department { get; set; }
        public DateTime? Datecreated { get; set; }
    }
}
