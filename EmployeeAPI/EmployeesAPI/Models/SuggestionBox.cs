﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class SuggestionBox
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime? DateCreated { get; set; }
        public string EmailAddress { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public string ContactNo { get; set; }
        public string Suggestions { get; set; }
    }
}
