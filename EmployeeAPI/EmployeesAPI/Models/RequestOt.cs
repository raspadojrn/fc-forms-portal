﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class RequestOt
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime? DateCreated { get; set; }
        public string EmailAddress { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public string ContactNo { get; set; }
        public string Reason { get; set; }
        public DateTime? DateOfOt { get; set; }
        public TimeSpan? TimeFrom { get; set; }
        public TimeSpan? TimeTo { get; set; }
        public string Position { get; set; }
        public string Status { get; set; }

        public string ApproveRemarks { get; set; }
        public DateTime DateApproved { get; set; }
        public string AcceptBy { get; set; }
        public string ApprovedBy { get; set; }

        public string Approver { get; set; }
    }
}
