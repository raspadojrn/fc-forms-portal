﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class PrOrderedItems
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public DateTime DateCreated { get; set; }
        public string BranchRequest { get; set; }
        public string ItemCode { get; set; }
        public string ItemDesc { get; set; }
        public string UnitMeasure { get; set; }
        public string Quantity { get; set; }
        public string ApprovedQuantity { get; set; }
        public int PurchaseRequestId { get; set; }
        public string Status { get; set; }
    }
}
