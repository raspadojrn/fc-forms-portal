﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class Tblprofile
    {
        public int Id { get; set; }
        public string Idno { get; set; }
        public string Lname { get; set; }
        public string Fname { get; set; }
        public string Mname { get; set; }
        public string Emaladd { get; set; }
        public string Address { get; set; }
        public string Telno { get; set; }
        public DateTime? Bday { get; set; }
        public string Gender { get; set; }
        public string Cstatus { get; set; }
        public string Nationalid { get; set; }
        public string Sssno { get; set; }
        public string Pagibigno { get; set; }
        public string Tinno { get; set; }
        public string Philhealthno { get; set; }
        public string Conperson { get; set; }
        public string Conpersontelno { get; set; }
        public string Inviteby { get; set; }
        public string Educattain { get; set; }
        public string Schoolname { get; set; }
        public string Course { get; set; }
        public string Attendyear { get; set; }
        public string Dependents { get; set; }
        public string Employststus { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public string Company { get; set; }
        public string Warehouse { get; set; }
        public string Position { get; set; }
        public DateTime? Hiringdate { get; set; }
        public string Hiresource { get; set; }
        public string Feedback { get; set; }
        public DateTime? Datestart { get; set; }
        public DateTime? Dateinitialinterview { get; set; } 
        public DateTime? Datefinalinterview { get; set; }
        public string Salary { get; set; }
        public string Cola { get; set; }
        public string Allowance { get; set; }
        public DateTime? Datesalrychange { get; set; }
        public DateTime? Evaluationdate { get; set; }
        public string Regstatus { get; set; }
        public DateTime? Datereg { get; set; }
        public DateTime? Dateeoc { get; set; }
        public string Marks1 { get; set; }
        public string Marks2 { get; set; }
        public string Marks3 { get; set; }
        public string Marks4 { get; set; }
        public string Bio { get; set; }
        public string Conemergency { get; set; }
        public int? Idemployehistory { get; set; }
    }
}
