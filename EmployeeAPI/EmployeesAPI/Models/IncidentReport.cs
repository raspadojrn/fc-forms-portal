﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class IncidentReport
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime? DateCreated { get; set; }
        public string EmailAddress { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public string ContactNo { get; set; }
        public DateTime? DateOfEvent { get; set; }
        public TimeSpan? TimeOfEvent { get; set; }
        public string DescriptionOfHappening { get; set; }
        public string Supervisor { get; set; }
        public string IncidentLocation { get; set; }
        public string Witness { get; set; }
        public string PersonReporting { get; set; }
        public string Status { get; set; }

        public string ApproveRemarks { get; set; }
        public DateTime DateApproved { get; set; }
        public string AcceptBy { get; set; }
        public string ApprovedBy { get; set; }

        public string Approver { get; set; }
    }
}
