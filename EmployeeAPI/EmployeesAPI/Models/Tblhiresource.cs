﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class Tblhiresource
    {
        public int Id { get; set; }
        public string Sourcehire { get; set; }
        public DateTime? Datecreated { get; set; }
    }
}
