﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class Tblposition
    {
        public int Id { get; set; }
        public string Position { get; set; }
        public DateTime? Datecreated { get; set; }
    }
}
