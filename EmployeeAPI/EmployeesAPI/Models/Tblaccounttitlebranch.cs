﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class Tblaccounttitlebranch
    {
        public int Id { get; set; }
        public string Company { get; set; }
        public string Branch { get; set; }
        public DateTime? Datecreated { get; set; }
    }
}
