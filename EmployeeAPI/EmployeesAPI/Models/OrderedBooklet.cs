﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class OrderedBooklet
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public DateTime DateCreated { get; set; }
        public string RequestType { get; set; }
        public string ManualType { get; set; }
        public string FromToMsiv { get; set; }
        public string UnusedMsiv { get; set; }
        public int? Quantity { get; set; }
        public int? ApprovedQuantity { get; set; }
        public int BookletRequestId { get; set; }
        public string Status { get; set; }
    }
}
