﻿using System;
using System.Collections.Generic;

namespace EmployeesAPI.Models
{
    public partial class Tblassignmenthistory
    {
        public int Id { get; set; }
        public int? Assidemp { get; set; }
        public int? Assidbranch { get; set; }
        public int? Assiddept { get; set; }
        public int? Assidcompany { get; set; }
        public int? Assidwarehouse { get; set; }
        public int? Assidposition { get; set; }
        public DateTime? Assdatestart { get; set; }
    }
}
