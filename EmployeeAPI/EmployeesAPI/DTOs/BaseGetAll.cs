﻿

namespace EmployeesAPI.DTOs
{
    public abstract class BaseGetAll
    {

        public BaseGetAll()
        {
            CurrentPage = 1;
        }

        public int CurrentPage { get; set; }

        public bool ShowAll { get; set; }

    }
}
