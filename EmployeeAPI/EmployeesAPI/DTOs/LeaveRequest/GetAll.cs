﻿using Employee.Core.Domain.Forms;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeesAPI.DTOs.LeaveRequest
{
    public class GetAll : BaseGetAll
    {
        public int? EmpID { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateFrom { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateTo { get; set; }


        public OrderBy? OrderBy { get; set; }

        public OrderType? OrderType { get; set; }

        public string Status{ get; set; }
    }
}
