﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesAPI.Helpers.Responses
{
    public abstract class BaseResponse
    {

        public bool Success { get; internal set; }

    }
}
