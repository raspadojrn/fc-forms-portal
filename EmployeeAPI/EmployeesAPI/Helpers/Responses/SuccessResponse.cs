﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesAPI.Helpers.Responses
{
    public class SuccessResponse : BaseResponse
    {

        public SuccessResponse()
        {
            base.Success = true;
        }

    }
}
