﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesAPI.Helpers.Responses
{
    public class ErrorResponse : BaseResponse
    {

        public ErrorResponse()
        {
            this.ErrorMessages = new List<object>();
        }

        public List<object> ErrorMessages { get; set; }

    }
}
