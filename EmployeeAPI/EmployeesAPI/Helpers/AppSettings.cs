﻿namespace EmployeesAPI.Helpers
{
    public class AppSettings
    {

        public int RecordDisplayPerPage { get; set; }

        public string Secret { get; set; }

        public int TokenExpiration { get; set; }

        public string Item_temp_image { get; set; }

    }
}
