﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace EmployeesAPI.Models
{
    public partial class dbhprofilingContext : DbContext
    {
        public dbhprofilingContext()
        {
        }

        public dbhprofilingContext(DbContextOptions<dbhprofilingContext> options)
            : base(options)
        {
        }

        public virtual DbSet<IncidentReport> IncidentReport { get; set; }
        public virtual DbSet<LeaveRequest> LeaveRequest { get; set; }
        public virtual DbSet<OfficialBusiness> OfficialBusiness { get; set; }
        public virtual DbSet<PrOrderedItems> PrOrderedItems { get; set; }
        public virtual DbSet<PurchaseRequest> PurchaseRequest { get; set; }
        public virtual DbSet<UniformRequest> UniformRequest { get; set; }
        public virtual DbSet<UniformOrder> UniformOrder { get; set; }
        public virtual DbSet<BusinessCardRequest> BusinessCardRequest { get; set; }
        public virtual DbSet<BookletRequest> BookletRequest { get; set; }
        public virtual DbSet<OrderedBooklet> OrderedBooklet { get; set; }
        public virtual DbSet<MarketingMaterialRequest> MarketingMaterialRequest { get; set; }
        public virtual DbSet<MarketingOrder> MarketingOrder { get; set; }
        public virtual DbSet<MaintenanceRequest> MaintenanceRequest { get; set; }
        public virtual DbSet<PettyCash> PettyCash { get; set; }
        public virtual DbSet<PettyCashOrders> PettyCashOrders { get; set; }
        public virtual DbSet<RequestOt> RequestOt { get; set; }
        public virtual DbSet<SuggestionBox> SuggestionBox { get; set; }
        public virtual DbSet<Faqs> Faqs { get; set; }
        public virtual DbSet<ChangeInfoRequest> ChangeInfoRequest { get; set; }
        public virtual DbSet<Tblaccounttitle> Tblaccounttitle { get; set; }
        public virtual DbSet<Tblaccounttitlebranch> Tblaccounttitlebranch { get; set; }
        public virtual DbSet<Tblpayee> Tblpayee { get; set; }

        public virtual DbSet<Tblassignmenthistory> Tblassignmenthistory { get; set; }
        public virtual DbSet<Tblattachment> Tblattachment { get; set; }
        public virtual DbSet<Tblbranch> Tblbranch { get; set; }
        public virtual DbSet<Tblcompany> Tblcompany { get; set; }
        public virtual DbSet<Tbldepartment> Tbldepartment { get; set; }
        public virtual DbSet<Tbleducational> Tbleducational { get; set; }
        public virtual DbSet<Tblhiresource> Tblhiresource { get; set; }
        public virtual DbSet<Tblposition> Tblposition { get; set; }
        public virtual DbSet<Tblprofile> Tblprofile { get; set; }
        public virtual DbSet<Tblwarehouse> Tblwarehouse { get; set; }
        public virtual DbSet<Tbuser> Tbuser { get; set; }

        // Unable to generate entity type for table 'dbo.tblemploymenthistory'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tblsalaryhistory'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=LAPTOP-CSESUCBC;Database=dbh-profiling;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IncidentReport>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Branch)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNo)
                    .HasColumnName("contactNo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("date");

                entity.Property(e => e.DateOfEvent)
                    .HasColumnName("dateOfEvent")
                    .HasColumnType("date");

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DescriptionOfHappening)
                    .HasColumnName("descriptionOfHappening")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress)
                    .HasColumnName("emailAddress")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmpId).HasColumnName("empId");

                entity.Property(e => e.FirstName)
                    .HasColumnName("firstName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IncidentLocation)
                    .HasColumnName("incidentLocation")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("lastName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PersonReporting)
                    .HasColumnName("personReporting")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Supervisor)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TimeOfEvent)
                    .HasColumnName("timeOfEvent")
                    .HasColumnType("time(0)");

                entity.Property(e => e.Witness)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LeaveRequest>(entity =>
            {
                entity.ToTable("leaveRequest");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BiometricId)
                    .HasColumnName("biometricId")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Branch)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Comment)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNo)
                    .HasColumnName("contactNo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("date");

                entity.Property(e => e.DateFrom)
                    .HasColumnName("dateFrom")
                    .HasColumnType("date");

                entity.Property(e => e.DateTo)
                    .HasColumnName("dateTo")
                    .HasColumnType("date");

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress)
                    .HasColumnName("emailAddress")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmpId).HasColumnName("empId");

                entity.Property(e => e.FirstName)
                    .HasColumnName("firstName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("lastName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LeaveType)
                    .IsRequired()
                    .HasColumnName("leaveType")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Period)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Reason)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OfficialBusiness>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BiometricId)
                    .HasColumnName("biometricId")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Branch)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CntactNoOfContactPerson)
                    .HasColumnName("cntactNoOfContactPerson")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNo)
                    .HasColumnName("contactNo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPerson)
                    .HasColumnName("contactPerson")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("date");

                entity.Property(e => e.WorkLocationName)
                    .HasColumnName("workLocationName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateOfActivity)
                    .HasColumnName("dateOfActivity")
                    .HasColumnType("date");

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress)
                    .HasColumnName("emailAddress")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmpId).HasColumnName("empId");

                entity.Property(e => e.FirstName)
                    .HasColumnName("firstName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Position)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Purpose)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Remarks)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TimeFrom)
                    .HasColumnName("timeFrom")
                    .HasColumnType("time(0)");

                entity.Property(e => e.TimeTo)
                    .HasColumnName("timeTo")
                    .HasColumnType("time(0)");
            });

            modelBuilder.Entity<PrOrderedItems>(entity =>
            {
                entity.ToTable("PR_orderedItems");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("date");

                entity.Property(e => e.EmpId).HasColumnName("empId");

                entity.Property(e => e.BranchRequest)
                    .HasColumnName("branchRequest")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ItemCode)
                    .HasColumnName("itemCode")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ItemDesc)
                    .HasColumnName("itemDesc")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PurchaseRequestId).HasColumnName("purchaseRequestId");

                entity.Property(e => e.UnitMeasure)
                    .HasColumnName("unitMeasure")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ApprovedQuantity)
                    .HasColumnName("approvedQuantity")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PurchaseRequest>(entity =>
            {
                entity.ToTable("purchaseRequest");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AdminApproval)
                    .HasColumnName("adminApproval")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ApproveRemarks)
                    .HasColumnName("approveRemarks")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Approver)
                    .HasColumnName("approver")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Branch)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNo)
                    .HasColumnName("contactNo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateApprove)
                    .HasColumnName("dateApprove")
                    .HasColumnType("date");

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("date");

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress)
                    .HasColumnName("emailAddress")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmpId).HasColumnName("empId");

                entity.Property(e => e.FirstName)
                    .HasColumnName("firstName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("lastName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PciApproval)
                    .HasColumnName("pciApproval")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Price).HasColumnName("price");

                entity.Property(e => e.Purpose)
                    .HasColumnName("purpose")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PurchaseType)
                    .HasColumnName("purchaseType ")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RequestingParty)
                    .HasColumnName("requestingParty")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UniformRequest>(entity =>
            {
                entity.ToTable("uniformRequest");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AdminApproval)
                    .HasColumnName("adminApproval")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ApproveRemarks)
                    .HasColumnName("approveRemarks")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Approver)
                    .HasColumnName("approver")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Branch)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNo)
                    .HasColumnName("contactNo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateApprove)
                    .HasColumnName("dateApprove")
                    .HasColumnType("date");

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("date");

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress)
                    .HasColumnName("emailAddress")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmpId).HasColumnName("empId");

                entity.Property(e => e.EmployeeStatus)
                    .HasColumnName("employeeStatus")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasColumnName("firstName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .HasColumnName("gender")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("lastName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Position)
                    .HasMaxLength(225)
                    .IsUnicode(false);

                entity.Property(e => e.Price).HasColumnName("price");

                entity.Property(e => e.Purpose)
                    .HasColumnName("purpose")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                //entity.Property(e => e.Quantity)
                //    .HasColumnName("quantity")
                //    .HasMaxLength(255)
                //    .IsUnicode(false);

                entity.Property(e => e.ReplacementOrAdditional)
                    .HasColumnName("replacementOrAdditional")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                //entity.Property(e => e.SizeBlouse)
                //    .HasColumnName("size_Blouse")
                //    .HasMaxLength(255)
                //    .IsUnicode(false);

                //entity.Property(e => e.SizePoloShirt)
                //    .HasColumnName("size_poloShirt")
                //    .HasMaxLength(255)
                //    .IsUnicode(false);

                //entity.Property(e => e.SizeShoes)
                //    .HasColumnName("size_shoes")
                //    .HasMaxLength(255)
                //    .IsUnicode(false);

                //entity.Property(e => e.SizeSlacks)
                //    .HasColumnName("size_Slacks")
                //    .HasMaxLength(255)
                //    .IsUnicode(false);

                //entity.Property(e => e.SizeTshirt)
                //    .HasColumnName("size_Tshirt")
                //    .HasMaxLength(255)
                //    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                //entity.Property(e => e.UniformType)
                //    .HasColumnName("uniformType")
                //    .HasMaxLength(255)
                //    .IsUnicode(false);
            });

            modelBuilder.Entity<UniformOrder>(entity =>
            {
                entity.ToTable("uniformOrder");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("date");

                entity.Property(e => e.EmpId).HasColumnName("empId");

                entity.Property(e => e.Gender)
                    .HasColumnName("gender")
                    .HasMaxLength(255)
                    .IsUnicode(false);


                entity.Property(e => e.UnitMeasure)
                    .HasColumnName("unitMeasure")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.Property(e => e.ApprovedQuantity).HasColumnName("approvedQuantity");

                entity.Property(e => e.Size)
                    .HasColumnName("size")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UniformRequestId).HasColumnName("uniformRequestId");

                entity.Property(e => e.UniformType)
                    .HasColumnName("uniformType")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BusinessCardRequest>(entity =>
            {
                entity.ToTable("businessCardRequest");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Address)
                    .HasColumnName("address")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AdminApproval)
                    .HasColumnName("adminApproval")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ApproveRemarks)
                    .HasColumnName("approveRemarks")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Approver)
                    .HasColumnName("approver")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Branch)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Company)
                    .HasMaxLength(225)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNo)
                    .HasColumnName("contactNo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateApprove)
                    .HasColumnName("dateApprove")
                    .HasColumnType("date");

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("date");

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress)
                    .HasColumnName("emailAddress")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmpId).HasColumnName("empId");

                entity.Property(e => e.Brand)
                    .HasColumnName("brand")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasColumnName("firstName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Landline)
                    .HasColumnName("landline")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("lastName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Mobile)
                    .HasColumnName("mobile")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NameOnCard)
                    .HasColumnName("nameOnCard")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Position)
                    .HasMaxLength(225)
                    .IsUnicode(false);

                entity.Property(e => e.Price).HasColumnName("price");

                entity.Property(e => e.Purpose)
                    .HasColumnName("purpose")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UnitMeasure)
                    .HasColumnName("unitMeasure")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BookletRequest>(entity =>
            {
                entity.ToTable("bookletRequest");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AdminApproval)
                    .HasColumnName("adminApproval")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ApproveRemarks)
                    .HasColumnName("approveRemarks")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Approver)
                    .HasColumnName("approver")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Branch)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNo)
                    .HasColumnName("contactNo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateApprove)
                    .HasColumnName("dateApprove")
                    .HasColumnType("date");

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("date");

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress)
                    .HasColumnName("emailAddress")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmpId).HasColumnName("empId");

                entity.Property(e => e.FirstName)
                    .HasColumnName("firstName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                //entity.Property(e => e.FromToManualSaleInVoice)
                //    .HasColumnName("fromToManualSaleInVoice")
                //    .HasMaxLength(255)
                //    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("lastName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                //entity.Property(e => e.ManualType)
                //    .HasColumnName("manualType")
                //    .HasMaxLength(255)
                //    .IsUnicode(false);

                entity.Property(e => e.Purpose)
                    .HasColumnName("purpose")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                //entity.Property(e => e.Quantity)
                //    .HasColumnName("quantity")
                //    .HasMaxLength(255)
                //    .IsUnicode(false);

                //entity.Property(e => e.RequestType)
                //    .HasColumnName("requestType")
                //    .HasMaxLength(255)
                //    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                //entity.Property(e => e.UsedManualSaleInVoice)
                //    .HasColumnName("usedManualSaleInVoice")
                //    .HasMaxLength(255)
                //    .IsUnicode(false);
            });

            modelBuilder.Entity<OrderedBooklet>(entity =>
            {
                entity.ToTable("orderedBooklet");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BookletRequestId).HasColumnName("bookletRequestId");

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("date");

                entity.Property(e => e.EmpId).HasColumnName("empId");

                entity.Property(e => e.FromToMsiv)
                    .HasColumnName("fromToMSIV")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ManualType)
                    .HasColumnName("manualType")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.Property(e => e.ApprovedQuantity).HasColumnName("approvedQuantity");

                entity.Property(e => e.RequestType)
                    .HasColumnName("requestType")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UnusedMsiv)
                    .HasColumnName("unusedMSIV")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MarketingMaterialRequest>(entity =>
            {
                entity.ToTable("marketingMaterialRequest");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AdminApproval)
                    .HasColumnName("adminApproval")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ApproveRemarks)
                    .HasColumnName("approveRemarks")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Approver)
                    .HasColumnName("approver")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Branch)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Company)
                    .HasMaxLength(225)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNo)
                    .HasColumnName("contactNo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateApprove)
                    .HasColumnName("dateApprove")
                    .HasColumnType("date");

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("date");

                entity.Property(e => e.DateNeeded)
                    .HasColumnName("dateNeeded")
                    .HasColumnType("date");

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress)
                    .HasColumnName("emailAddress")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmpId).HasColumnName("empId");

                entity.Property(e => e.FirstName)
                    .HasColumnName("firstName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ItemDescription)
                    .HasColumnName("itemDescription")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("lastName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Position)
                    .HasMaxLength(225)
                    .IsUnicode(false);

                entity.Property(e => e.Price).HasColumnName("price");

                entity.Property(e => e.ImageName)
                    .HasColumnName("imageName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RequestType)
                    .HasColumnName("requestType")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MarketingOrder>(entity =>
            {
                entity.ToTable("marketingOrder");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("date");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmpId).HasColumnName("empId");

                entity.Property(e => e.Item)
                    .HasColumnName("item")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MarketingRequestId).HasColumnName("marketingRequestId");

                entity.Property(e => e.UnitMeasure)
                    .HasColumnName("unitMeasure")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.Property(e => e.ApprovedQuantity).HasColumnName("approvedQuantity");

                entity.Property(e => e.RequestType)
                    .HasColumnName("requestType")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MaintenanceRequest>(entity =>
            {
                entity.ToTable("maintenanceRequest");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AdminApproval)
                    .HasColumnName("adminApproval")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ApproveRemarks)
                    .HasColumnName("approveRemarks")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Approver)
                    .HasColumnName("approver")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Branch)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Company)
                    .HasMaxLength(225)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNo)
                    .HasColumnName("contactNo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateApprove)
                    .HasColumnName("dateApprove")
                    .HasColumnType("date");

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("date");

                entity.Property(e => e.DateNeeded)
                    .HasColumnName("dateNeeded")
                    .HasColumnType("date");

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress)
                    .HasColumnName("emailAddress")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmpId).HasColumnName("empId");

                entity.Property(e => e.FirstName)
                    .HasColumnName("firstName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("lastName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ImageName)
                    .HasColumnName("imageName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RequestType)
                    .HasColumnName("requestType")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NatureOfWork)
                    .HasColumnName("natureOfWork")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Position)
                    .HasMaxLength(225)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PettyCash>(entity =>
            {
                entity.ToTable("pettyCash");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AmountInWord)
                    .IsRequired()
                    .HasColumnName("amountInWord")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ApprovedBy)
                    .HasColumnName("approvedBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AuditBy)
                    .HasColumnName("auditBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Branch)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CheckedBy)
                    .HasColumnName("checkedBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Classification)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Comment)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Company)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNo)
                    .HasColumnName("contactNo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("date");

                entity.Property(e => e.PettyCashDate)
                    .HasColumnName("pettyCashDate")
                    .HasColumnType("date");

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DivourcherId)
                    .IsRequired()
                    .HasColumnName("divourcherId")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress)
                    .HasColumnName("emailAddress")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmpId).HasColumnName("empId");

                entity.Property(e => e.FirstName)
                    .HasColumnName("firstName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("lastName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Payee)
                    .IsRequired()
                    .HasColumnName("Payee")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PreparedBy)
                    .HasColumnName("preparedBy")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Reason)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TotalAmount).HasColumnName("totalAmount");
            });

            modelBuilder.Entity<PettyCashOrders>(entity =>
            {
                entity.ToTable("pettyCashOrders");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Amount)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("date");

                entity.Property(e => e.ImageName)
                    .HasColumnName("imageName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AccountTitle)
                    .HasColumnName("accountTitle")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.AccountTitleId)
                    .HasColumnName("accountTitleId")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Particulars)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PettyCashId).HasColumnName("pettyCashId");
            });

            modelBuilder.Entity<RequestOt>(entity =>
            {
                entity.ToTable("RequestOT");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ApproveRemarks)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ApprovedBy)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Branch)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNo)
                    .HasColumnName("contactNo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateApproved).HasColumnType("date");

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("date");

                entity.Property(e => e.DateOfOt)
                    .HasColumnName("dateOfOT")
                    .HasColumnType("date");

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress)
                    .HasColumnName("emailAddress")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmpId).HasColumnName("empId");

                entity.Property(e => e.FirstName)
                    .HasColumnName("firstName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("lastName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Reason)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TimeFrom)
                    .HasColumnName("timeFrom")
                    .HasColumnType("time(0)");

                entity.Property(e => e.TimeTo)
                    .HasColumnName("timeTo")
                    .HasColumnType("time(0)");
            });


            modelBuilder.Entity<SuggestionBox>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Branch)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNo)
                    .HasColumnName("contactNo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("date");

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress)
                    .HasColumnName("emailAddress")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmpId).HasColumnName("empId");

                entity.Property(e => e.FirstName)
                    .HasColumnName("firstName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("lastName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Suggestions)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Faqs>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");


                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("date");

                entity.Property(e => e.Questions)
                      .HasColumnName("questions")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Answers)
                    .HasColumnName("answers")
                    .HasMaxLength(255)
                    .IsUnicode(false);


             });

            modelBuilder.Entity<ChangeInfoRequest>(entity =>
            {
                entity.ToTable("changeInfoRequest");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AdminApproval)
                    .HasColumnName("adminApproval")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ApproveRemarks)
                    .HasColumnName("approveRemarks")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Approver)
                    .HasColumnName("approver")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CurrentBiometricId)
                    .HasColumnName("currentBiometricId")
                    .HasMaxLength(225)
                    .IsUnicode(false);

                entity.Property(e => e.CurrentBranch)
                    .HasColumnName("currentBranch")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CurrentCompany)
                    .HasColumnName("currentCompany")
                    .HasMaxLength(225)
                    .IsUnicode(false);

                entity.Property(e => e.CurrentDepartment)
                    .HasColumnName("currentDepartment")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CurrentPosition)
                    .HasColumnName("currentPosition")
                    .HasMaxLength(225)
                    .IsUnicode(false);

                entity.Property(e => e.CurrentHiringDate)
                    .HasColumnName("currentHiringDate")
                    .HasColumnType("date");

                entity.Property(e => e.CurrentDateStart)
                    .HasColumnName("currentDateStart")
                    .HasColumnType("date");

                entity.Property(e => e.CurrentEmpStatus)
                     .HasColumnName("currentEmpStatus")
                     .HasMaxLength(225)
                     .IsUnicode(false);
                entity.Property(e => e.CurrentEducAttain)
                     .HasColumnName("currentEducAttain")
                     .HasMaxLength(225)
                     .IsUnicode(false);
                entity.Property(e => e.CurrentSchoolName)
                     .HasColumnName("currentSchoolName")
                     .HasMaxLength(225)
                     .IsUnicode(false);
                entity.Property(e => e.CurrentCourse)
                     .HasColumnName("currentCourse")
                     .HasMaxLength(225)
                     .IsUnicode(false);
                entity.Property(e => e.CurrentAttendYr)
                     .HasColumnName("currentAttendYr")
                     .HasMaxLength(225)
                     .IsUnicode(false);
                entity.Property(e => e.CurrentSssNo)
                     .HasColumnName("currentSssNo")
                     .HasMaxLength(225)
                     .IsUnicode(false);
                entity.Property(e => e.CurrentPagibigNo)
                     .HasColumnName("currentPagibigNo")
                     .HasMaxLength(225)
                     .IsUnicode(false);
                entity.Property(e => e.CurrentTinNo)
                     .HasColumnName("currentTinNo")
                     .HasMaxLength(225)
                     .IsUnicode(false);
                entity.Property(e => e.CurrentPhilHealthNo)
                     .HasColumnName("currentPhilHealthNo")
                     .HasMaxLength(225)
                     .IsUnicode(false);


                entity.Property(e => e.DateApprove)
                    .HasColumnName("dateApprove")
                    .HasColumnType("date");

                entity.Property(e => e.DateCreated)
                    .HasColumnName("dateCreated")
                    .HasColumnType("date");

                entity.Property(e => e.EmailAddress)
                    .HasColumnName("emailAddress")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmpId).HasColumnName("empId");

                entity.Property(e => e.FirstName)
                    .HasColumnName("firstName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("lastName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NewBranch)
                    .HasColumnName("newBranch")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NewCompany)
                    .HasColumnName("newCompany")
                    .HasMaxLength(225)
                    .IsUnicode(false);

                entity.Property(e => e.NewDepartment)
                    .HasColumnName("newDepartment")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NewPosition)
                    .HasColumnName("newPosition")
                    .HasMaxLength(225)
                    .IsUnicode(false);

                entity.Property(e => e.NewbiometricId)
                    .HasColumnName("newbiometricId")
                    .HasMaxLength(225)
                    .IsUnicode(false);


                entity.Property(e => e.NewHiringDate)
                    .HasColumnName("newHiringDate")
                    .HasColumnType("date");

                entity.Property(e => e.NewDateStart)
                    .HasColumnName("newDateStart")
                    .HasColumnType("date");

                entity.Property(e => e.NewEmpStatus)
                     .HasColumnName("newEmpStatus")
                     .HasMaxLength(225)
                     .IsUnicode(false);
                entity.Property(e => e.NewEducAttain)
                     .HasColumnName("newEducAttain")
                     .HasMaxLength(225)
                     .IsUnicode(false);
                entity.Property(e => e.NewSchoolName)
                     .HasColumnName("newSchoolName")
                     .HasMaxLength(225)
                     .IsUnicode(false);
                entity.Property(e => e.NewCourse)
                     .HasColumnName("newCourse")
                     .HasMaxLength(225)
                     .IsUnicode(false);
                entity.Property(e => e.NewAttendYr)
                     .HasColumnName("newAttendYr")
                     .HasMaxLength(225)
                     .IsUnicode(false);
                entity.Property(e => e.NewSssNo)
                     .HasColumnName("newSssNo")
                     .HasMaxLength(225)
                     .IsUnicode(false);
                entity.Property(e => e.NewPagibigNo)
                     .HasColumnName("newPagibigNo")
                     .HasMaxLength(225)
                     .IsUnicode(false);
                entity.Property(e => e.NewTinNo)
                     .HasColumnName("newTinNo")
                     .HasMaxLength(225)
                     .IsUnicode(false);
                entity.Property(e => e.NewPhilHealthNo)
                     .HasColumnName("newPhilHealthNo")
                     .HasMaxLength(225)
                     .IsUnicode(false);

                entity.Property(e => e.Reason)
                    .HasMaxLength(225)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Tblaccounttitle>(entity =>
            {
                entity.ToTable("tblaccounttitle");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Category)
                    .HasColumnName("category")
                    .HasMaxLength(50);

                entity.Property(e => e.Class)
                    .HasColumnName("class")
                    .HasMaxLength(50);

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasMaxLength(50);

                entity.Property(e => e.Datecreated)
                    .HasColumnName("datecreated")
                    .HasColumnType("date");

                entity.Property(e => e.Particulars)
                    .HasColumnName("particulars")
                    .HasMaxLength(100);

                entity.Property(e => e.Percentage)
                    .HasColumnName("percentage")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Tblaccounttitlebranch>(entity =>
            {
                entity.ToTable("tblaccounttitlebranch");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Branch)
                    .HasColumnName("branch")
                    .HasMaxLength(50);

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(50);

                entity.Property(e => e.Datecreated)
                    .HasColumnName("datecreated")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<Tblpayee>(entity =>
            {
                entity.ToTable("tblpayee");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Accountname)
                    .HasColumnName("accountname")
                    .HasMaxLength(150);

                entity.Property(e => e.Accountnumber)
                    .HasColumnName("accountnumber")
                    .HasMaxLength(50);

                entity.Property(e => e.Address)
                    .HasColumnName("address")
                    .HasMaxLength(150);

                entity.Property(e => e.Branch)
                    .HasColumnName("branch")
                    .HasMaxLength(50);

                entity.Property(e => e.Datecreated)
                    .HasColumnName("datecreated")
                    .HasColumnType("date");

                entity.Property(e => e.Payee)
                    .HasColumnName("payee")
                    .HasMaxLength(150);

                entity.Property(e => e.Tin)
                    .HasColumnName("tin")
                    .HasMaxLength(50);

                entity.Property(e => e.Vat)
                    .HasColumnName("vat")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Tblassignmenthistory>(entity =>
            {
                entity.ToTable("tblassignmenthistory");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Assdatestart)
                    .HasColumnName("assdatestart")
                    .HasColumnType("date");

                entity.Property(e => e.Assidbranch).HasColumnName("assidbranch");

                entity.Property(e => e.Assidcompany).HasColumnName("assidcompany");

                entity.Property(e => e.Assiddept).HasColumnName("assiddept");

                entity.Property(e => e.Assidemp).HasColumnName("assidemp");

                entity.Property(e => e.Assidposition).HasColumnName("assidposition");

                entity.Property(e => e.Assidwarehouse).HasColumnName("assidwarehouse");
            });

            modelBuilder.Entity<Tblattachment>(entity =>
            {
                entity.ToTable("tblattachment");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Attidemp).HasColumnName("attidemp");

                entity.Property(e => e.Datecreated)
                    .HasColumnName("datecreated")
                    .HasColumnType("date");

                entity.Property(e => e.Datedocs)
                    .HasColumnName("datedocs")
                    .HasColumnType("date");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(50);

                entity.Property(e => e.Filelocation)
                    .HasColumnName("filelocation")
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<Tblbranch>(entity =>
            {
                entity.ToTable("tblbranch");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Branch)
                    .HasColumnName("branch")
                    .HasMaxLength(30);

                entity.Property(e => e.Branchadd)
                    .HasColumnName("branchadd")
                    .HasMaxLength(50);

                entity.Property(e => e.Datecreated)
                    .HasColumnName("datecreated")
                    .HasColumnType("date");

                entity.Property(e => e.Idcompany).HasColumnName("idcompany");
            });

            modelBuilder.Entity<Tblcompany>(entity =>
            {
                entity.ToTable("tblcompany");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(30);

                entity.Property(e => e.Datecreated)
                    .HasColumnName("datecreated")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<Tbldepartment>(entity =>
            {
                entity.ToTable("tbldepartment");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Datecreated)
                    .HasColumnName("datecreated")
                    .HasColumnType("date");

                entity.Property(e => e.Department)
                    .HasColumnName("department")
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Tbleducational>(entity =>
            {
                entity.ToTable("tbleducational");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Datecreated)
                    .HasColumnName("datecreated")
                    .HasColumnType("date");

                entity.Property(e => e.Eduattain)
                    .HasColumnName("eduattain")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Tblhiresource>(entity =>
            {
                entity.ToTable("tblhiresource");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Datecreated)
                    .HasColumnName("datecreated")
                    .HasColumnType("date");

                entity.Property(e => e.Sourcehire)
                    .HasColumnName("sourcehire")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Tblposition>(entity =>
            {
                entity.ToTable("tblposition");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Datecreated)
                    .HasColumnName("datecreated")
                    .HasColumnType("date");

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Tblprofile>(entity =>
            {
                entity.ToTable("tblprofile");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Address).HasMaxLength(250);

                entity.Property(e => e.Allowance)
                    .HasColumnName("allowance")
                    .HasMaxLength(50);

                entity.Property(e => e.Attendyear)
                    .HasColumnName("attendyear")
                    .HasMaxLength(15);

                entity.Property(e => e.Bday)
                    .HasColumnName("bday")
                    .HasColumnType("date");

                entity.Property(e => e.Bio)
                    .HasColumnName("bio")
                    .HasMaxLength(15);

                entity.Property(e => e.Branch)
                    .HasColumnName("branch")
                    .HasMaxLength(20);

                entity.Property(e => e.Cola)
                    .HasColumnName("cola")
                    .HasMaxLength(50);

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(20);

                entity.Property(e => e.Conemergency)
                    .HasColumnName("conemergency")
                    .HasMaxLength(20);

                entity.Property(e => e.Conperson)
                    .HasColumnName("conperson")
                    .HasMaxLength(50);

                entity.Property(e => e.Conpersontelno)
                    .HasColumnName("conpersontelno")
                    .HasMaxLength(30);

                entity.Property(e => e.Course)
                    .HasColumnName("course")
                    .HasMaxLength(150);

                entity.Property(e => e.Cstatus)
                    .HasColumnName("cstatus")
                    .HasMaxLength(10);

                entity.Property(e => e.Dateeoc)
                    .HasColumnName("dateeoc")
                    .HasColumnType("date");

                entity.Property(e => e.Datefinalinterview)
                    .HasColumnName("datefinalinterview")
                    .HasColumnType("date");

                entity.Property(e => e.Dateinitialinterview)
                    .HasColumnName("dateinitialinterview")
                    .HasColumnType("date");

                entity.Property(e => e.Datereg)
                    .HasColumnName("datereg")
                    .HasColumnType("date");

                entity.Property(e => e.Datesalrychange)
                    .HasColumnName("datesalrychange")
                    .HasColumnType("date");

                entity.Property(e => e.Datestart)
                    .HasColumnName("datestart")
                    .HasColumnType("date");

                entity.Property(e => e.Department)
                    .HasColumnName("department")
                    .HasMaxLength(20);

                entity.Property(e => e.Dependents)
                    .HasColumnName("dependents")
                    .HasMaxLength(50);

                entity.Property(e => e.Educattain)
                    .HasColumnName("educattain")
                    .HasMaxLength(50);

                entity.Property(e => e.Emaladd)
                    .HasColumnName("emaladd")
                    .HasMaxLength(130);

                entity.Property(e => e.Employststus)
                    .HasColumnName("employststus")
                    .HasMaxLength(15);

                entity.Property(e => e.Evaluationdate)
                    .HasColumnName("evaluationdate")
                    .HasColumnType("date");

                entity.Property(e => e.Feedback)
                    .HasColumnName("feedback")
                    .HasMaxLength(80);

                entity.Property(e => e.Fname)
                    .HasColumnName("fname")
                    .HasMaxLength(25);

                entity.Property(e => e.Gender)
                    .HasColumnName("gender")
                    .HasMaxLength(10);

                entity.Property(e => e.Hiresource)
                    .HasColumnName("hiresource")
                    .HasMaxLength(50);

                entity.Property(e => e.Hiringdate)
                    .HasColumnName("hiringdate")
                    .HasColumnType("date");

                entity.Property(e => e.Idemployehistory).HasColumnName("idemployehistory");

                entity.Property(e => e.Idno)
                    .HasColumnName("idno")
                    .HasMaxLength(25);

                entity.Property(e => e.Inviteby)
                    .HasColumnName("inviteby")
                    .HasMaxLength(50);

                entity.Property(e => e.Lname)
                    .HasColumnName("lname")
                    .HasMaxLength(25);

                entity.Property(e => e.Marks1)
                    .HasColumnName("marks1")
                    .HasMaxLength(250);

                entity.Property(e => e.Marks2)
                    .HasColumnName("marks2")
                    .HasMaxLength(250);

                entity.Property(e => e.Marks3)
                    .HasColumnName("marks3")
                    .HasMaxLength(250);

                entity.Property(e => e.Marks4)
                    .HasColumnName("marks4")
                    .HasMaxLength(250);

                entity.Property(e => e.Mname)
                    .HasColumnName("mname")
                    .HasMaxLength(25);

                entity.Property(e => e.Nationalid)
                    .HasColumnName("nationalid")
                    .HasMaxLength(30);

                entity.Property(e => e.Pagibigno)
                    .HasColumnName("pagibigno")
                    .HasMaxLength(30);

                entity.Property(e => e.Philhealthno)
                    .HasColumnName("philhealthno")
                    .HasMaxLength(30);

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasMaxLength(20);

                entity.Property(e => e.Regstatus)
                    .HasColumnName("regstatus")
                    .HasMaxLength(20);

                entity.Property(e => e.Salary)
                    .HasColumnName("salary")
                    .HasMaxLength(50);

                entity.Property(e => e.Schoolname)
                    .HasColumnName("schoolname")
                    .HasMaxLength(150);

                entity.Property(e => e.Sssno)
                    .HasColumnName("sssno")
                    .HasMaxLength(30);

                entity.Property(e => e.Telno).HasMaxLength(120);

                entity.Property(e => e.Tinno)
                    .HasColumnName("tinno")
                    .HasMaxLength(30);

                entity.Property(e => e.Warehouse)
                    .HasColumnName("warehouse")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<Tblwarehouse>(entity =>
            {
                entity.ToTable("tblwarehouse");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Datecreated)
                    .HasColumnName("datecreated")
                    .HasColumnType("date");

                entity.Property(e => e.Wareadd)
                    .HasColumnName("wareadd")
                    .HasMaxLength(50);

                entity.Property(e => e.Warehouse)
                    .HasColumnName("warehouse")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Tbuser>(entity =>
            {
                entity.ToTable("tbuser");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Access)
                    .HasColumnName("access")
                    .HasMaxLength(50);

                entity.Property(e => e.Datecreated)
                    .HasColumnName("datecreated")
                    .HasColumnType("date");

                entity.Property(e => e.Empid).HasColumnName("empid");

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(50);

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(50);
            });
        }
    }
}
