﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderedBookletsController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        public OrderedBookletsController(dbhprofilingContext context)
        {
            _context = context;
        }

        // GET: api/OrderedBooklets
        [HttpGet]
        public IEnumerable<OrderedBooklet> GetOrderedBooklet()
        {
            return _context.OrderedBooklet;
        }

        // GET: api/OrderedBooklets/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOrderedBooklet([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var orderedBooklet = await _context.OrderedBooklet.FindAsync(id);

            if (orderedBooklet == null)
            {
                return NotFound();
            }

            return Ok(orderedBooklet);
        }

        // PUT: api/OrderedBooklets/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrderedBooklet([FromRoute] int id, [FromBody] OrderedBooklet orderedBooklet)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != orderedBooklet.Id)
            {
                return BadRequest();
            }

            _context.Entry(orderedBooklet).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderedBookletExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/OrderedBooklets
        [HttpPost]
        public async Task<IActionResult> PostOrderedBooklet([FromBody] OrderedBooklet orderedBooklet)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.OrderedBooklet.Add(orderedBooklet);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetOrderedBooklet", new { id = orderedBooklet.Id }, orderedBooklet);
        }

        // DELETE: api/OrderedBooklets/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrderedBooklet([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var orderedBooklet = await _context.OrderedBooklet.FindAsync(id);
            if (orderedBooklet == null)
            {
                return NotFound();
            }

            _context.OrderedBooklet.Remove(orderedBooklet);
            await _context.SaveChangesAsync();

            return Ok(orderedBooklet);
        }

        private bool OrderedBookletExists(int id)
        {
            return _context.OrderedBooklet.Any(e => e.Id == id);
        }
    }
}