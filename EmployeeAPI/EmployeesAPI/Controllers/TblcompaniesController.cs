﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TblcompaniesController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        public TblcompaniesController(dbhprofilingContext context)
        {
            _context = context;
        }

        // GET: api/Tblcompanies
        [HttpGet]
        public IEnumerable<Tblcompany> GetTblcompany()
        {
            return _context.Tblcompany;
        }

        // GET: api/Tblcompanies/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTblcompany([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tblcompany = await _context.Tblcompany.FindAsync(id);

            if (tblcompany == null)
            {
                return NotFound();
            }

            return Ok(tblcompany);
        }

        // PUT: api/Tblcompanies/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTblcompany([FromRoute] int id, [FromBody] Tblcompany tblcompany)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tblcompany.Id)
            {
                return BadRequest();
            }

            _context.Entry(tblcompany).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TblcompanyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Tblcompanies
        [HttpPost]
        public async Task<IActionResult> PostTblcompany([FromBody] Tblcompany tblcompany)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Tblcompany.Add(tblcompany);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TblcompanyExists(tblcompany.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTblcompany", new { id = tblcompany.Id }, tblcompany);
        }

        // DELETE: api/Tblcompanies/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTblcompany([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tblcompany = await _context.Tblcompany.FindAsync(id);
            if (tblcompany == null)
            {
                return NotFound();
            }

            _context.Tblcompany.Remove(tblcompany);
            await _context.SaveChangesAsync();

            return Ok(tblcompany);
        }

        private bool TblcompanyExists(int id)
        {
            return _context.Tblcompany.Any(e => e.Id == id);
        }
    }
}