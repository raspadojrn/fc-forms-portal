﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;
using EmployeesAPI.Services.PurchaseForm.BusinessCard;
using EmployeesAPI.Helpers;
using Microsoft.Extensions.Options;
using EmployeesAPI.DTOs.LeaveRequest;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BusinessCardRequestsController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        private readonly IBusinessCardService _service;

        private readonly AppSettings _appSettings;

        public BusinessCardRequestsController(dbhprofilingContext context, IBusinessCardService BusinessCardService,
                         IOptions<AppSettings> appSettings)
        {
            _context = context;
            _service = BusinessCardService;
            _appSettings = appSettings.Value;
        }


        //Get data with page, search, orderby, ordertype
        [HttpPost]
        [Route("Filter")]
        public IActionResult GetLeaveRequest([FromBody]GetAll criteria)
        {

            var response = _service.GetAllFilter(criteria, _appSettings);
            return Ok(response);
        }

        // GET: api/BusinessCardRequests
        [HttpGet]
        public IEnumerable<BusinessCardRequest> GetBusinessCardRequest()
        {
            return _context.BusinessCardRequest;
        }

        // GET: api/BusinessCardRequests/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBusinessCardRequest([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var businessCardRequest = await _context.BusinessCardRequest.FindAsync(id);

            if (businessCardRequest == null)
            {
                return NotFound();
            }

            return Ok(businessCardRequest);
        }

        // PUT: api/BusinessCardRequests/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBusinessCardRequest([FromRoute] int id, [FromBody] BusinessCardRequest businessCardRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != businessCardRequest.Id)
            {
                return BadRequest();
            }

            _context.Entry(businessCardRequest).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BusinessCardRequestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/BusinessCardRequests
        [HttpPost]
        public async Task<IActionResult> PostBusinessCardRequest([FromBody] BusinessCardRequest businessCardRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.BusinessCardRequest.Add(businessCardRequest);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBusinessCardRequest", new { id = businessCardRequest.Id }, businessCardRequest);
        }

        // DELETE: api/BusinessCardRequests/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBusinessCardRequest([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var businessCardRequest = await _context.BusinessCardRequest.FindAsync(id);
            if (businessCardRequest == null)
            {
                return NotFound();
            }

            _context.BusinessCardRequest.Remove(businessCardRequest);
            await _context.SaveChangesAsync();

            return Ok(businessCardRequest);
        }

        private bool BusinessCardRequestExists(int id)
        {
            return _context.BusinessCardRequest.Any(e => e.Id == id);
        }
    }
}