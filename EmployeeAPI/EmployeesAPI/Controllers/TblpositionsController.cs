﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TblpositionsController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        public TblpositionsController(dbhprofilingContext context)
        {
            _context = context;
        }

        // GET: api/Tblpositions
        [HttpGet]
        public IEnumerable<Tblposition> GetTblposition()
        {
            return _context.Tblposition;
        }

        // GET: api/Tblpositions/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTblposition([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tblposition = await _context.Tblposition.FindAsync(id);

            if (tblposition == null)
            {
                return NotFound();
            }

            return Ok(tblposition);
        }

        // PUT: api/Tblpositions/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTblposition([FromRoute] int id, [FromBody] Tblposition tblposition)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tblposition.Id)
            {
                return BadRequest();
            }

            _context.Entry(tblposition).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TblpositionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Tblpositions
        [HttpPost]
        public async Task<IActionResult> PostTblposition([FromBody] Tblposition tblposition)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Tblposition.Add(tblposition);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TblpositionExists(tblposition.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTblposition", new { id = tblposition.Id }, tblposition);
        }

        // DELETE: api/Tblpositions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTblposition([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tblposition = await _context.Tblposition.FindAsync(id);
            if (tblposition == null)
            {
                return NotFound();
            }

            _context.Tblposition.Remove(tblposition);
            await _context.SaveChangesAsync();

            return Ok(tblposition);
        }

        private bool TblpositionExists(int id)
        {
            return _context.Tblposition.Any(e => e.Id == id);
        }
    }
}