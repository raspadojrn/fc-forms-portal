﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;
using EmployeesAPI.Services.PurchaseForm.Maintenance;
using EmployeesAPI.Helpers;
using Microsoft.Extensions.Options;
using EmployeesAPI.DTOs.LeaveRequest;
using System.IO;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MaintenanceRequestsController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        private readonly IMaintenanceService _service;

        private readonly AppSettings _appSettings;

        public MaintenanceRequestsController(dbhprofilingContext context, IMaintenanceService MaintenanceService,
                         IOptions<AppSettings> appSettings)
        {
            _context = context;
            _service = MaintenanceService;
            _appSettings = appSettings.Value;
        }

        //Get data with page, search, orderby, ordertype
        [HttpPost]
        [Route("Filter")]
        public IActionResult GetLeaveRequest([FromBody]GetAll criteria)
        {

            var response = _service.GetAllFilter(criteria, _appSettings);
            return Ok(response);
        }

        // GET: api/MaintenanceRequests
        [HttpGet]
        public IEnumerable<MaintenanceRequest> GetMaintenanceRequest()
        {
            return _context.MaintenanceRequest;
        }

        // GET: api/MaintenanceRequests/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMaintenanceRequest([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var maintenanceRequest = await _context.MaintenanceRequest.FindAsync(id);

            if (maintenanceRequest == null)
            {
                return NotFound();
            }

            return Ok(maintenanceRequest);
        }

        // PUT: api/MaintenanceRequests/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMaintenanceRequest([FromRoute] int id, [FromBody] MaintenanceRequest maintenanceRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != maintenanceRequest.Id)
            {
                return BadRequest();
            }

            _context.Entry(maintenanceRequest).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MaintenanceRequestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MaintenanceRequests
        [HttpPost]
        public async Task<IActionResult> PostMaintenanceRequest([FromBody] MaintenanceRequest maintenanceRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.MaintenanceRequest.Add(maintenanceRequest);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMaintenanceRequest", new { id = maintenanceRequest.Id }, maintenanceRequest);
        }

        // DELETE: api/MaintenanceRequests/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMaintenanceRequest([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var maintenanceRequest = await _context.MaintenanceRequest.FindAsync(id);
            if (maintenanceRequest == null)
            {
                return NotFound();
            }

            _context.MaintenanceRequest.Remove(maintenanceRequest);
            await _context.SaveChangesAsync();

            return Ok(maintenanceRequest);
        }

        [HttpPost]
        [Route("upload")]
        public async Task Upload(IFormFile file)
        {
            if (file == null) throw new Exception("File is null");
            if (file.Length == 0) throw new Exception("File is empty");


            if (file.Length <= 7000000)
            {
                var filePath = Path.Combine(_appSettings.Item_temp_image, file.FileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }
        }

        private bool MaintenanceRequestExists(int id)
        {
            return _context.MaintenanceRequest.Any(e => e.Id == id);
        }
    }
}