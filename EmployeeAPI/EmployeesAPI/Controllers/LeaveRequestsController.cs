﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;
using EmployeesAPI.Services.LeaveRequest;
using EmployeesAPI.Helpers;
using Microsoft.Extensions.Options;
using EmployeesAPI.DTOs.LeaveRequest;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LeaveRequestsController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        private readonly ILeaveRequestService _service;

        private readonly AppSettings _appSettings;

        public LeaveRequestsController(dbhprofilingContext context, ILeaveRequestService LeaveReqService,
                                IOptions<AppSettings> appSettings)
        {
            _context = context;
            _service = LeaveReqService;
            _appSettings = appSettings.Value;
        }

        //Get data with page, search, orderby, ordertype
        [HttpPost]
        [Route("Filter")]
        public IActionResult GetLeaveRequest([FromBody]GetAll criteria)
        {

            var response = _service.GetAllFilter(criteria, _appSettings);
            return Ok(response);
        }

        // GET: api/LeaveRequests
        [HttpGet]
        public ActionResult<IEnumerable<LeaveRequest>> GetLeaveRequest()
        {
            var response = _service.GetAll(_appSettings);
            return Ok(response);
        }



        // GET: api/LeaveRequests/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetLeaveRequest([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var leaveRequest = await _context.LeaveRequest.FindAsync(id);

            if (leaveRequest == null)
            {
                return NotFound();
            }

            return Ok(leaveRequest);
        }

        // PUT: api/LeaveRequests/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLeaveRequest([FromRoute] int id, [FromBody] LeaveRequest leaveRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != leaveRequest.Id)
            {
                return BadRequest();
            }

            _context.Entry(leaveRequest).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LeaveRequestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LeaveRequests
        [HttpPost]
        public async Task<IActionResult> PostLeaveRequest([FromBody] LeaveRequest leaveRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.LeaveRequest.Add(leaveRequest);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (LeaveRequestExists(leaveRequest.EmpId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetLeaveRequest", new { id = leaveRequest.EmpId }, leaveRequest);
        }

        // DELETE: api/LeaveRequests/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLeaveRequest([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var leaveRequest = await _context.LeaveRequest.FindAsync(id);
            if (leaveRequest == null)
            {
                return NotFound();
            }

            _context.LeaveRequest.Remove(leaveRequest);
            await _context.SaveChangesAsync();

            return Ok(leaveRequest);
        }

        private bool LeaveRequestExists(int id)
        {
            return _context.LeaveRequest.Any(e => e.EmpId == id);
        }
    }
}