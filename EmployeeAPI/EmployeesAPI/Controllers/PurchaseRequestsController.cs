﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;
using EmployeesAPI.Services.PurchaseForm.Purchaserequest;
using EmployeesAPI.Helpers;
using Microsoft.Extensions.Options;
using EmployeesAPI.DTOs.LeaveRequest;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PurchaseRequestsController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        private readonly IPurchaseRequestService _service;

        private readonly AppSettings _appSettings;

        public PurchaseRequestsController(dbhprofilingContext context, IPurchaseRequestService PurchaseReqService,
                         IOptions<AppSettings> appSettings)
        {
            _context = context;
            _service = PurchaseReqService;
            _appSettings = appSettings.Value;
        }


        //Get data with page, search, orderby, ordertype
        [HttpPost]
        [Route("Filter")]
        public IActionResult GetLeaveRequest([FromBody]GetAll criteria)
        {

            var response = _service.GetAllFilter(criteria, _appSettings);
            return Ok(response);
        }

        // GET: api/PurchaseRequests
        [HttpGet]
        public IEnumerable<PurchaseRequest> GetPurchaseRequest()
        {
            return _context.PurchaseRequest;
        }

        // GET: api/PurchaseRequests/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPurchaseRequest([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var purchaseRequest = await _context.PurchaseRequest.FindAsync(id);

            if (purchaseRequest == null)
            {
                return NotFound();
            }

            return Ok(purchaseRequest);
        }

        // PUT: api/PurchaseRequests/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPurchaseRequest([FromRoute] int id, [FromBody] PurchaseRequest purchaseRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != purchaseRequest.Id)
            {
                return BadRequest();
            }

            _context.Entry(purchaseRequest).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PurchaseRequestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PurchaseRequests
        [HttpPost]
        public async Task<IActionResult> PostPurchaseRequest([FromBody] PurchaseRequest purchaseRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.PurchaseRequest.Add(purchaseRequest);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPurchaseRequest", new { id = purchaseRequest.Id }, purchaseRequest);
        }

        // DELETE: api/PurchaseRequests/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePurchaseRequest([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var purchaseRequest = await _context.PurchaseRequest.FindAsync(id);
            if (purchaseRequest == null)
            {
                return NotFound();
            }

            _context.PurchaseRequest.Remove(purchaseRequest);
            await _context.SaveChangesAsync();

            return Ok(purchaseRequest);
        }

        private bool PurchaseRequestExists(int id)
        {
            return _context.PurchaseRequest.Any(e => e.Id == id);
        }
    }
}