﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MarketingOrdersController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        public MarketingOrdersController(dbhprofilingContext context)
        {
            _context = context;
        }

        // GET: api/MarketingOrders
        [HttpGet]
        public IEnumerable<MarketingOrder> GetMarketingOrder()
        {
            return _context.MarketingOrder;
        }

        // GET: api/MarketingOrders/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMarketingOrder([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var marketingOrder = await _context.MarketingOrder.FindAsync(id);

            if (marketingOrder == null)
            {
                return NotFound();
            }

            return Ok(marketingOrder);
        }

        // PUT: api/MarketingOrders/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMarketingOrder([FromRoute] int id, [FromBody] MarketingOrder marketingOrder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != marketingOrder.Id)
            {
                return BadRequest();
            }

            _context.Entry(marketingOrder).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MarketingOrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MarketingOrders
        [HttpPost]
        public async Task<IActionResult> PostMarketingOrder([FromBody] MarketingOrder marketingOrder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.MarketingOrder.Add(marketingOrder);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMarketingOrder", new { id = marketingOrder.Id }, marketingOrder);
        }

        // DELETE: api/MarketingOrders/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMarketingOrder([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var marketingOrder = await _context.MarketingOrder.FindAsync(id);
            if (marketingOrder == null)
            {
                return NotFound();
            }

            _context.MarketingOrder.Remove(marketingOrder);
            await _context.SaveChangesAsync();

            return Ok(marketingOrder);
        }

        private bool MarketingOrderExists(int id)
        {
            return _context.MarketingOrder.Any(e => e.Id == id);
        }
    }
}