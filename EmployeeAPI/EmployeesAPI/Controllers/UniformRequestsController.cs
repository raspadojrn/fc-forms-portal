﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;
using EmployeesAPI.Services.PurchaseForm.Uniform;
using EmployeesAPI.Helpers;
using EmployeesAPI.DTOs.LeaveRequest;
using Microsoft.Extensions.Options;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UniformRequestsController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        private readonly IUniformService _service;

        private readonly AppSettings _appSettings;

        public UniformRequestsController(dbhprofilingContext context, IUniformService UniformService,
                         IOptions<AppSettings> appSettings)
        {
            _context = context;
            _service = UniformService;
            _appSettings = appSettings.Value;
        }


        //Get data with page, search, orderby, ordertype
        [HttpPost]
        [Route("Filter")]
        public IActionResult GetLeaveRequest([FromBody]GetAll criteria)
        {

            var response = _service.GetAllFilter(criteria, _appSettings);
            return Ok(response);
        }


        // GET: api/UniformRequests
        [HttpGet]
        public IEnumerable<UniformRequest> GetUniformRequest()
        {
            return _context.UniformRequest;
        }

        // GET: api/UniformRequests/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUniformRequest([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var uniformRequest = await _context.UniformRequest.FindAsync(id);

            if (uniformRequest == null)
            {
                return NotFound();
            }

            return Ok(uniformRequest);
        }

        // PUT: api/UniformRequests/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUniformRequest([FromRoute] int id, [FromBody] UniformRequest uniformRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != uniformRequest.Id)
            {
                return BadRequest();
            }

            _context.Entry(uniformRequest).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UniformRequestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/UniformRequests
        [HttpPost]
        public async Task<IActionResult> PostUniformRequest([FromBody] UniformRequest uniformRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.UniformRequest.Add(uniformRequest);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUniformRequest", new { id = uniformRequest.Id }, uniformRequest);
        }

        // DELETE: api/UniformRequests/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUniformRequest([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var uniformRequest = await _context.UniformRequest.FindAsync(id);
            if (uniformRequest == null)
            {
                return NotFound();
            }

            _context.UniformRequest.Remove(uniformRequest);
            await _context.SaveChangesAsync();

            return Ok(uniformRequest);
        }

        private bool UniformRequestExists(int id)
        {
            return _context.UniformRequest.Any(e => e.Id == id);
        }
    }
}