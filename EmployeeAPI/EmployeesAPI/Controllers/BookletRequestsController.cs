﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;
using EmployeesAPI.Services.PurchaseForm.Booklet;
using EmployeesAPI.Helpers;
using Microsoft.Extensions.Options;
using EmployeesAPI.DTOs.LeaveRequest;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookletRequestsController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        private readonly IBookletService _service;

        private readonly AppSettings _appSettings;

        public BookletRequestsController(dbhprofilingContext context, IBookletService BookletService,
                         IOptions<AppSettings> appSettings)
        {
            _context = context;
            _service = BookletService;
            _appSettings = appSettings.Value;
        }


        //Get data with page, search, orderby, ordertype
        [HttpPost]
        [Route("Filter")]
        public IActionResult GetLeaveRequest([FromBody]GetAll criteria)
        {

            var response = _service.GetAllFilter(criteria, _appSettings);
            return Ok(response);
        }
        // GET: api/BookletRequests
        [HttpGet]
        public IEnumerable<BookletRequest> GetBookletRequest()
        {
            return _context.BookletRequest;
        }

        // GET: api/BookletRequests/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBookletRequest([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bookletRequest = await _context.BookletRequest.FindAsync(id);

            if (bookletRequest == null)
            {
                return NotFound();
            }

            return Ok(bookletRequest);
        }

        // PUT: api/BookletRequests/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBookletRequest([FromRoute] int id, [FromBody] BookletRequest bookletRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bookletRequest.Id)
            {
                return BadRequest();
            }

            _context.Entry(bookletRequest).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookletRequestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/BookletRequests
        [HttpPost]
        public async Task<IActionResult> PostBookletRequest([FromBody] BookletRequest bookletRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.BookletRequest.Add(bookletRequest);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBookletRequest", new { id = bookletRequest.Id }, bookletRequest);
        }

        // DELETE: api/BookletRequests/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBookletRequest([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bookletRequest = await _context.BookletRequest.FindAsync(id);
            if (bookletRequest == null)
            {
                return NotFound();
            }

            _context.BookletRequest.Remove(bookletRequest);
            await _context.SaveChangesAsync();

            return Ok(bookletRequest);
        }

        private bool BookletRequestExists(int id)
        {
            return _context.BookletRequest.Any(e => e.Id == id);
        }
    }
}