﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TblbranchesController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        public TblbranchesController(dbhprofilingContext context)
        {
            _context = context;
        }

        // GET: api/Tblbranches
        [HttpGet]
        public IEnumerable<Tblbranch> GetTblbranch()
        {
            return _context.Tblbranch;
        }

        // GET: api/Tblbranches/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTblbranch([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tblbranch = await _context.Tblbranch.FindAsync(id);

            if (tblbranch == null)
            {
                return NotFound();
            }

            return Ok(tblbranch);
        }

        // PUT: api/Tblbranches/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTblbranch([FromRoute] int id, [FromBody] Tblbranch tblbranch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tblbranch.Id)
            {
                return BadRequest();
            }

            _context.Entry(tblbranch).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TblbranchExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Tblbranches
        [HttpPost]
        public async Task<IActionResult> PostTblbranch([FromBody] Tblbranch tblbranch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Tblbranch.Add(tblbranch);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TblbranchExists(tblbranch.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTblbranch", new { id = tblbranch.Id }, tblbranch);
        }

        // DELETE: api/Tblbranches/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTblbranch([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tblbranch = await _context.Tblbranch.FindAsync(id);
            if (tblbranch == null)
            {
                return NotFound();
            }

            _context.Tblbranch.Remove(tblbranch);
            await _context.SaveChangesAsync();

            return Ok(tblbranch);
        }

        private bool TblbranchExists(int id)
        {
            return _context.Tblbranch.Any(e => e.Id == id);
        }
    }
}