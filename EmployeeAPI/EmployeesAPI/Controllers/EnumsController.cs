﻿
using Microsoft.AspNetCore.Mvc;
using Employee.Core.Domain.Enums;
using EmployeesAPI.Helpers;
using Employee.Core.Domain.Enums.Marketing;

namespace EmployeesAPI.Controllers
{
    [Produces("application/json")]
    [Route("api")]
    public class EnumsController : ControllerBase
    {
        // GET: api/Enums

        [HttpGet]
        [Route("leavetypes/all")]
        public IActionResult leaveTypes()
        {
            return Ok(EnumExtensions.GetValues<LeaveTypeEnum>());

        }

        [HttpGet]
        [Route("PurchaseRequestTypes/all")]
        public IActionResult PurchaseRequestTypes()
        {
            return Ok(EnumExtensions.GetValues<PurchaseRequestTypesEnum>());

        }

        [HttpGet]
        [Route("BookletRequestEnum/all")]
        public IActionResult BookletRequestenum()
        {
            return Ok(EnumExtensions.GetValues<BookletRequestEnum>());

        }

        [HttpGet]
        [Route("BookletManualTypeEnum/all")]
        public IActionResult BookletManualTypeEnum()
        {
            return Ok(EnumExtensions.GetValues<BookletManualTypeEnum>());

        }

        [HttpGet]
        [Route("MarketingRequestTypeEnum/all")]
        public IActionResult MarketingRequestTypeEnum()
        {
            return Ok(EnumExtensions.GetValues<MarketingRequestTypeEnum>());

        }

        [HttpGet]
        [Route("MarketingPrintedCatalogueEnum/all")]
        public IActionResult MarketingPrintedCatalogueEnum()
        {
            return Ok(EnumExtensions.GetValues<MarketingPrintedCatalogueEnum>());

        }

        [HttpGet]
        [Route("MarketingGiveAwaysEnum/all")]
        public IActionResult MarketingGiveAwaysEnum()
        {
            return Ok(EnumExtensions.GetValues<MarketingGiveAwaysEnum>());

        }

        [HttpGet]
        [Route("MarketingSampleSwatchesEnum/all")]
        public IActionResult MarketingSampleSwatchesEnum()
        {
            return Ok(EnumExtensions.GetValues<MarketingSampleSwatchesEnum>());

        }

        [HttpGet]
        [Route("UniformTypeEnum/all")]
        public IActionResult UniformTypeEnum()
        {
            return Ok(EnumExtensions.GetValues<UniformTypeEnum>());

        }

        [HttpGet]
        [Route("LeaveReqDatePeriodEnum/all")]
        public IActionResult LeaveReqDatePeriodEnum()
        {
            return Ok(EnumExtensions.GetValues<LeaveReqDatePeriodEnum>());

        }


        [HttpGet]
        [Route("leaveforms/all")]
        public IActionResult leaveForms()
        {
            return Ok(EnumExtensions.GetValues<LeaveFormEnum>());
        }


        [HttpGet]
        [Route("accountAccees/all")]
        public IActionResult access()
        {
            return Ok(EnumExtensions.GetValues<AccountAccessEnum>());
        }
        
        [HttpGet]
        [Route("department/all")]
        public IActionResult department()
        {
            return Ok(EnumExtensions.GetValues<DepartmentEnum>());
        }

        [HttpGet]
        [Route("branch/all")]
        public IActionResult branch()
        {
            return Ok(EnumExtensions.GetValues<BranchEnum>());
        }

        [HttpGet]
        [Route("company/all")]
        public IActionResult company()
        {
            return Ok(EnumExtensions.GetValues<CompanyEnum>());
        }

        [HttpGet]
        [Route("position/all")]
        public IActionResult position()
        {
            return Ok(EnumExtensions.GetValues<PositionEnum>());
        }
        
        [HttpGet]
        [Route("AccountTitleEnum/all")]
        public IActionResult AccountTitleEnum()
        {
            return Ok(EnumExtensions.GetValues<AccountTitleEnum>());
        }
    }
}
