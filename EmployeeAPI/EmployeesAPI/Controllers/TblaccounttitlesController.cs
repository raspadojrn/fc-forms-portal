﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TblaccounttitlesController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        public TblaccounttitlesController(dbhprofilingContext context)
        {
            _context = context;
        }

        // GET: api/Tblaccounttitles
        [HttpGet]
        public IEnumerable<Tblaccounttitle> GetTblaccounttitle()
        {
            return _context.Tblaccounttitle;
        }

        // GET: api/Tblaccounttitles/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTblaccounttitle([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tblaccounttitle = await _context.Tblaccounttitle.FindAsync(id);

            if (tblaccounttitle == null)
            {
                return NotFound();
            }

            return Ok(tblaccounttitle);
        }

        // PUT: api/Tblaccounttitles/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTblaccounttitle([FromRoute] int id, [FromBody] Tblaccounttitle tblaccounttitle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tblaccounttitle.Id)
            {
                return BadRequest();
            }

            _context.Entry(tblaccounttitle).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TblaccounttitleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Tblaccounttitles
        [HttpPost]
        public async Task<IActionResult> PostTblaccounttitle([FromBody] Tblaccounttitle tblaccounttitle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Tblaccounttitle.Add(tblaccounttitle);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTblaccounttitle", new { id = tblaccounttitle.Id }, tblaccounttitle);
        }

        // DELETE: api/Tblaccounttitles/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTblaccounttitle([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tblaccounttitle = await _context.Tblaccounttitle.FindAsync(id);
            if (tblaccounttitle == null)
            {
                return NotFound();
            }

            _context.Tblaccounttitle.Remove(tblaccounttitle);
            await _context.SaveChangesAsync();

            return Ok(tblaccounttitle);
        }

        private bool TblaccounttitleExists(int id)
        {
            return _context.Tblaccounttitle.Any(e => e.Id == id);
        }
    }
}