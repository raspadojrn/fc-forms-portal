﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;
using EmployeesAPI.Services.Overtime;
using EmployeesAPI.Helpers;
using Microsoft.Extensions.Options;
using EmployeesAPI.DTOs.LeaveRequest;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RequestOtsController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        private readonly IOvertimeService _service;

        private readonly AppSettings _appSettings;

        public RequestOtsController(dbhprofilingContext context, IOvertimeService OvertimeService,
                                IOptions<AppSettings> appSettings)
        {
            _context = context;
            _service = OvertimeService;
            _appSettings = appSettings.Value;
        }

        //Get data with page, search, orderby, ordertype
        //api/IncidentReports/Filter
        [HttpPost]
        [Route("Filter")]
        public IActionResult GetLeaveRequest([FromBody]GetAll criteria)
        {

            var response = _service.GetAll(criteria, _appSettings);
            return Ok(response);
        }

        // GET: api/RequestOts
        [HttpGet]
        public IEnumerable<RequestOt> GetRequestOt()
        {
            return _context.RequestOt;
        }

        // GET: api/RequestOts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRequestOt([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var requestOt = await _context.RequestOt.FindAsync(id);

            if (requestOt == null)
            {
                return NotFound();
            }

            return Ok(requestOt);
        }

        // PUT: api/RequestOts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRequestOt([FromRoute] int id, [FromBody] RequestOt requestOt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != requestOt.Id)
            {
                return BadRequest();
            }

            _context.Entry(requestOt).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RequestOtExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/RequestOts
        [HttpPost]
        public async Task<IActionResult> PostRequestOt([FromBody] RequestOt requestOt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.RequestOt.Add(requestOt);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (RequestOtExists(requestOt.EmpId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetRequestOt", new { id = requestOt.EmpId }, requestOt);
        }

        // DELETE: api/RequestOts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRequestOt([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var requestOt = await _context.RequestOt.FindAsync(id);
            if (requestOt == null)
            {
                return NotFound();
            }

            _context.RequestOt.Remove(requestOt);
            await _context.SaveChangesAsync();

            return Ok(requestOt);
        }

        private bool RequestOtExists(int id)
        {
            return _context.RequestOt.Any(e => e.EmpId == id);
        }
    }
}