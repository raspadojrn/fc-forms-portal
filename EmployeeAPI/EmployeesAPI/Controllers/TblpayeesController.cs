﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TblpayeesController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        public TblpayeesController(dbhprofilingContext context)
        {
            _context = context;
        }

        // GET: api/Tblpayees
        [HttpGet]
        public IEnumerable<Tblpayee> GetTblpayee()
        {
            return _context.Tblpayee;
        }

        // GET: api/Tblpayees/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTblpayee([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tblpayee = await _context.Tblpayee.FindAsync(id);

            if (tblpayee == null)
            {
                return NotFound();
            }

            return Ok(tblpayee);
        }

        // PUT: api/Tblpayees/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTblpayee([FromRoute] int id, [FromBody] Tblpayee tblpayee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tblpayee.Id)
            {
                return BadRequest();
            }

            _context.Entry(tblpayee).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TblpayeeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Tblpayees
        [HttpPost]
        public async Task<IActionResult> PostTblpayee([FromBody] Tblpayee tblpayee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Tblpayee.Add(tblpayee);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTblpayee", new { id = tblpayee.Id }, tblpayee);
        }

        // DELETE: api/Tblpayees/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTblpayee([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tblpayee = await _context.Tblpayee.FindAsync(id);
            if (tblpayee == null)
            {
                return NotFound();
            }

            _context.Tblpayee.Remove(tblpayee);
            await _context.SaveChangesAsync();

            return Ok(tblpayee);
        }

        private bool TblpayeeExists(int id)
        {
            return _context.Tblpayee.Any(e => e.Id == id);
        }
    }
}