﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;
using EmployeesAPI.Services.ChangeInfoRequest;
using EmployeesAPI.Helpers;
using Microsoft.Extensions.Options;
using EmployeesAPI.DTOs.LeaveRequest;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChangeInfoRequestsController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        private readonly IChangeInfoService _service;

        private readonly AppSettings _appSettings;

        public ChangeInfoRequestsController(dbhprofilingContext context, IChangeInfoService ChangeInfoService,
                                IOptions<AppSettings> appSettings)
        {
            _context = context;
            _service = ChangeInfoService;
            _appSettings = appSettings.Value;
        }

        //Get data with page, search, orderby, ordertype
        //api/ChangeInfoRequests/Filter
        [HttpPost]
        [Route("Filter")]
        public IActionResult GetLeaveRequest([FromBody]GetAll criteria)
        {

            var response = _service.GetAll(criteria, _appSettings);
            return Ok(response);
        }

        // GET: api/ChangeInfoRequests
        [HttpGet]
        public IEnumerable<ChangeInfoRequest> GetChangeInfoRequest()
        {
            return _context.ChangeInfoRequest;
        }

        // GET: api/ChangeInfoRequests/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetChangeInfoRequest([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var changeInfoRequest = await _context.ChangeInfoRequest.FindAsync(id);

            if (changeInfoRequest == null)
            {
                return NotFound();
            }

            return Ok(changeInfoRequest);
        }

        // PUT: api/ChangeInfoRequests/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutChangeInfoRequest([FromRoute] int id, [FromBody] ChangeInfoRequest changeInfoRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != changeInfoRequest.Id)
            {
                return BadRequest();
            }

            _context.Entry(changeInfoRequest).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ChangeInfoRequestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ChangeInfoRequests
        [HttpPost]
        public async Task<IActionResult> PostChangeInfoRequest([FromBody] ChangeInfoRequest changeInfoRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.ChangeInfoRequest.Add(changeInfoRequest);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetChangeInfoRequest", new { id = changeInfoRequest.Id }, changeInfoRequest);
        }

        // DELETE: api/ChangeInfoRequests/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteChangeInfoRequest([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var changeInfoRequest = await _context.ChangeInfoRequest.FindAsync(id);
            if (changeInfoRequest == null)
            {
                return NotFound();
            }

            _context.ChangeInfoRequest.Remove(changeInfoRequest);
            await _context.SaveChangesAsync();

            return Ok(changeInfoRequest);
        }

        private bool ChangeInfoRequestExists(int id)
        {
            return _context.ChangeInfoRequest.Any(e => e.Id == id);
        }
    }
}