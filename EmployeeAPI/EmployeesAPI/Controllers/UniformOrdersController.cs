﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UniformOrdersController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        public UniformOrdersController(dbhprofilingContext context)
        {
            _context = context;
        }

        // GET: api/UniformOrders
        [HttpGet]
        public IEnumerable<UniformOrder> GetUniformOrder()
        {
            return _context.UniformOrder;
        }

        // GET: api/UniformOrders/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUniformOrder([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var uniformOrder = await _context.UniformOrder.FindAsync(id);

            if (uniformOrder == null)
            {
                return NotFound();
            }

            return Ok(uniformOrder);
        }

        // PUT: api/UniformOrders/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUniformOrder([FromRoute] int id, [FromBody] UniformOrder uniformOrder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != uniformOrder.Id)
            {
                return BadRequest();
            }

            _context.Entry(uniformOrder).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UniformOrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/UniformOrders
        [HttpPost]
        public async Task<IActionResult> PostUniformOrder([FromBody] UniformOrder uniformOrder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.UniformOrder.Add(uniformOrder);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUniformOrder", new { id = uniformOrder.Id }, uniformOrder);
        }

        // DELETE: api/UniformOrders/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUniformOrder([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var uniformOrder = await _context.UniformOrder.FindAsync(id);
            if (uniformOrder == null)
            {
                return NotFound();
            }

            _context.UniformOrder.Remove(uniformOrder);
            await _context.SaveChangesAsync();

            return Ok(uniformOrder);
        }

        private bool UniformOrderExists(int id)
        {
            return _context.UniformOrder.Any(e => e.Id == id);
        }
    }
}