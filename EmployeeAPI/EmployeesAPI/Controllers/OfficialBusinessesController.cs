﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;
using EmployeesAPI.Helpers;
using Microsoft.Extensions.Options;
using EmployeesAPI.Services.OfficialBusiness;
using EmployeesAPI.DTOs.LeaveRequest;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OfficialBusinessesController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        private readonly IOfficialBusinessService _service;

        private readonly AppSettings _appSettings;

        public OfficialBusinessesController(dbhprofilingContext context, IOfficialBusinessService OfficialBusinessService,
                                IOptions<AppSettings> appSettings)
        {
            _context = context;
            _service = OfficialBusinessService;
            _appSettings = appSettings.Value;
        }

        //Get data with page, search, orderby, ordertype
        //api/IncidentReports/Filter
        [HttpPost]
        [Route("Filter")]
        public IActionResult GetLeaveRequest([FromBody]GetAll criteria)
        {

            var response = _service.GetAll(criteria, _appSettings);
            return Ok(response);
        }

        // GET: api/OfficialBusinesses
        [HttpGet]
        public IEnumerable<OfficialBusiness> GetOfficialBusiness()
        {
            return _context.OfficialBusiness;
        }

        // GET: api/OfficialBusinesses/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOfficialBusiness([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var officialBusiness = await _context.OfficialBusiness.FindAsync(id);

            if (officialBusiness == null)
            {
                return NotFound();
            }

            return Ok(officialBusiness);
        }

        // PUT: api/OfficialBusinesses/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOfficialBusiness([FromRoute] int id, [FromBody] OfficialBusiness officialBusiness)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != officialBusiness.Id)
            {
                return BadRequest();
            }

            _context.Entry(officialBusiness).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OfficialBusinessExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/OfficialBusinesses
        [HttpPost]
        public async Task<IActionResult> PostOfficialBusiness([FromBody] OfficialBusiness officialBusiness)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.OfficialBusiness.Add(officialBusiness);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (OfficialBusinessExists(officialBusiness.EmpId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetOfficialBusiness", new { id = officialBusiness.EmpId }, officialBusiness);
        }

        // DELETE: api/OfficialBusinesses/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOfficialBusiness([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var officialBusiness = await _context.OfficialBusiness.FindAsync(id);
            if (officialBusiness == null)
            {
                return NotFound();
            }

            _context.OfficialBusiness.Remove(officialBusiness);
            await _context.SaveChangesAsync();

            return Ok(officialBusiness);
        }

        private bool OfficialBusinessExists(int id)
        {
            return _context.OfficialBusiness.Any(e => e.EmpId == id);
        }
    }
}