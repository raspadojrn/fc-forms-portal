﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TbldepartmentsController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        public TbldepartmentsController(dbhprofilingContext context)
        {
            _context = context;
        }

        // GET: api/Tbldepartments
        [HttpGet]
        public IEnumerable<Tbldepartment> GetTbldepartment()
        {
            return _context.Tbldepartment;
        }

        // GET: api/Tbldepartments/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTbldepartment([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tbldepartment = await _context.Tbldepartment.FindAsync(id);

            if (tbldepartment == null)
            {
                return NotFound();
            }

            return Ok(tbldepartment);
        }

        // PUT: api/Tbldepartments/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTbldepartment([FromRoute] int id, [FromBody] Tbldepartment tbldepartment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tbldepartment.Id)
            {
                return BadRequest();
            }

            _context.Entry(tbldepartment).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TbldepartmentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Tbldepartments
        [HttpPost]
        public async Task<IActionResult> PostTbldepartment([FromBody] Tbldepartment tbldepartment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Tbldepartment.Add(tbldepartment);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TbldepartmentExists(tbldepartment.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTbldepartment", new { id = tbldepartment.Id }, tbldepartment);
        }

        // DELETE: api/Tbldepartments/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTbldepartment([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tbldepartment = await _context.Tbldepartment.FindAsync(id);
            if (tbldepartment == null)
            {
                return NotFound();
            }

            _context.Tbldepartment.Remove(tbldepartment);
            await _context.SaveChangesAsync();

            return Ok(tbldepartment);
        }

        private bool TbldepartmentExists(int id)
        {
            return _context.Tbldepartment.Any(e => e.Id == id);
        }
    }
}