﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuggestionBoxesController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        public SuggestionBoxesController(dbhprofilingContext context)
        {
            _context = context;
        }

        // GET: api/SuggestionBoxes
        [HttpGet]
        public IEnumerable<SuggestionBox> GetSuggestionBox()
        {
            return _context.SuggestionBox;
        }

        // GET: api/SuggestionBoxes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSuggestionBox([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var suggestionBox = await _context.SuggestionBox.FindAsync(id);

            if (suggestionBox == null)
            {
                return NotFound();
            }

            return Ok(suggestionBox);
        }

        // PUT: api/SuggestionBoxes  /5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSuggestionBox([FromRoute] int id, [FromBody] SuggestionBox suggestionBox)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != suggestionBox.Id)
            {
                return BadRequest();
            }

            _context.Entry(suggestionBox).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SuggestionBoxExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SuggestionBoxes
        [HttpPost]
        public async Task<IActionResult> PostSuggestionBox([FromBody] SuggestionBox suggestionBox)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.SuggestionBox.Add(suggestionBox);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SuggestionBoxExists(suggestionBox.EmpId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetSuggestionBox", new { id = suggestionBox.EmpId }, suggestionBox);
        }

        // DELETE: api/SuggestionBoxes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSuggestionBox([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var suggestionBox = await _context.SuggestionBox.FindAsync(id);
            if (suggestionBox == null)
            {
                return NotFound();
            }

            _context.SuggestionBox.Remove(suggestionBox);
            await _context.SaveChangesAsync();

            return Ok(suggestionBox);
        }

        private bool SuggestionBoxExists(int id)
        {
            return _context.SuggestionBox.Any(e => e.EmpId == id);
        }
    }
}