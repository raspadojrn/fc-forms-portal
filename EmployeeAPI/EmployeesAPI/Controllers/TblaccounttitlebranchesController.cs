﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TblaccounttitlebranchesController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        public TblaccounttitlebranchesController(dbhprofilingContext context)
        {
            _context = context;
        }

        // GET: api/Tblaccounttitlebranches
        [HttpGet]
        public IEnumerable<Tblaccounttitlebranch> GetTblaccounttitlebranch()
        {
            return _context.Tblaccounttitlebranch;
        }

        // GET: api/Tblaccounttitlebranches/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTblaccounttitlebranch([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tblaccounttitlebranch = await _context.Tblaccounttitlebranch.FindAsync(id);

            if (tblaccounttitlebranch == null)
            {
                return NotFound();
            }

            return Ok(tblaccounttitlebranch);
        }

        // PUT: api/Tblaccounttitlebranches/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTblaccounttitlebranch([FromRoute] int id, [FromBody] Tblaccounttitlebranch tblaccounttitlebranch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tblaccounttitlebranch.Id)
            {
                return BadRequest();
            }

            _context.Entry(tblaccounttitlebranch).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TblaccounttitlebranchExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Tblaccounttitlebranches
        [HttpPost]
        public async Task<IActionResult> PostTblaccounttitlebranch([FromBody] Tblaccounttitlebranch tblaccounttitlebranch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Tblaccounttitlebranch.Add(tblaccounttitlebranch);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTblaccounttitlebranch", new { id = tblaccounttitlebranch.Id }, tblaccounttitlebranch);
        }

        // DELETE: api/Tblaccounttitlebranches/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTblaccounttitlebranch([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tblaccounttitlebranch = await _context.Tblaccounttitlebranch.FindAsync(id);
            if (tblaccounttitlebranch == null)
            {
                return NotFound();
            }

            _context.Tblaccounttitlebranch.Remove(tblaccounttitlebranch);
            await _context.SaveChangesAsync();

            return Ok(tblaccounttitlebranch);
        }

        private bool TblaccounttitlebranchExists(int id)
        {
            return _context.Tblaccounttitlebranch.Any(e => e.Id == id);
        }
    }
}