﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FaqsController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        public FaqsController(dbhprofilingContext context)
        {
            _context = context;
        }

        // GET: api/Faqs
        [HttpGet]
        public IEnumerable<Faqs> GetFaqs()
        {
            return _context.Faqs;
        }

        // GET: api/Faqs/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFaqs([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var faqs = await _context.Faqs.FindAsync(id);

            if (faqs == null)
            {
                return NotFound();
            }

            return Ok(faqs);
        }

        // PUT: api/Faqs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFaqs([FromRoute] int id, [FromBody] Faqs faqs)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != faqs.Id)
            {
                return BadRequest();
            }

            _context.Entry(faqs).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FaqsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Faqs
        [HttpPost]
        public async Task<IActionResult> PostFaqs([FromBody] Faqs faqs)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Faqs.Add(faqs);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFaqs", new { id = faqs.Id }, faqs);
        }

        // DELETE: api/Faqs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFaqs([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var faqs = await _context.Faqs.FindAsync(id);
            if (faqs == null)
            {
                return NotFound();
            }

            _context.Faqs.Remove(faqs);
            await _context.SaveChangesAsync();

            return Ok(faqs);
        }

        private bool FaqsExists(int id)
        {
            return _context.Faqs.Any(e => e.Id == id);
        }
    }
}