﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PrOrderedItemsController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        public PrOrderedItemsController(dbhprofilingContext context)
        {
            _context = context;
        }

        // GET: api/PrOrderedItems
        [HttpGet]
        public IEnumerable<PrOrderedItems> GetPrOrderedItems()
        {
            return _context.PrOrderedItems;
        }

        // GET: api/PrOrderedItems/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPrOrderedItems([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var prOrderedItems = await _context.PrOrderedItems.FindAsync(id);

            if (prOrderedItems == null)
            {
                return NotFound();
            }

            return Ok(prOrderedItems);
        }

        // PUT: api/PrOrderedItems/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPrOrderedItems([FromRoute] int id, [FromBody] PrOrderedItems prOrderedItems)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != prOrderedItems.Id)
            {
                return BadRequest();
            }

            _context.Entry(prOrderedItems).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PrOrderedItemsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PrOrderedItems
        [HttpPost]
        public async Task<IActionResult> PostPrOrderedItems([FromBody] PrOrderedItems prOrderedItems)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.PrOrderedItems.Add(prOrderedItems);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPrOrderedItems", new { id = prOrderedItems.Id }, prOrderedItems);
        }

        // DELETE: api/PrOrderedItems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePrOrderedItems([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var prOrderedItems = await _context.PrOrderedItems.FindAsync(id);
            if (prOrderedItems == null)
            {
                return NotFound();
            }

            _context.PrOrderedItems.Remove(prOrderedItems);
            await _context.SaveChangesAsync();

            return Ok(prOrderedItems);
        }

        private bool PrOrderedItemsExists(int id)
        {
            return _context.PrOrderedItems.Any(e => e.Id == id);
        }
    }
}