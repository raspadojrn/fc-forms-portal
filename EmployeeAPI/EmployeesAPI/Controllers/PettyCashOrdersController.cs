﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PettyCashOrdersController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        public PettyCashOrdersController(dbhprofilingContext context)
        {
            _context = context;
        }

        // GET: api/PettyCashOrders
        [HttpGet]
        public IEnumerable<PettyCashOrders> GetPettyCashOrders()
        {
            return _context.PettyCashOrders;
        }

        // GET: api/PettyCashOrders/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPettyCashOrders([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pettyCashOrders = await _context.PettyCashOrders.FindAsync(id);

            if (pettyCashOrders == null)
            {
                return NotFound();
            }

            return Ok(pettyCashOrders);
        }

        // PUT: api/PettyCashOrders/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPettyCashOrders([FromRoute] int id, [FromBody] PettyCashOrders pettyCashOrders)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pettyCashOrders.Id)
            {
                return BadRequest();
            }

            _context.Entry(pettyCashOrders).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PettyCashOrdersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PettyCashOrders
        [HttpPost]
        public async Task<IActionResult> PostPettyCashOrders([FromBody] PettyCashOrders pettyCashOrders)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.PettyCashOrders.Add(pettyCashOrders);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPettyCashOrders", new { id = pettyCashOrders.Id }, pettyCashOrders);
        }

        // DELETE: api/PettyCashOrders/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePettyCashOrders([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pettyCashOrders = await _context.PettyCashOrders.FindAsync(id);
            if (pettyCashOrders == null)
            {
                return NotFound();
            }

            _context.PettyCashOrders.Remove(pettyCashOrders);
            await _context.SaveChangesAsync();

            return Ok(pettyCashOrders);
        }

        private bool PettyCashOrdersExists(int id)
        {
            return _context.PettyCashOrders.Any(e => e.Id == id);
        }
    }
}