﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;
using EmployeesAPI.DTOs.LeaveRequest;
using EmployeesAPI.Services.IncidentReport;
using EmployeesAPI.Helpers;
using Microsoft.Extensions.Options;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IncidentReportsController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        private readonly IIncidentReportService _service;

        private readonly AppSettings _appSettings;

        public IncidentReportsController(dbhprofilingContext context, IIncidentReportService IncidentReportService,
                                IOptions<AppSettings> appSettings)
        {
            _context = context;
            _service = IncidentReportService;
            _appSettings = appSettings.Value;
        }

        //Get data with page, search, orderby, ordertype
        //api/IncidentReports/Filter
        [HttpPost]
        [Route("Filter")]
        public IActionResult GetLeaveRequest([FromBody]GetAll criteria)
        {

            var response = _service.GetAll(criteria, _appSettings);
            return Ok(response);
        }

        // GET: api/IncidentReports
        [HttpGet]
        public IEnumerable<IncidentReport> GetIncidentReport()
        {
            return _context.IncidentReport;
        }

        // GET: api/IncidentReports/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetIncidentReport([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var incidentReport = await _context.IncidentReport.FindAsync(id);

            if (incidentReport == null)
            {
                return NotFound();
            }

            return Ok(incidentReport);
        }

        // PUT: api/IncidentReports/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutIncidentReport([FromRoute] int id, [FromBody] IncidentReport incidentReport)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != incidentReport.Id)
            {
                return BadRequest();
            }

            _context.Entry(incidentReport).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IncidentReportExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/IncidentReports
        [HttpPost]
        public async Task<IActionResult> PostIncidentReport([FromBody] IncidentReport incidentReport)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.IncidentReport.Add(incidentReport);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (IncidentReportExists(incidentReport.EmpId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetIncidentReport", new { id = incidentReport.EmpId }, incidentReport);
        }

        // DELETE: api/IncidentReports/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteIncidentReport([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var incidentReport = await _context.IncidentReport.FindAsync(id);
            if (incidentReport == null)
            {
                return NotFound();
            }

            _context.IncidentReport.Remove(incidentReport);
            await _context.SaveChangesAsync();

            return Ok(incidentReport);
        }

        private bool IncidentReportExists(int id)
        {
            return _context.IncidentReport.Any(e => e.EmpId == id);
        }
    }
}