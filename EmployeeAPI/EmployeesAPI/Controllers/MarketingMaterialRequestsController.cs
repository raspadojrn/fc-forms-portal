﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;
using EmployeesAPI.Services.PurchaseForm.Marketing;
using EmployeesAPI.Helpers;
using Microsoft.Extensions.Options;
using EmployeesAPI.DTOs.LeaveRequest;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MarketingMaterialRequestsController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        private readonly IMarketingService _service;

        private readonly AppSettings _appSettings;

        public MarketingMaterialRequestsController(dbhprofilingContext context, IMarketingService MarketingService,
                         IOptions<AppSettings> appSettings)
        {
            _context = context;
            _service = MarketingService;
            _appSettings = appSettings.Value;
        }


        //Get data with page, search, orderby, ordertype
        [HttpPost]
        [Route("Filter")]
        public IActionResult GetLeaveRequest([FromBody]GetAll criteria)
        {

            var response = _service.GetAllFilter(criteria, _appSettings);
            return Ok(response);
        }




        // GET: api/MarketingMaterialRequests
        [HttpGet]
        public IEnumerable<MarketingMaterialRequest> GetMarketingMaterialRequest()
        {
            return _context.MarketingMaterialRequest;
        }

        // GET: api/MarketingMaterialRequests/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMarketingMaterialRequest([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var marketingMaterialRequest = await _context.MarketingMaterialRequest.FindAsync(id);

            if (marketingMaterialRequest == null)
            {
                return NotFound();
            }

            return Ok(marketingMaterialRequest);
        }

        // PUT: api/MarketingMaterialRequests/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMarketingMaterialRequest([FromRoute] int id, [FromBody] MarketingMaterialRequest marketingMaterialRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != marketingMaterialRequest.Id)
            {
                return BadRequest();
            }

            _context.Entry(marketingMaterialRequest).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MarketingMaterialRequestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MarketingMaterialRequests
        [HttpPost]
        public async Task<IActionResult> PostMarketingMaterialRequest([FromBody] MarketingMaterialRequest marketingMaterialRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.MarketingMaterialRequest.Add(marketingMaterialRequest);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMarketingMaterialRequest", new { id = marketingMaterialRequest.Id }, marketingMaterialRequest);
        }

        // DELETE: api/MarketingMaterialRequests/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMarketingMaterialRequest([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var marketingMaterialRequest = await _context.MarketingMaterialRequest.FindAsync(id);
            if (marketingMaterialRequest == null)
            {
                return NotFound();
            }

            _context.MarketingMaterialRequest.Remove(marketingMaterialRequest);
            await _context.SaveChangesAsync();

            return Ok(marketingMaterialRequest);
        }

        private bool MarketingMaterialRequestExists(int id)
        {
            return _context.MarketingMaterialRequest.Any(e => e.Id == id);
        }
    }
}