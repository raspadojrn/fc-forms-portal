﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;
using EmployeesAPI.Services.Profile;
using EmployeesAPI.Helpers;
using EmployeesAPI.Services.EmpProfile;
using Microsoft.Extensions.Options;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TblprofilesController : ControllerBase
    {
        private readonly dbhprofilingContext _context;
        // private readonly IProfileService _service;

        private readonly IEmpProfileService _service;

        private readonly AppSettings _appSettings;

        public TblprofilesController(dbhprofilingContext context, IEmpProfileService EmpProfile,
                                IOptions<AppSettings> appSettings
            )
        {
            _context = context;
            _service = EmpProfile;
            _appSettings = appSettings.Value;
        }

        // GET: api/Tblprofiles
        [HttpGet]
        public ActionResult<IEnumerable<Tblprofile>> GetTblprofile()
        {
            var response = _service.GetAll(_appSettings);
            return Ok(response);
        }

        // GET: api/Tblprofiles/5
        [HttpGet("{id}")]
        public IActionResult GetTblprofile([FromRoute] int id)
        {

            var list = _service.GetDataById(id);
            return Ok(list);
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            //var tblprofile = await _context.Tblprofile.FindAsync(id);

            //if (tblprofile == null)
            //{
            //    return NotFound();
            //}

            //return Ok(tblprofile);
        }

        // PUT: api/Tblprofiles/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTblprofile([FromRoute] int id, [FromBody] Tblprofile tblprofile)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tblprofile.Id)
            {
                return BadRequest();
            }

            _context.Entry(tblprofile).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TblprofileExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Tblprofiles
        [HttpPost]
        public async Task<IActionResult> PostTblprofile([FromBody] Tblprofile tblprofile)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Tblprofile.Add(tblprofile);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TblprofileExists(tblprofile.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTblprofile", new { id = tblprofile.Id }, tblprofile);
        }

        // DELETE: api/Tblprofiles/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTblprofile([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tblprofile = await _context.Tblprofile.FindAsync(id);
            if (tblprofile == null)
            {
                return NotFound();
            }

            _context.Tblprofile.Remove(tblprofile);
            await _context.SaveChangesAsync();

            return Ok(tblprofile);
        }

        private bool TblprofileExists(int id)
        {
            return _context.Tblprofile.Any(e => e.Id == id);
        }
    }
}