﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;
using Microsoft.AspNetCore.Authorization;
using EmployeesAPI.Services;
using EmployeesAPI.Helpers;
using Microsoft.Extensions.Options;
using EmployeesAPI.DTOs.LeaveRequest;

namespace EmployeesAPI.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TbusersController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        private readonly IUserService _service;

        private readonly AppSettings _appSettings;

        public TbusersController(dbhprofilingContext context, 
            IUserService userService, 
            IOptions<AppSettings> appSettings)
        {
            _context = context;
            _service = userService;
            _appSettings = appSettings.Value;
            
          
        }

        /// <summary>
        /// Auntheticate User
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        /// <response code="200">User was authorized.</response>
        /// <response code="401">Unauthorized user.</response>
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate(AuthenticateUser user)
        {
            var authenticatedUser = _service.Authenticate(user);
            if (authenticatedUser == null)
            {
                return Unauthorized();
            }

            return Ok(_service.GenerateToken(authenticatedUser, _appSettings));
        }

        //Get data with page, search, orderby, ordertype
        [HttpPost]
        [Route("Filter")]
        public IActionResult GetAllFilter([FromBody]GetAll criteria)
        {

            var response = _service.GetAllFilter(criteria, _appSettings);
            return Ok(response);
        }

        // GET: api/Tbusers
        [HttpGet]
        public ActionResult<IEnumerable<Tbuser>> GetTbuser()
        {

            var response = _service.GetAll(_appSettings);
            return Ok(response);
          //  return _context.Tbuser;
        }

        // GET: api/Tbusers/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTbuser([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tbuser = await _context.Tbuser.FindAsync(id);

            if (tbuser == null)
            {
                return NotFound();
            }

            return Ok(tbuser);
        }

        // PUT: api/Tbusers/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTbuser([FromRoute] int id, [FromBody] Tbuser tbuser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tbuser.Id)
            {
                return BadRequest();
            }

            _context.Entry(tbuser).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TbuserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Tbusers
        [HttpPost]
        public async Task<IActionResult> PostTbuser([FromBody] Tbuser tbuser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Tbuser.Add(tbuser);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TbuserExists(tbuser.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTbuser", new { id = tbuser.Id }, tbuser);
        }

        // DELETE: api/Tbusers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTbuser([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tbuser = await _context.Tbuser.FindAsync(id);
            if (tbuser == null)
            {
                return NotFound();
            }

            _context.Tbuser.Remove(tbuser);
            await _context.SaveChangesAsync();

            return Ok(tbuser);
        }

        private bool TbuserExists(int id)
        {
            return _context.Tbuser.Any(e => e.Id == id);
        }
    }
}