﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeesAPI.Models;
using EmployeesAPI.Services.PettyCash;
using EmployeesAPI.Helpers;
using Microsoft.Extensions.Options;
using EmployeesAPI.DTOs.LeaveRequest;

namespace EmployeesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PettyCashesController : ControllerBase
    {
        private readonly dbhprofilingContext _context;

        private readonly IPettyCashService _service;

        private readonly AppSettings _appSettings;

        public PettyCashesController(dbhprofilingContext context, IPettyCashService PettyCashService,
                         IOptions<AppSettings> appSettings)
        {
            _context = context;
            _service = PettyCashService;
            _appSettings = appSettings.Value;
        }

        //Get data with page, search, orderby, ordertype
        [HttpPost]
        [Route("Filter")]
        public IActionResult GetLeaveRequest([FromBody]GetAll criteria)
        {

            var response = _service.GetAllFilter(criteria, _appSettings);
            return Ok(response);
        }

        // GET: api/PettyCashes
        [HttpGet]
        public IEnumerable<PettyCash> GetPettyCash()
        {
            return _context.PettyCash;
        }

        // GET: api/PettyCashes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPettyCash([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pettyCash = await _context.PettyCash.FindAsync(id);

            if (pettyCash == null)
            {
                return NotFound();
            }

            return Ok(pettyCash);
        }

        // PUT: api/PettyCashes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPettyCash([FromRoute] int id, [FromBody] PettyCash pettyCash)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pettyCash.Id)
            {
                return BadRequest();
            }

            _context.Entry(pettyCash).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PettyCashExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PettyCashes
        [HttpPost]
        public async Task<IActionResult> PostPettyCash([FromBody] PettyCash pettyCash)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.PettyCash.Add(pettyCash);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPettyCash", new { id = pettyCash.Id }, pettyCash);
        }

        // DELETE: api/PettyCashes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePettyCash([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pettyCash = await _context.PettyCash.FindAsync(id);
            if (pettyCash == null)
            {
                return NotFound();
            }

            _context.PettyCash.Remove(pettyCash);
            await _context.SaveChangesAsync();

            return Ok(pettyCash);
        }

        private bool PettyCashExists(int id)
        {
            return _context.PettyCash.Any(e => e.Id == id);
        }
    }
}