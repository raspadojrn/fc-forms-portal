﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeesAPI.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using EmployeesAPI.Services;
using EmployeesAPI.Helpers;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using EmployeesAPI.Services.LeaveRequest;
using EmployeesAPI.Services.Overtime;
using EmployeesAPI.Services.OfficialBusiness;
using EmployeesAPI.Services.IncidentReport;
using EmployeesAPI.Services.EmpProfile;
using EmployeesAPI.Services.PurchaseForm.Purchaserequest;
using EmployeesAPI.Services.PurchaseForm.Uniform;
using EmployeesAPI.Services.PurchaseForm.Booklet;
using EmployeesAPI.Services.PurchaseForm.BusinessCard;
using EmployeesAPI.Services.PurchaseForm.Marketing;
using EmployeesAPI.Services.PurchaseForm.Maintenance;
using EmployeesAPI.Services.ChangeInfoRequest;
using EmployeesAPI.Services.PettyCash;

namespace EmployeesAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //To Enable Cors Plicy
            services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
            {
                builder
                    .AllowAnyMethod()
                    .AllowAnyHeader();
                    //.WithOrigins("http://localhost:4200");
            }));

            #region UserService
            services.AddScoped<IUserService, UserService>();
            #endregion

            #region LeaveRequestService
            services.AddScoped<ILeaveRequestService, LeaveRequestService>();
            #endregion

            #region OvertimetService
            services.AddScoped<IOvertimeService, OvertimeService>();
            #endregion

            #region OfficialBusinessService
            services.AddScoped<IOfficialBusinessService, OfficialBusinessService>();
            #endregion

            #region IncidentReportService
            services.AddScoped<IIncidentReportService, IncidentReportService>();
            #endregion

            #region ChangeInfoService
            services.AddScoped<IChangeInfoService, ChangeInfoService>();
            #endregion

            #region EmpPropileService
            services.AddScoped<IEmpProfileService, EmpProfileService>();
            #endregion

            //Purchase Request Forms
            #region PurchaseRequestService
            services.AddScoped<IPurchaseRequestService, PurchaseRequestService>();
            #endregion

            #region UniformService
            services.AddScoped<IUniformService, UniformService>();
            #endregion

            #region BookletService
            services.AddScoped<IBookletService, BookletService>();
            #endregion

            #region BusinessCardService
            services.AddScoped<IBusinessCardService, BusinessCardService>();
            #endregion

            #region MarketingService
            services.AddScoped<IMarketingService, MarketingService>();
            #endregion

            #region MaintenanceService
            services.AddScoped<IMaintenanceService, MaintenanceService>();
            #endregion

            //Petty Cash
            #region PettyCashService
            services.AddScoped<IPettyCashService, PettyCashService>();
            #endregion

            //DB Connection
            services.AddDbContext<dbhprofilingContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);


            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);


            // Configure JWT Authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(builder =>
            {
                builder.AllowAnyOrigin()
                .AllowAnyHeader().AllowAnyMethod().AllowCredentials();
            });

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
