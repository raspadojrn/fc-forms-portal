﻿using System;
using System.Collections.Generic;

namespace EmpProfilingAPI.Models
{
    public partial class Employee
    {
        public int Empid { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public int? Age { get; set; }
    }
}
