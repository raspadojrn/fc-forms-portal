﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace EmpProfilingAPI.Models
{
    public partial class dbh_profilingContext : DbContext
    {
        public virtual DbSet<Employee> Employee { get; set; }

        // Unable to generate entity type for table 'dbo.tblassignmenthistory'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tblattachment'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tblbranch'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tblcompany'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbldepartment'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbleducational'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tblemploymenthistory'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tblhiresource'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tblposition'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tblprofile'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tblsalaryhistory'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tblwarehouse'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbuser'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"Server=LAPTOP-CSESUCBC;Database=dbh-profiling;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.Empid);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });
        }
    }
}
