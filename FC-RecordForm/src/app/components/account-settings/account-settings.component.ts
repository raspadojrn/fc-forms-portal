import { Component, OnInit } from '@angular/core';
import { RequestService } from 'app/Services/request.service';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { CustomValidator } from 'app/models/validators/custom.validator';
import { EnumsService } from 'app/Services/common/enums.service';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.css']
})
export class AccountSettingsComponent implements OnInit {

  title = "Account Settings"
  data : any =[];
  step :any;
  userData : any = [];
  hidePassword = true;
  hideCPassword = true;
  successMessage: any;
  myDate = new Date();

  branch : any = [];
  department : any = [];
  company : any = [];
  position : any = [];
  
  userForm : FormGroup;
  changeInfoRequest : FormGroup;
  stepperIndex : any;
  EmpInformation : FormGroup;

  constructor(private _requestService : RequestService, private fb: FormBuilder, private _enums : EnumsService) {
    this.load()
   }

  private load(): void {
    this._requestService.action ="Tblbranches"
    this._requestService.getRequest().subscribe(data => this.branch = data.sort((a, b) => (a.branch > b.branch) ? 1 : -1));

    this._requestService.action ="Tbldepartments"
    this._requestService.getRequest().subscribe(data => this.department = data.sort((a, b) => (a.department > b.department) ? 1 : -1));

    this._requestService.action ="Tblcompanies"
    this._requestService.getRequest().subscribe(data => this.company = data.sort((a, b) => (a.company > b.company) ? 1 : -1));

    this._requestService.action ="Tblpositions"
    this._requestService.getRequest().subscribe(data => this.position = data.sort((a, b) => (a.position > b.position) ? 1 : -1));

    // this._enums.getCommonList("branch",true).subscribe(ddl => this.branch=ddl.sort((a, b) => (a.name > b.name) ? 1 : -1));  
    // this._enums.getCommonList("department",true).subscribe(ddl => this.department=ddl.sort((a, b) => (a.name > b.name) ? 1 : -1));  
    // this._enums.getCommonList("company",true).subscribe(ddl => this.company=ddl.sort((a, b) => (a.name > b.name) ? 1 : -1));  
    // this._enums.getCommonList("position",true).subscribe(ddl => this.position=ddl.sort((a, b) => (a.name > b.name) ? 1 : -1));  
    }

  onSave(formData : any){

    let action = "Tblprofiles";

   if( this.stepperIndex == 1){
    action = "tbusers" 
    formData.password = formData.passwords.Password
   }

    console.log(formData.id, formData)
   
    this._requestService.action = action
    this._requestService.updateRecord(formData.id, formData).subscribe(res =>{
       console.log(this.successMessage ='Succesfully Updated')
       this.step =0;
       this.getData()
       this.userForm.controls.passwords.reset()
      })
  }

  onSubmit(formData : any){
    console.log(formData)
    this.setCurrentAndNewValue(formData)



    this._requestService.action = "changeInfoRequests"
    this._requestService.newRecord(formData).subscribe(res =>{
       console.log(this.successMessage ='Succesfully Updated')
       this.step =0;
       this.getData()
      })
  }
  setCurrentAndNewValue(formData){
    formData.newBranch = formData.currentBranch
    formData.newDepartment = formData.currentDepartment
    formData.newPosition = formData.currentPosition
    formData.newCompany = formData.currentCompany
    formData.newbiometricId = formData.currentBiometricId

    formData.newHiringDate = formData.currentHiringDate
    formData.newDateStart = formData.currentDateStart
    formData.newEmpStatus = formData.currentEmpStatus
    formData.newEducAttain = formData.currentEducAttain
    formData.newSchoolName = formData.currentSchoolName
    formData.newCourse = formData.currentCourse
    formData.newAttendYr = formData.currentAttendYr
    formData.newSssNo = formData.currentSssNo
    formData.newPagibigNo = formData.currentPagibigNo
    formData.newTinNo = formData.currentTinNo
    formData.newPhilHealthNo = formData.currentPhilHealthNo


    formData.currentBranch = this.data["branch"]
    formData.currentDepartment = this.data["department"]
    formData.currentPosition = this.data["position"]
    formData.currentCompany = this.data["company"]
    formData.currentBiometricId =  this.data["bio"]

    formData.currentHiringDate = this.data["hiringdate"]
    formData.currentDateStart = this.data["datestart"]
    formData.currentEmpStatus = this.data["regstatus"]
    formData.currentEducAttain = this.data["educattain"]
    formData.currentSchoolName =  this.data["schoolname"]
    formData.currentCourse = this.data["course"]
    formData.currentAttendYr = this.data["attendyear"]
    formData.currentSssNo = this.data["sssno"]
    formData.currentPagibigNo = this.data["pagibigno"]
    formData.currentTinNo =  this.data["tinno"]
    formData.currentPhilHealthNo =  this.data["philhealthno"]

    console.log( formData)
    
  }
  onClosePanel(){
    this.getData();
    this.userForm.controls.passwords.reset()
  }
  getIndex(e){
    console.log(this.stepperIndex = e.selectedIndex)
    this.successMessage = null
  }
  ngOnInit() {
    this.createForms();
    this.getData();
    this.userForm.reset()
  }


  getData(){

    let user = JSON.parse(localStorage.getItem("currentUser"))

    this._requestService.action = "tbusers/"
    this._requestService.getRequestById(user.id).subscribe( user => {
      this.userData = user
      
      this.userForm.patchValue({
        id: this.userData["id"],
        empId: this.userData["empid"],
        dateCreated: this.userData["datecreated"],
        username: this.userData["username"],
        
        passwords: this.fb.group({
          Password: [''],
          ConfirmPassword: [''],
  
        }),

        access: this.userData["access"],
        status: this.userData["status"],
        
      });
    })

    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(user.empId).subscribe(data => {
      this.data = data

      this.changeInfoRequest.patchValue ({
        empId: this.data["id"],
        firstName: this.data["fname"],
        lastName: this.data["lname"],
        emailAddress:  this.data["emaladd"],
  
        currentBranch:  this.data["branch"],
        currentDepartment: this.data["department"],
        currentPosition:  this.data["position"],
        currentCompany:  this.data["company"],
        currentBiometricId : this.data["bio"],

        currentHiringDate : this.data["hiringdate"],
        currentDateStart : this.data["datestart"],
        currentEmpStatus : this.data["regstatus"],
        currentEducAttain : this.data["educattain"],
        currentSchoolName :  this.data["schoolname"],
        currentCourse : this.data["course"],
        currentAttendYr : this.data["attendyear"],
        currentSssNo : this.data["sssno"],
        currentPagibigNo : this.data["pagibigno"],
        currentTinNo :  this.data["tinno"],
        currentPhilHealthNo :  this.data["philhealthno"],

        status : "Pending",
      });
      console.log(this.changeInfoRequest)
      this.EmpInformation.patchValue ({
              
        id: this.data["id"],

        fname: this.data["fname"],
        lname:this.data["lname"],
        mname: this.data["mname"],

        telno:  this.data["telno"],
        emaladd:  this.data["emaladd"],
        bday: this.data["bday"],

        address: this.data["address"],
        cstatus: this.data["cstatus"],
        conperson: this.data["conperson"],
        conpersontelno: this.data["conpersontelno"],
        

        Allowance: data["allowance"],
        Branch: this.data["branch"],
        Department: this.data["department"],
        Attendyear: this.data["attendyear"],
        Bio: this.data["bio"],
        Cola: this.data["cola"],
        Company: this.data["company"],
        Conemergency: this.data["conemergency"],
        Course: this.data["course"],
        Dateeoc: this.data["dateeoc"],
        Datefinalinterview: this.data["datefinalinterview"],
        Dateinitialinterview: this.data["dateinitialinterview"],
        Datereg: this.data["datereg"],
        Datesalrychange: this.data["datesalrychange"],
        Datestart: this.data["datestart"],
        Dependents: this.data["dependents"],
        Educattain: this.data["educattain"],

        Employststus: this.data["employststus"],
        Evaluationdate: this.data["evaluationdate"],
        Feedback: this.data["feedback"],
        Gender: this.data["gender"],
        Hiresource: this.data["hiresource"],
        Hiringdate: this.data["hiringdate"],
        Idemployehistory: this.data["idemployehistory"],
        Idno: this.data["idno"],
        Inviteby: this.data["inviteby"],
        Marks1: this.data["marks1"],
        Marks2: this.data["marks2"],
        Marks3: this.data["marks3"],
        Marks4: this.data["marks4"],
    
        Nationalid: this.data["nationalid"],
        Pagibigno: this.data["pagibigno"],
        Philhealthno: this.data["philhealthno"],
        Position: this.data["position"],
        Regstatus: this.data["regstatus"],
        Salary: this.data["salary"],
        Schoolname: this.data["schoolname"],
        Sssno: this.data["sssno"],
        Tinno: this.data["tinno"],
        Warehouse: this.data["warehouse"],
      });
      
    });
  }

  createForms(){
    this.userForm  = this.fb.group({
      id:[''],
      empId:[''],
      dateCreated: [''],
      username:[''],
      passwords: this.fb.group({
        Password:['',Validators.compose([Validators.required,Validators.minLength(6)])],
        ConfirmPassword:['',Validators.compose([Validators.required,Validators.minLength(6)])],

      },{validator: CustomValidator.checkConfirmPassword} ),

      access: [''],
      status: ['']
      
    });
    this.changeInfoRequest = this.fb.group({
      empId:['', Validators.required],
      fullName: [''],
      firstName:[''],
      lastName:['', Validators.required],
      dateCreated: this.myDate,
      emailAddress:  ['',Validators.compose([Validators.required,CustomValidator.emailAddress])],

      currentBranch:  ['', Validators.required],
      currentDepartment:['', Validators.required],
      currentPosition:  ['', Validators.required],
      currentCompany:  ['', Validators.required],
      currentBiometricId: ['', Validators.required],
      currentHiringDate: ['', Validators.required],
      currentDateStart: ['', Validators.required],
      currentEmpStatus: ['', Validators.required],
      currentEducAttain: ['', Validators.required],
      currentSchoolName: ['', Validators.required],
      currentCourse: ['', Validators.required],
      currentAttendYr: ['', Validators.required],
      currentSssNo: ['', Validators.required],
      currentPagibigNo: ['', Validators.required],
      currentTinNo: ['', Validators.required],
      currentPhilHealthNo: ['', Validators.required],

      newBranch:  [''],
      newDepartment:[''],
      newPosition:  [''],
      newCompany:  [''],
      newbiometricId: [''],
      newHiringDate: [''],
      newDateStart: [''],
      newEmpStatus: [''],
      newEducAttain: [''],
      newSchoolName: [''],
      newCourse: [''],
      newAttendYr: [''],
      newSssNo: [''],
      newPagibigNo: [''],
      newTinNo: [''],
      newPhilHealthNo: [''],

      reason: ['', Validators.required],
      status: [''],
  
      approver: ['']
    });

    this.EmpInformation = this.fb.group({
      id:['', Validators.required],
  
      fname:['', Validators.required],
      lname:['', Validators.required],
      mname:['', Validators.required],
  
      telno:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
      emaladd:  ['',Validators.compose([Validators.required,CustomValidator.emailAddress])],
      bday: ['',Validators.required],
  
      address: ['',Validators.required],
      cstatus: ['',Validators.required],
      conperson: ['',Validators.required],
      conpersontelno: ['',Validators.required],
  
      
      Allowance: ['',Validators.required],
      Branch: ['',Validators.required],
      Department: ['',Validators.required],
      Attendyear: [''],
  
      Bio: [''],
      Cola: [''],
      Company: ['',Validators.required],
      Conemergency: [''],
  
      Course: [''],
     
      Dateeoc: [''],
      Datefinalinterview: [''],
      Dateinitialinterview: [''],
      Datereg: [''],
      Datesalrychange: [''],
      Datestart: [''],
      Dependents: [''],
      Educattain: [''],
  
      Employststus: [''],
      Evaluationdate: [''],
      Feedback: [''],
      Gender: ['',Validators.required],
      Hiresource: [''],
      Hiringdate: [''],
      Idemployehistory: [''],
      Idno: ['',Validators.required],
      Inviteby: [''],
      Marks1: [''],
      Marks2: [''],
      Marks3: [''],
      Marks4: [''],
   
      Nationalid: [''],
      Pagibigno: [''],
      Philhealthno: [''],
      Position: ['',Validators.required],
      Regstatus: ['',Validators.required],
      Salary: [''],
      Schoolname: [''],
      Sssno: [''],
      Tinno: [''],
      Warehouse: [''],
  
    });
  
  }

  setStep(index: number) {
    this.step = index;
  }

  close() {
    this.step =0;
  }
}
