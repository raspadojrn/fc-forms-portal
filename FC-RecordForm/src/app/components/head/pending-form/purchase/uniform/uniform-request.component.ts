import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { RequestService } from 'app/Services/request.service';
import { FilterService } from 'app/Services/common/filter.service'
import { ApproveRequestURComponent } from './approve-request/approve-request.component';

@Component({
  selector: 'app-uniform-request',
  templateUrl: './uniform-request.component.html',
  styleUrls: ['./uniform-request.component.css']
})
export class UniformRequestPFComponent implements OnInit {

  title = "Uniform Request"
  myDate = new Date();
  tabIndex : any = 0;
 uniformRequestData: any =[];

 isDateFilter : boolean=false ;
 status: any={ 
   tab0 : "Pending",
   tab1 : "Accept",};
 filter = this.fb.group({
   DateFrom: [''],
   DateTo: [''],
   OrderBy: 'Id',
   OrderType: ['Descending'],
   ShowAll: true,
   Status : null
 })
 statusList: string[] = ['Pending',  'Approved', 'Cancelled'];
 constructor(private _requestService : RequestService, private _filterService : FilterService,
             private fb: FormBuilder, private dialog: MatDialog) { }
   
 getFilterStatusTab0(e, filterType){

   console.log(filterType =='date'? this.isDateFilter = true : this.isDateFilter = false)
   this.status.tab0 = e.value

     this.filter.value['Status'] =  this.status.tab0 
     this.getData(0)  
     console.log(e)  
 }
 resetFilter(){
   this.filter.value['DateFrom']= null
   this.filter.value['DateTo']= null
   this.getData(0)  
    console.log("click",this.filter.value)
  }
  sortByDate(e){
    e == "Descending" ? this.filter.value.OrderType = "Ascending" : 
                        this.filter.value.OrderType = "Descending"
    console.log(e, this.filter.value.OrderType)
    this.getData(0)  
  }

  getFilterStatusTab1(e){
    this.status.tab1 = e.value

      this.filter.value['Status'] = this.status.tab1
      this.getData(1)  
      console.log(e.value)  
  
    
  }

  openDialog(id: any, act : any, currStatus : any, index: any) {
    const dialogRef = this.dialog.open(ApproveRequestURComponent, {
      // height:'1000px',
      width: '800px',
      panelClass: 'my-dialog',
    });

    dialogRef.componentInstance.id = id;
    dialogRef.componentInstance.actStatus = act;
    dialogRef.componentInstance.currentStatus = currStatus;
    dialogRef.componentInstance.tabIndex =  this.tabIndex;
    dialogRef.componentInstance.selectedDAta = this.uniformRequestData.tab0[index];

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
     
      this.getData(0)
      this.getData(1)
    
      
    });
  }
  onTabChanged(e){
    console.log(e.index)
    this.tabIndex = e.index
  }
  
  
  ngOnInit() {
    this.getData(null)     
  }

  getData(tab : any){

    this._filterService.action= "UniformRequests/Filter"
    let criteria = this.filter.value

      switch(tab){
        case(0):
              this.filter.value['Status'] =  this.status.tab0
              criteria = this.filter.value
              this._filterService.GetfilterData(criteria).subscribe(data=> this.uniformRequestData.tab0 =this._filterService.filterData(data.list, 0))
              break;
        case(1):
              this.filter.value['Status'] =  this.status.tab1
              criteria = this.filter.value
              this._filterService.GetfilterData(criteria).subscribe(data=> this.uniformRequestData.tab1=data.list)
              break;
        default:
              this.filter.value['Status'] = "Pending"
              this._filterService.GetfilterData(criteria).subscribe(data=>  this.uniformRequestData.tab0  =this._filterService.filterData(data.list, 0))

              this.filter.value['Status'] = "Accept"
              this._filterService.GetfilterData(criteria).subscribe(data=>this.uniformRequestData.tab1  =data.list)
              console.log(this.uniformRequestData)
              

      }

  }
}