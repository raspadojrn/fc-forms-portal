import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { RequestService } from 'app/Services/request.service';
import { CustomValidator } from 'app/models/validators/custom.validator';

@Component({
  selector: 'app-approve-request',
  templateUrl: './approve-request.component.html',
  styleUrls: ['./approve-request.component.css']
})
export class ApproveRequestMRComponent implements OnInit {

  id : any;
  actStatus : any;
  currentStatus : any;
  tabIndex: any;
  selectedDAta : any;


  myDate = new Date();
  maintenanceRequestData: any ;
  approvedBy: any;
  successMessage: any;


  maintenanceForm = this.fb.group({
    id:[''],
    empId:['', Validators.required],
    fullName: [''],
    firstName:[''],
    lastName:['', Validators.required],
    dateCreated: this.myDate,
    branch:  ['', Validators.required],
    department:[''],

    branch_Dept:[''],
    position:  ['', Validators.required],
    company:  ['', Validators.required],
    contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
    emailAddress:  ['',Validators.compose([Validators.required,CustomValidator.emailAddress])],
    
    imageName: [''],
    requestType: ['', Validators.required],
    natureOfWork: ['', Validators.required],
    dateNeeded: ['', Validators.required],

    comment: [''],
    status: [''],

    adminApproval : ['', Validators.required],
    dateApprove: [''],
    approveRemarks: ['', Validators.required],
    approver: [''],


  });
  constructor(private _requestService : RequestService, private fb: FormBuilder, private dialog: MatDialog) { }


  approveRequest(){
 
   this.tabIndex == 0 ? this.maintenanceForm.value['adminApproval'] =this.approvedBy :
                        this.maintenanceForm.value['adminApproval'] =this.approvedBy 
    this.maintenanceForm.value['dateApprove'] =this.myDate
    let formData = this.maintenanceForm.value;
    console.log(this.maintenanceRequestData.id ,formData);
    
    this._requestService.action = "MaintenanceRequests"
    this._requestService.updateRecord(this.maintenanceRequestData.id ,formData).subscribe( res=> {
      console.log(this.successMessage= 'Succesfully Updated' )
    })
    
  }

  cancelRequest(){
    
    this.tabIndex == 0 ? this.maintenanceForm.value['adminApproval'] =this.approvedBy :
                         this.maintenanceForm.value['adminApproval'] =this.approvedBy 
    this.maintenanceForm.value["status"] = "Cancelled"
    this.maintenanceForm.value['dateApprove'] =this.myDate
    let formData = this.maintenanceForm.value;
    console.log(this.maintenanceRequestData.id ,formData);
    
    this._requestService.action = "MaintenanceRequests"
    this._requestService.updateRecord(this.maintenanceRequestData.id ,formData).subscribe( res=> {
      console.log(this.successMessage= 'Succesfully Updated')
    })
  }
  //Modal Close
  onClose(){
    console.log(this.tabIndex)
  this.dialog.closeAll()
  }

  ngOnInit() {

    console.log("id: ",this.id, this.selectedDAta)
    let data = this.selectedDAta
    this.maintenanceRequestData = data
    
      this.maintenanceForm.patchValue ({

        id: data["id"],   
        empId: data["empId"],
        dateCreated: data["dateCreated"],
        firstName: data["firstName"],
        lastName: data["lastName"],
        branch:  data["branch"],
        department: data["department"],
        biometricId:  data["biometricId"],
        contactNo:  data["contactNo"],
        emailAddress:  data["emailAddress"],

        position:  data["position"],
        company:  data["company"],

        imageName: data["imageName"],
        requestType: data['requestType'],
        natureOfWork: data["natureOfWork"],
        dateNeeded : data["dateNeeded"],
   
        comment: data["comment"],

        status: "Approved",
        approveRemarks:  data["approveRemarks"] == null ? data["approveRemarks"] : data["approveRemarks"]  +";\n",
        adminApproval:  data['adminApproval'],
        approver: data['approver']
      })
      
      console.log(this.maintenanceForm)

      //get approver name
    let id = JSON.parse(localStorage.getItem("currentUser")).empId 
    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id).subscribe(data => this.approvedBy = data.fname +' '+ data.lname);
  }
  
  closeImgModal(){
    var modal = document.getElementById("myModal");
    modal.style.display = "none";
  }
  imgModal(){
    var modal = document.getElementById("myModal");
    let img : any = document.getElementById("myImg");
    document.getElementById('image').setAttribute( 'src', img.src )
    modal.style.display = "block";
  }
}
