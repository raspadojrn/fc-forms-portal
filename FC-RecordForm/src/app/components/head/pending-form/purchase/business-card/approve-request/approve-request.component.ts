import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { RequestService } from 'app/Services/request.service';
import { CustomValidator } from 'app/models/validators/custom.validator';

@Component({
  selector: 'app-approve-request',
  templateUrl: './approve-request.component.html',
  styleUrls: ['./approve-request.component.css']
})
export class ApproveRequestBCComponent implements OnInit {

  id : any;
  actStatus : any;
  currentStatus : any;
  tabIndex: any;
  selectedDAta : any;


  myDate = new Date();
  businessCardData: any ;
  approvedBy: any;
  successMessage: any;
  orderedItems : any =[]


  businessCardForm = this.fb.group({

    id:[''],
    empId:['', Validators.required],
    fullName: [''],
    firstName:[''],
    lastName:['', Validators.required],
    dateCreated: this.myDate,
    branch:  ['', Validators.required],
    department:[''],

    branch_Dept:[''],
    position:  ['', Validators.required],
    company:  ['', Validators.required],
    contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
    emailAddress:  ['',Validators.compose([Validators.required,CustomValidator.emailAddress])],
    address: ['', Validators.required],

    brand: ['', Validators.required],
    nameOnCard: ['', Validators.required],
    landline: ['', Validators.required],
    mobile: ['', Validators.required],
    quantity: ['', Validators.required],

    purpose:  ['', Validators.required],
    comment: [''],
    status: [''],

    adminApproval : ['', Validators.required],
    dateApprove: [''],
    approveRemarks: ['', Validators.required],
    approver: [''],

  });
  constructor(private _requestService : RequestService, private fb: FormBuilder, private dialog: MatDialog) { }


  approveRequest(){
 
   this.tabIndex == 0 ? this.businessCardForm.value['adminApproval'] =this.approvedBy :
                        this.businessCardForm.value['adminApproval'] =this.approvedBy 
    this.businessCardForm.value['dateApprove'] =this.myDate
    let formData = this.businessCardForm.value;
    console.log(this.businessCardData.id ,formData);
    
    this._requestService.action = "BusinessCardRequests"
    this._requestService.updateRecord(this.businessCardData.id ,formData).subscribe( res=> {
      console.log(this.successMessage= 'Succesfully Updated' )
    })
    
  }

  cancelRequest(){
    
    this.tabIndex == 0 ? this.businessCardForm.value['adminApproval'] =this.approvedBy :
                         this.businessCardForm.value['adminApproval'] =this.approvedBy 
    this.businessCardForm.value["status"] = "Cancelled"
    this.businessCardForm.value['dateApprove'] =this.myDate
    let formData = this.businessCardForm.value;
    console.log(this.businessCardData.id ,formData);
    
    this._requestService.action = "BusinessCardRequests"
    this._requestService.updateRecord(this.businessCardData.id ,formData).subscribe( res=> {
      console.log(this.successMessage= 'Succesfully Updated')
    })
  }
  //Modal Close
  onClose(){
    console.log(this.tabIndex)
  this.dialog.closeAll()
  }

  ngOnInit() {
    // this.updateOrderedItems(null)

    console.log("id: ",this.id, this.selectedDAta)
    let data = this.selectedDAta
    this.businessCardData = data
    
      this.businessCardForm.patchValue ({
        id: data["id"],   
        empId: data["empId"],
        dateCreated: data["dateCreated"],
        firstName: data["firstName"],
        lastName: data["lastName"],
        branch:  data["branch"],
        department: data["department"],
        biometricId:  data["biometricId"],
        contactNo:  data["contactNo"],
        emailAddress:  data["emailAddress"],

        position:  data["position"],
        company:  data["company"],

        brand: data["brand"],
        nameOnCard: data["nameOnCard"],
        address: data["address"],
        mobile: data["mobile"],
        landline: data["landline"],
        quantity: data["quantity"],

        
        purpose:  data["purpose"],
        comment: data["comment"],

        status: "Approved",
        approveRemarks:  data["approveRemarks"] == null ? data["approveRemarks"] : data["approveRemarks"]  +";\n",
        adminApproval:  data['adminApproval'],
        approver: data['approver']
      })
      
      console.log(this.businessCardForm)

      //get approver name
    let id = JSON.parse(localStorage.getItem("currentUser")).empId 
    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id).subscribe(data => this.approvedBy = data.fname +' '+ data.lname);
  }

}
