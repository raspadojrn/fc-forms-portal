import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { RequestService } from 'app/Services/request.service';

@Component({
  selector: 'app-approve-request',
  templateUrl: './approve-request.component.html',
  styleUrls: ['./approve-request.component.css']
})
export class ApproveRequestPRComponent implements OnInit {

  id : any;
  actStatus : any;
  currentStatus : any;
  tabIndex: any;
  selectedDAta : any;


  myDate = new Date();
  purchaseRequestData: any ;
  approvedBy: any;
  successMessage: any;
  orderedItems : any =[]


  purchaseRequestForm = this.fb.group({
    id:[''],
    empId:['', Validators.required],
    fullName: [''],
    firstName:[''],
    lastName:['', Validators.required],
    dateCreated: this.myDate,
    branch:  ['', Validators.required],
    department:[''],

    branch_Dept:[''],
    contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
    emailAddress:  ['', Validators.required],
    
    purchaseType: ['', Validators.required],
    requestingParty: [''],
    orderItems : this.fb.array([]),	

    purpose:  ['', Validators.required],
    comment: [''],
    status: [''],

    adminApproval : ['', Validators.required],
    dateApprove: [''],
    approveRemarks: ['', Validators.required],
    approver: [''],



  });
  constructor(private _requestService : RequestService, private fb: FormBuilder, private dialog: MatDialog) { }



  approveRequest(){
 
   this.tabIndex == 0 ? this.purchaseRequestForm.value['adminApproval'] =this.approvedBy :
                        this.purchaseRequestForm.value['adminApproval'] =this.approvedBy 
    this.purchaseRequestForm.value['dateApprove'] =this.myDate
    let formData = this.purchaseRequestForm.value;
    console.log(this.purchaseRequestData.id ,formData);
    
    this._requestService.action = "PurchaseRequests"
    this._requestService.updateRecord(this.purchaseRequestData.id ,formData).subscribe( res=> {
      // console.log(this.successMessage= 'Succesfully Updated' )

      this.updateOrderedItems(formData.status)
    })
    
  }

  cancelRequest(){
    
    this.tabIndex == 0 ? this.purchaseRequestForm.value['adminApproval'] =this.approvedBy :
                         this.purchaseRequestForm.value['adminApproval'] =this.approvedBy 
    this.purchaseRequestForm.value["status"] = "Cancelled"
    this.purchaseRequestForm.value['dateApprove'] =this.myDate
    let formData = this.purchaseRequestForm.value;
    console.log(this.purchaseRequestData.id ,formData);
    
    this._requestService.action = "PurchaseRequests"
    this._requestService.updateRecord(this.purchaseRequestData.id ,formData).subscribe( res=> {
      // console.log(this.successMessage= 'Succesfully Updated')

      this.updateOrderedItems(formData.status)
    })
  }
  //Modal Close
  onClose(){
    console.log(this.tabIndex)
  this.dialog.closeAll()
  }

  updateOrderedItems(status : any){
    let formData =this.purchaseRequestForm.controls.orderItems.value
    console.log(status)
    
    this._requestService.action = "PrOrderedItems"
    for(let i =0; i< formData.length; i++){
      console.log(formData[i].status = status,formData[i].id)
      this._requestService.updateRecord(formData[i].id ,formData[i]).subscribe( res=> console.log(this.successMessage= 'Succesfully Updated'))
    }
  
 
  }

  get formArr() {
    return this.purchaseRequestForm.get('orderItems') as FormArray;
  }

  ngOnInit() {
    this.updateOrderedItems(null)

    console.log("id: ",this.id, this.selectedDAta)
    let data = this.selectedDAta
    this.purchaseRequestData = data
    
      this.purchaseRequestForm.patchValue ({
        
        id: data["id"],   
        empId: data["empId"],
        dateCreated: data["dateCreated"],
        firstName: data["firstName"],
        lastName: data["lastName"],
        branch:  data["branch"],
        department: data["department"],
        contactNo:  data["contactNo"],
        emailAddress:  data["emailAddress"],

        purchaseType: data["purchaseType"],
        requestingParty: data["requestingParty"],
        
        purpose:  data["purpose"],
        comment: data["comment"],

        status: "Approved",
        approveRemarks:  data["approveRemarks"] == null ? data["approveRemarks"] : data["approveRemarks"]  +";\n",
        adminApproval:  data['adminApproval'],
        approver: data['approver']
      })
      
      //set value to form array OrderedItems
      data.orderedItems.forEach((x) => {
        this.formArr.push(this.fb.group(x))
      });
      console.log(this.purchaseRequestForm)

      //get approver name
    let id = JSON.parse(localStorage.getItem("currentUser")).empId 
    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id).subscribe(data => this.approvedBy = data.fname +' '+ data.lname);
  }

}
