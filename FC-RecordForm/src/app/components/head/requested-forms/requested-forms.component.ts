import { Component, OnInit } from '@angular/core';
import { RequestService } from 'app/Services/request.service';
import { FilterService } from 'app/Services/common/filter.service'
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-requested-forms',
  templateUrl: './requested-forms.component.html',
  styleUrls: ['./requested-forms.component.css']
})
export class RequestedFormsComponent implements OnInit {
  
  title: any = "Form Request List";
  leaveRequest : any=[];
  overtime : any =[];
  officialBusiness : any =[];
  incidentReport : any =[];
  suggestionBox : any =[];
  //Purchase Forms
  purchaseRequest : any =[];
  uniformRequest : any =[];
  businessCard : any =[];
  bookletRequest : any=[];
  maintenanceRequest : any=[];
  marketingRequest : any=[];

  changeInfoRequest : any = [];
  pettyCashRequest : any =[];

  empId = JSON.parse(localStorage.getItem("currentUser")).empId

  filter = this.fb.group({
    OrderBy: 'Id',
    OrderType: ['Descending'],
    ShowAll: true,
    // Status : null
  })

  selectedIndex: any;
  mobView: boolean = true;
  constructor(private _requestService : RequestService,
              private _filterService : FilterService,
              private fb: FormBuilder,
              private route: ActivatedRoute,) { }

  ngOnInit() {
    this.route.queryParams.subscribe( data =>this.selectedIndex = data.Index);
    this.getData()
    this.mobileViewConf()
  }

  mobileViewConf(){
    if(screen.width < 600 || screen.height < 500){
      this.mobView= false
   }else{

   }
   console.log(screen.width)
  }
  getData(){
    let criteria = this.filter.value

    this._filterService.action= "LeaveRequests/Filter"
    this._filterService.GetfilterData(criteria).subscribe(data=> {console.log("LeaveRequests: ", data.list, data.list.length)   
      this.filterCurrentUserData(data.list, "LeaveRequests/Filter");
    })

    this._filterService.action = "RequestOts/Filter"
    this._filterService.GetfilterData(criteria).subscribe(data=> {console.log("RequestOts: ", data.list, data.list.length)   
      this.filterCurrentUserData(data.list, "RequestOts/Filter");
    })

    this._filterService.action = "OfficialBusinesses/Filter"
    this._filterService.GetfilterData(criteria).subscribe(data=> {console.log("OfficialBusinesses: ", data.list, data.list.length)   
      this.filterCurrentUserData(data.list, "OfficialBusinesses/Filter");
    })

    this._filterService.action = "IncidentReports/Filter"
    this._filterService.GetfilterData(criteria).subscribe(data=> {console.log("IncidentReports: ", data.list, data.list.length)   
      this.filterCurrentUserData(data.list, "IncidentReports/Filter");
    })

    this._requestService.action = "SuggestionBoxes"
    this._requestService.getRequest().subscribe(data=> {console.log("SuggestionBoxes: ", data, data.length)   
      this.filterCurrentUserData(data.reverse(), "SuggestionBoxes");
    })

    //PURCHASE FORM
    this._filterService.action = "PurchaseRequests/Filter"
    this._filterService.GetfilterData(criteria).subscribe(data=> {console.log("PurchaseRequests: ", data.list, data.list.length)   
      this.filterCurrentUserData(data.list, "PurchaseRequests/Filter");
    })

    this._filterService.action = "UniformRequests/Filter"
    this._filterService.GetfilterData(criteria).subscribe(data=> {console.log("UniformRequests: ", data.list, data.list.length)   
      this.filterCurrentUserData(data.list, "UniformRequests/Filter");
    })

    this._filterService.action = "BusinessCardRequests/Filter"
    this._filterService.GetfilterData(criteria).subscribe(data=> {console.log("BusinessCardRequests: ", data.list, data.list.length)   
      this.filterCurrentUserData(data.list, "BusinessCardRequests/Filter");
    })

    this._filterService.action = "BookletRequests/Filter"
    this._filterService.GetfilterData(criteria).subscribe(data=> {console.log("BookletRequests: ", data.list, data.list.length)   
      this.filterCurrentUserData(data.list, "BookletRequests/Filter");
    })

    this._filterService.action = "MaintenanceRequests/Filter"
    this._filterService.GetfilterData(criteria).subscribe(data=> {console.log("MaintenanceRequests: ", data.list, data.list.length)   
      this.filterCurrentUserData(data.list, "MaintenanceRequests/Filter");
    })

    this._filterService.action = "MarketingMaterialRequests/Filter"
    this._filterService.GetfilterData(criteria).subscribe(data=> {console.log("MarketingMaterialRequests: ", data.list, data.list.length)   
      this.filterCurrentUserData(data.list, "MarketingMaterialRequests/Filter");
    })

    this._filterService.action = "PettyCashes/Filter"
    this._filterService.GetfilterData(criteria).subscribe(data=> {console.log("pettyCashRequest: ", data.list, data.list.length)   
      this.filterCurrentUserData(data.list, "pettyCashRequest/Filter");
    })
    
    this._requestService.action = "changeInfoRequests"
    this._requestService.getRequest().subscribe(data=> {console.log("changeInfoRequests: ", data, data.length)   
      this.filterCurrentUserData(data.reverse(), "changeInfoRequests");
    })
  }

//User only see their form request data
  filterCurrentUserData(data : any, action: string){
    let cntr =0;
    for(let i =0; i < data.length; i ++){
      if(this.empId == data[i].empId){
        
        switch(action){
          case"LeaveRequests/Filter":
               this.leaveRequest[cntr] = data[i];
               break;
          case"RequestOts/Filter":
              this.overtime[cntr] = data[i];
              break;
          case"OfficialBusinesses/Filter":
              this.officialBusiness[cntr] = data[i];
              break;
          case"IncidentReports/Filter":
              this.incidentReport[cntr] = data[i];
              break;
          case"SuggestionBoxes":
              this.suggestionBox[cntr] = data[i];
              break;
          case"PurchaseRequests/Filter":
              this.purchaseRequest[cntr] = data[i];
              break;
          case"UniformRequests/Filter":
              this.uniformRequest[cntr] = data[i];
              break;
          case"BusinessCardRequests/Filter":
              this.businessCard[cntr] = data[i];
              break;
          case"BookletRequests/Filter":
              this.bookletRequest[cntr] = data[i];

              break;
          case"MaintenanceRequests/Filter":
              this.maintenanceRequest[cntr] = data[i];
              break;
          case"MarketingMaterialRequests/Filter":
              this.marketingRequest[cntr] = data[i];
              break;

          case"pettyCashRequest/Filter":
              this.pettyCashRequest[cntr] = data[i];
              break;

          case"changeInfoRequests":
              this.changeInfoRequest[cntr] = data[i];
              break;
              
        }
        
        cntr++;
      }
    }  
  }

}
