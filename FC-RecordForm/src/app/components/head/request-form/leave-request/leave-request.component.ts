import { Component, OnInit } from '@angular/core';
import { RequestService } from 'app/Services/request.service';
import { FilterService } from 'app/Services/common/filter.service'
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import * as $ from 'jquery';
import { EnumsService } from 'app/Services/common/enums.service';
import { CustomValidator } from 'app/models/validators/custom.validator';

@Component({
  selector: 'app-leave-request',
  templateUrl: './leave-request.component.html',
  styleUrls: ['./leave-request.component.css']
})
export class LeaveRequestRFComponent implements OnInit {
  
  title = "Leave Request"
  myDate = new Date();
  successMessage : any;
  data: any ;
  leavetypes : any =[];
  datePeriodEnum : any =[];
  leaveReqData : any =[];
  leaveResetDateYr = new Date().getFullYear()

  remainingLeave : any= [
    {name: 'VL', value: 7},
    {name: 'SL', value: 7},
  ]
  filter = this.fb.group({
    OrderBy: 'Id',
    OrderType: ['Descending'],
    ShowAll: true,
    // Status : null
  })
  approverList: any = [
    {
      name:'Approve- Mgmt',
      data :[]
    },
    { 
      name:'',
      data:[]
    }
  ];

  requestLeaveForm = this.fb.group({
    empId:['', Validators.required],
    fullName: [''],
    firstName:[''],
    lastName:['', Validators.required],
    dateCreated: this.myDate,
    branch:  ['', Validators.required],
    department:[''],

    branch_Dept:[''],
    biometricId:  ['', Validators.required],
    contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
    emailAddress:  ['',Validators.compose([Validators.required,CustomValidator.emailAddress])],

    leaveType:  ['', Validators.required],
    period:  ['', Validators.required],
    dateFrom:  ['', Validators.required],
    dateTo:  ['', Validators.required],
    reason:  ['', Validators.required],
    comment: [''],
    status: [''],

    approver: ['']
    

  });


  constructor(private _requestService : RequestService, private _filterService : FilterService, private _enums : EnumsService, private fb: FormBuilder) { 

    this.load()
  }
    

  onSubmit(e){

    let aprrv =this.requestLeaveForm.value.approver
    let Approver =null+ " , "+aprrv
        
  
    this.requestLeaveForm.value.approver = Approver
    let formData = this.requestLeaveForm.value;
    console.log(formData);

    this._requestService.action = "LeaveRequests"
    this._requestService.newRecord(formData)
    .subscribe(successCode => {
      this.successMessage = "Successfully Created"
      console.log(successCode)
      this.requestLeaveForm.reset();
      
      
    })
  }

  onChange(){
    this.requestLeaveForm.get('contactNo').value
    .subscribe(val => {
      console.log(val)
      if (val != null) {    
        this.requestLeaveForm.get('contactNo').disable();
    }
    })
  }

  private load(): void {

		this._enums.getCommonList("leavetypes",true).subscribe(ddl => this.leavetypes = ddl);  
    this._enums.getCommonList("LeaveReqDatePeriodEnum",true).subscribe(ddl => this.datePeriodEnum = ddl);  
  }
  getRemainingLeave(empId){

    let criteria = this.filter.value

    this._filterService.action= "LeaveRequests/Filter"
    this._filterService.GetfilterData(criteria).subscribe(data=> {  
      let cntr =0;
      for(let i =0; i < data.list.length; i ++){
        if(empId == data.list[i].empId && data.list[i].dateCreated > this.leaveResetDateYr +'-01-01'){
          this.leaveReqData[cntr]  = data.list[i]
          if(this.leaveReqData[cntr].leaveType == 'Vacation Leave' && this.leaveReqData[cntr].status == 'Approved'){
            let date1 = new Date(this.leaveReqData[cntr].dateFrom);
            let date2 = new Date(this.leaveReqData[cntr].dateTo);

            const diffTime = Math.abs(Number(date2) - Number(date1));
            const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)) + 1; 

            this.leaveReqData[cntr].period != 1 ?  this.remainingLeave[0].value -= .5:
            this.remainingLeave[0].value -= diffDays;

            console.log( this.leaveReqData[cntr].period != 1 ?.5 + " VL days": diffDays + " VL days")
          }
          else if(this.leaveReqData[cntr].leaveType == 'Sick Leave' && this.leaveReqData[cntr].status == 'Approved'){
            let date1 = new Date(this.leaveReqData[cntr].dateFrom);
            let date2 = new Date(this.leaveReqData[cntr].dateTo);

            const diffTime = Math.abs(Number(date2) - Number(date1));
            const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)) + 1; 

            this.leaveReqData[cntr].period != 1 ?  this.remainingLeave[1].value -= .5:
            this.remainingLeave[1].value -= diffDays;

            
            console.log( this.leaveReqData[cntr].period != 1 ?.5 + " SL days": diffDays + " SL days")
          }
          cntr++;
        }
      }
      console.log("LeaveRequests: ",  this.leaveReqData, this.remainingLeave, data.list[0].dateCreated >  this.leaveResetDateYr +'-12-20') 
    })
  }
  remainingLeaveValidator(e){
    console.log(e)
    if(e.leaveType == 'Vacation Leave' && this.remainingLeave[0].value <= 0){
      return true
    }
    else if(e.leaveType == 'Sick Leave' && this.remainingLeave[1].value <= 0){
      return true
    }
    else{
      return false
    }
  }
  ngOnInit() {
    //Extract Data on LocalStorage
  
    let id = JSON.parse(localStorage.getItem("currentUser")).empId

    this. getRemainingLeave(id);
    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id)
        .subscribe(data =>  {
            console.log("datos! ",this.data = data)
        
            this.requestLeaveForm.patchValue ({
              
              empId: this.data["id"],
              fullName: this.data["fname"] +" "+ this.data["lname"],
              firstName: this.data["fname"],
              lastName:this.data["lname"],
              branch:  this.data["branchName"],
              department: this.data["departmentName"],

              branch_Dept: this.data["branch_Dept"],
              biometricId:  this.data["bio"],
              contactNo:  this.data["telno"],
              emailAddress:  this.data["emaladd"],
              status: "Pending"
            });

            //enable input when no value
            // this.requestLeaveForm.get('contactNo').enable();
            // this.requestLeaveForm.get('emailAddress').enable();

            // if(this.requestLeaveForm.controls['contactNo'].value != ''){
            //   this.requestLeaveForm.get('contactNo').disable();
            // }
            // if(this.requestLeaveForm.controls['emailAddress'].value != ''){
            //   this.requestLeaveForm.get('emailAddress').disable();
            // }
        });  

        this.getApprover();
  }
  getApprover(){

    this._requestService.action="tbusers"
    this._requestService.getRequest().subscribe(data =>{
       let cntr = 0, cntr1 =0
       for( let key in data.sort((a, b) => (a.fullName > b.fullName) ? 1 : -1)){
         if(data[key].access.toLowerCase() == "head"){
          // this.approverList[0].data[cntr] = (data[key])  
          // cntr++
         }    
         else if(data[key].access.toLowerCase() == "mgmt"){
          this.approverList[0].data[cntr1] = (data[key])  
          cntr1++
         }  
       }
       console.log(this.approverList);
      })
  }

}
