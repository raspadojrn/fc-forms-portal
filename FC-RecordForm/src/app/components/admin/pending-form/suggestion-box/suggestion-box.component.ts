import { Component, OnInit } from '@angular/core';
import { RequestService } from 'app/Services/request.service';

@Component({
  selector: 'app-suggestion-box',
  templateUrl: './suggestion-box.component.html',
  styleUrls: ['./suggestion-box.component.css']
})
export class SuggestionBoxComponent implements OnInit {

  suggestionBox : any =[];

  constructor(private _requestService : RequestService,) { }

  ngOnInit() {
    this.getData();
  }

  getData(){

    this._requestService.action = "SuggestionBoxes"
    this._requestService.getRequest().subscribe(data=> {console.log("SuggestionBoxes: ", this.suggestionBox =data)   
    
    })
  }
}
