import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { RequestService } from 'app/Services/request.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';

@Component({
  selector: 'app-approve-request-ir',
  templateUrl: './approve-request-ir.component.html',
  styleUrls: ['./approve-request-ir.component.css']
})
export class ApproveRequestIRComponent implements OnInit {


  id : any;
  actStatus: any;
  currentStatus: any;
  tabIndex: any;

  myDate = new Date();
  leaveReqData: any ;
  approvedBy: any;
  successMessage: any;
  
  incidentReportForm = this.fb.group({
    id: [''],
    empId:['', Validators.required],
    firstName:[''],
    lastName:['', Validators.required],
    dateCreated: this.myDate,
    branch:  ['', Validators.required],
    department:['', Validators.required],
    contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
    emailAddress:  ['', Validators.required],

    personReporting: ['', Validators.required],
    supervisor:  ['', Validators.required],
    incidentLocation:  ['', Validators.required],

    dateOfEvent:  ['', Validators.required],
    timeOfEvent:  ['', Validators.required],
    witness: [''],
    descriptionOfHappening: ['', Validators.required],

    status: [''],
    approveRemarks:  [''],
    acceptBy:  [''],
    approvedBy:  [''],
    dateApproved: [''],

    approver: ['']
  });

  constructor(private _requestService : RequestService, private fb: FormBuilder, private dialog: MatDialog) { }



  approveRequest(){
    
    this.tabIndex == 0 ? this.incidentReportForm.value['approvedBy'] =this.approvedBy :
                         this.incidentReportForm.value['approvedBy'] =this.approvedBy 
    this.incidentReportForm.value['dateApproved'] =this.myDate
    let formData = this.incidentReportForm.value;
    console.log(this.leaveReqData.id ,formData);
    
    this._requestService.action = "IncidentReports"
    this._requestService.updateRecord(this.leaveReqData.id ,formData).subscribe( res=> console.log(this.successMessage= 'Succesfully Updated'))
    
  }

  cancelRequest(){
    
    this.tabIndex == 0 ? this.incidentReportForm.value['approvedBy'] =this.approvedBy :
                         this.incidentReportForm.value['approvedBy'] =this.approvedBy 
    this.incidentReportForm.value["status"] = "Cancelled"
    this.incidentReportForm.value['dateApproved'] =this.myDate
    let formData = this.incidentReportForm.value;
    console.log(this.leaveReqData.id ,formData);
    
    this._requestService.action = "IncidentReports"
    this._requestService.updateRecord(this.leaveReqData.id ,formData).subscribe( res=> console.log(this.successMessage= 'Succesfully Updated'))
  }
  //Modal Close
  onClose(){
  this.dialog.closeAll()
  }

  ngOnInit() {
    console.log(this.id)
    this._requestService.action = "IncidentReports/"
    this._requestService.getRequestById(this.id).subscribe(data=> {console.log(this.leaveReqData = data)
      
    
      this.incidentReportForm.patchValue ({
        id: data["id"],   
        empId: data["empId"],
        dateCreated: data["dateCreated"],
        firstName: data["firstName"],
        lastName: data["lastName"],
        branch:  data["branch"],
        department: data["department"],
        contactNo:  data["contactNo"],
        emailAddress:  data["emailAddress"],

        personReporting: data["personReporting"],
        supervisor:  data["supervisor"],
        incidentLocation: data["incidentLocation"],

        dateOfEvent:  data["dateOfEvent"],
        timeOfEvent:  data["timeOfEvent"],
        witness:  data["witness"],
        descriptionOfHappening:  data["descriptionOfHappening"],


        // status: this.actStatus == "Accept" ?  "Accept" : "Approved" ,
        status: "Approved",
        approveRemarks:  data["approveRemarks"] == null ? data["approveRemarks"] : data["approveRemarks"]  +";\n",
        acceptBy:  data['acceptBy'],
        approvedBy:  data["approvedBy"],

        approver: data['approver']
      });
    
    
    
    })

    let id = JSON.parse(localStorage.getItem("currentUser")).empId
    
    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id).subscribe(data => this.approvedBy = data.fname +' '+ data.lname);
  }

}
