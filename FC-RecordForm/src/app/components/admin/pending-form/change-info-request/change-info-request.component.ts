import { Component, OnInit } from '@angular/core';
import { RequestService } from 'app/Services/request.service';
import { FilterService } from 'app/Services/common/filter.service'
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { ApproveRequestCIComponent } from './approve-request/approve-request.component';
import { EnumsService } from 'app/Services/common/enums.service';
import { IfObservable } from 'rxjs/observable/IfObservable';

@Component({
  selector: 'app-change-info-request',
  templateUrl: './change-info-request.component.html',
  styleUrls: ['./change-info-request.component.css']
})
export class ChangeInfoRequestComponent implements OnInit {

  title = "Change Information"
  myDate = new Date();
  tabIndex : any = 0;
  changeInfoReqData: any =[];

  branch : any = [];
  department : any = [];
  position : any = [];
  company : any = [];

  status: any={ 
    tab0 : "Pending",
    tab1 : "Accept",};
    
  filter = this.fb.group({
    OrderBy: 'Id',
    OrderType: ['Descending'],
    ShowAll: true,
    Status : null
  })
  statusList: string[] = ['Pending',  'Approved', 'Cancelled'];
  constructor(private _requestService : RequestService, private _filterService : FilterService,
              private fb: FormBuilder, private dialog: MatDialog, private _enums : EnumsService) {
  this.load()
  }

private load(): void {
  this._requestService.action ="Tblbranches"
  this._requestService.getRequest().subscribe(data => this.branch = data.sort((a, b) => (a.branch > b.branch) ? 1 : -1));

  this._requestService.action ="Tbldepartments"
  this._requestService.getRequest().subscribe(data => this.department = data.sort((a, b) => (a.department > b.department) ? 1 : -1));

  this._requestService.action ="Tblcompanies"
  this._requestService.getRequest().subscribe(data => this.company = data.sort((a, b) => (a.company > b.company) ? 1 : -1));

  this._requestService.action ="Tblpositions"
  this._requestService.getRequest().subscribe(data => this.position = data.sort((a, b) => (a.position > b.position) ? 1 : -1));
  // this._enums.getCommonList("branch",true).subscribe(ddl => this.branch=ddl.sort((a, b) => (a.name > b.name) ? 1 : -1));  
  // this._enums.getCommonList("department",true).subscribe(ddl => this.department=ddl.sort((a, b) => (a.name > b.name) ? 1 : -1));  
  // this._enums.getCommonList("company",true).subscribe(ddl => this.company=ddl.sort((a, b) => (a.name > b.name) ? 1 : -1));  
  // this._enums.getCommonList("position",true).subscribe(ddl => this.position=ddl.sort((a, b) => (a.name > b.name) ? 1 : -1));  
  }

  getFilterStatusTab0(e){
    this.status.tab0 = e.value

      this.filter.value['Status'] =  this.status.tab0 
      this.getData(0)  
      console.log(e)  
  }

  getFilterStatusTab1(e){
    this.status.tab1 = e.value

      this.filter.value['Status'] = this.status.tab1
      this.getData(1)  
      console.log(e.value)  
  }
  openDialog(id: any, act : string, currStatus: any) {
    const dialogRef = this.dialog.open(ApproveRequestCIComponent, {
      height:'auto',
      minHeight: 'calc(100vh - 90px)',
      width: '800px',
      panelClass: 'my-dialog',
    });

    dialogRef.componentInstance.id = id;
    dialogRef.componentInstance.actStatus = act;
    dialogRef.componentInstance.currentStatus = currStatus;
    dialogRef.componentInstance.tabIndex =  this.tabIndex;

    dialogRef.componentInstance.branch = this.branch;
    dialogRef.componentInstance.department = this.department;
    dialogRef.componentInstance.position = this.position;
    dialogRef.componentInstance.company =  this.company;

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
     
      this.getData(0)
      this.getData(1)
    
      
    });
  }
  onTabChanged(e){
    console.log(e.index)
    this.tabIndex = e.index
  }

  
  ngOnInit() {
    this.getData(null)     
  }

  getData(tab : any){

    this._filterService.action= "ChangeInfoRequests/Filter"
    let criteria = this.filter.value

      switch(tab){
        case(0):
              this.filter.value['Status'] =  this.status.tab0
              criteria = this.filter.value
              this._filterService.GetfilterData(criteria).subscribe(data=> this.changeInfoReqData.tab0 =data.list)
              break;
        case(1):
              this.filter.value['Status'] =  this.status.tab1
              criteria = this.filter.value
              this._filterService.GetfilterData(criteria).subscribe(data=> this.changeInfoReqData.tab1=data.list)
              break;
        default:
              this.filter.value['Status'] = "Pending"
              this._filterService.GetfilterData(criteria).subscribe(data=>  this.changeInfoReqData.tab0  =data.list)

              this.filter.value['Status'] = "Accept"
              this._filterService.GetfilterData(criteria).subscribe(data=>this.changeInfoReqData.tab1  =data.list)
              console.log(this.changeInfoReqData)
              break;

      }
  }
}
