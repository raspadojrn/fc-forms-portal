import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { RequestService } from 'app/Services/request.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';

@Component({
  selector: 'app-approve-request',
  templateUrl: './approve-request.component.html',
  styleUrls: ['./approve-request.component.css']
})
export class ApproveRequestCIComponent implements OnInit {


  id : any;
  actStatus: any;
  currentStatus: any;
  tabIndex: any;

  branch : any = [];
  department : any = [];
  position : any = [];
  company : any = [];

  data : any =[];
  myDate = new Date();
  changeInfoReqData: any ;
  approvedBy: any;
  successMessage: any;
  
  EmpInformation : FormGroup;
  changeInfoForm : FormGroup;
 
  constructor(private _requestService : RequestService, private fb: FormBuilder, private dialog: MatDialog) { }



  approveRequest(){
    
    this.tabIndex == 0 ? this.changeInfoForm.value['adminApproval'] =this.approvedBy :
                         this.changeInfoForm.value['adminApproval'] =this.approvedBy 
    this.changeInfoForm.value['dateApprove'] =this.myDate
    let formData = this.changeInfoForm.value;
    console.log(this.changeInfoReqData.id ,formData);
    
    this._requestService.action = "ChangeInfoRequests"
    this._requestService.updateRecord(this.changeInfoReqData.id ,formData).subscribe( res=> {
 
      this.updateEmployeeData(formData)
     
      })
    
  }

  cancelRequest(){
    
    this.tabIndex == 0 ? this.changeInfoForm.value['adminApproval'] =this.approvedBy :
                         this.changeInfoForm.value['adminApproval'] =this.approvedBy 
    this.changeInfoForm.value["status"] = "Cancelled"
    this.changeInfoForm.value['dateApprove'] =this.myDate
    let formData = this.changeInfoForm.value;
    console.log(this.changeInfoReqData.id ,formData);
    
    this._requestService.action = "ChangeInfoRequests"
    this._requestService.updateRecord(this.changeInfoReqData.id ,formData).subscribe( res=> console.log(this.successMessage= 'Succesfully Updated'))
  }
  //Modal Close
  onClose(){
  this.dialog.closeAll()
  }

  ngOnInit() {
    console.log(this.id)
    this.createForms();
    this.getData();

    let id = JSON.parse(localStorage.getItem("currentUser")).empId
    
    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id).subscribe(data => this.approvedBy = data.fname +' '+ data.lname);
  }
  
  updateEmployeeData(data){
    this.EmpInformation.value.Bio = data.newbiometricId
    this.EmpInformation.value.Branch = data.newBranch
    this.EmpInformation.value.Department = data.newDepartment
    this.EmpInformation.value.Position = data.newPosition
    this.EmpInformation.value.Company = data.newCompany

    this.EmpInformation.value.Hiringdate = data.newHiringDate
    this.EmpInformation.value.Datestart = data.newDateStart
    this.EmpInformation.value.Regstatus = data.newEmpStatus
    this.EmpInformation.value.Educattain = data.newEducAttain
    this.EmpInformation.value.Schoolname = data.newSchoolName
    this.EmpInformation.value.Course = data.newCourse
    this.EmpInformation.value.Attendyear = data.newAttendYr
    this.EmpInformation.value.Sssno = data.newSssNo
    this.EmpInformation.value.Pagibigno = data.newPagibigNo
    this.EmpInformation.value.Tinno = data.newTinNo
    this.EmpInformation.value.Philhealthno = data.newPhilHealthNo
    
    let formData = this.EmpInformation.value
    
    this._requestService.action = "Tblprofiles"
    this._requestService.updateRecord(this.changeInfoReqData.empId ,formData).subscribe(res => console.log(this.successMessage= 'Succesfully Updated'))

  }
  getData(){

    let user = JSON.parse(localStorage.getItem("currentUser"))

    this._requestService.action = "ChangeInfoRequests/"
    this._requestService.getRequestById(this.id).subscribe(data=> {console.log(this.changeInfoReqData = data)
      
    
      this.changeInfoForm.patchValue ({
        id: data["id"],   
        empId: data["empId"],
        dateCreated: data["dateCreated"],
        firstName: data["firstName"],
        lastName: data["lastName"],
        contactNo:  data["contactNo"],
        emailAddress:  data["emailAddress"],

        currentBranch:  data["currentBranch"],
        currentDepartment: data["currentDepartment"],
        currentPosition:  data["currentPosition"],
        currentCompany:  data["currentCompany"],
        currentBiometricId: data["currentBiometricId"],
        currentHiringDate : data["currentHiringDate"],
        currentDateStart : data["currentDateStart"],
        currentEmpStatus : data["currentEmpStatus"],
        currentEducAttain : data["currentEducAttain"],
        currentSchoolName :  data["currentSchoolName"],
        currentCourse : data["currentCourse"],
        currentAttendYr : data["currentAttendYr"],
        currentSssNo : data["currentSssNo"],
        currentPagibigNo : data["currentPagibigNo"],
        currentTinNo :  data["currentTinNo"],
        currentPhilHealthNo :  data["currentPhilHealthNo"],


        newBranch: data["newBranch"],
        newDepartment: data["newDepartment"],
        newPosition: data["newPosition"],
        newCompany: data["newCompany"],
        newbiometricId: data["newbiometricId"],
        newHiringDate : data["newHiringDate"],
        newDateStart : data["newDateStart"],
        newEmpStatus : data["newEmpStatus"],
        newEducAttain : data["newEducAttain"],
        newSchoolName :  data["newSchoolName"],
        newCourse : data["newCourse"],
        newAttendYr : data["newAttendYr"],
        newSssNo : data["newSssNo"],
        newPagibigNo : data["newPagibigNo"],
        newTinNo :  data["newTinNo"],
        newPhilHealthNo :  data["newPhilHealthNo"],
    
        reason: data["reason"],

        status: "Approved",
        approveRemarks:  data["approveRemarks"] == null ? data["approveRemarks"] : data["approveRemarks"]  +";\n",
        adminApproval:  data['adminApproval'],
        approver: data['approver']

      });
    console.log(this.changeInfoForm)

    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById( data["empId"]).subscribe(data => {
      this.data = data

      this.EmpInformation.patchValue ({
              
        id: this.data["id"],

        fname: this.data["fname"],
        lname:this.data["lname"],
        mname: this.data["mname"],

        telno:  this.data["telno"],
        emaladd:  this.data["emaladd"],
        bday: this.data["bday"],

        address: this.data["address"],
        cstatus: this.data["cstatus"],
        conperson: this.data["conperson"],
        conpersontelno: this.data["conpersontelno"],
        

        Allowance: data["allowance"],
        Branch: this.data["branch"],
        Department: this.data["department"],
        Attendyear: this.data["attendyear"],
        Bio: this.data["bio"],
        Cola: this.data["cola"],
        Company: this.data["company"],
        Conemergency: this.data["conemergency"],
        Course: this.data["course"],
        Dateeoc: this.data["dateeoc"],
        Datefinalinterview: this.data["datefinalinterview"],
        Dateinitialinterview: this.data["dateinitialinterview"],
        Datereg: this.data["datereg"],
        Datesalrychange: this.data["datesalrychange"],
        Datestart: this.data["datestart"],
        Dependents: this.data["dependents"],
        Educattain: this.data["educattain"],

        Employststus: this.data["employststus"],
        Evaluationdate: this.data["evaluationdate"],
        Feedback: this.data["feedback"],
        Gender: this.data["gender"],
        Hiresource: this.data["hiresource"],
        Hiringdate: this.data["hiringdate"],
        Idemployehistory: this.data["idemployehistory"],
        Idno: this.data["idno"],
        Inviteby: this.data["inviteby"],
        Marks1: this.data["marks1"],
        Marks2: this.data["marks2"],
        Marks3: this.data["marks3"],
        Marks4: this.data["marks4"],
    
        Nationalid: this.data["nationalid"],
        Pagibigno: this.data["pagibigno"],
        Philhealthno: this.data["philhealthno"],
        Position: this.data["position"],
        Regstatus: this.data["regstatus"],
        Salary: this.data["salary"],
        Schoolname: this.data["schoolname"],
        Sssno: this.data["sssno"],
        Tinno: this.data["tinno"],
        Warehouse: this.data["warehouse"],
      });
      
    });
    })
  }

  createForms(){
    this.changeInfoForm = this.fb.group({
      id: [''],
      empId:['', Validators.required],
      fullName: [''],
      firstName:[''],
      lastName:['', Validators.required],
      dateCreated: this.myDate,
      emailAddress:  ['', Validators.required],
  
      currentBranch:  ['', Validators.required],
      currentDepartment:['', Validators.required],
      currentPosition:  ['', Validators.required],
      currentCompany:  ['', Validators.required],
      currentBiometricId: ['', Validators.required],
      currentHiringDate: ['', Validators.required],
      currentDateStart: ['', Validators.required],
      currentEmpStatus: ['', Validators.required],
      currentEducAttain: ['', Validators.required],
      currentSchoolName: ['', Validators.required],
      currentCourse: ['', Validators.required],
      currentAttendYr: ['', Validators.required],
      currentSssNo: ['', Validators.required],
      currentPagibigNo: ['', Validators.required],
      currentTinNo: ['', Validators.required],
      currentPhilHealthNo: ['', Validators.required],

      newBranch:  [''],
      newDepartment:[''],
      newPosition:  [''],
      newCompany:  [''],
      newbiometricId: [''],
      newHiringDate: [''],
      newDateStart: [''],
      newEmpStatus: [''],
      newEducAttain: [''],
      newSchoolName: [''],
      newCourse: [''],
      newAttendYr: [''],
      newSssNo: [''],
      newPagibigNo: [''],
      newTinNo: [''],
      newPhilHealthNo: [''],
  
      reason: ['', Validators.required],
      status: [''],
  
      adminApproval : ['', Validators.required],
      dateApprove: [''],
      approveRemarks: ['', Validators.required],
      approver: [''],
    });

    this.EmpInformation = this.fb.group({
      id:['', Validators.required],
  
      fname:['', Validators.required],
      lname:['', Validators.required],
      mname:['', Validators.required],
  
      telno:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
      emaladd:  ['',Validators.required],
      bday: ['',Validators.required],
  
      address: ['',Validators.required],
      cstatus: ['',Validators.required],
      conperson: ['',Validators.required],
      conpersontelno: ['',Validators.required],
  
      
      Allowance: ['',Validators.required],
      Branch: ['',Validators.required],
      Department: ['',Validators.required],
      Attendyear: [''],
  
      Bio: ['',Validators.required],
      Cola: [''],
      Company: ['',Validators.required],
      Conemergency: [''],
  
      Course: [''],
     
      Dateeoc: [''],
      Datefinalinterview: [''],
      Dateinitialinterview: [''],
      Datereg: [''],
      Datesalrychange: [''],
      Datestart: [''],
      Dependents: [''],
      Educattain: [''],
  
      Employststus: [''],
      Evaluationdate: [''],
      Feedback: [''],
      Gender: ['',Validators.required],
      Hiresource: [''],
      Hiringdate: [''],
      Idemployehistory: [''],
      Idno: ['',Validators.required],
      Inviteby: [''],
      Marks1: [''],
      Marks2: [''],
      Marks3: [''],
      Marks4: [''],
   
      Nationalid: [''],
      Pagibigno: [''],
      Philhealthno: [''],
      Position: ['',Validators.required],
      Regstatus: ['',Validators.required],
      Salary: [''],
      Schoolname: [''],
      Sssno: [''],
      Tinno: [''],
      Warehouse: [''],
  
    });
  }
  isChange(action : any){

    let isChange : boolean;
    switch(action){
      case"Branch":
           this.changeInfoForm.value.currentBranch != this.changeInfoForm.value.newBranch? 
           isChange = true : isChange = false
           return isChange
      case"Department":
          this.changeInfoForm.value.currentDepartment != this.changeInfoForm.value.newDepartment ? 
          isChange = true : isChange = false
          return isChange
      case"Position":
          this.changeInfoForm.value.currentPosition != this.changeInfoForm.value.newPosition ? 
          isChange = true : isChange = false
          return isChange
      case"Company":
          this.changeInfoForm.value.currentCompany != this.changeInfoForm.value.newCompany ? 
          isChange = true : isChange = false
          return isChange
      case"Bio":
          this.changeInfoForm.value.currentBiometricId != this.changeInfoForm.value.newbiometricId ? 
          isChange = true : isChange = false
          return isChange

      case"HiringDate":
          this.changeInfoForm.value.currentHiringDate != this.changeInfoForm.value.newHiringDate ? 
          isChange = true : isChange = false
          return isChange

      case"DateStart":
          this.changeInfoForm.value.currentDateStart != this.changeInfoForm.value.newDateStart ? 
          isChange = true : isChange = false
          return isChange

      case"EmpStatus":
          this.changeInfoForm.value.currentEmpStatus != this.changeInfoForm.value.newEmpStatus ? 
          isChange = true : isChange = false
          return isChange

     case"EducAttain":
          this.changeInfoForm.value.currentEducAttain != this.changeInfoForm.value.newEducAttain ? 
          isChange = true : isChange = false
          return isChange
     case"SchoolName":
          this.changeInfoForm.value.currentSchoolName != this.changeInfoForm.value.newSchoolName ? 
          isChange = true : isChange = false
          return isChange
      case"Course":
          this.changeInfoForm.value.currentCourse != this.changeInfoForm.value.newCourse ? 
          isChange = true : isChange = false
          return isChange
      case"AttendYr":
          this.changeInfoForm.value.currentAttendYr != this.changeInfoForm.value.newAttendYr ? 
          isChange = true : isChange = false
          return isChange
      case"SssNo":
          this.changeInfoForm.value.currentSssNo != this.changeInfoForm.value.newSssNo ? 
          isChange = true : isChange = false
          return isChange
     case"PagibigNo":
          this.changeInfoForm.value.currentPagibigNo != this.changeInfoForm.value.newPagibigNo ? 
          isChange = true : isChange = false
          return isChange
      case"TinNo":
          this.changeInfoForm.value.currentTinNo != this.changeInfoForm.value.newTinNo ? 
          isChange = true : isChange = false
          return isChange
      case"PhilHealthNo":
          this.changeInfoForm.value.currentPhilHealthNo != this.changeInfoForm.value.newPhilHealthNo ? 
          isChange = true : isChange = false
          return isChange
    }
  }

}