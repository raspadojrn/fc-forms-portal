import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { RequestService } from 'app/Services/request.service';

@Component({
  selector: 'app-approve-request',
  templateUrl: './approve-request.component.html',
  styleUrls: ['./approve-request.component.css']
})
export class ApproveRequestURComponent implements OnInit {

  id : any;
  actStatus : any;
  currentStatus : any;
  tabIndex: any;
  selectedDAta : any;


  myDate = new Date();
  uniformRequestData: any ;
  approvedBy: any;
  successMessage: any;
  orderedItems : any =[]
  approvedQuantity : any = [];

  urOrder = this.fb.group({})
  uniformRequestForm = this.fb.group({

    id:[''],
    empId:['', Validators.required],
    fullName: [''],
    firstName:[''],
    lastName:['', Validators.required],
    dateCreated: this.myDate,
    branch:  ['', Validators.required],
    department:[''],

    biometricId:  ['', Validators.required],
    contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
    emailAddress:  ['', Validators.required],
    position:  ['', Validators.required],

    employeeStatus:  ['', Validators.required],
    replacementOrAdditional:  ['', Validators.required],
    gender : ['', Validators.required],

    orderItems : this.fb.array([])	,	
    purpose:  ['', Validators.required],
    comment: [''],
    status: [''],

    adminApproval : ['', Validators.required],
    dateApprove: [''],
    approveRemarks: ['', Validators.required],
    approver: [''],
  });
  constructor(private _requestService : RequestService, private fb: FormBuilder, private dialog: MatDialog) { }



  approveRequest(){
 
   this.tabIndex == 0 ? this.uniformRequestForm.value['adminApproval'] =this.approvedBy :
                        this.uniformRequestForm.value['adminApproval'] =this.approvedBy 
    this.uniformRequestForm.value['dateApprove'] =this.myDate
    let formData = this.uniformRequestForm.value;
    console.log(this.uniformRequestData.id ,formData);
    
    this._requestService.action = "UniformRequests"
    this._requestService.updateRecord(this.uniformRequestData.id ,formData).subscribe( res=> {
      // console.log(this.successMessage= 'Succesfully Updated' )

      this.updateOrderedItems(formData.status)
    })
    
  }

  cancelRequest(){
    
    this.tabIndex == 0 ? this.uniformRequestForm.value['adminApproval'] =this.approvedBy :
                         this.uniformRequestForm.value['adminApproval'] =this.approvedBy 
    this.uniformRequestForm.value["status"] = "Cancelled"
    this.uniformRequestForm.value['dateApprove'] =this.myDate
    let formData = this.uniformRequestForm.value;
    console.log(this.uniformRequestData.id ,formData);
    
    this._requestService.action = "UniformRequests"
    this._requestService.updateRecord(this.uniformRequestData.id ,formData).subscribe( res=> {
      // console.log(this.successMessage= 'Succesfully Updated')

      this.updateOrderedItems(formData.status)
    })
  }
  //Modal Close
  onClose(){
    console.log(this.tabIndex)
  this.dialog.closeAll()
  }

  updateOrderedItems(status : any){
    let formData =this.uniformRequestForm.controls.orderItems.value
    console.log(status)
    
    this._requestService.action = "UniformOrders"
    for(let i =0; i< formData.length; i++){
      formData[i].approvedQuantity = this.approvedQuantity[i]
      this.urOrder = formData
      console.log(formData[i].status = status,formData[i].id)
      this._requestService.updateRecord(formData[i].id ,formData[i]).subscribe( res=> console.log(this.successMessage= 'Succesfully Updated'))
    }
  }

  get formArr() {
    return this.uniformRequestForm.get('orderItems') as FormArray;
  }

  getApprovedQty(){
    for(let i = 0; i < this.approvedQuantity.length; i++)
    {
      this.urOrder.value[i] = this.approvedQuantity[i]
    }
  }
  validators(){
    for(let i = 0; i < this.approvedQuantity.length; i++)
    {
      if(this.approvedQuantity[i] == null){
        return true
      }
    }
    if(this.approvedQuantity.length != this.selectedDAta.orderedItems.length){
      return true
    }
    else{
      return false
    }
  }
  
  ngOnInit() {
    this.updateOrderedItems(null)

    console.log("id: ",this.id, this.selectedDAta)
    let data = this.selectedDAta
    this.uniformRequestData = data
    
      this.uniformRequestForm.patchValue ({
        id: data["id"],   
        empId: data["empId"],
        dateCreated: data["dateCreated"],
        firstName: data["firstName"],
        lastName: data["lastName"],
        branch:  data["branch"],
        department: data["department"],
        biometricId:  data["biometricId"],
        contactNo:  data["contactNo"],
        emailAddress:  data["emailAddress"],
        position: data["position"],
        
        employeeStatus: data["employeeStatus"],
        replacementOrAdditional:  data["replacementOrAdditional"],
        gender : data["gender"],
        
        purpose:  data["purpose"],
        comment: data["comment"],

        status: "Approved",
        approveRemarks:  data["approveRemarks"] == null ? data["approveRemarks"] : data["approveRemarks"]  +";\n",
        adminApproval:  data['adminApproval'],
        approver: data['approver']
      })
      
      //set value to form array OrderedItems
      data.orderedItems.forEach((x) => {
        this.formArr.push(this.fb.group(x))
        this.approvedQuantity.push(x.quantity)
      });
      console.log(this.uniformRequestForm)

      //get approver name
    let id = JSON.parse(localStorage.getItem("currentUser")).empId 
    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id).subscribe(data => this.approvedBy = data.fname +' '+ data.lname);
  }

}
