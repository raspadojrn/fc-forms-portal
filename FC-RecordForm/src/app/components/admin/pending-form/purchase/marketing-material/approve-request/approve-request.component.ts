import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { RequestService } from 'app/Services/request.service';
import { CustomValidator } from 'app/models/validators/custom.validator';

@Component({
  selector: 'app-approve-request',
  templateUrl: './approve-request.component.html',
  styleUrls: ['./approve-request.component.css']
})
export class ApproveRequestMMComponent implements OnInit {

  id : any;
  actStatus : any;
  currentStatus : any;
  tabIndex: any;
  selectedDAta : any;


  myDate = new Date();
  marketingData: any ;
  approvedBy: any;
  successMessage: any;
  orderedItems : any =[]
  approvedQuantity : any = [];

  mmOrder = this.fb.group({})
  marketingMaterialsForm = this.fb.group({
    id:[''],
    empId:['', Validators.required],
    fullName: [''],
    firstName:[''],
    lastName:['', Validators.required],
    dateCreated: this.myDate,
    branch:  ['', Validators.required],
    department:[''],

    branch_Dept:[''],
    position:  ['', Validators.required],
    company:  ['', Validators.required],
    contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
    emailAddress:  ['',Validators.compose([Validators.required,CustomValidator.emailAddress])],
    
    imageName: [''],
    requestType: ['', Validators.required],
    itemDescription: ['', Validators.required],
    dateNeeded: ['', Validators.required],

    orderItems : this.fb.array([])	,	

    comment: [''],
    status: [''],

    adminApproval : ['', Validators.required],
    dateApprove: [''],
    approveRemarks: ['', Validators.required],
    approver: [''],


  });
  constructor(private _requestService : RequestService, private fb: FormBuilder, private dialog: MatDialog) { }


  approveRequest(){
 
   this.tabIndex == 0 ? this.marketingMaterialsForm.value['adminApproval'] =this.approvedBy :
                        this.marketingMaterialsForm.value['adminApproval'] =this.approvedBy 
    this.marketingMaterialsForm.value['dateApprove'] =this.myDate
    let formData = this.marketingMaterialsForm.value;
    console.log(this.marketingData.id ,formData);
    
    this._requestService.action = "MarketingMaterialRequests"
    this._requestService.updateRecord(this.marketingData.id ,formData).subscribe( res=> {

      if(this.selectedDAta.orderedItems.length > 0){
        this.updateOrderedItems(formData.status)
      }
      else{
        console.log(this.successMessage= 'Succesfully Updated' )
      }
    })
    
  }

  cancelRequest(){
    
    this.tabIndex == 0 ? this.marketingMaterialsForm.value['adminApproval'] =this.approvedBy :
                         this.marketingMaterialsForm.value['adminApproval'] =this.approvedBy 
    this.marketingMaterialsForm.value["status"] = "Cancelled"
    this.marketingMaterialsForm.value['dateApprove'] =this.myDate
    let formData = this.marketingMaterialsForm.value;
    console.log(this.marketingData.id ,formData);
    
    this._requestService.action = "MarketingMaterialRequests"
    this._requestService.updateRecord(this.marketingData.id ,formData).subscribe( res=> {
      
      if(this.selectedDAta.orderedItems.length > 0){
        this.updateOrderedItems(formData.status)
      }
      else{
        console.log(this.successMessage= 'Succesfully Updated' )
      }
    })
  }
  //Modal Close
  onClose(){
    console.log(this.tabIndex)
  this.dialog.closeAll()
  }

  
  updateOrderedItems(status : any){
    let formData =this.marketingMaterialsForm.controls.orderItems.value
    console.log(status)
    
    this._requestService.action = "MarketingOrders"
    for(let i =0; i< formData.length; i++){
      formData[i].approvedQuantity = this.approvedQuantity[i]
      this.mmOrder = formData
      console.log(formData[i].status = status,formData[i].id)
      this._requestService.updateRecord(formData[i].id ,formData[i]).subscribe( res=> console.log(this.successMessage= 'Succesfully Updated'))
    }
  }

  get formArr() {
    return this.marketingMaterialsForm.get('orderItems') as FormArray;
  }

  getApprovedQty(){
    for(let i = 0; i < this.approvedQuantity.length; i++)
    {
      this.mmOrder.value[i] = this.approvedQuantity[i]
    }
  }
  validators(){
    for(let i = 0; i < this.approvedQuantity.length; i++)
    {
      if(this.approvedQuantity[i] == null){
        return true
      }
    }
    if(this.approvedQuantity.length != this.selectedDAta.orderedItems.length){
      return true
    }
    else{
      return false
    }
  }
  
  ngOnInit() {
    // this.updateOrderedItems(null)

    console.log("id: ",this.id, this.selectedDAta)
    let data = this.selectedDAta
    this.marketingData = data
    
      this.marketingMaterialsForm.patchValue ({

        id: data["id"],   
        empId: data["empId"],
        dateCreated: data["dateCreated"],
        firstName: data["firstName"],
        lastName: data["lastName"],
        branch:  data["branch"],
        department: data["department"],
        biometricId:  data["biometricId"],
        contactNo:  data["contactNo"],
        emailAddress:  data["emailAddress"],

        position:  data["position"],
        company:  data["company"],

        imageName: data["imageName"],
        requestType: data["requestType"],
        itemDescription: data["itemDescription"],
        dateNeeded : data["dateNeeded"],
   
        comment: data["comment"],

        status: "Approved",
        approveRemarks:  data["approveRemarks"] == null ? data["approveRemarks"] : data["approveRemarks"]  +";\n",
        adminApproval:  data['adminApproval'],
        approver: data['approver']
      })
      //set value to form array OrderedItems
      data.orderedItems.forEach((x) => {
        this.formArr.push(this.fb.group(x))
        this.approvedQuantity.push(x.quantity)
      });
      console.log(this.marketingMaterialsForm, this.approvedQuantity)

      //get approver name
    let id = JSON.parse(localStorage.getItem("currentUser")).empId 
    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id).subscribe(data => this.approvedBy = data.fname +' '+ data.lname);
  }

  closeImgModal(){
    var modal = document.getElementById("myModal");
    modal.style.display = "none";
  }
  imgModal(){
    var modal = document.getElementById("myModal");
    let img : any = document.getElementById("myImg");
    document.getElementById('image').setAttribute( 'src', img.src )
    modal.style.display = "block";
  }
}
