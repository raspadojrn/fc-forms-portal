import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { RequestService } from 'app/Services/request.service';
import { FilterService } from 'app/Services/common/filter.service'
import { ApproveRequestComponent } from './approve-request/approve-request.component';


@Component({
  selector: 'app-leave-request',
  templateUrl: './leave-request.component.html',
  styleUrls: ['./leave-request.component.css']
})
export class LeaveRequestComponent implements OnInit {
  
  title = "Leave Request"
  myDate = new Date();
  tabIndex : any = 0;
  leaveReqData: any =[];
  status: any={ 
    tab0 : "Pending",
    tab1 : "Accept",};
 
  isDateFilter : boolean=false ;
  filter = this.fb.group({
    DateFrom: [''],
    DateTo: [''],
    OrderBy: 'Id',
    OrderType: ['Descending'],
    ShowAll: true,
    Status : null
  })
  statusList: string[] = ['Pending',  'Approved', 'Cancelled'];
  constructor(private _requestService : RequestService, private _filterService : FilterService,
              private fb: FormBuilder, private dialog: MatDialog) { }
    
  getFilterStatusTab0(e, filterType){

    console.log(filterType =='date'? this.isDateFilter = true : this.isDateFilter = false)
    this.status.tab0 = e.value

      this.filter.value['Status'] =  this.status.tab0 
      this.getData(0)  
      console.log(e)  
  }
  resetFilter(){
    this.filter.value['DateFrom']= null
    this.filter.value['DateTo']= null
    this.filter.value['OrderType']= "Descending"
    this.getData(0)  
    console.log("click",this.filter.value)
  }
  sortByDate(e){
    e == "Descending" ? this.filter.value.OrderType = "Ascending" : 
                        this.filter.value.OrderType = "Descending"
    console.log(e, this.filter.value.OrderType)
    this.getData(0)  

  }

  getFilterStatusTab1(e){
 
    this.status.tab1 = e.value

      this.filter.value['Status'] = this.status.tab1
      this.getData(1)  
      console.log(e.value)  
  
    
  }

  openDialog(id: any, act : any, currStatus : any) {
    const dialogRef = this.dialog.open(ApproveRequestComponent, {
      // height:'1000px',
      width: '800px',
      panelClass: 'my-dialog',
    });

    dialogRef.componentInstance.id = id;
    dialogRef.componentInstance.actStatus = act;
    dialogRef.componentInstance.currentStatus = currStatus;
    dialogRef.componentInstance.tabIndex =  this.tabIndex;

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
     
      this.getData(0)
      this.getData(1)
    
      
    });
  }
  onTabChanged(e){
    console.log(e.index)
    this.tabIndex = e.index
  }
  
  
  ngOnInit() {
    this.getData(null)     
  }

  getData(tab : any){

    this._filterService.action= "LeaveRequests/Filter"
    let criteria = this.filter.value

      switch(tab){
        case(0):
              this.filter.value['Status'] =  this.status.tab0
              criteria = this.filter.value
              this._filterService.GetfilterData(criteria).subscribe(data=> this.leaveReqData.tab0 =data.list)
              break;
        case(1):
              this.filter.value['Status'] =  this.status.tab1
              criteria = this.filter.value
              this._filterService.GetfilterData(criteria).subscribe(data=> this.leaveReqData.tab1=data.list)
              break;
        default:
              this.filter.value['Status'] = "Pending"
              this._filterService.GetfilterData(criteria).subscribe(data=>  this.leaveReqData.tab0  =data.list)

              this.filter.value['Status'] = "Accept"
              this._filterService.GetfilterData(criteria).subscribe(data=>this.leaveReqData.tab1  =data.list)
              console.log(this.leaveReqData)
              

      }

  }
}