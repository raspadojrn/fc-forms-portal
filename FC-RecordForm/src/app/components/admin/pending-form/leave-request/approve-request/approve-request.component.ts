import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { RequestService } from 'app/Services/request.service';
import { EnumsService } from 'app/Services/common/enums.service';

@Component({
  selector: 'app-approve-request',
  templateUrl: './approve-request.component.html',
  styleUrls: ['./approve-request.component.css']
})
export class ApproveRequestComponent implements OnInit {

  id : any;
  actStatus : any;
  currentStatus : any;
  tabIndex: any;

  datePeriod :any =[];
  myDate = new Date();
  leaveReqData: any ;
  approvedBy: any;
  successMessage: any;
  requestLeaveForm = this.fb.group({
    id:[''],
    empId:['', Validators.required],
    firstName:[''],
    lastName:['', Validators.required],
    dateCreated: [''],
    branch:  ['', Validators.required],
    department:['', Validators.required],
    biometricId:  ['', Validators.required],
    contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
    emailAddress:  ['', Validators.required],

    leaveType:  ['', Validators.required],
    period:  ['', Validators.required],
    dateFrom:  ['', Validators.required],
    dateTo:  ['', Validators.required],
    reason:  ['', Validators.required],
    comment: [''],

    status: [''],
    approveRemarks:  [''],
    acceptBy:  [''],
    approvedBy:  [''],
    dateApproved: [''],

    approver: ['']
  });
  constructor(private _requestService : RequestService, private fb: FormBuilder, private dialog: MatDialog,
              private _enums : EnumsService,) {this.load() }

  private load(): void {
    this._enums.getCommonList("LeaveReqDatePeriodEnum",true).subscribe(ddl => console.log(this.datePeriod=ddl));  
  }


  approveRequest(){
 
   this.tabIndex == 0 ? this.requestLeaveForm.value['approvedBy'] =this.approvedBy :
                        this.requestLeaveForm.value['approvedBy'] =this.approvedBy 
    this.requestLeaveForm.value['dateApproved'] =this.myDate
    let formData = this.requestLeaveForm.value;
    console.log(this.leaveReqData.id ,formData);
    
    this._requestService.action = "LeaveRequests"
    this._requestService.updateRecord(this.leaveReqData.id ,formData).subscribe( res=> console.log(this.successMessage= 'Succesfully Updated'))
    
  }

  cancelRequest(){
    
    this.tabIndex == 0 ? this.requestLeaveForm.value['approvedBy'] =this.approvedBy :
                         this.requestLeaveForm.value['approvedBy'] =this.approvedBy 
    this.requestLeaveForm.value["status"] = "Cancelled"
    this.requestLeaveForm.value['dateApproved'] =this.myDate
    let formData = this.requestLeaveForm.value;
    console.log(this.leaveReqData.id ,formData);
    
    this._requestService.action = "LeaveRequests"
    this._requestService.updateRecord(this.leaveReqData.id ,formData).subscribe( res=> console.log(this.successMessage= 'Succesfully Updated'))
  }
  //Modal Close
  onClose(){
    console.log(this.tabIndex)
  this.dialog.closeAll()
  }

  ngOnInit() {
    console.log("id: ",this.id)
    this._requestService.action = "LeaveRequests/"
    this._requestService.getRequestById(this.id).subscribe(data=> {console.log(this.leaveReqData = data)
    
    
      this.requestLeaveForm.patchValue ({
        id: data["id"],   
        empId: data["empId"],
        dateCreated: data["dateCreated"],
        firstName: data["firstName"],
        lastName: data["lastName"],
        branch:  data["branch"],
        department: data["department"],
        biometricId:  data["biometricId"],
        contactNo:  data["contactNo"],
        emailAddress:  data["emailAddress"],

        leaveType: data["leaveType"],
        period:  data["period"],
        dateFrom: data["dateFrom"],
        dateTo:  data["dateTo"],
        reason:  data["reason"],
        comment:  data["comment"],


        // status: this.actStatus == "Accept" ?  "Accept" : "Approved" ,
        status: "Approved",
        approveRemarks:  data["approveRemarks"] == null ? data["approveRemarks"] : data["approveRemarks"]  +";\n",
        acceptBy:  data['acceptBy'],
        approvedBy:  data["approvedBy"],

        approver: data['approver']
      });
    
    
    
    })

    let id = JSON.parse(localStorage.getItem("currentUser")).empId
    
    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id).subscribe(data => this.approvedBy = data.fname +' '+ data.lname);
  }

}
