import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { RequestService } from 'app/Services/request.service';
import * as $ from 'jquery';
import { CustomValidator } from 'app/models/validators/custom.validator';

@Component({
  selector: 'app-official-business',
  templateUrl: './official-business.component.html',
  styleUrls: ['./official-business.component.css']
})
export class OfficialBusinessRFComponent implements OnInit {

  title: any ="Official Business";
  data: any ;
  myDate = new Date();
  successMessage: any;
  approverList: any = [
    {
      name:'Approver- Head',
      data :[]
    },
    { 
      name:'',
      data:[]
    }
  ];

  officialBusinessForm = this.fb.group({
    empId:['', Validators.required],
    fullName: [''],
    firstName:[''],
    lastName:['', Validators.required],
    dateCreated: this.myDate,
    branch:  ['', Validators.required],
    department:[''],
    branch_Dept:[''],
    biometricId:  ['', Validators.required],
    contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
    emailAddress:  ['',Validators.compose([Validators.required,CustomValidator.emailAddress])],
    position: ['',Validators.required],

    contactPerson:  ['', Validators.required],
    cntactNoOfContactPerson:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],

    dateOfActivity:  ['', Validators.required],
    timeFrom:  ['', Validators.required],
    timeTo:  ['', Validators.required],
    purpose:  ['', Validators.required],
    status: [''],
    remarks:[''],

    approver: ['']

  });

  constructor(private _requestService : RequestService, private fb: FormBuilder) { }


  onSubmit(e){

    let aprrv =this.officialBusinessForm.value.approver
    // let Approver
    // for( let key in aprrv){
    //     console.log(Approver =aprrv[0]+ " , "+aprrv[1])
        
    // }
    this.officialBusinessForm.value.approver = aprrv

    let formData = this.officialBusinessForm.value;
    console.log(formData);

    this._requestService.action = "OfficialBusinesses"
    this._requestService.newRecord(formData)
    .subscribe(successCode => {
      this.successMessage = "Successfully Created"
      console.log(successCode)
      this.officialBusinessForm.reset();
    })
  }
  ngOnInit() {
    let id = JSON.parse(localStorage.getItem("currentUser")).empId

    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id)
        .subscribe(data =>  {
            console.log("datos! ",this.data = data)
        
            this.officialBusinessForm.patchValue ({
              
              empId: this.data["id"],
              fullName: this.data["fname"] +" "+ this.data["lname"],
              firstName: this.data["fname"],
              lastName:this.data["lname"],
              branch:  this.data["branchName"],
              department: this.data["departmentName"],
              branch_Dept: this.data["branch_Dept"],
              position:  this.data["positionName"],
              biometricId:  this.data["bio"],
              contactNo:  this.data["telno"],
              emailAddress:  this.data["emaladd"],
              status: "Pending"
            });

        });
        this.getApprover();
  }
  getApprover(){

    this._requestService.action="tbusers"
    this._requestService.getRequest().subscribe(data =>{
       let cntr = 0, cntr1 =0
       for( let key in data){
         if(data[key].access.toLowerCase() == "head"){
          this.approverList[0].data[cntr] = (data[key])  
          cntr++
         }    
        //  else if(data[key].access.toLowerCase() == "mgmt"){
        //   this.approverList[1].data[cntr1] = (data[key])  
        //   cntr1++
        //  }  
       }
       console.log(this.approverList);
      })
  }
}
