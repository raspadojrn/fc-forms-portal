import { Component, OnInit } from '@angular/core';
import { RequestService } from 'app/Services/request.service';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import * as $ from 'jquery';
import { EnumsService } from 'app/Services/common/enums.service';
import { CustomValidator } from 'app/models/validators/custom.validator';

@Component({
  selector: 'app-leave-request',
  templateUrl: './leave-request.component.html',
  styleUrls: ['./leave-request.component.css']
})
export class LeaveRequestRFComponent implements OnInit {
  
  title = "Leave Request"
  myDate = new Date();
  successMessage : any;
  data: any ;
  leavetypes : any =[];
  approverList: any = [
    {
      name:'Approver- Head',
      data :[]
    },
    { 
      name:'',
      data:[]
    }
  ];

  requestLeaveForm = this.fb.group({
    empId:['', Validators.required],
    fullName: [''],
    firstName:[''],
    lastName:['', Validators.required],
    dateCreated: this.myDate,
    branch:  ['', Validators.required],
    department:[''],

    branch_Dept:[''],
    biometricId:  ['', Validators.required],
    contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
    emailAddress:  ['',Validators.compose([Validators.required,CustomValidator.emailAddress])],

    leaveType:  ['', Validators.required],
    period:  ['', Validators.required],
    dateFrom:  ['', Validators.required],
    dateTo:  ['', Validators.required],
    reason:  ['', Validators.required],
    comment: [''],
    status: [''],

    approver: ['']
    

  });


  constructor(private _requestService : RequestService, private _enums : EnumsService, private fb: FormBuilder) { 

    this.load()
  }
    

  onSubmit(e){

    let aprrv =this.requestLeaveForm.value.approver
    // let Approver
    // for( let key in aprrv){
    //     console.log(Approver =aprrv[0]+ " , "+aprrv[1])
        
    // }
    this.requestLeaveForm.value.approver = aprrv
    let formData = this.requestLeaveForm.value;
    console.log(formData);

    this._requestService.action = "LeaveRequests"
    this._requestService.newRecord(formData)
    .subscribe(successCode => {
      this.successMessage = "Successfully Created"
      console.log(successCode)
      this.requestLeaveForm.reset();
      
      
    })
  }

  onChange(){
    this.requestLeaveForm.get('contactNo').value
    .subscribe(val => {
      console.log(val)
      if (val != null) {    
        this.requestLeaveForm.get('contactNo').disable();
    }
    })
  }

  private load(): void {

		this._enums.getCommonList("leavetypes",true).subscribe(ddl => this.leavetypes = ddl);  

  }
  
  ngOnInit() {
    //Extract Data on LocalStorage
  
    let id = JSON.parse(localStorage.getItem("currentUser")).empId

    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id)
        .subscribe(data =>  {
            console.log("datos! ",this.data = data)
        
            this.requestLeaveForm.patchValue ({
              
              empId: this.data["id"],
              fullName: this.data["fname"] +" "+ this.data["lname"],
              firstName: this.data["fname"],
              lastName:this.data["lname"],
              branch:  this.data["branchName"],
              department: this.data["departmentName"],

              branch_Dept: this.data["branch_Dept"],
              biometricId:  this.data["bio"],
              contactNo:  this.data["telno"],
              emailAddress:  this.data["emaladd"],
              status: "Pending"
            });

            //enable input when no value
            // this.requestLeaveForm.get('contactNo').enable();
            // this.requestLeaveForm.get('emailAddress').enable();

            // if(this.requestLeaveForm.controls['contactNo'].value != ''){
            //   this.requestLeaveForm.get('contactNo').disable();
            // }
            // if(this.requestLeaveForm.controls['emailAddress'].value != ''){
            //   this.requestLeaveForm.get('emailAddress').disable();
            // }
        });  

        this.getApprover();
  }
  getApprover(){

    this._requestService.action="tbusers"
    this._requestService.getRequest().subscribe(data =>{
       let cntr = 0, cntr1 =0
       for( let key in data){
         if(data[key].access.toLowerCase() == "head"){
          this.approverList[0].data[cntr] = (data[key])  
          cntr++
         }    
        //  else if(data[key].access.toLowerCase() == "mgmt"){
        //   this.approverList[1].data[cntr1] = (data[key])  
        //   cntr1++
        //  }  
       }
       console.log(this.approverList);
      })
  }

}
