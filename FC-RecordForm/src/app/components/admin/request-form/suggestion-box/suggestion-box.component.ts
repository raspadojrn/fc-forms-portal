import { Component, OnInit } from '@angular/core';
import { RequestService } from 'app/Services/request.service';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { CustomValidator } from 'app/models/validators/custom.validator';

@Component({
  selector: 'app-rf-suggestion-box',
  templateUrl: './suggestion-box.component.html',
  styleUrls: ['./suggestion-box.component.css']
})
export class SuggestionBoxRFComponent implements OnInit {

  title = "Suggestion Box"
  successMessage : any;
  myDate = new Date();
  data: any;

  suggestionBox = this.fb.group({
    empId:['', Validators.required],
    fullName: [''],
    firstName:[''],
    lastName:['', Validators.required],
    dateCreated: this.myDate,
    branch:  ['', Validators.required],
    department:[''],
    contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
    emailAddress:  ['',Validators.compose([Validators.required,CustomValidator.emailAddress])],

   
    Suggestions:  ['', Validators.required],
 

  });
  
  constructor(private _requestService : RequestService, private fb: FormBuilder) { }

  onSubmit(e){
    let formData = this.suggestionBox.value;
    console.log(formData);
    this.successMessage = "Successfully Created"
    this._requestService.action = "SuggestionBoxes"
    this._requestService.newRecord(formData)
    .subscribe(successCode => {
      console.log(successCode)
      this.suggestionBox.reset();
    })
  }

  ngOnInit() {
    let id = JSON.parse(localStorage.getItem("currentUser")).empId

    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id)
        .subscribe(data =>  {
            console.log("datos! ",this.data = data)
        
            this.suggestionBox.patchValue ({
              
              empId: this.data["id"],
              fullName: this.data["fname"] +" "+ this.data["lname"],
              firstName: this.data["fname"],
              lastName:this.data["lname"],
              branch:  this.data["branchName"],
              department: this.data["departmentName"],
              contactNo:  this.data["telno"],
              emailAddress:  this.data["emaladd"],
              status: "Pending"
            });

        });
  }

}