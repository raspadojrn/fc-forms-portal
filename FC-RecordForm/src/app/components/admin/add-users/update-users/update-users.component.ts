import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { RequestService } from 'app/Services/request.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { EnumsService } from 'app/Services/common/enums.service';

@Component({
  selector: 'app-update-users',
  templateUrl: './update-users.component.html',
})
export class UpdateUsersComponent implements OnInit {


  id : any;
  accountAccess : any;
  myDate = new Date();
  userData: any ;

  accessVal ='Admin';
  successMessage: any;

  userForm  = this.fb.group({
    id:[''],
    empId:['', Validators.required],
    dateCreated: [''],
    username:['', Validators.required],
    password:['',Validators.compose([Validators.required,Validators.minLength(6)])],
    access: ['', Validators.required],
    department:[''],
    status: ['ACTIVE']
    
  });
  statusList: string[] = ['Pending', 'Accept', 'Approved', 'Cancelled'];
  constructor(private _requestService : RequestService, private fb: FormBuilder, 
              private _enums : EnumsService ,private dialog: MatDialog)
               { this.load(); 
                this.userForm.controls['access'].setValue(this.accessVal, {onlySelf: true});
              }

  private load(): void {

    this._enums.getCommonList("accountAccees",true).subscribe(ddl => this.accountAccess = ddl);  

  }
  updateRecords(){
    
    let formData = this.userForm.value;
    console.log(this.userData.id ,formData);
    
    this._requestService.action = "tbusers"
    this._requestService.updateRecord(this.userData.id ,formData).subscribe( res=>{
       console.log(this.successMessage= 'Succesfully Updated')
      
      })
  }

  //Modal Close
  onClose(){
  this.dialog.closeAll()
  }

  ngOnInit() {

      this.getUserData();
  }

  getUserData(){

    console.log(this.id)
    this._requestService.action = "tbusers/"
    this._requestService.getRequestById(this.id).subscribe(data=> {console.log(this.userData = data)
      this.userForm.patchValue({
        id: data['id'],
        empId: data['empid'],
        dateCreated: data['datecreated'],
        username: data['username'],
        // password: data['password'],
        access : data['access'],
        status: data['status']      
      });   
    })
  }
}
