import { Component, OnInit, ViewChild } from '@angular/core';
import { RequestService } from 'app/Services/request.service';
import { FilterService } from 'app/Services/common/filter.service'
import { EnumsService } from 'app/Services/common/enums.service';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { PagerComponent } from 'app/common/pager/pager.component';
import { UpdateUsersComponent } from './update-users/update-users.component';

@Component({
  selector: 'app-add-users',
  templateUrl: './add-users.component.html',
  styleUrls: ['./add-users.component.css']
})
export class AddUsersComponent implements OnInit {

  title = "Add Users"
  myDate = new Date();
  users : any = []; // user data filtered

  UserData : any = []; //user data not filtered
  empInfo : any = [];
  accountAccess : any;
  department : any;
  successMessage : any;
  customError : any;

  filter = this.fb.group({
    OrderBy: 'Id',
    OrderType: ['Descending'],
    ShowAll: false,
    Status : null,
    currentPage: 1
  })
  pagerAction : any = "tbusers/filter";
  pagerData : any = []
  module : string = "user";

 userForm  = this.fb.group({
    id:[''],
    empId:['', Validators.required],
    dateCreated: this.myDate,
    username:['', Validators.required],
    password:['',Validators.compose([Validators.required,Validators.minLength(6)])],
    access: ['', Validators.required],
    department:[''],
    status: ['ACTIVE']
    
  });

  constructor(private _requestService : RequestService, private _filterService : FilterService,
               private _enums : EnumsService ,private fb: FormBuilder, private dialog: MatDialog)
                { this.load();}

               
  @ViewChild(PagerComponent)
  private pager : PagerComponent;
  getPagerData(pagerModel : any) {

            // this.allRecords = pagerModel["allRecords"];
            this.users =  pagerModel["pageRecord"]; 
            // this.totalRecordMessage =  pagerModel["totalRecordMessage"]; 
            // this.pageRecordMessage =  pagerModel["pageRecordMessage"]; 

            console.log( this.users,pagerModel );
  }

  private load(): void {

    this._enums.getCommonList("accountAccees",true).subscribe(ddl => this.accountAccess = ddl);  
 
    this._enums.getCommonList("department",true).subscribe(ddl => this.department = ddl);  
  }

  openDialog(id: any) {
    const dialogRef = this.dialog.open(UpdateUsersComponent, {
      // height:'1000px',
      width: '5000px',
      panelClass: 'my-dialog',
    });

    dialogRef.componentInstance.id = id;

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.getUsers()
    });
  }

  createUser(e){
    console.log("create User",e.empId,)

    this.userForm.patchValue ({
      dateCreated: this.myDate,
      status: 'ACTIVE'
    })

  //If the emp Id is not Existing in the db the user creaation will not contineu
    for( let i =0; i< this.empInfo.length; i++){
      if(e.empId == this.empInfo[i].id && e.empId == this.users.empid){
       
        //increment id
        this.userForm.value.id = this.UserData[this.UserData.length - 1].id + 1
          let formData = this.userForm.value;
          console.log(formData);
          this._requestService.action = "Tbusers"

          this._requestService.newRecord(formData)
          .subscribe(successCode => {
            this.successMessage = "Successfully Created"
            console.log(successCode)
            this.userForm.reset();
            this.customError = null
            this.getUsers();
          })
      }
      else{
        // console.log(this.userForm.empId = null)
        this.customError = "Employee Id is not valid"
      }
    }
  }

  changeAccStatus(data){
    let formData = data;
    if(data.status == "ACTIVE"){
      formData.status = "IN-ACTIVE"
    }else{
      formData.status = "ACTIVE"
    }
    console.log(formData)
    this._requestService.action = "tbusers"
    this._requestService.updateRecord(data.id ,formData).subscribe( res=>{
       console.log(res, 'Succesfully Updated')     
      })
  }
 
  ngOnInit() {
    this.getUsers();
  }

  getUsers(){
    this._requestService.action = "tbusers"
    this._requestService.getRequest().subscribe( user => this.UserData = user)

    //use for updating data to be dynamic 
    let criteria = this.filter.value
    this._filterService.action="tbusers/filter"
    this._filterService.GetfilterData(criteria).subscribe(user => this.users = user.list)

    this._requestService.action ="Tblprofiles"
    this._requestService.getRequest().subscribe( emp => this.empInfo = emp)
  }

  isSameEmpId(){

  }
}
