import { Component, OnInit } from '@angular/core';
import { RequestService } from 'app/Services/request.service';

@Component({
  selector: 'app-dashboard-admin',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {


  data : any = [];
  leaveRequest : any=[];
  overtime : any =[];
  officialBusiness : any =[];
  incidentReport : any =[];
  suggestionBox : any =[];

  purchaseRequest : any =[];
  uniformRequest : any =[];
  businessCard : any =[];
  bookletRequest : any =[];
  maintenanceRequest : any =[];
  marketingRequest : any =[];

  department : any;
  // empId = JSON.parse(localStorage.getItem("currentUser")).empId
 
  constructor(private _requestService : RequestService) { }

  
  ngOnInit() {

    this.getData();
    console.log(this.leaveRequest,this.overtime, this.officialBusiness, this.incidentReport)
  }

  getData(){

    let id = JSON.parse(localStorage.getItem("currentUser")).empId
  
    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id).subscribe(data => {
      console.log(this.data = data) 
    });

    this._requestService.action = "LeaveRequests"
    this._requestService.getRequest().subscribe(data=> {console.log("LeaveRequests: ", data, data.length)   
      this.filterCurrentUserData(data, "LeaveRequests");
    })

    this._requestService.action = "RequestOts"
    this._requestService.getRequest().subscribe(data=> {console.log("RequestOts: ", data, data.length)   
      this.filterCurrentUserData(data, "RequestOts");
    })

    this._requestService.action = "OfficialBusinesses"
    this._requestService.getRequest().subscribe(data=> {console.log("OfficialBusinesses: ", data, data.length)   
      this.filterCurrentUserData(data, "OfficialBusinesses");
    })

    this._requestService.action = "IncidentReports"
    this._requestService.getRequest().subscribe(data=> {console.log("IncidentReports: ", data, data.length)   
      this.filterCurrentUserData(data, "IncidentReports");
    })

    this._requestService.action = "SuggestionBoxes"
    this._requestService.getRequest().subscribe(data=> {console.log("SuggestionBoxes: ", data, data.length)   
      this.filterCurrentUserData(data, "SuggestionBoxes");
    })

    this._requestService.action = "PurchaseRequests"
    this._requestService.getRequest().subscribe(data=> {console.log("PurchaseRequests: ", data, data.length)   
      this.filterCurrentUserData(data, "PurchaseRequests");
    })
    
    this._requestService.action = "UniformRequests"
    this._requestService.getRequest().subscribe(data=> {console.log("UniformRequests: ", data, data.length)   
      this.filterCurrentUserData(data, "UniformRequests");
    })
    
    this._requestService.action = "BusinessCardRequests"
    this._requestService.getRequest().subscribe(data=> {console.log("BusinessCardRequests: ", data, data.length)   
      this.filterCurrentUserData(data, "BusinessCardRequests");
    })
    
    this._requestService.action = "BookletRequests"
    this._requestService.getRequest().subscribe(data=> {console.log("BookletRequests: ", data, data.length)   
      this.filterCurrentUserData(data, "BookletRequests");
    })
    
    this._requestService.action = "maintenanceRequests"
    this._requestService.getRequest().subscribe(data=> {console.log("maintenanceRequests: ", data, data.length)   
      this.filterCurrentUserData(data, "maintenanceRequests");
    })
    
    this._requestService.action = "MarketingMaterialRequests"
    this._requestService.getRequest().subscribe(data=> {console.log("MarketingMaterialRequests: ", data, data.length)   
      this.filterCurrentUserData(data, "MarketingMaterialRequests");
    })
  }

  //User only see their form request data
  filterCurrentUserData(data : any, action: string){
    let cntr =0;
    let apprvd_len =0, acpt_len =0, cncl_len =0, pndng_len =0;
    
    for(let i =0; i < data.length; i ++){
        
        switch(action){
          case"LeaveRequests":
          
               this.leaveRequest[cntr] = data[i];
               (data[i].status == "Approved" ? this.leaveRequest["approved"] = ++apprvd_len: null),
               (data[i].status == "Accept" ? this.leaveRequest["accept"] = ++acpt_len: null),
               (data[i].status == "Cancelled" ? this.leaveRequest["cancelled"] = ++cncl_len: null),
               (data[i].status == "Pending" ? this.leaveRequest["pending"] = ++pndng_len: null)
               break;
          case"RequestOts":
              this.overtime[cntr] = data[i];
              (data[i].status == "Approved" ? this.overtime["approved"] = ++apprvd_len: null),
              (data[i].status == "Accept" ? this.overtime["accept"] = ++acpt_len: null),
              (data[i].status == "Cancelled" ? this.overtime["cancelled"] = ++cncl_len: null),
              (data[i].status == "Pending" ? this.overtime["pending"] = ++pndng_len: null)
              break;
          case"OfficialBusinesses":
              this.officialBusiness[cntr] = data[i];
              (data[i].status == "Approved" ? this.officialBusiness["approved"] = ++apprvd_len: null),
              (data[i].status == "Accept" ? this.officialBusiness["accept"] = ++acpt_len: null),
              (data[i].status == "Cancelled" ? this.officialBusiness["cancelled"] = ++cncl_len: null),
              (data[i].status == "Pending" ? this.officialBusiness["pending"] = ++pndng_len: null)
              break;
          case"IncidentReports":
              this.incidentReport[cntr] = data[i];
              (data[i].status == "Approved" ? this.incidentReport["approved"] = ++apprvd_len: null),
              (data[i].status == "Accept" ? this.incidentReport["accept"] = ++acpt_len: null),
              (data[i].status == "Cancelled" ? this.incidentReport["cancelled"] = ++cncl_len: null),
              (data[i].status == "Pending" ? this.incidentReport["pending"] = ++pndng_len: null)
              break;
          case"SuggestionBoxes":
              this.suggestionBox[cntr] = data[i];
              break;
          case"PurchaseRequests":
              this.purchaseRequest[cntr] = data[i];
              (data[i].status == "Approved" ? this.purchaseRequest["approved"] = ++apprvd_len: null),
              (data[i].status == "Accept" ? this.purchaseRequest["accept"] = ++acpt_len: null),
              (data[i].status == "Cancelled" ? this.purchaseRequest["cancelled"] = ++cncl_len: null),
              (data[i].status == "Pending" ? this.purchaseRequest["pending"] = ++pndng_len: null)
              break;
          case"UniformRequests":
              this.uniformRequest[cntr] = data[i];
              (data[i].status == "Approved" ? this.uniformRequest["approved"] = ++apprvd_len: null),
              (data[i].status == "Accept" ? this.uniformRequest["accept"] = ++acpt_len: null),
              (data[i].status == "Cancelled" ? this.uniformRequest["cancelled"] = ++cncl_len: null),
              (data[i].status == "Pending" ? this.uniformRequest["pending"] = ++pndng_len: null)
              break;
          case"BusinessCardRequests":
              this.businessCard[cntr] = data[i];
              (data[i].status == "Approved" ? this.businessCard["approved"] = ++apprvd_len: null),
              (data[i].status == "Accept" ? this.businessCard["accept"] = ++acpt_len: null),
              (data[i].status == "Cancelled" ? this.businessCard["cancelled"] = ++cncl_len: null),
              (data[i].status == "Pending" ? this.businessCard["pending"] = ++pndng_len: null)
              break;
          case"BookletRequests":
              this.bookletRequest[cntr] = data[i];
              (data[i].status == "Approved" ? this.bookletRequest["approved"] = ++apprvd_len: null),
              (data[i].status == "Accept" ? this.bookletRequest["accept"] = ++acpt_len: null),
              (data[i].status == "Cancelled" ? this.bookletRequest["cancelled"] = ++cncl_len: null),
              (data[i].status == "Pending" ? this.bookletRequest["pending"] = ++pndng_len: null)
              break;
          case"maintenanceRequests":
              this.maintenanceRequest[cntr] = data[i];
              (data[i].status == "Approved" ? this.maintenanceRequest["approved"] = ++apprvd_len: null),
              (data[i].status == "Accept" ? this.maintenanceRequest["accept"] = ++acpt_len: null),
              (data[i].status == "Cancelled" ? this.maintenanceRequest["cancelled"] = ++cncl_len: null),
              (data[i].status == "Pending" ? this.maintenanceRequest["pending"] = ++pndng_len: null)
              break;
          case"MarketingMaterialRequests":
              this.marketingRequest[cntr] = data[i];
              (data[i].status == "Approved" ? this.marketingRequest["approved"] = ++apprvd_len: null),
              (data[i].status == "Accept" ? this.marketingRequest["accept"] = ++acpt_len: null),
              (data[i].status == "Cancelled" ? this.marketingRequest["cancelled"] = ++cncl_len: null),
              (data[i].status == "Pending" ? this.marketingRequest["pending"] = ++pndng_len: null)
              break;
        }
        cntr++;
    }  
  }
  isHr(){
    if(this.data.departmentName == "Human Resource"){
      return true;
    }
    else{
      return false;
    }
  }
}
