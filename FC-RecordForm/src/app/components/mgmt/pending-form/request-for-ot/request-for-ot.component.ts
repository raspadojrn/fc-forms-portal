import { Component, OnInit } from '@angular/core';
import { RequestService } from 'app/Services/request.service';
import { FilterService } from 'app/Services/common/filter.service'
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { ApproveRequestOTComponent } from './approve-request-ot/approve-request-ot.component';

@Component({
  selector: 'app-request-for-ot',
  templateUrl: './request-for-ot.component.html',
  styleUrls: ['./request-for-ot.component.css']
})
export class RequestForOTComponent implements OnInit {

  title = "Overtime"
  myDate = new Date();
  tabIndex : any = 0;
  RequestOtData: any =[];

  isDateFilter : boolean=false ;
  status: any={ 
    tab0 : "Pending",
    tab1 : "Accept",};
  filter = this.fb.group({
    DateFrom: [''],
    DateTo: [''],
    OrderBy: 'Id',
    OrderType: ['Descending'],
    ShowAll: true,
    Status : null
  })
  statusList: string[] = ['Pending',  'Approved', 'Cancelled'];
  constructor(private _requestService : RequestService, private _filterService : FilterService,
              private fb: FormBuilder, private dialog: MatDialog) { }
    
  getFilterStatusTab0(e, filterType){

    console.log(filterType =='date'? this.isDateFilter = true : this.isDateFilter = false)
    this.status.tab0 = e.value

      this.filter.value['Status'] =  this.status.tab0 
      this.getData(0)  
      console.log(e)  
  }
  resetFilter(){
    this.filter.value['DateFrom']= null
    this.filter.value['DateTo']= null
    this.getData(0)  
    console.log("click",this.filter.value)
  }

  getFilterStatusTab1(e, filterType){

    console.log(filterType =='date'? this.isDateFilter = true : this.isDateFilter = false)
    this.status.tab1 = e.value

      this.filter.value['Status'] = this.status.tab1
      this.getData(1)  
      console.log(e.value)  
  }
  
  openDialog(id: any, act : any, currStatus : any) {
    const dialogRef = this.dialog.open(ApproveRequestOTComponent, {
      // height:'1000px',
      width: '800px',
      panelClass: 'my-dialog',
    });

    dialogRef.componentInstance.id = id;
    dialogRef.componentInstance.actStatus = act;
    dialogRef.componentInstance.currentStatus = currStatus;

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
     
      this.getData(0)
      // this.getData(1)
    
      
    });
  }
  onTabChanged(e){
    console.log(e.index)
    this.tabIndex = e.index
  }

  
  ngOnInit() {
    this.getData(null)     
  }

  getData(tab : any){

    this._filterService.action= "RequestOts/Filter"
    let criteria = this.filter.value

      switch(tab){
        case(0):
              this.filter.value['Status'] =  this.status.tab0
              criteria = this.filter.value
              this._filterService.GetfilterData(criteria).subscribe(data=> this.RequestOtData.tab1=this._filterService.filterData(data.list, 1))
              break;
        case(1):
              this.filter.value['Status'] =  this.status.tab1
              criteria = this.filter.value
              this._filterService.GetfilterData(criteria).subscribe(data=> this.RequestOtData.tab1=this._filterService.filterData(data.list, 1))
              break;
        default:
              this.filter.value['Status'] = "Pending"
              this._filterService.GetfilterData(criteria).subscribe(data=>  this.RequestOtData.tab0  =data.list)

              this.filter.value['Status'] = "Pending"
              this._filterService.GetfilterData(criteria).subscribe(data=>this.RequestOtData.tab1  =this._filterService.filterData(data.list, 1))
              console.log(this.RequestOtData)
              break;

      }
  }
}
