import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { RequestService } from 'app/Services/request.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';

@Component({
  selector: 'app-approve-request-ot',
  templateUrl: './approve-request-ot.component.html',
  styleUrls: ['./approve-request-ot.component.css']
})
export class ApproveRequestOTComponent implements OnInit {

  id : any;
  actStatus : any;
  currentStatus : any;

  myDate = new Date();
  leaveReqData: any ;
  approvedBy: any;
  successMessage: any;
  overtimeForm = this.fb.group({
    id:[''],
    empId:['', Validators.required],
    firstName:[''],
    lastName:['', Validators.required],
    dateCreated: this.myDate,
    branch:  ['', Validators.required],
    department:['', Validators.required],
    position:  ['', Validators.required],
    contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
    emailAddress:  [''],

   
    dateOfOt:  ['', Validators.required],
    timeFrom:  ['', Validators.required],
    timeTo:  ['', Validators.required],
    reason:  ['', Validators.required],

    status: [''],
    approveRemarks:  ['', Validators.required],
    acceptBy: [''],
    approvedBy:  [''],
    dateApproved: [''],

    approver: ['']
  });

  constructor(private _requestService : RequestService, private fb: FormBuilder, private dialog: MatDialog) { }



  approveRequest(){
 
    this.overtimeForm.value['approvedBy'] =this.approvedBy
    this.overtimeForm.value['dateApproved'] =this.myDate
    let formData = this.overtimeForm.value;
    console.log(this.leaveReqData.id ,formData);
    
    this._requestService.action = "RequestOts"
    this._requestService.updateRecord(this.leaveReqData.id ,formData).subscribe( res=> console.log(this.successMessage= 'Succesfully Updated'))
    
  }

  cancelRequest(){
    
    this.overtimeForm.value["status"] = "Cancelled"
    this.overtimeForm.value['approvedBy'] =this.approvedBy
    this.overtimeForm.value['dateApproved'] =this.myDate
    let formData = this.overtimeForm.value;
    console.log(this.leaveReqData.id ,formData);
    
    this._requestService.action = "RequestOts"
    this._requestService.updateRecord(this.leaveReqData.id ,formData).subscribe( res=> console.log(this.successMessage= 'Succesfully Updated'))
  }
  //Modal Close
  onClose(){
  this.dialog.closeAll()
  }

  ngOnInit() {
    console.log(this.id)
    this._requestService.action = "RequestOts/"
    this._requestService.getRequestById(this.id).subscribe(data=> {console.log(this.leaveReqData = data)
      
    
      this.overtimeForm.patchValue ({
        id: data["id"],   
        empId: data["empId"],
        dateCreated: data["dateCreated"],
        firstName: data["firstName"],
        lastName: data["lastName"],
        branch:  data["branch"],
        department: data["department"],
        contactNo:  data["contactNo"],
        emailAddress:  data["emailAddress"],
        position:  data["position"],

        dateOfOt: data["dateOfOt"],
        timeFrom:  data["timeFrom"],
        timeTo:  data["timeTo"],
        reason:  data["reason"],
  

        status: this.actStatus == "Accept" ?  "Accept" : "Approved" ,
        approveRemarks:  data["approveRemarks"] == null ? data["approveRemarks"] : data["approveRemarks"]  +";\n",
        acceptBy: data["acceptBy"],
        approvedBy:  data["approvedBy"],

        approver: data['approver']
      });
    
    
    
    })

    let id = JSON.parse(localStorage.getItem("currentUser")).empId
    
    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id).subscribe(data => this.approvedBy = data.fname +' '+ data.lname);
  }
}
