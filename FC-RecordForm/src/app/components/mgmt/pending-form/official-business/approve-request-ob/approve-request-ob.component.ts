import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { RequestService } from 'app/Services/request.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';

@Component({
  selector: 'app-approve-request-ob',
  templateUrl: './approve-request-ob.component.html',
  styleUrls: ['./approve-request-ob.component.css']
})
export class ApproveRequestOBComponent implements OnInit {

  id : any;
  actStatus : any;
  currentStatus : any;

  myDate = new Date();
  leaveReqData: any ;
  approvedBy: any;
  successMessage: any;
  officialBusinessForm = this.fb.group({
    id:[''],
    empId:['', Validators.required],
    firstName:[''],
    lastName:['', Validators.required],
    dateCreated: this.myDate,
    branch:  ['', Validators.required],
    department:['', Validators.required],
    biometricId:  ['', Validators.required],
    contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
    emailAddress:  ['', Validators.required],
    position: ['',Validators.required],

    contactPerson:  ['', Validators.required],
    cntactNoOfContactPerson:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],

    dateOfActivity:  ['', Validators.required],
    timeFrom:  ['', Validators.required],
    timeTo:  ['', Validators.required],
    workLocationName : ['', Validators.required],
    purpose:  ['', Validators.required],
    remarks:[''],

    status: [''],
    approveRemarks:  [''],
    acceptBy: [''],
    approvedBy:  [''],
    dateApproved: [''],
    approver: ['']
  });

  constructor(private _requestService : RequestService, private fb: FormBuilder, private dialog: MatDialog) { }



  approveRequest(){
 
    this.officialBusinessForm.value['approvedBy'] =this.approvedBy
    this.officialBusinessForm.value['dateApproved'] =this.myDate
    let formData = this.officialBusinessForm.value;
    console.log(this.leaveReqData.id ,formData);
    
    this._requestService.action = "OfficialBusinesses"
    this._requestService.updateRecord(this.leaveReqData.id ,formData).subscribe( res=> console.log(this.successMessage= 'Succesfully Updated'))
    
  }

  cancelRequest(){
    
    this.officialBusinessForm.value["status"] = "Cancelled"
    this.officialBusinessForm.value['approvedBy'] =this.approvedBy
    this.officialBusinessForm.value['dateApproved'] =this.myDate
    let formData = this.officialBusinessForm.value;
    console.log(this.leaveReqData.id ,formData);
    
    this._requestService.action = "OfficialBusinesses"
    this._requestService.updateRecord(this.leaveReqData.id ,formData).subscribe( res=> console.log(this.successMessage= 'Succesfully Updated'))
  }
  //Modal Close
  onClose(){
  this.dialog.closeAll()
  }

  ngOnInit() {
    console.log(this.id)
    this._requestService.action = "OfficialBusinesses/"
    this._requestService.getRequestById(this.id).subscribe(data=> {console.log(this.leaveReqData = data)
      
    
      this.officialBusinessForm.patchValue ({
        id: data["id"],   
        empId: data["empId"],
        dateCreated: data["dateCreated"],
        firstName: data["firstName"],
        lastName: data["lastName"],
        branch:  data["branch"],
        department: data["department"],
        biometricId:  data["biometricId"],
        contactNo:  data["contactNo"],
        emailAddress:  data["emailAddress"],
        position:  data["position"],

        contactPerson: data["contactPerson"],
        cntactNoOfContactPerson:  data["cntactNoOfContactPerson"],

        dateOfActivity: data["dateOfActivity"],
        timeFrom:  data["timeFrom"],
        timeTo:  data["timeTo"],
        workLocationName : data["workLocationName"],
        purpose:  data["purpose"],
        remarks:  data["remarks"],

        status: this.actStatus == "Accept" ?  "Accept" : "Approved" ,
        approveRemarks:  data["approveRemarks"] == null ? data["approveRemarks"] : data["approveRemarks"]  +";\n",
        acceptBy: data["acceptBy"],
        approvedBy:  data["approvedBy"],

        approver: data['approver']
      });
    
    
    
    })

    let id = JSON.parse(localStorage.getItem("currentUser")).empId
    
    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id).subscribe(data => this.approvedBy = data.fname +' '+ data.lname);
  }

}
