import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { RequestService } from 'app/Services/request.service';
import { FilterService } from 'app/Services/common/filter.service'

@Component({
  selector: 'app-petty-cash-forms',
  templateUrl: './petty-cash-forms.component.html',
  styleUrls: ['./petty-cash-forms.component.css']
})
export class PettyCashFormsComponent implements OnInit {

  title = "Petty Cash list"
  pettyCashRequest : any = []
  filter = this.fb.group({
    DateFrom: [''],
    DateTo: [''],
    OrderBy: 'Id',
    OrderType: ['Descending'],
    ShowAll: true,
    Status : null
  })
  constructor(private _requestService : RequestService, private _filterService : FilterService,
              private fb: FormBuilder) { }

  ngOnInit() {
    this._filterService.action= "PettyCashes/Filter"
    let criteria = this.filter.value
    this._filterService.GetfilterData(criteria).subscribe(data=> console.log(this.pettyCashRequest=data.list))

    // this._requestService.action ='PettyCashes'
    // this._requestService.getRequest().subscribe(data => console.log(this.pettyCashRequest = data))
  }

}
