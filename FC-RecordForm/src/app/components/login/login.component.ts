import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { RequestService } from 'app/Services/request.service';
import { AuthService } from 'app/Services/auth.service';
import { EnumsService } from 'app/Services/common/enums.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  users: any;
	errorMessage : string;
	errorMessage2 : string;

  isLoading : boolean = false;
  
  loginForm = this.fb.group({
    username: ['', [Validators.required]],
    password: ['',Validators.compose([Validators.required,Validators.minLength(6)])],
  });


  constructor(private router: Router,
              private _requestService : RequestService,
              private _authService : AuthService,
              private _common : EnumsService,
              private fb: FormBuilder) { }
           
  onSubmit(){
    this.isLoading = true;
    let formData = this.loginForm.value
    console.log("Login...", formData)

    this._authService.action = "tbusers/authenticate";
    this._authService.authenticateUser(formData)
    .subscribe(result => {
        console.log(result)
        result == "ACTIVE" ?  this.errorMessage = null :  this.errorMessage = result;
    },
    error =>{
        formData["password"] = null;
        this.loginForm.reset(formData);
        this.errorMessage = 'Username or password is incorrect';
        this.isLoading = false;
    });
    
  }
  ngOnInit() {
    this._requestService.action ="Tbusers/"
    this._requestService.getRequest().subscribe(data => console.log(this.users=data))
  }

}
