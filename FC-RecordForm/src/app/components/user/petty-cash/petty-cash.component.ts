import {Component, OnInit, ViewChild } from '@angular/core';
import { RequestService } from 'app/Services/request.service';
import { NgForm, Validators } from '@angular/forms';
import { FormControl, FormGroup, FormBuilder, FormArray} from '@angular/forms';
import { Router } from '@angular/router'
import { EnumsService } from 'app/Services/common/enums.service';
import { Ng2ImgMaxService } from 'ng2-img-max';
declare var require: any

@Component({
  selector: 'app-petty-cash',
  templateUrl: './petty-cash.component.html',
  styleUrls: ['./petty-cash.component.css']
})
export class PettyCashComponent implements OnInit {
  title = "Petty Cash Request Form"

  myDate = new Date();
  successMessage : any;
  data: any ;
  pettyCashRequest : any =[];
  manualtype : any =[];

  orderItems :any = [];
  accountTitle : any =[];
  accountTitleBranches : any= [];
  payeesList : any =[];
  company : any=[];
  branch : any =[];
  PayeesAndAccName : any =[];

  url: any;
  imagePath : string = null;
  imageErrorMessage : string;
  uploadedImage: File;

  selectedIndex: any;
  
  approverList: any = [
    {
      name:'Approver- Head',
      data :[]
    },
    { 
      name:'',
      data:[]
    }
  ];
  PCOrderItems : FormGroup;
  pettyCashRequestForm : FormGroup;

  constructor(private _requestService : RequestService, private _enums : EnumsService, private fb: FormBuilder, private ng2ImgMax: Ng2ImgMaxService) { 

   this.load()
  }
  getIndex(e){
    // console.log(e.selectedIndex == 1)
    console.log(e.selectedIndex == 1)
    //Remove Item,ItemDesc,Quantity Validation when user's on last step to pass data
    if(e.selectedIndex == 1){

      this.pettyCashRequestForm.controls['Particulars'].setValidators([])
      this.pettyCashRequestForm.controls['Amount'].setValidators([])
      this.pettyCashRequestForm.controls['accountTitle'].setValidators([])

      this.pettyCashRequestForm.controls['Particulars'].updateValueAndValidity()
      this.pettyCashRequestForm.controls['Amount'].updateValueAndValidity()
      this.pettyCashRequestForm.controls['accountTitle'].updateValueAndValidity()
    }else{
      this.pettyCashRequestForm.controls['Particulars'].setValidators([Validators.required])
      this.pettyCashRequestForm.controls['Amount'].setValidators([Validators.required])
      this.pettyCashRequestForm.controls['accountTitle'].setValidators([Validators.required])
      
      this.pettyCashRequestForm.controls['Particulars'].updateValueAndValidity()
      this.pettyCashRequestForm.controls['Amount'].updateValueAndValidity()
      this.pettyCashRequestForm.controls['accountTitle'].updateValueAndValidity()
    }

  }

  onSubmit(e){

    // let aprrv =this.pettyCashRequestForm.value.approver
    // let Approver
    // for( let key in aprrv){
    //     console.log(Approver =aprrv[0]+ " , "+aprrv[1])
        
    // }
    // this.pettyCashRequestForm.value.approver = aprrv
    let formData = this.pettyCashRequestForm.value;
    console.log(formData,this.pettyCashRequestForm,e);

    this._requestService.action = "PettyCashes"
    this._requestService.newRecord(formData)
    .subscribe(successCode => { 
      console.log(successCode) 
      this.getPROrderItem(successCode["id"], formData);
      // this.successMessage = "Successfully Created"
      console.log(successCode) 
     
      this.pettyCashRequestForm.reset();
      this.orderItems =null
      
    })
  }

  AddItem(e){


      // control refers to your formarray
      const control = <FormArray>this.pettyCashRequestForm.controls['orderItems'];
      this.orderItems.push(this.pettyCashRequestForm.value)
      console.log(control, this.orderItems)
      let index = this.orderItems.length -1


      if(this.orderItems[index].accountTitleId != 9999){
        var accountTitle : any= "";
        var accountTitleId : any= "";
        for (let i = 0; i < this.orderItems[index].accountTitleId.length; i++) {
          accountTitleId +=this.orderItems[index].accountTitleId[i] +',\n';
          for(let i1 = 0; i1 < this.accountTitle.length; i1++){
            if(this.accountTitle[i1].value == this.orderItems[index].accountTitleId[i]){

              accountTitle +=this.accountTitle[i1].name +',\n';
            }
          }
        }
        this.orderItems[index].accountTitle =accountTitle
        console.log(accountTitle,accountTitleId)
      }
      console.log(this.pettyCashRequestForm)
      this.pettyCashRequestForm.controls.Particulars.reset();
      this.pettyCashRequestForm.controls.Amount.reset();
      this.pettyCashRequestForm.controls.accountTitle.reset();
      this.pettyCashRequestForm.controls.accountTitleId.reset();
     
      let orderitems = this.fb.group({
        particulars : new FormControl(this.orderItems[index].Particulars,Validators.required),
        amount : new FormControl(this.orderItems[index].Amount,Validators.required),
        imageName : new FormControl(this.orderItems[index].imageName),
        accountTitleId : new FormControl(accountTitleId? accountTitleId : this.orderItems[index].accountTitleId[0],Validators.required),
        accountTitle : new FormControl(accountTitle? accountTitle : this.orderItems[index].accountTitle,Validators.required),
      })
      
      // add new formgroup

      control.push(orderitems);
      console.log(control.value , orderitems)
      this.getTotalAmount(control)
      this.url = "";
  }

  deleteRow(index: number) {
  
    // control refers to your formarray
    const control = <FormArray>this.pettyCashRequestForm.controls['orderItems'];
    // remove the chosen row
    this.orderItems.splice(index,1);
    control.removeAt(index);
    console.log(index, control.value)
    this.getTotalAmount(control)
}
  // onChange(){
  //   this.pettyCashRequestForm.get('contactNo').value
  //   .subscribe(val => {
  //     console.log(val)
  //     if (val != null) {    
  //       this.pettyCashRequestForm.get('contactNo').disable();
  //   }
  //   })
  // }

  private load(): void {
		this._enums.getCommonList("AccountTitleEnum",true).subscribe(ddl => console.log(this.accountTitle = ddl));  
  }
  onSelectFile1(event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      console.log(event)
      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (e) => { // called once readAsDataURL is completed
        let target: any = e.target;
        this.url = target.result
      }
 
    }
 
}
onSelectFile(event) {
this.onSelectFile1(event)
if(event.target.files.length > 0) {
  let file = event.target.files[0];


  if (file.type.includes("image")) {
      if (file["size"] <= 7000000) {


          // this.isUploading = true;
          // this.imagePath = "assets/images/spinner.gif";

          this.ng2ImgMax.resizeImage(file, 800, 750).subscribe(
            result => {
          
              this.uploadedImage = new File([result], result.name);
          
              // If image > 1.2mb save resized image else save uploaded.
              this.uploadedImage = (file["size"] >= 1300000) ? this.uploadedImage : file;
              console.log(this.uploadedImage)
              this._requestService
                  .uploadImage(this.uploadedImage)
                  .subscribe(res => {
                      this.imagePath = "assets/images/request-images/" + file.name + "?_=" + Date.now();
                      // this.isUploading = false;
              });

              this.pettyCashRequestForm.get('imageName').setValue(file.name); 
              this.imageErrorMessage = null; 
            },
            error => {
              console.log(error);
            }
          );

      }
      else {
          this.imageErrorMessage = "Image too large (Maximum Size of 7MB only)";
      }
  }
  else {
    this.imageErrorMessage = "Invalid File Format";
  }
}
else {
  this.pettyCashRequestForm.get('imageName').setValue(''); 
}
}
getTotalAmount(control){
  let totalAmount =0;
  for(let i =0; i < control.value.length; i++){
    totalAmount +=  control.value[i].amount
  }
  this.NumberTowords(totalAmount)
  this.pettyCashRequestForm.patchValue ({ TotalAmount: totalAmount})
  console.log(totalAmount)
}
isOtherExpense(e){
  let str = e
  if(str== null){
    return false
  }
  else if(str.includes(9999)){
    return true
  }
  else{
     return false
  }
}
  NumberTowords(e){
    var converter = require('number-to-words');
    this.pettyCashRequestForm.patchValue ({ AmountInWord: converter.toWords(e) })
  }
  onPayeeSelect(e){
    console.log(e.target.value, this.PayeesAndAccName)    
    for( let i = 0; i < this.PayeesAndAccName.length; i++){
      this.pettyCashRequestForm.patchValue ({ accountName: this.payeesList[i].accountname })  
    }    
                                            
  }
  onBranchSelect(e){
    this.PayeesAndAccName.length = 0
    this.pettyCashRequestForm.controls.accountName.reset();
    console.log(e.target.value,this.payeesList)
    for( let i = 0; i < this.payeesList.length; i++){
      if(this.payeesList[i].branch == e.target.value){
        this.PayeesAndAccName.push(this.payeesList[i])
      }
    }
  
    console.log(this.PayeesAndAccName)
  }
  onCompanySelect(e){
    console.log(e.target.value)
    this.branch.length =0

    for(let i =0; i < this.accountTitleBranches.length; i++ ){
      if(this.accountTitleBranches[i].company == e.target.value){
        this.branch.push(this.accountTitleBranches[i].branch)
      }
      else if(e.target.value == "null"){
        this.branch.length = 0
      }
    }
    console.log(this.branch.sort())
  }
  ngOnInit() {

    var converter = require('number-to-words');
    //Extract Data on LocalStorage
    this.createForms();
    let id = JSON.parse(localStorage.getItem("currentUser")).empId

    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id)
        .subscribe(data =>  {
            console.log("datos! ",this.data = data)
        
            this.pettyCashRequestForm.patchValue ({
              
              empId: this.data["id"],
              fullName: this.data["fname"] +" "+ this.data["lname"],
              firstName: this.data["fname"],
              lastName:this.data["lname"],
              department: this.data["departmentName"],
 
    
              branch_Dept: this.data["branch_Dept"],
              contactNo:  this.data["telno"],
              emailAddress:  this.data["emaladd"],
              status: "Pending",
              TotalAmount: 0,
              accountName : "",
              PreparedBy: this.data["fname"] +" "+ this.data["lname"],
              AmountInWord: converter.toWords(0),

   
            });
        });  

    this._requestService.action = "Tblaccounttitlebranches/"
    this._requestService.getRequest().subscribe(data => { this.accountTitleBranches = data
      var flags = [], output = [], l = data.length, i;
      for( i=0; i<l; i++) {
          if( flags[data[i].company]) continue;
          flags[data[i].company] = true;
          output.push(data[i].company);
      }
      console.log(this.company = output)
    })
    this._requestService.action = "Tblpayees/"
    this._requestService.getRequest().subscribe(data => { this.payeesList = data})

    // this._requestService.action = "Tblaccounttitles/"
    // this._requestService.getRequest().subscribe(data => { this.accountTitle = data})
  }
  // getApprover(){
 
  //   this._requestService.action="tbusers"
  //   this._requestService.getRequest().subscribe(data =>{
  //      let cntr = 0, cntr1 =0
  //      for( let key in data.sort((a, b) => (a.fullName > b.fullName) ? 1 : -1)){
  //        if(data[key].access.toLowerCase() == "head"){
  //         this.approverList[0].data[cntr] = (data[key])  
  //         cntr++
  //        }    
  //       //  else if(data[key].access.toLowerCase() == "mgmt"){
  //       //   this.approverList[1].data[cntr1] = (data[key])  
  //       //   cntr1++
  //       //  }  
  //      }
  //      console.log(this.approverList);
  //     })
  // }

getPROrderItem(id : number, data : any){

  for(let i = 0; i < data.orderItems.length; i++){
    this.PCOrderItems.patchValue({
      DateCreated: data["dateCreated"],
      
      imageName: data.orderItems[i]["imageName"],
      accountTitleId: data.orderItems[i]["accountTitleId"],
      accountTitle: data.orderItems[i]["accountTitle"],
      Particulars: data.orderItems[i]["particulars"],
      Amount:  data.orderItems[i]["amount"],

      PettyCashId: id,
    })

    let formData = this.PCOrderItems.value
    
    this._requestService.action = "PettyCashOrders"
    this._requestService.newRecord(formData)
    .subscribe(successCode => { 
  
      console.log(successCode) 
      this.successMessage = "Successfully Created"
      this.PCOrderItems.reset();
    })
  
    console.log(this.PCOrderItems.value)
  }
}
   
  createForms(){

    this.PCOrderItems = this.fb.group({

      DateCreated: this.myDate,

      PettyCashId: ['', Validators.required],
      imageName: [''],
      Amount:  ['', Validators.required],
      accountTitleId: [''],
      accountTitle: [''],
      Particulars: ['', Validators.required],
    })

    this.pettyCashRequestForm = this.fb.group({
      empId:['', Validators.required],
      fullName: [''],
      firstName:[''],
      lastName:['', Validators.required],
      dateCreated: this.myDate,
      pettyCashDate: ['', Validators.required],
      branch:  ['', Validators.required],
      department:[''],
      company:['', Validators.required],
      payee : ['', Validators.required],

      branch_Dept:[''],
      contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
      emailAddress:  ['', Validators.required],

      accountName:[''],
      TotalAmount: ['', Validators.required],
      AmountInWord:  ['', Validators.required],
      Classification: ['', Validators.required],
      DivourcherId:  ['', Validators.required],

      imageName: [''],
      Particulars: ['', Validators.required],
      Amount: ['', Validators.required],
      accountTitle: [''],
      accountTitleId: [''],

      orderItems : this.fb.array([])	,	

      Reason:  ['', Validators.required],
      comment: [''],
      status: [''],

      PreparedBy: [''],
      CheckedBy:  [''],
      ApprovedBy: [''],
      AuditBy:  [''],

      
    });
  }
}
   