import {Component, OnInit, ViewChild } from '@angular/core';
import { RequestService } from 'app/Services/request.service';
import { NgForm, Validators } from '@angular/forms';
import { FormControl, FormGroup, FormBuilder, FormArray} from '@angular/forms';
import { Router } from '@angular/router'
import { EnumsService } from 'app/Services/common/enums.service';

@Component({
  selector: 'app-uniform-request',
  templateUrl: './uniform-request.component.html',
  styleUrls: ['./uniform-request.component.css']
})
export class UniformRequestComponent implements OnInit {

  title = "Uniform Request"
  myDate = new Date();
  successMessage : any;
  data: any ;
  uniformTypes : any =[];
  orderItems :any = [];
  shoeSizes : any = [40, 41, 42, 43, 44]
  selectedIndex: any;

  approverList: any = [
    {
      name:'Approver- Head',
      data :[]
    },
    { 
      name:'',
      data:[]
    }
  ];
  UROrderItems : FormGroup;
  uniformRequestForm : FormGroup;

  

  constructor(private _requestService : RequestService, private _enums : EnumsService, private fb: FormBuilder) { 

    this.load()


 
  }
  getIndex(e){
    // console.log(e.selectedIndex == 1)
    console.log(e.selectedIndex == 1)
    //Remove Item,ItemDesc,Quantity Validation when user's on last step to pass data
    if(e.selectedIndex == 1){
      
      this.uniformRequestForm.controls['uniformType'].setValidators([])
      this.uniformRequestForm.controls['gender'].setValidators([])
      this.uniformRequestForm.controls['replacementOrAdditional'].setValidators([])
      this.uniformRequestForm.controls['size'].setValidators([])
      this.uniformRequestForm.controls['quantity'].setValidators([])
      this.uniformRequestForm.controls['unitMeasure'].setValidators([])

      this.uniformRequestForm.controls['uniformType'].updateValueAndValidity()
      this.uniformRequestForm.controls['gender'].updateValueAndValidity()
      this.uniformRequestForm.controls['replacementOrAdditional'].updateValueAndValidity()
      this.uniformRequestForm.controls['size'].updateValueAndValidity()
      this.uniformRequestForm.controls['quantity'].updateValueAndValidity()
      this.uniformRequestForm.controls['unitMeasure'].updateValueAndValidity()
    }else{
      this.uniformRequestForm.controls['uniformType'].setValidators([Validators.required])
      this.uniformRequestForm.controls['gender'].setValidators([Validators.required])
      this.uniformRequestForm.controls['replacementOrAdditional'].setValidators([Validators.required])
      this.uniformRequestForm.controls['size'].setValidators([Validators.required])
      this.uniformRequestForm.controls['quantity'].setValidators([Validators.required])
      this.uniformRequestForm.controls['unitMeasure'].setValidators([Validators.required])

      this.uniformRequestForm.controls['uniformType'].updateValueAndValidity()
      this.uniformRequestForm.controls['gender'].updateValueAndValidity()
      this.uniformRequestForm.controls['replacementOrAdditional'].updateValueAndValidity()
      this.uniformRequestForm.controls['size'].updateValueAndValidity()
      this.uniformRequestForm.controls['quantity'].updateValueAndValidity()
      this.uniformRequestForm.controls['unitMeasure'].updateValueAndValidity()
    }

  }

  onSubmit(e){

    let aprrv =this.uniformRequestForm.value.approver
    // let Approver
    // for( let key in aprrv){
    //     console.log(Approver =aprrv[0]+ " , "+aprrv[1])
        
    // }
    this.uniformRequestForm.value.approver = aprrv
    let formData = this.uniformRequestForm.value;
    console.log(formData);

   

    this._requestService.action = "UniformRequests"
    this._requestService.newRecord(formData)
    .subscribe(successCode => { 

      this.getPROrderItem(successCode["id"], formData);
      this.successMessage = "Successfully Created"
      console.log(successCode) 
     
      this.uniformRequestForm.reset();
      this.orderItems =null
      
    })
  }

  AddItem(e){

      // control refers to your formarray
      const control = <FormArray>this.uniformRequestForm.controls['orderItems'];
      this.orderItems.push(this.uniformRequestForm.value)

      let index = this.orderItems.length -1

      this.uniformRequestForm.controls.uniformType.reset();
      // this.uniformRequestForm.controls.replacementOrAdditional.reset();
      this.uniformRequestForm.controls.size.reset();
      this.uniformRequestForm.controls.quantity.reset();
      this.uniformRequestForm.controls.unitMeasure.reset();


      let orderitems = this.fb.group({
        
        Gender : new FormControl(this.orderItems[index].gender,Validators.required),
        UniformType : new FormControl(this.orderItems[index].uniformType,Validators.required),
        Size : new FormControl(this.orderItems[index].size,Validators.required),
        Quantity : new FormControl(this.orderItems[index].quantity,Validators.required),
        UnitMeasure : new FormControl(this.orderItems[index].unitMeasure,Validators.required),
        
      })
      
      // add new formgroup
      control.push(orderitems);
      console.log(control.value)
    

  }
  deleteRow(index: number) {
  
    // control refers to your formarray
    const control = <FormArray>this.uniformRequestForm.controls['orderItems'];
    // remove the chosen row
    this.orderItems.splice(index,1);
    control.removeAt(index);
    console.log(index, control.value)
}
  onChange(){
    this.uniformRequestForm.get('contactNo').value
    .subscribe(val => {
      console.log(val)
      if (val != null) {    
        this.uniformRequestForm.get('contactNo').disable();
    }
    })
  }

  private load(): void {
    
		// this._enums.getCommonList("leavetypes",true).subscribe(ddl => this.leavetypes = ddl);  
    this._enums.getCommonList("UniformTypeEnum",true).subscribe(ddl => this.uniformTypes = ddl);  
  }
  
  ngOnInit() {
    //Extract Data on LocalStorage
    this.createForms();
    let id = JSON.parse(localStorage.getItem("currentUser")).empId

    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id)
        .subscribe(data =>  {
            console.log("datos! ",this.data = data)
        
            this.uniformRequestForm.patchValue ({
              
              empId: this.data["id"],
              fullName: this.data["fname"] +" "+ this.data["lname"],
              firstName: this.data["fname"],
              lastName:this.data["lname"],
              branch:  this.data["branchName"],
              department: this.data["departmentName"],
              position: this.data["positionName"],
              employeeStatus : this.data["regstatus"],
              gender : this.data["gender"],

              branch_Dept: this.data["branch_Dept"],
              biometricId:  this.data["bio"],
              contactNo:  this.data["telno"],
              emailAddress:  this.data["emaladd"],
              status: "Pending",
              approver :"568" //Jennifer Mallorca only Uniform order approver empId:582 id:568/tbusers table
            });
        });  

        this.getApprover();
  }
  getApprover(){

    this._requestService.action="tbusers"
    this._requestService.getRequest().subscribe(data =>{
       let cntr = 0, cntr1 =0
       for( let key in data.sort((a, b) => (a.fullName > b.fullName) ? 1 : -1)){
         if(data[key].access.toLowerCase() == "head"){
          this.approverList[0].data[cntr] = (data[key])  
          cntr++
         }    
        //  else if(data[key].access.toLowerCase() == "mgmt"){
        //   this.approverList[1].data[cntr1] = (data[key])  
        //   cntr1++
        //  }  
       }
       console.log(this.approverList);
      })
  }
getPROrderItem(id : number, data : any){

  for(let i = 0; i < data.orderItems.length; i++){
    this.UROrderItems.patchValue({
      empId: data["empId"],
      DateCreated: data["dateCreated"],

      Gender : data.orderItems[i]["Gender"],
      UniformType: data.orderItems[i]["UniformType"],
      Size:  data.orderItems[i]["Size"],
      Quantity: data.orderItems[i]["Quantity"],
      UnitMeasure: data.orderItems[i]["UnitMeasure"],

      UniformRequestId: id,
      Status: data["status"],
    })

    let formData = this.UROrderItems.value
    
    this._requestService.action = "UniformOrders"
    this._requestService.newRecord(formData)
    .subscribe(successCode => { 
  
      console.log(successCode) 
      this.UROrderItems.reset();
    })
  
    console.log(this.UROrderItems.value)
  }
}
   
createForms(){

  this.UROrderItems = this.fb.group({
    empId: ['', Validators.required],
    DateCreated: this.myDate,

    Gender: ['', Validators.required],
    Size: ['', Validators.required],
    UniformType:  ['', Validators.required],
    Quantity: ['', Validators.required],
    UnitMeasure : ['', Validators.required],

   UniformRequestId: ['', Validators.required],
    Status: ['', Validators.required],
  })

  this.uniformRequestForm = this.fb.group({
    empId:['', Validators.required],
    fullName: [''],
    firstName:[''],
    lastName:['', Validators.required],
    dateCreated: this.myDate,
    branch:  ['', Validators.required],
    department:[''],

    branch_Dept:[''],
    biometricId:  ['', Validators.required],
    contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
    emailAddress:  ['', Validators.required],
    position:  ['', Validators.required],

    employeeStatus:  ['', Validators.required],
    replacementOrAdditional:  ['', Validators.required],
    purpose:  ['', Validators.required],

    gender : ['', Validators.required],
    uniformType: ['', Validators.required],
    size:  ['', Validators.required],
    quantity:  ['', Validators.required],
    unitMeasure : ['', Validators.required],

    orderItems : this.fb.array([])	,	

    comment: [''],
    requestingParty: [''],
    status: [''],

    approver: ['']
    
  });
}
}
   