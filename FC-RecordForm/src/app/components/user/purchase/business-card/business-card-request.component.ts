import { Component, OnInit } from '@angular/core';
import { RequestService } from 'app/Services/request.service';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { EnumsService } from 'app/Services/common/enums.service';
import { CustomValidator } from 'app/models/validators/custom.validator';

@Component({
  selector: 'app-business-card-request',
  templateUrl: './business-card-request.component.html',
  styleUrls: ['./business-card-request.component.css']
})
export class BusinessCardRequestComponent implements OnInit {

  title = "Business Card Request"
  myDate = new Date();
  successMessage : any;
  data: any ;
  leavetypes : any =[];
  approverList: any = [
    {
      name:'Approver- Head',
      data :[]
    },
    { 
      name:'',
      data:[]
    }
  ];

  businessCardRequestFrom = this.fb.group({
    empId:['', Validators.required],
    fullName: [''],
    firstName:[''],
    lastName:['', Validators.required],
    dateCreated: this.myDate,
    branch:  ['', Validators.required],
    department:[''],

    branch_Dept:[''],
    position:  ['', Validators.required],
    company:  ['', Validators.required],
    contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
    emailAddress:  ['',Validators.compose([Validators.required,CustomValidator.emailAddress])],
    address: ['', Validators.required],

    brand: ['', Validators.required],
    NameonCard: ['', Validators.required],
    landline: ['', Validators.required],
    mobile: ['', Validators.required],
    quantity: ['', Validators.required],

    purpose:  ['', Validators.required],
    comment: [''],
    status: [''],

    approver: ['']
    
  });


  constructor(private _requestService : RequestService, private _enums : EnumsService, private fb: FormBuilder) { 

    this.load()
  }
    

  onSubmit(e){

    let aprrv =this.businessCardRequestFrom.value.approver

    // let Approver
    // for( let key in aprrv){
    //     console.log(Approver =aprrv[0]+ " , "+aprrv[1])
        
    // }
    this.businessCardRequestFrom.value.approver = aprrv
    let formData = this.businessCardRequestFrom.value;
    console.log(formData);

    this._requestService.action = "BusinessCardRequests"
    this._requestService.newRecord(formData)
    .subscribe(successCode => {
      this.successMessage = "Successfully Created"
      console.log(successCode)
      this.businessCardRequestFrom.reset();
      
      
    })
  }

  onChange(){
    this.businessCardRequestFrom.get('contactNo').value
    .subscribe(val => {
      console.log(val)
      if (val != null) {    
        this.businessCardRequestFrom.get('contactNo').disable();
    }
    })
  }

  private load(): void {

		this._enums.getCommonList("leavetypes",true).subscribe(ddl => this.leavetypes = ddl);  

  }
  
  ngOnInit() {
    //Extract Data on LocalStorage
  
    let id = JSON.parse(localStorage.getItem("currentUser")).empId

    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id)
        .subscribe(data =>  {
            console.log("datos! ",this.data = data)
        
            this.businessCardRequestFrom.patchValue ({
              
              empId: this.data["id"],
              fullName: this.data["fname"] +" "+ this.data["lname"],
              firstName: this.data["fname"],
              lastName:this.data["lname"],
              branch:  this.data["branchName"],
              department: this.data["departmentName"],
              address: this.data["address"],
              mobile: this.data["telno"],
              NameonCard: this.data["fname"] +" "+ this.data["mname"] +" "+ this.data["lname"],

              branch_Dept: this.data["branch_Dept"],
              position:  this.data["positionName"],
              company:  this.data["companyName"],
              contactNo:  this.data["telno"],
              emailAddress:  this.data["emaladd"],
              status: "Pending",
              approver :"457" //Anna Isabel Ferrer only Purchase Request order approver empId:471 id:457/tbusers table
            });
        });  

        this.getApprover();
  }
  getApprover(){

    this._requestService.action="tbusers"
    this._requestService.getRequest().subscribe(data =>{
       let cntr = 0, cntr1 =0
       for( let key in data.sort((a, b) => (a.fullName > b.fullName) ? 1 : -1)){
         if(data[key].access.toLowerCase() == "head"){
          this.approverList[0].data[cntr] = (data[key])  
          cntr++
         }    
        //  else if(data[key].access.toLowerCase() == "mgmt"){
        //   this.approverList[1].data[cntr1] = (data[key])  
        //   cntr1++
        //  }  
       }
       console.log(this.approverList);
      })
  }

}
