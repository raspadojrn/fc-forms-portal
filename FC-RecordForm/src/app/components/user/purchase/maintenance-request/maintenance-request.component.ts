import { Component, OnInit } from '@angular/core';
import { RequestService } from 'app/Services/request.service';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { EnumsService } from 'app/Services/common/enums.service';
import { CustomValidator } from 'app/models/validators/custom.validator';
import { Ng2ImgMaxService } from 'ng2-img-max';

@Component({
  selector: 'app-maintenance-request',
  templateUrl: './maintenance-request.component.html',
  styleUrls: ['./maintenance-request.component.css']
})
export class MaintenanceRequestComponent implements OnInit {

  title = "Maintenance Request"
  myDate = new Date();
  successMessage : any;
  data: any ;
  url: any;
  
  imagePath : string = null;
  imageErrorMessage : string;
  uploadedImage: File;
  
  approverList: any = [
    {
      name:'Approver- Head',
      data :[]
    },
    { 
      name:'',
      data:[]
    }
  ];

  maintenanceRequestFrom = this.fb.group({
    empId:['', Validators.required],
    fullName: [''],
    firstName:[''],
    lastName:['', Validators.required],
    dateCreated: this.myDate,
    branch:  ['', Validators.required],
    department:[''],

    branch_Dept:[''],
    position:  ['', Validators.required],
    company:  ['', Validators.required],
    contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
    emailAddress:  ['',Validators.compose([Validators.required,CustomValidator.emailAddress])],
    
    imageName: [''],
    requestType: [''],
    natureOfWork: ['', Validators.required],
    dateNeeded: ['', Validators.required],

    comment: [''],
    status: [''],

    approver: ['']
    
  });


  constructor(private _requestService : RequestService, private _enums : EnumsService, private fb: FormBuilder, private ng2ImgMax: Ng2ImgMaxService) { }
    

  onSelectFile1(event) { // called each time file input changes
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        console.log(event)
        reader.readAsDataURL(event.target.files[0]); // read file as data url

        reader.onload = (e) => { // called once readAsDataURL is completed
          let target: any = e.target;
          this.url = target.result
        }
   
      }
   
  }

onSelectFile(event) {
  this.onSelectFile1(event)
  if(event.target.files.length > 0) {
    let file = event.target.files[0];


    if (file.type.includes("image")) {
        if (file["size"] <= 7000000) {


            // this.isUploading = true;
            // this.imagePath = "assets/images/spinner.gif";

            this.ng2ImgMax.resizeImage(file, 800, 750).subscribe(
              result => {
            
                this.uploadedImage = new File([result], result.name);
            
                // If image > 1.2mb save resized image else save uploaded.
                this.uploadedImage = (file["size"] >= 1300000) ? this.uploadedImage : file;
                console.log(this.uploadedImage)
                this._requestService
                    .uploadImage(this.uploadedImage)
                    .subscribe(res => {
                        this.imagePath = "assets/images/request-images/" + file.name + "?_=" + Date.now();
                        // this.isUploading = false;
                });

                this.maintenanceRequestFrom.get('imageName').setValue(file.name); 
                this.imageErrorMessage = null; 
              },
              error => {
                console.log(error);
              }
            );

        }
        else {
            this.imageErrorMessage = "Image too large (Maximum Size of 7MB only)";
        }
    }
    else {
      this.imageErrorMessage = "Invalid File Format";
    }
  }
  else {
    this.maintenanceRequestFrom.get('imageName').setValue(''); 
  }
}

  onSubmit(e){
    
    let aprrv =this.maintenanceRequestFrom.value.approver

    this.maintenanceRequestFrom.value.approver = aprrv
    let formData = this.maintenanceRequestFrom.value;
    console.log(formData);

    this._requestService.action = "MaintenanceRequests"
    this._requestService.newRecord(formData)
    .subscribe(successCode => {
      this.successMessage = "Successfully Created"
      console.log(successCode)
      this.maintenanceRequestFrom.reset();
      
      
    })
  }

  onChange(){
    this.maintenanceRequestFrom.get('contactNo').value
    .subscribe(val => {
      console.log(val)
      if (val != null) {    
        this.maintenanceRequestFrom.get('contactNo').disable();
    }
    })
  }
  
  ngOnInit() {
    //Extract Data on LocalStorage
  
    let id = JSON.parse(localStorage.getItem("currentUser")).empId

    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id)
        .subscribe(data =>  {
            console.log("datos! ",this.data = data)
        
            this.maintenanceRequestFrom.patchValue ({
              
              empId: this.data["id"],
              fullName: this.data["fname"] +" "+ this.data["lname"],
              firstName: this.data["fname"],
              lastName:this.data["lname"],
              branch:  this.data["branchName"],
              department: this.data["departmentName"],
              address: this.data["address"],
              mobile: this.data["telno"],
              NameonCard: this.data["fname"] +" "+ this.data["mname"] +" "+ this.data["lname"],

              branch_Dept: this.data["branch_Dept"],
              position:  this.data["positionName"],
              company:  this.data["companyName"],
              contactNo:  this.data["telno"],
              emailAddress:  this.data["emaladd"],
              status: "Pending",
              approver :"810" //Faiyaz Hossein only maintenance/job order approver empId:843 id:810/tbusers table
            });
        });  

        this.getApprover();
  }
  getApprover(){

    this._requestService.action="tbusers"
    this._requestService.getRequest().subscribe(data =>{
       let cntr = 0, cntr1 =0
       for( let key in data.sort((a, b) => (a.fullName > b.fullName) ? 1 : -1)){
         if(data[key].access.toLowerCase() == "head"){
          this.approverList[0].data[cntr] = (data[key])  
          cntr++
         }    
        //  else if(data[key].access.toLowerCase() == "mgmt"){
        //   this.approverList[1].data[cntr1] = (data[key])  
        //   cntr1++
        //  }  
       }
       console.log(this.approverList);
      })
  }

}
