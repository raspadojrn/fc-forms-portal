import { Component, OnInit } from '@angular/core';
import { RequestService } from 'app/Services/request.service';
import { FormBuilder, Validators, FormControl, FormGroup, FormArray } from '@angular/forms';
import { EnumsService } from 'app/Services/common/enums.service';
import { CustomValidator } from 'app/models/validators/custom.validator';
import { Ng2ImgMaxService } from 'ng2-img-max';

@Component({
  selector: 'app-marketing-material',
  templateUrl: './marketing-material.component.html',
  styleUrls: ['./marketing-material.component.css']
})
export class MarketingMaterialComponent implements OnInit {

  title = "Marketing Materials and Display Change Request"
  myDate = new Date();
  successMessage : any;
  data: any ;
  requesttypes : any =[];
  printedCatalogueList : any [];
  giveAwayList : any = [];
  sampleSwatcheslist : any = [];

  itemlist : any =[]
  orderItems :any = [];
  haveItemList : boolean= false ;

  url: any;
  imagePath : string = null;
  imageErrorMessage : string;
  uploadedImage: File;
  
  approverList: any = [
    {
      name:'Approver- Head',
      data :[]
    },
    { 
      name:'',
      data:[]
    }
  ];
  MROrderItems : FormGroup;
  marketingMaterialRequestFrom : FormGroup;
 


  constructor(private _requestService : RequestService, private _enums : EnumsService, private fb: FormBuilder, private ng2ImgMax: Ng2ImgMaxService) { 

    this.load()
  }
  onSelectFile1(event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      console.log(event)
      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (e) => { // called once readAsDataURL is completed
        let target: any = e.target;
        this.url = target.result
      }
 
    }
 
}

onSelectFile(event) {
this.onSelectFile1(event)
if(event.target.files.length > 0) {
  let file = event.target.files[0];


  if (file.type.includes("image")) {
      if (file["size"] <= 7000000) {


          // this.isUploading = true;
          // this.imagePath = "assets/images/spinner.gif";

          this.ng2ImgMax.resizeImage(file, 800, 750).subscribe(
            result => {
          
              this.uploadedImage = new File([result], result.name);
          
              // If image > 1.2mb save resized image else save uploaded.
              this.uploadedImage = (file["size"] >= 1300000) ? this.uploadedImage : file;
              console.log(this.uploadedImage)
              this._requestService
                  .uploadImage(this.uploadedImage)
                  .subscribe(res => {
                      this.imagePath = "assets/images/request-images/" + file.name + "?_=" + Date.now();
                      // this.isUploading = false;
              });

              this.marketingMaterialRequestFrom.get('imageName').setValue(file.name); 
              this.imageErrorMessage = null; 
            },
            error => {
              console.log(error);
            }
          );

      }
      else {
          this.imageErrorMessage = "Image too large (Maximum Size of 7MB only)";
      }
  }
  else {
    this.imageErrorMessage = "Invalid File Format";
  }
}
else {
  this.marketingMaterialRequestFrom.get('imageName').setValue(''); 
}
}

  getIndex(e){
    // console.log(e.selectedIndex == 1)
    console.log(e.selectedIndex == 1)
    //Remove Item,ItemDesc,Quantity Validation when user's on last step to pass data
    if(e.selectedIndex == 1){

      this.marketingMaterialRequestFrom.controls['requestType'].setValidators([])
      this.marketingMaterialRequestFrom.controls['Item'].setValidators([])
      this.marketingMaterialRequestFrom.controls['Description'].setValidators([])
      this.marketingMaterialRequestFrom.controls['Quantity'].setValidators([])
      this.marketingMaterialRequestFrom.controls['unitMeasure'].setValidators([])

      
      this.marketingMaterialRequestFrom.controls['requestType'].updateValueAndValidity()
      this.marketingMaterialRequestFrom.controls['Item'].updateValueAndValidity()
      this.marketingMaterialRequestFrom.controls['Description'].updateValueAndValidity()
      this.marketingMaterialRequestFrom.controls['Quantity'].updateValueAndValidity()
      this.marketingMaterialRequestFrom.controls['unitMeasure'].updateValueAndValidity()
    }else{
      this.marketingMaterialRequestFrom.controls['requestType'].setValidators([Validators.required])
      this.marketingMaterialRequestFrom.controls['Item'].setValidators([Validators.required])
      this.marketingMaterialRequestFrom.controls['Description'].setValidators([Validators.required])
      this.marketingMaterialRequestFrom.controls['Quantity'].setValidators([Validators.required])
      this.marketingMaterialRequestFrom.controls['unitMeasure'].setValidators([Validators.required])
      
      this.marketingMaterialRequestFrom.controls['requestType'].updateValueAndValidity()
      this.marketingMaterialRequestFrom.controls['Item'].updateValueAndValidity()
      this.marketingMaterialRequestFrom.controls['Description'].updateValueAndValidity()
      this.marketingMaterialRequestFrom.controls['Quantity'].updateValueAndValidity()
      this.marketingMaterialRequestFrom.controls['nitMeasure'].updateValueAndValidity()
    }

  }

  onSubmit(e){

    let aprrv =this.marketingMaterialRequestFrom.value.approver

    this.marketingMaterialRequestFrom.value.approver = aprrv
    let formData = this.marketingMaterialRequestFrom.value;
    console.log(formData);

    this._requestService.action = "MarketingMaterialRequests"
    this._requestService.newRecord(formData)
    .subscribe(successCode => {

      this.getPROrderItem(successCode["id"], formData);
      this.successMessage = "Successfully Created"
      console.log(successCode) 
     
      this.marketingMaterialRequestFrom.reset();
      this.orderItems =null
      console.log(successCode)

    })
  }

  AddItem(e){

  //  console.log(this.MarketingReqType =this.marketingMaterialRequestFrom.controls.requestType.value)
    // control refers to your formarray
    const control = <FormArray>this.marketingMaterialRequestFrom.controls['orderItems'];
    this.orderItems.push(this.marketingMaterialRequestFrom.value)
    console.log(control, this.orderItems)
    let index = this.orderItems.length -1

    this.marketingMaterialRequestFrom.controls.requestType.reset();
    this.marketingMaterialRequestFrom.controls.Item.reset();
    this.marketingMaterialRequestFrom.controls.Description.reset();
    this.marketingMaterialRequestFrom.controls.Quantity.reset();
    this.marketingMaterialRequestFrom.controls.unitMeasure.reset();

    this.itemlist = null;
    this.haveItemList = false;

    let orderitems = this.fb.group({
      requestType : new FormControl(this.orderItems[index].requestType,Validators.required),
      item : new FormControl(this.orderItems[index].Item,Validators.required),
      description : new FormControl(this.orderItems[index].Description,Validators.required),
      quantity : new FormControl(this.orderItems[index].Quantity,Validators.required),
      unitMeasure : new FormControl(this.orderItems[index].unitMeasure,Validators.required),
    })
    
    // add new formgroup
    control.push(orderitems);
    console.log(control.value)

}
deleteRow(index: number) {
  
  // control refers to your formarray
  const control = <FormArray>this.marketingMaterialRequestFrom.controls['orderItems'];
  // remove the chosen row
  this.orderItems.splice(index,1);
  control.removeAt(index);
  console.log(index, control.value)
}

  onChange(e){

    console.log(e)
    if(e =="Printed Catalogue"){
      this.marketingMaterialRequestFrom.controls['itemDescription'].setValidators([])
      this.marketingMaterialRequestFrom.controls['itemDescription'].updateValueAndValidity()

      this.itemlist = this.printedCatalogueList;
      console.log(this.itemlist)
      this.haveItemList = true
    }
    else if(e =="Giveaways"){
      this.marketingMaterialRequestFrom.controls['itemDescription'].setValidators([])
      this.marketingMaterialRequestFrom.controls['itemDescription'].updateValueAndValidity()
      this.itemlist = this.giveAwayList;
      console.log(this.itemlist)
      this.haveItemList = true
    }
    else if(e =="Sample Swatches"){
      this.marketingMaterialRequestFrom.controls['itemDescription'].setValidators([])
      this.marketingMaterialRequestFrom.controls['itemDescription'].updateValueAndValidity()
      this.itemlist = this.sampleSwatcheslist;
      console.log(this.itemlist)
      this.haveItemList = true
    }
    else{
      this.marketingMaterialRequestFrom.controls['itemDescription'].setValidators([Validators.required])
      this.marketingMaterialRequestFrom.controls['itemDescription'].updateValueAndValidity()
      this.haveItemList = false;
    }
  }

  private load(): void {

		this._enums.getCommonList("MarketingRequestTypeEnum",true).subscribe(ddl => this.requesttypes = ddl);  
    this._enums.getCommonList("MarketingPrintedCatalogueEnum",true).subscribe(ddl => this.printedCatalogueList = ddl);  
    this._enums.getCommonList("MarketingGiveAwaysEnum",true).subscribe(ddl => this.giveAwayList = ddl);  
    this._enums.getCommonList("MarketingSampleSwatchesEnum",true).subscribe(ddl => this.sampleSwatcheslist = ddl); 
  }
  
  ngOnInit() {
    //Extract Data on LocalStorage
    this.createForms();
    let id = JSON.parse(localStorage.getItem("currentUser")).empId

    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id)
        .subscribe(data =>  {
            console.log("datos! ",this.data = data)
        
            this.marketingMaterialRequestFrom.patchValue ({
              
              empId: this.data["id"],
              fullName: this.data["fname"] +" "+ this.data["lname"],
              firstName: this.data["fname"],
              lastName:this.data["lname"],
              branch:  this.data["branchName"],
              department: this.data["departmentName"],
              address: this.data["address"],
              mobile: this.data["telno"],
              NameonCard: this.data["fname"] +" "+ this.data["mname"] +" "+ this.data["lname"],

              branch_Dept: this.data["branch_Dept"],
              position:  this.data["positionName"],
              company:  this.data["companyName"],
              contactNo:  this.data["telno"],
              emailAddress:  this.data["emaladd"],
              status: "Pending",
              approver :"437" //Israel Doton only Marketing MAterial order approver empId:451 id:437/tbusers table
            });
        });  

        this.getApprover();
  }
  getApprover(){

    this._requestService.action="tbusers"
    this._requestService.getRequest().subscribe(data =>{
       let cntr = 0, cntr1 =0
       for( let key in data.sort((a, b) => (a.fullName > b.fullName) ? 1 : -1)){
         if(data[key].access.toLowerCase() == "head"){
          this.approverList[0].data[cntr] = (data[key])  
          cntr++
         }    
        //  else if(data[key].access.toLowerCase() == "mgmt"){
        //   this.approverList[1].data[cntr1] = (data[key])  
        //   cntr1++
        //  }  
       }
       console.log(this.approverList);
      })
  }
  getPROrderItem(id : number, data : any){

    for(let i = 0; i < data.orderItems.length; i++){
      this.MROrderItems.patchValue({
        empId: data["empId"],
        DateCreated: data["dateCreated"],
        
        RequestType: data.orderItems[i]["requestType"],
        Item: data.orderItems[i]["item"],
        Description:  data.orderItems[i]["description"],
        Quantity: data.orderItems[i]["quantity"],
        UnitMeasure: data.orderItems[i]["unitMeasure"],
  
        MarketingRequestId: id,
        Status: data["status"],
      })
  
      let formData = this.MROrderItems.value
      
      this._requestService.action = "MarketingOrders"
      this._requestService.newRecord(formData)
      .subscribe(successCode => { 
    
        console.log(successCode) 
        this.MROrderItems.reset();
      })
    
      console.log(this.MROrderItems.value)
    }
  } 
  createForms(){

    this.MROrderItems = this.fb.group({
      empId: ['', Validators.required],
      DateCreated: this.myDate,

      RequestType: ['', Validators.required],
      Item: ['', Validators.required],
      Description:  ['', Validators.required],
      Quantity: ['', Validators.required],
      UnitMeasure: ['', Validators.required],

      MarketingRequestId: ['', Validators.required],
      Status: ['', Validators.required],
    })

    this.marketingMaterialRequestFrom = this.fb.group({
      empId:['', Validators.required],
      fullName: [''],
      firstName:[''],
      lastName:['', Validators.required],
      dateCreated: this.myDate,
      branch:  ['', Validators.required],
      department:[''],

      branch_Dept:[''],
      position:  ['', Validators.required],
      company:  ['', Validators.required],
      contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
      emailAddress:  ['',Validators.compose([Validators.required,CustomValidator.emailAddress])],
      
      requestType: ['', Validators.required],
      imageName: [''],
      Item: ['', Validators.required],
      Description:  ['', Validators.required],
      Quantity: ['', Validators.required],
      unitMeasure: ['', Validators.required],

      itemDescription: ['', Validators.required],
      dateNeeded: ['', Validators.required],

      orderItems : this.fb.array([]),	

      comment: [''],
      status: [''],

      approver: ['']
      
    });
  }

}
