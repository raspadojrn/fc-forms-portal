import {Component, OnInit, ViewChild } from '@angular/core';
import { RequestService } from 'app/Services/request.service';
import { NgForm, Validators } from '@angular/forms';
import { FormControl, FormGroup, FormBuilder, FormArray} from '@angular/forms';
import { Router } from '@angular/router'
import { EnumsService } from 'app/Services/common/enums.service';


@Component({
  selector: 'app-booklets-request',
  templateUrl: './booklets-request.component.html',
  styleUrls: ['./booklets-request.component.css']
})
export class BookletsRequestComponent implements OnInit {

  title = "Manual/POS Request Form"

  myDate = new Date();
  successMessage : any;
  data: any ;
  bookletRequest : any =[];
  manualtype : any [];

  orderItems :any = [];

  selectedIndex: any;
  
  approverList: any = [
    {
      name:'Approver- Head',
      data :[]
    },
    { 
      name:'',
      data:[]
    }
  ];
  BROrderItems : FormGroup;
  bookletRequestForm : FormGroup;

  

  constructor(private _requestService : RequestService, private _enums : EnumsService, private fb: FormBuilder) { 

    this.load()


 
  }
  getIndex(e){
    // console.log(e.selectedIndex == 1)
    console.log(e.selectedIndex == 1)
    //Remove Item,ItemDesc,Quantity Validation when user's on last step to pass data
    if(e.selectedIndex == 1){

      this.bookletRequestForm.controls['RequestType'].setValidators([])
      this.bookletRequestForm.controls['ManualType'].setValidators([])
      this.bookletRequestForm.controls['FromToMsiv'].setValidators([])
      this.bookletRequestForm.controls['unUsedMsiv'].setValidators([])
      this.bookletRequestForm.controls['Quantity'].setValidators([])

      
      this.bookletRequestForm.controls['RequestType'].updateValueAndValidity()
      this.bookletRequestForm.controls['ManualType'].updateValueAndValidity()
      this.bookletRequestForm.controls['FromToMsiv'].updateValueAndValidity()
      this.bookletRequestForm.controls['unUsedMsiv'].updateValueAndValidity()
      this.bookletRequestForm.controls['Quantity'].updateValueAndValidity()
    }else{
      this.bookletRequestForm.controls['RequestType'].setValidators([Validators.required])
      this.bookletRequestForm.controls['ManualType'].setValidators([Validators.required])
      this.bookletRequestForm.controls['FromToMsiv'].setValidators([Validators.required])
      this.bookletRequestForm.controls['unUsedMsiv'].setValidators([Validators.required])
      this.bookletRequestForm.controls['Quantity'].setValidators([Validators.required])
      
      this.bookletRequestForm.controls['RequestType'].updateValueAndValidity()
      this.bookletRequestForm.controls['ManualType'].updateValueAndValidity()
      this.bookletRequestForm.controls['FromToMsiv'].updateValueAndValidity()
      this.bookletRequestForm.controls['unUsedMsiv'].updateValueAndValidity()
      this.bookletRequestForm.controls['Quantity'].updateValueAndValidity()
    }

  }

  onSubmit(e){

    let aprrv =this.bookletRequestForm.value.approver
    // let Approver
    // for( let key in aprrv){
    //     console.log(Approver =aprrv[0]+ " , "+aprrv[1])
        
    // }
    this.bookletRequestForm.value.approver = aprrv
    let formData = this.bookletRequestForm.value;
    console.log(formData);

   

    this._requestService.action = "BookletRequests"
    this._requestService.newRecord(formData)
    .subscribe(successCode => { 

      this.getPROrderItem(successCode["id"], formData);
      this.successMessage = "Successfully Created"
      console.log(successCode) 
     
      this.bookletRequestForm.reset();
      this.orderItems =null
      
    })
  }

  AddItem(e){

   
      // control refers to your formarray
      const control = <FormArray>this.bookletRequestForm.controls['orderItems'];
      this.orderItems.push(this.bookletRequestForm.value)
      console.log(control, this.orderItems)
      let index = this.orderItems.length -1

      this.bookletRequestForm.controls.RequestType.reset();
      this.bookletRequestForm.controls.ManualType.reset();
      this.bookletRequestForm.controls.FromToMsiv.reset();
      this.bookletRequestForm.controls.unUsedMsiv.reset();
      this.bookletRequestForm.controls.Quantity.reset();

      let orderitems = this.fb.group({
        requestType : new FormControl(this.orderItems[index].RequestType,Validators.required),
        manualType : new FormControl(this.orderItems[index].ManualType,Validators.required),
        fromToMsiv : new FormControl(this.orderItems[index].FromToMsiv,Validators.required),
        unusedMsiv : new FormControl(this.orderItems[index].unUsedMsiv,Validators.required),
        quantity : new FormControl(this.orderItems[index].Quantity,Validators.required),
      })
      
      // add new formgroup
      control.push(orderitems);
      console.log(control.value)
    

  }
  deleteRow(index: number) {
  
    // control refers to your formarray
    const control = <FormArray>this.bookletRequestForm.controls['orderItems'];
    // remove the chosen row
    this.orderItems.splice(index,1);
    control.removeAt(index);
    console.log(index, control.value)
}
  onChange(){
    this.bookletRequestForm.get('contactNo').value
    .subscribe(val => {
      console.log(val)
      if (val != null) {    
        this.bookletRequestForm.get('contactNo').disable();
    }
    })
  }

  private load(): void {
    
		this._enums.getCommonList("BookletManualTypeEnum",true).subscribe(ddl => this.manualtype = ddl);  
    this._enums.getCommonList("BookletRequestEnum",true).subscribe(ddl => {
      ddl[11].name ="Thermal Paper (Credit Card)"
      this.bookletRequest = ddl
    });  
  }
  
  ngOnInit() {
    //Extract Data on LocalStorage
    this.createForms();
    let id = JSON.parse(localStorage.getItem("currentUser")).empId

    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id)
        .subscribe(data =>  {
            console.log("datos! ",this.data = data)
        
            this.bookletRequestForm.patchValue ({
              
              empId: this.data["id"],
              fullName: this.data["fname"] +" "+ this.data["lname"],
              firstName: this.data["fname"],
              lastName:this.data["lname"],
              branch:  this.data["branchName"],
              department: this.data["departmentName"],
              position: this.data["positionName"],

              branch_Dept: this.data["branch_Dept"],
              biometricId:  this.data["bio"],
              contactNo:  this.data["telno"],
              emailAddress:  this.data["emaladd"],
              status: "Pending",
              approver :"330" //Rosalie Bisda only booklet order approver empId:344 id:330/tbusers table
            });
        });  

        this.getApprover();
  }
  getApprover(){

    this._requestService.action="tbusers"
    this._requestService.getRequest().subscribe(data =>{
       let cntr = 0, cntr1 =0
       for( let key in data.sort((a, b) => (a.fullName > b.fullName) ? 1 : -1)){
         if(data[key].access.toLowerCase() == "head"){
          this.approverList[0].data[cntr] = (data[key])  
          cntr++
         }    
        //  else if(data[key].access.toLowerCase() == "mgmt"){
        //   this.approverList[1].data[cntr1] = (data[key])  
        //   cntr1++
        //  }  
       }
       console.log(this.approverList);
      })
  }

getPROrderItem(id : number, data : any){

  for(let i = 0; i < data.orderItems.length; i++){
    this.BROrderItems.patchValue({
      empId: data["empId"],
      DateCreated: data["dateCreated"],
      
      RequestType: data.orderItems[i]["requestType"],
      ManualType:  data.orderItems[i]["manualType"],
      FromToMsiv: data.orderItems[i]["fromToMsiv"],
      unUsedMsiv:  data.orderItems[i]["unusedMsiv"],
      Quantity: data.orderItems[i]["quantity"],

      BookletRequestId: id,
      Status: data["status"],
    })

    let formData = this.BROrderItems.value
    
    this._requestService.action = "OrderedBooklets"
    this._requestService.newRecord(formData)
    .subscribe(successCode => { 
  
      console.log(successCode) 
      this.BROrderItems.reset();
    })
  
    console.log(this.BROrderItems.value)
  }
}
   
  createForms(){

    this.BROrderItems = this.fb.group({
      empId: ['', Validators.required],
      DateCreated: this.myDate,

      RequestType: ['', Validators.required],
      ManualType:  ['', Validators.required],
      FromToMsiv: ['', Validators.required],
      unUsedMsiv:  ['', Validators.required],
      Quantity: ['', Validators.required],

      BookletRequestId: ['', Validators.required],
      Status: ['', Validators.required],
    })

    this.bookletRequestForm = this.fb.group({
      empId:['', Validators.required],
      fullName: [''],
      firstName:[''],
      lastName:['', Validators.required],
      dateCreated: this.myDate,
      branch:  ['', Validators.required],
      department:[''],

      branch_Dept:[''],
      position: ['', Validators.required],
      biometricId:  ['', Validators.required],
      contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
      emailAddress:  ['', Validators.required],

      purpose:  ['', Validators.required],

      RequestType: ['', Validators.required],
      ManualType:  ['', Validators.required],
      FromToMsiv: ['', Validators.required],
      unUsedMsiv:  ['', Validators.required],
      Quantity: ['', Validators.required],

      orderItems : this.fb.array([])	,	

      comment: [''],
      status: [''],

      approver: ['']
      
    });
  }
}
   