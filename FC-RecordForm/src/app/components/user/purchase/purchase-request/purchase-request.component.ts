import { Response } from '@angular/http';
import {Component, OnInit, ViewChild } from '@angular/core';
import { RequestService } from 'app/Services/request.service';
import { NgForm, Validators } from '@angular/forms';
import { FormControl, FormGroup, FormBuilder, FormArray} from '@angular/forms';
import { Router } from '@angular/router'
import { EnumsService } from 'app/Services/common/enums.service';
import { V } from '@angular/core/src/render3';


declare var $:any;

@Component({
  selector: 'app-purchase-request',
  templateUrl: './purchase-request.component.html',
  styleUrls: ['./purchase-request.component.css']
})
export class PurchaseRequestComponent implements OnInit {
  
  // @ViewChild('stepper') stepper;

  title = "Purchase Request"
  myDate = new Date();
  successMessage : any;
  data: any ;
  purchaseRequest : any =[];
  orderItems :any = [];

  selectedIndex: any;
  prevOrderItem: any=[]; 
  branch: any= [];
  isHrAcc: boolean = true;

  isMaintenancePR : boolean;
  approverList: any = [
    {
      name:'Approver- Head',
      data :[]
    },
    { 
      name:'',
      data:[]
    }
  ];
  PROrderItems : FormGroup;
  purchaseRequestForm : FormGroup;

  

  constructor(private _requestService : RequestService, private _enums : EnumsService, private fb: FormBuilder) { 

    this.load()


 
  }
  getIndex(e){
    // console.log(e.selectedIndex == 2)
    console.log(e.selectedIndex == 1)
    //Remove Item,ItemDesc,Quantity Validation when user's on last step to pass data
    if(e.selectedIndex == 1){
      this.purchaseRequestForm.controls['item'].setValidators([])
      this.purchaseRequestForm.controls['itemDesc'].setValidators([])
      this.purchaseRequestForm.controls['quantity'].setValidators([])
      this.purchaseRequestForm.controls['unitMeasure'].setValidators([])

      this.purchaseRequestForm.controls['item'].updateValueAndValidity()
      this.purchaseRequestForm.controls['itemDesc'].updateValueAndValidity()
      this.purchaseRequestForm.controls['quantity'].updateValueAndValidity()
      this.purchaseRequestForm.controls['unitMeasure'].updateValueAndValidity()
    }else{
      this.purchaseRequestForm.controls['item'].setValidators([Validators.required]) 
      this.purchaseRequestForm.controls['itemDesc'].setValidators([Validators.required])
      this.purchaseRequestForm.controls['quantity'].setValidators([Validators.required])
      this.purchaseRequestForm.controls['unitMeasure'].setValidators([Validators.required])

      this.purchaseRequestForm.controls['item'].updateValueAndValidity()
      this.purchaseRequestForm.controls['itemDesc'].updateValueAndValidity()
      this.purchaseRequestForm.controls['quantity'].updateValueAndValidity()
      this.purchaseRequestForm.controls['unitMeasure'].updateValueAndValidity()
    }

  }

  onSubmit(e){

    let aprrv =this.purchaseRequestForm.value.approver
    // let Approver
    // for( let key in aprrv){
    //     console.log(Approver =aprrv[0]+ " , "+aprrv[1])
        
    // }
    this.purchaseRequestForm.value.approver = aprrv
    let formData = this.purchaseRequestForm.value;
    console.log(formData);

   

    this._requestService.action = "PurchaseRequests"
    this._requestService.newRecord(formData)
    .subscribe(successCode => { 

      this.getPROrderItem(successCode["id"], formData);
      this.successMessage = "Successfully Created"
      console.log(successCode) 
     
      this.purchaseRequestForm.reset();
      this.orderItems =null
      
    })
  }

  AddItem(e){

      // control refers to your formarray
      const control = <FormArray>this.purchaseRequestForm.controls['orderItems'];
      this.orderItems.push(this.purchaseRequestForm.value)
      console.log(control, this.orderItems)
      let index = this.orderItems.length -1

      this.purchaseRequestForm.controls.item.reset();
      this.purchaseRequestForm.controls.itemDesc.reset();
      this.purchaseRequestForm.controls.quantity.reset();
      this.purchaseRequestForm.controls.unitMeasure.reset();

      let orderitems = this.fb.group({
        Item : new FormControl(this.orderItems[index].item,Validators.required),
        Description : new FormControl(this.orderItems[index].itemDesc,Validators.required),
        Quantity : new FormControl(this.orderItems[index].quantity,Validators.required),
        UnitMeasure :  new FormControl(this.orderItems[index].unitMeasure,Validators.required),
        BranchRequest :  new FormControl(this.orderItems[index].branchRequest),
      })
      
      // add new formgroup
      control.push(orderitems);
      console.log(control.value)
    

  }
  deleteRow(index: number) {
  
    // control refers to your formarray
    const control = <FormArray>this.purchaseRequestForm.controls['orderItems'];
    // remove the chosen row
    this.orderItems.splice(index,1);
    control.removeAt(index);
    console.log(index, control.value)
}
  onChange(){
    this.purchaseRequestForm.get('contactNo').value
    .subscribe(val => {
      console.log(val)
      if (val != null) {    
        this.purchaseRequestForm.get('contactNo').disable();
    }
    })
  }

  private load(): void {
    
		// this._enums.getCommonList("branch",true).subscribe(ddl => this.branch = ddl.sort((a, b) => (a.name > b.name) ? 1 : -1));  
    this._enums.getCommonList("PurchaseRequestTypes",true).subscribe(ddl => this.purchaseRequest = ddl);  

    this._requestService.action ="Tblbranches"
    this._requestService.getRequest().subscribe(data => this.branch = data.sort((a, b) => (a.branch > b.branch) ? 1 : -1));
  }
  
  ngOnInit() {
    let user = JSON.parse(localStorage.getItem("currentUser"))

    //Check is Hr  Department
    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(user.empId).subscribe(data => {
    
      data.departmentName == "Human Resource"? this.isHrAcc =true : this.isHrAcc =false

      if(!this.isHrAcc){
        this.purchaseRequestForm.controls['branchRequest'].setValidators([Validators.required])
        this.purchaseRequestForm.controls['branchRequest'].updateValueAndValidity()
      }
      console.log(data.departmentName,this.isHrAcc) 
    });
    

    console.log( this.isMaintenancePR = "/"+user.userType.toLocaleLowerCase()+"/maintenance-purchase-req" == window.location.pathname)
    this.getPrevOrder(user.empId)
    //Extract Data on LocalStorage
    this.createForms();

    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(user.empId)
        .subscribe(data =>  {
            console.log("datos! ",this.data = data)
        
            this.purchaseRequestForm.patchValue ({
              
              empId: this.data["id"],
              fullName: this.data["fname"] +" "+ this.data["lname"],
              firstName: this.data["fname"],
              lastName:this.data["lname"],
              branch:  this.data["branchName"],
              department: this.data["departmentName"],

              purchaseType :this.isMaintenancePR ? "Repair & Maintenance" : null,
              branch_Dept: this.data["branch_Dept"],
              biometricId:  this.data["bio"],
              contactNo:  this.data["telno"],
              emailAddress:  this.data["emaladd"],
              status: "Pending",
              approver :"457" //Anna Isabel Ferrer only Purchase Request order approver empId:471 id:457/tbusers table
            });
        });  

        this.getApprover();
  }
  getApprover(){

    this._requestService.action="tbusers"
    this._requestService.getRequest().subscribe(data =>{
       let cntr = 0, cntr1 =0
       for( let key in data.sort((a, b) => (a.fullName > b.fullName) ? 1 : -1)){
         if(data[key].access.toLowerCase() == "head"){
          this.approverList[0].data[cntr] = (data[key])  
          cntr++
         }    
        //  else if(data[key].access.toLowerCase() == "mgmt"){
        //   this.approverList[1].data[cntr1] = (data[key])  
        //   cntr1++
        //  }  
       }
       console.log(this.approverList);
      })
  }
getPROrderItem(id : number, data : any){

  for(let i = 0; i < data.orderItems.length; i++){
    this.PROrderItems.patchValue({
      empId: data["empId"],
      DateCreated: data["dateCreated"],
      BranchRequest:  data.orderItems[i]["BranchRequest"],
      ItemCode: data.orderItems[i]["Item"],
      ItemDesc:  data.orderItems[i]["Description"],
      Quantity: data.orderItems[i]["Quantity"],
      UnitMeasure: data.orderItems[i]["UnitMeasure"],
      PurchaseRequestId: id,
      Status: data["status"],
    })

    let formData = this.PROrderItems.value
    
    this._requestService.action = "PrOrderedItems"
    this._requestService.newRecord(formData)
    .subscribe(successCode => { 
  
      console.log(successCode) 
      this.PROrderItems.reset();
    })
  
    console.log(this.PROrderItems.value)
  }
}
   
  createForms(){

    this.PROrderItems = this.fb.group({
      empId: ['', Validators.required],
      DateCreated: this.myDate,
      BranchRequest: [''],
      ItemCode: ['', Validators.required],
      ItemDesc:  ['', Validators.required],
      UnitMeasure: ['', Validators.required],
      Quantity: ['', Validators.required],
      PurchaseRequestId: ['', Validators.required],
      Status: ['', Validators.required],
    })

    this.purchaseRequestForm = this.fb.group({
      empId:['', Validators.required],
      fullName: [''],
      firstName:[''],
      lastName:['', Validators.required],
      dateCreated: this.myDate,
      branch:  ['', Validators.required],
      department:[''],

      branch_Dept:[''],
      biometricId:  ['', Validators.required],
      contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
      emailAddress:  ['', Validators.required],
      
      branchRequest: [''],
      purchaseType: ['', Validators.required],
      purpose:  ['', Validators.required],
      item:  ['', Validators.required],
      itemDesc:  ['', Validators.required],
      quantity:  ['', Validators.required],
      unitMeasure: ['', Validators.required],

      orderItems : this.fb.array([])	,	

      comment: [''],
      requestingParty: [''],
      status: [''],

      approver: ['']
      
    });
  }
  getPrevOrder(currUserEmpId){
    
    this._requestService.action = "PrOrderedItems";
    this._requestService.getRequest().subscribe(data => {
      console.log(currUserEmpId,data)
      let cntr=0
      for(let i = 0; i <data.length; i++){
        if(currUserEmpId == data[i].empId && data[i].status == "Approved"){
          
          this.prevOrderItem[cntr] =data[i].itemCode
          //remove data duplication
          var unique =  this.prevOrderItem.filter((v, i, a) => a.indexOf(v) === i); 

          this.prevOrderItem = unique
          console.log( this.prevOrderItem)
          cntr++;
        }
      }
    })
  }
}
   