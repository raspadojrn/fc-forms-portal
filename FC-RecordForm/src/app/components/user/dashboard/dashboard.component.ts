import { Component, OnInit } from '@angular/core';
import { RequestService } from 'app/Services/request.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-user',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardUserComponent implements OnInit {

  data : any = [];
  leaveRequest : any=[];
  overtime : any =[];
  officialBusiness : any =[];
  incidentReport : any =[];
  suggestionBox : any =[];
  empId = JSON.parse(localStorage.getItem("currentUser")).empId
 
  constructor(private _requestService : RequestService,
              private router: Router,) { }

  
  ngOnInit() {

    this.getData();
    console.log(this.leaveRequest,this.overtime, this.officialBusiness, this.incidentReport)
  }

  getFormIndex(index : number){
    let param = { Index : index };
    
    this.router.navigate(['/user/requested-forms'], { queryParams : param });
  }
  getData(){

    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequest().subscribe(data => this.data = data);

    this._requestService.action = "LeaveRequests"
    this._requestService.getRequest().subscribe(data=> {console.log("LeaveRequests: ", data, data.length)   
      this.filterCurrentUserData(data, "LeaveRequests");
    })

    this._requestService.action = "RequestOts"
    this._requestService.getRequest().subscribe(data=> {console.log("RequestOts: ", data, data.length)   
      this.filterCurrentUserData(data, "RequestOts");
    })

    this._requestService.action = "OfficialBusinesses"
    this._requestService.getRequest().subscribe(data=> {console.log("OfficialBusinesses: ", data, data.length)   
      this.filterCurrentUserData(data, "OfficialBusinesses");
    })

    this._requestService.action = "IncidentReports"
    this._requestService.getRequest().subscribe(data=> {console.log("IncidentReports: ", data, data.length)   
      this.filterCurrentUserData(data, "IncidentReports");
    })

    this._requestService.action = "SuggestionBoxes"
    this._requestService.getRequest().subscribe(data=> {console.log("SuggestionBoxes: ", data, data.length)   
      this.filterCurrentUserData(data, "SuggestionBoxes");
    })
  }

  //User only see their form request data
  filterCurrentUserData(data : any, action: string){
    let cntr =0;
    let apprvd_len =0, acpt_len =0, cncl_len =0, pndng_len =0;
    
    for(let i =0; i < data.length; i ++){
      if(this.empId == data[i].empId){
        
        switch(action){
          case"LeaveRequests":
          
               this.leaveRequest[cntr] = data[i];
               (data[i].status == "Approved" ? this.leaveRequest["approved"] = ++apprvd_len: null),
               (data[i].status == "Accept" ? this.leaveRequest["accept"] = ++acpt_len: null),
               (data[i].status == "Cancelled" ? this.leaveRequest["cancelled"] = ++cncl_len: null),
               (data[i].status == "Pending" ? this.leaveRequest["pending"] = ++pndng_len: null)
               break;
          case"RequestOts":
              this.overtime[cntr] = data[i];
              (data[i].status == "Approved" ? this.overtime["approved"] = ++apprvd_len: null),
              (data[i].status == "Accept" ? this.overtime["accept"] = ++acpt_len: null),
              (data[i].status == "Cancelled" ? this.overtime["cancelled"] = ++cncl_len: null),
              (data[i].status == "Pending" ? this.overtime["pending"] = ++pndng_len: null)
              break;
          case"OfficialBusinesses":
              this.officialBusiness[cntr] = data[i];
              (data[i].status == "Approved" ? this.officialBusiness["approved"] = ++apprvd_len: null),
              (data[i].status == "Accept" ? this.officialBusiness["accept"] = ++acpt_len: null),
              (data[i].status == "Cancelled" ? this.officialBusiness["cancelled"] = ++cncl_len: null),
              (data[i].status == "Pending" ? this.officialBusiness["pending"] = ++pndng_len: null)
              break;
          case"IncidentReports":
              this.incidentReport[cntr] = data[i];
              (data[i].status == "Approved" ? this.incidentReport["approved"] = ++apprvd_len: null),
              (data[i].status == "Accept" ? this.incidentReport["accept"] = ++acpt_len: null),
              (data[i].status == "Cancelled" ? this.incidentReport["cancelled"] = ++cncl_len: null),
              (data[i].status == "Pending" ? this.incidentReport["pending"] = ++pndng_len: null)
              break;
          case"SuggestionBoxes":
              this.suggestionBox[cntr] = data[i];
              break;
        }
        
        cntr++;
      }
    }  
  }
}
