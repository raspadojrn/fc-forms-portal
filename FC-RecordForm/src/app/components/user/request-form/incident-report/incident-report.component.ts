import { Component, OnInit } from '@angular/core';
import { RequestService } from 'app/Services/request.service';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { CustomValidator } from 'app/models/validators/custom.validator';


@Component({
  selector: 'app-incident-report-user',
  templateUrl: './incident-report.component.html',
  styleUrls: ['./incident-report.component.css']
})
export class IncidentReportComponent implements OnInit {

  title = "Incident Report"
  myDate = new Date();
  successMessage: any;
  data: any ;
  
  approverList: any = [
    {
      name:'Approver- Head',
      data :[]
    },
    { 
      name:'',
      data:[]
    }
  ];

  incidentReportForm = this.fb.group({
    empId:['', Validators.required],
    fullName: [''],
    firstName:[''],
    lastName:['', Validators.required],
    dateCreated: this.myDate,
    branch:  ['', Validators.required],
    department:[''],

    branch_Dept: [''],
    biometricId:  ['', Validators.required],
    contactNo:  ['',Validators.compose([Validators.required,Validators.minLength(6)])],
    emailAddress:  ['',Validators.compose([Validators.required,CustomValidator.emailAddress])],

    personReporting: ['', Validators.required],
    supervisor:  ['', Validators.required],
    incidentLocation:  ['', Validators.required],

    dateOfEvent:  ['', Validators.required],
    timeOfEvent:  ['', Validators.required],
    witness: [''],
    descriptionOfHappening: ['', Validators.required],
    status: [''],

    approver: ['',Validators.required]

  });


  constructor(private _requestService : RequestService, private fb: FormBuilder) { }
  
  onSubmit(e){
    let aprrv =this.incidentReportForm.value.approver
    // let Approver
    // for( let key in aprrv){
    //     console.log(Approver =aprrv[0]+ " , "+aprrv[1])
        
    // }
    this.incidentReportForm.value.approver = aprrv

    let formData = this.incidentReportForm.value;
    console.log(formData);

    this._requestService.action = "IncidentReports"
    this._requestService.newRecord(formData)
    .subscribe(successCode => {
      this.successMessage = "Successfully Created"
      console.log(successCode)
      this.incidentReportForm.reset();
    })
  }

  ngOnInit() {
      
    let id = JSON.parse(localStorage.getItem("currentUser")).empId

    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(id)
        .subscribe(data =>  {
            console.log("datos! ",this.data = data)
        
            this.incidentReportForm.patchValue ({
              
              empId: this.data["id"],
              fullName: this.data["fname"] +" "+ this.data["lname"],
              firstName: this.data["fname"],
              lastName:this.data["lname"],
              branch:  this.data["branchName"],
              department: this.data["departmentName"],

              branch_Dept: this.data["branch_Dept"],
              biometricId:  this.data["bio"],
              contactNo:  this.data["telno"],
              emailAddress:  this.data["emaladd"],
              status: "Pending"
            });
        });
        this.getApprover();
  }
  getApprover(){

    this._requestService.action="tbusers"
    this._requestService.getRequest().subscribe(data =>{
       let cntr = 0, cntr1 =0
       for( let key in data.sort((a, b) => (a.fullName > b.fullName) ? 1 : -1)){
         if(data[key].access.toLowerCase() == "head"){
          this.approverList[0].data[cntr] = (data[key])  
          cntr++
         }    
        //  else if(data[key].access.toLowerCase() == "mgmt"){
        //   this.approverList[1].data[cntr1] = (data[key])  
        //   cntr1++
        //  }  
       }
       console.log(this.approverList);
      })
  }
}
