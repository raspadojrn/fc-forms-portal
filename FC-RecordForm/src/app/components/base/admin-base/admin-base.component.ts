import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { Route } from '@angular/compiler/src/core';
import { AuthService } from 'app/Services/auth.service';
import { RequestService } from 'app/Services/request.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import { AnnouncementComponent } from 'app/common/announcement/announcement.component';

@Component({
  selector: 'app-admin-base',
  templateUrl: './admin-base.component.html',
  styleUrls: ['./admin-base.component.css']
})
export class AdminBaseComponent implements OnInit {

    title = 'FC Team Portal';
    name = 'Admin';
    department : any;

    isLogin : boolean = true;
    data :any;

    showMenu : boolean = false
    navMode: any;
    openNav: boolean = true

    durationInSeconds = 5; // 5 sec
    hasAnnouncement: boolean = false
    constructor(private router: Router, private _authService : AuthService, private _requestService : RequestService,
                private _snackBar: MatSnackBar ) {

 
      if(localStorage.getItem("currentUser") == null){
    
        this.isLogin= false;
      }
      else{
        this.isLogin=true;
      }
     }
     isHr(){
       if(this.department == "Human Resource"){
         return true;
       }
       else{
         return false;
       }
     }

     
     onResize(event) {

       if(event.target.innerWidth < 600 || event.target.outerHeight < 500){
         this.showMenu = true
         this.navMode = "over"
         this.openNav = false
         console.log(event.target.innerWidth, event.target.outerHeight)
       }else{
        this.navMode = "side"
        this.showMenu = false
        this.openNav = true
       }
    

    }
     onLogout(){
      // this.router.navigate(['/login']);
      this.isLogin= false;
      this._authService.logout();
     
     }

     ngOnInit(){
       if(this.hasAnnouncement){
         this.openSnackBar();
       }
      let id = JSON.parse(localStorage.getItem("currentUser")).empId
  
      this._requestService.action = "Tblprofiles/"
      this._requestService.getRequestById(id).subscribe(data => {
        this.name = data.fname +' '+ data.lname
        console.log(this. department =data.departmentName) 
      });
      this.onLoadScreen()
     }

     onLoadScreen(){
      if(screen.width < 600 || screen.height < 500){
        this.showMenu = true
        this.navMode = "over"
      }else{
       this.showMenu = false
       this.navMode = "side"
      }
      console.log("Mobile View",this.showMenu)
     }

     openSnackBar() {
      this._snackBar.openFromComponent(AnnouncementComponent, {
        duration: this.durationInSeconds * 1000,
        horizontalPosition: "center",
        verticalPosition: "top",
      });
    }

  }
  
