import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { Route } from '@angular/compiler/src/core';
import { AuthService } from 'app/Services/auth.service';
import { RequestService } from 'app/Services/request.service';

@Component({
  selector: 'app-head-base',
  templateUrl: './head-base.component.html',
  styleUrls: ['./head-base.component.css']
})
export class HeadBaseComponent implements OnInit {

    title = 'FC Team Portal';
    name = 'Head Admin';
    empId : any ;

    isLogin : boolean = true;
    data :any;

    showMenu : boolean = false
    navMode: any;
    openNav: boolean = true

    constructor(private router: Router, private _authService : AuthService, private _requestService : RequestService ) {
      if(localStorage.getItem("currentUser") == null){
    
        this.isLogin= false;
      }
      else{
        this.isLogin=true;
      }
     }
     
     onResize(event) {
      if(event.target.innerWidth < 600 || event.target.outerHeight < 500){
        this.showMenu = true
        this.navMode = "over"
        this.openNav = false
        console.log(event.target.innerWidth )
      }else{
       this.navMode = "side"
       this.showMenu = false
       this.openNav = true
      }
    }
     onLogout(){
      // this.router.navigate(['/login']);
      this.isLogin= false;
      this._authService.logout();
     
     }
  
     ngOnInit(){
  
      let id = JSON.parse(localStorage.getItem("currentUser")).empId
      this.empId = id
      
      this._requestService.action = "Tblprofiles/"
      this._requestService.getRequestById(id).subscribe(data => this.name = data.fname +' '+ data.lname);
      this.onLoadScreen()
     }
     onLoadScreen(){
      if(screen.width < 600 || screen.height < 500){
        this.showMenu = true
        this.navMode = "over"
      }else{
       this.showMenu = false
       this.navMode = "side"
      }
      console.log(screen.width)
     }
  }
  