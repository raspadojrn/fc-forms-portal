import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [

  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent},

  {
    path: 'admin',  
    loadChildren: './modules/admin-routing/admin.module#AdminModule'

 },
 {
    path: 'user',
    loadChildren: './modules/user-routing/user.module#UserModule'
 },
  {
    path: 'head',
    loadChildren: './modules/head-routing/head.module#HeadModule'
  },
  {
    path: 'mgmt',
    loadChildren: './modules/mgmt-routing/mgmt.module#MgmtModule'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }