//Modules
import { NgModule } from '@angular/core';
import { CommonModule }   from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

// Service
import { RequestService } from 'app/Services/request.service';
import { AuthService } from 'app/Services/auth.service';
import { AuthGuard } from 'app/Services/auth.guard';
import { EnumsService } from 'app/Services/common/enums.service';
import { FilterService } from 'app/Services/common/filter.service';
import { PagerService } from 'app/Services/common/pager.service';
import { BaseService } from 'app/Services/base.service';
import { ApiBaseService } from 'app/Services/api-base.service';

import { ShowErrorComponent } from 'app/common/show-error/show-error.component';
import { PagerComponent } from 'app/common/pager/pager.component';
import { SuggestionBoxComponent } from 'app/components/admin/pending-form/suggestion-box/suggestion-box.component';
import { AccountSettingsComponent } from 'app/components/account-settings/account-settings.component';
import { FAQsComponent } from 'app/common/faqs/faqs.component';
import { AnnouncementComponent } from 'app/common/announcement/announcement.component';
import { HomeComponent } from 'app/common/home/home.component';

//For Material Design
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatStepperModule} from '@angular/material/stepper';
import {MatButtonModule} from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTabsModule} from '@angular/material/tabs';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatSnackBarModule} from '@angular/material/snack-bar';

//hr request forms
import { LeaveRequestComponent } from 'app/components/user/request-form/leave-request/leave-request.component'
import { RequestForOTComponent } from 'app/components/user/request-form/request-for-ot/request-for-ot.component';
import { OfficialBusinessComponent } from 'app/components/user/request-form/official-business/official-business.component';
import { IncidentReportComponent } from 'app/components/user/request-form/incident-report/incident-report.component';

// purchase request

import { BookletsRequestComponent } from 'app/components/user/purchase/booklets/booklets-request.component';
import { BusinessCardRequestComponent } from 'app/components/user/purchase/business-card/business-card-request.component';
import { PurchaseRequestComponent } from 'app/components/user/purchase/purchase-request/purchase-request.component';
import { UniformRequestComponent } from 'app/components/user/purchase/uniform/uniform-request.component';
import { MarketingMaterialComponent } from 'app/components/user/purchase/marketing-material/marketing-material.component';
import { MaintenanceRequestComponent } from 'app/components/user/purchase/maintenance-request/maintenance-request.component';

import { DashboardComponent } from 'app/components/admin/dashboard/dashboard.component';
import { DashboardUserComponent } from 'app/components/user/dashboard/dashboard.component'
import { DashboardMgmtComponent } from 'app/components/mgmt/dashboard/dashboard.component';
import { DashboardHeadComponent } from 'app/components/head/dashboard/dashboard.component';
import { PettyCashComponent } from 'app/components/user/petty-cash/petty-cash.component';
import { PettyCashFormsComponent } from 'app/components/mgmt/petty-cash-forms/petty-cash-forms.component';

import { Ng2ImgMaxModule } from 'ng2-img-max'; 
@NgModule({
  declarations: [
    ShowErrorComponent,
    PagerComponent,
    SuggestionBoxComponent,
    AccountSettingsComponent,
    FAQsComponent,
    AnnouncementComponent,
    HomeComponent,
    PettyCashComponent,
    PettyCashFormsComponent,

    // hr request forms
    LeaveRequestComponent,
    RequestForOTComponent,
    OfficialBusinessComponent,
    IncidentReportComponent,

    // purchaseREQ
    BookletsRequestComponent,
    BusinessCardRequestComponent,
    PurchaseRequestComponent,
    UniformRequestComponent,
    MarketingMaterialComponent,
    MaintenanceRequestComponent,
    
    DashboardComponent,
    DashboardUserComponent,
    DashboardMgmtComponent,
    DashboardHeadComponent,
  ],
  imports: [
    CommonModule,  
    RouterModule,

    ReactiveFormsModule,  

      //For Material Design
      MatMenuModule,
      MatToolbarModule,
      MatSidenavModule,
      MatDividerModule,
      MatListModule,
      MatIconModule,
      MatExpansionModule,
      MatStepperModule,
      MatButtonModule,
      MatDatepickerModule,
      MatNativeDateModule,
      MatProgressSpinnerModule,
      MatDialogModule,
      MatTabsModule,
      MatSelectModule,
      MatFormFieldModule,
      MatInputModule,
      MatRadioModule,
      MatSnackBarModule,

      Ng2ImgMaxModule,


  ],
  exports : [
    ShowErrorComponent,
    PagerComponent,
    SuggestionBoxComponent,
    AccountSettingsComponent,
    FAQsComponent,
    PettyCashComponent,
    PettyCashFormsComponent,
    AnnouncementComponent,
    HomeComponent,

    LeaveRequestComponent,
    RequestForOTComponent,
    OfficialBusinessComponent,
    IncidentReportComponent,

    BookletsRequestComponent,
    BusinessCardRequestComponent,
    PurchaseRequestComponent,
    UniformRequestComponent,
    MarketingMaterialComponent,
    MaintenanceRequestComponent,

    DashboardComponent,
    DashboardUserComponent,
    DashboardMgmtComponent,
    DashboardHeadComponent,

],
  providers: [RequestService, AuthService, AuthGuard, EnumsService, FilterService, PagerService, BaseService, ApiBaseService],
})
export class SharedModule { }
