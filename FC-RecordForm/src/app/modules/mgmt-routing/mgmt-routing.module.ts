import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";

import { AuthGuard } from 'app/Services/auth.guard';

//Components
import { RequestFormComponent } from 'app/components/mgmt/pending-form/request-form.component';
import { LeaveRequestComponent } from 'app/components/mgmt/pending-form/leave-request/leave-request.component';
import { RequestForOTComponent } from 'app/components/mgmt/pending-form/request-for-ot/request-for-ot.component';
import { OfficialBusinessComponent } from 'app/components/mgmt/pending-form/official-business/official-business.component';
import { IncidentReportComponent } from 'app/components/mgmt/pending-form/incident-report/incident-report.component';
import { SuggestionBoxComponent } from 'app/components/mgmt/pending-form/suggestion-box/suggestion-box.component';


import { DashboardMgmtComponent } from 'app/components/mgmt/dashboard/dashboard.component';
import { MgmtBaseComponent } from 'app/components/base/mgmt-base/mgmt-base.component';
import { ApproveRequestComponent } from 'app/components/mgmt/pending-form/leave-request/approve-request/approve-request.component';
import { ApproveRequestOBComponent } from 'app/components/mgmt/pending-form/official-business/approve-request-ob/approve-request-ob.component';
import { ApproveRequestOTComponent } from 'app/components/mgmt/pending-form/request-for-ot/approve-request-ot/approve-request-ot.component';
import { ApproveRequestIRComponent } from 'app/components/mgmt/pending-form/incident-report/approve-request-ir/approve-request-ir.component';

import { MgmtAccountSettingsComponent } from 'app/components/mgmt/dashboard/account-settings/account-settings.component';
import { PettyCashFormsComponent } from 'app/components/mgmt/petty-cash-forms/petty-cash-forms.component';

import { FAQsComponent } from 'app/common/faqs/faqs.component';
import { HomeComponent } from 'app/common/home/home.component';

const mgmtRoutes: Routes = [

  { 
    path: '',
    component: MgmtBaseComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
      { path: 'dashboard', component: DashboardMgmtComponent, canActivate: [AuthGuard]},
      { path: 'pending-form', component: RequestFormComponent, canActivate: [AuthGuard] },
      { path: 'leave-request', component: LeaveRequestComponent, canActivate: [AuthGuard] },
      { path: 'request-ot', component: RequestForOTComponent, canActivate: [AuthGuard] },
      { path: 'official-business', component: OfficialBusinessComponent, canActivate: [AuthGuard] },
      { path: 'incident-report', component: IncidentReportComponent, canActivate: [AuthGuard] },
      { path: 'suggestion-box', component: SuggestionBoxComponent, canActivate: [AuthGuard] },

      { path: 'account-settings', component: MgmtAccountSettingsComponent, canActivate: [AuthGuard] },

      // modal
      { path: 'approve-req-leave', component: ApproveRequestComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-ob', component: ApproveRequestOBComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-ot', component: ApproveRequestOTComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-ir', component: ApproveRequestIRComponent, canActivate: [AuthGuard] },

      //FAQs
      { path: 'faqs', component: FAQsComponent, canActivate: [AuthGuard] },
      //PettyCashForms
      { path: 'petty-cash-forms', component: PettyCashFormsComponent, canActivate: [AuthGuard] },
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(mgmtRoutes)],
  exports: [RouterModule]
})
export class MgmtRoutingModule { }
