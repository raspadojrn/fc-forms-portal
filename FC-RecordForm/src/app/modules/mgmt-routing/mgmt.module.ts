// MODULES
import { NgModule } from '@angular/core';
import { CommonModule }   from '@angular/common';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MgmtRoutingModule } from 'app/modules/mgmt-routing/mgmt-routing.module';
import { SharedModule } from 'app/modules/shared.module';

//For Material Design
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatStepperModule} from '@angular/material/stepper';
import {MatButtonModule} from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTabsModule} from '@angular/material/tabs';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatTooltipModule} from '@angular/material/tooltip';

// Service
import { RequestService } from 'app/Services/request.service';
import { AuthService } from 'app/Services/auth.service';
import { AuthGuard } from 'app/Services/auth.guard';


//Components
import { MgmtBaseComponent } from 'app/components/base/mgmt-base/mgmt-base.component';

import { DashboardMgmtComponent } from 'app/components/mgmt/dashboard/dashboard.component';
import { RequestFormComponent } from 'app/components/mgmt/pending-form/request-form.component';
import { LeaveRequestComponent } from 'app/components/mgmt/pending-form/leave-request/leave-request.component';
import { RequestForOTComponent } from 'app/components/mgmt/pending-form/request-for-ot/request-for-ot.component';
import { OfficialBusinessComponent } from 'app/components/mgmt/pending-form/official-business/official-business.component';
import { IncidentReportComponent } from 'app/components/mgmt/pending-form/incident-report/incident-report.component';
 import { SuggestionBoxComponent } from 'app/components/mgmt/pending-form/suggestion-box/suggestion-box.component';

import { ApproveRequestComponent } from 'app/components/mgmt/pending-form/leave-request/approve-request/approve-request.component';
import { ApproveRequestOBComponent } from 'app/components/mgmt/pending-form/official-business/approve-request-ob/approve-request-ob.component';
import { ApproveRequestOTComponent } from 'app/components/mgmt/pending-form/request-for-ot/approve-request-ot/approve-request-ot.component';
import { ApproveRequestIRComponent } from 'app/components/mgmt/pending-form/incident-report/approve-request-ir/approve-request-ir.component';

import { MgmtAccountSettingsComponent } from 'app/components/mgmt/dashboard/account-settings/account-settings.component';


@NgModule({
  declarations: [
      
    // DashboardMgmtComponent,
    MgmtBaseComponent,
    RequestFormComponent,
    LeaveRequestComponent,
    RequestForOTComponent,
    OfficialBusinessComponent,
    IncidentReportComponent,
    SuggestionBoxComponent,
    ApproveRequestComponent,
    ApproveRequestOBComponent,
    ApproveRequestOTComponent,
    ApproveRequestIRComponent,
    MgmtAccountSettingsComponent,
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    MgmtRoutingModule,
    HttpModule,
    SharedModule,
    RouterModule,
    
    
    //For Material Design
    MatMenuModule,
    MatToolbarModule,
    MatSidenavModule,
    MatDividerModule,
    MatListModule,
    MatIconModule,
    MatExpansionModule,
    MatStepperModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatTabsModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatTooltipModule
  ],
  providers: [RequestService, AuthService, AuthGuard],
})
export class MgmtModule { }
