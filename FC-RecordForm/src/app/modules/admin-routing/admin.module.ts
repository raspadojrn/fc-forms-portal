import { MaintenanceRequestComponent } from 'app/components/user/purchase/maintenance-request/maintenance-request.component';
// import { UpdateUsersComponent } from './../../components/admin/add-users/update-users/update-users.component';
// MODULES
import { NgModule } from '@angular/core';
import { CommonModule }   from '@angular/common';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AdminRoutingModule } from 'app/modules/admin-routing/admin-routing.module';
import { SharedModule } from 'app/modules/shared.module';

//For Material Design
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatStepperModule} from '@angular/material/stepper';
import {MatButtonModule} from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTabsModule} from '@angular/material/tabs';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSnackBarModule} from '@angular/material/snack-bar';


// Service
import { RequestService } from 'app/Services/request.service';
import { AuthService } from 'app/Services/auth.service';
import { AuthGuard } from 'app/Services/auth.guard';


//Components
import { AdminBaseComponent } from 'app/components/base/admin-base/admin-base.component';

import { DashboardComponent } from 'app/components/admin/dashboard/dashboard.component';
import { LeaveRequestComponent } from 'app/components/admin/pending-form/leave-request/leave-request.component';
import { RequestForOTComponent } from 'app/components/admin/pending-form/request-for-ot/request-for-ot.component';
import { OfficialBusinessComponent } from 'app/components/admin/pending-form/official-business/official-business.component';
import { IncidentReportComponent } from 'app/components/admin/pending-form/incident-report/incident-report.component';
 import { SuggestionBoxComponent } from 'app/components/admin/pending-form/suggestion-box/suggestion-box.component';


import { LeaveRequestRFComponent } from 'app/components/admin/request-form/leave-request/leave-request.component'
import { RequestForOTRFComponent } from 'app/components/admin/request-form/request-for-ot/request-for-ot.component';
import { OfficialBusinessRFComponent } from 'app/components/admin/request-form/official-business/official-business.component';
import { IncidentReportRFComponent } from 'app/components/admin/request-form/incident-report/incident-report.component';
import { SuggestionBoxRFComponent } from 'app/components/admin/request-form/suggestion-box/suggestion-box.component';

import { ApproveRequestComponent } from 'app/components/admin/pending-form/leave-request/approve-request/approve-request.component';
import { ApproveRequestOBComponent } from 'app/components/admin/pending-form/official-business/approve-request-ob/approve-request-ob.component';
import { ApproveRequestOTComponent } from 'app/components/admin/pending-form/request-for-ot/approve-request-ot/approve-request-ot.component';
import { ApproveRequestIRComponent } from 'app/components/admin/pending-form/incident-report/approve-request-ir/approve-request-ir.component';
import { UpdateUsersComponent } from 'app/components/admin/add-users/update-users/update-users.component';

import { BookletsRequestPFComponent } from 'app/components/admin/pending-form/purchase/booklets/booklets-request.component';
import { BusinessCardRequestPFComponent } from 'app/components/admin/pending-form/purchase/business-card/business-card-request.component';
import { MarketingMaterialPFComponent } from 'app/components/admin/pending-form/purchase/marketing-material/marketing-material.component';
import { PurchaseRequestPFComponent } from 'app/components/admin/pending-form/purchase/purchase-request/purchase-request.component';
import { UniformRequestPFComponent } from 'app/components/admin/pending-form/purchase/uniform/uniform-request.component';
import { MaintenanceRequestPFComponent } from 'app/components/admin/pending-form/purchase/maintenance-request/maintenance-request.component';

import { ApproveRequestPRComponent } from 'app/components/admin/pending-form/purchase/purchase-request/approve-request/approve-request.component';
import { ApproveRequestURComponent } from 'app/components/admin/pending-form/purchase/uniform/approve-request/approve-request.component';
import { ApproveRequestBRComponent } from 'app/components/admin/pending-form/purchase/booklets/approve-request/approve-request.component';
import { ApproveRequestBCComponent } from 'app/components/admin/pending-form/purchase/business-card/approve-request/approve-request.component';
import { ApproveRequestMMComponent } from 'app/components/admin/pending-form/purchase/marketing-material/approve-request/approve-request.component';
import { ApproveRequestMRComponent } from 'app/components/admin/pending-form/purchase/maintenance-request/approve-request/approve-request.component';

import { RequestedFormsComponent } from 'app/components/admin/requested-forms/requested-forms.component';

import { AddUsersComponent } from 'app/components/admin/add-users/add-users.component';
import { AccountSettingsComponent } from 'app/components/account-settings/account-settings.component';

import { ChangeInfoRequestComponent } from 'app/components/admin/pending-form/change-info-request/change-info-request.component';
import { ApproveRequestCIComponent } from 'app/components/admin/pending-form/change-info-request/approve-request/approve-request.component';




@NgModule({
  declarations: [
      
    // DashboardComponent,
    AdminBaseComponent,
    LeaveRequestComponent,
    RequestForOTComponent,
    OfficialBusinessComponent,
    IncidentReportComponent,
    ChangeInfoRequestComponent,
    // SuggestionBoxComponent,
    ApproveRequestComponent,
    ApproveRequestOBComponent,
    ApproveRequestOTComponent,
    ApproveRequestIRComponent,
    UpdateUsersComponent,
    AddUsersComponent,
    RequestedFormsComponent,
    ApproveRequestCIComponent,
    // AccountSettingsComponent,

    //Request Forms
    LeaveRequestRFComponent,
    RequestForOTRFComponent,
    OfficialBusinessRFComponent,
    IncidentReportRFComponent,
    SuggestionBoxRFComponent,

    //Pending forms Purchase Request
    BookletsRequestPFComponent,
    BusinessCardRequestPFComponent,
    MarketingMaterialPFComponent,
    PurchaseRequestPFComponent,
    UniformRequestPFComponent,
    MaintenanceRequestPFComponent,
    
    //PR Modal
    ApproveRequestPRComponent,
    ApproveRequestURComponent,
    ApproveRequestBRComponent,
    ApproveRequestBCComponent,
    ApproveRequestMMComponent,
    ApproveRequestMRComponent,
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    AdminRoutingModule,
    HttpModule,
    SharedModule,
    RouterModule,
    

    //For Material Design
    MatMenuModule,
    MatToolbarModule,
    MatSidenavModule,
    MatDividerModule,
    MatListModule,
    MatIconModule,
    MatExpansionModule,
    MatStepperModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatTabsModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatTooltipModule,
    MatSnackBarModule
  ],exports : [
    // AccountSettingsComponent
  ],
  providers: [RequestService, AuthService, AuthGuard],
})
export class AdminModule { }
