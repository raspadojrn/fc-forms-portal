import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";

import { AuthGuard } from 'app/Services/auth.guard';

//Components
import { LeaveRequestComponent } from 'app/components/admin/pending-form/leave-request/leave-request.component';
import { RequestForOTComponent } from 'app/components/admin/pending-form/request-for-ot/request-for-ot.component';
import { OfficialBusinessComponent } from 'app/components/admin/pending-form/official-business/official-business.component';
import { IncidentReportComponent } from 'app/components/admin/pending-form/incident-report/incident-report.component';
import { SuggestionBoxComponent } from 'app/components/admin/pending-form/suggestion-box/suggestion-box.component';

import { LeaveRequestRFComponent } from 'app/components/admin/request-form/leave-request/leave-request.component'
import { RequestForOTRFComponent } from 'app/components/admin/request-form/request-for-ot/request-for-ot.component';
import { OfficialBusinessRFComponent } from 'app/components/admin/request-form/official-business/official-business.component';
import { IncidentReportRFComponent } from 'app/components/admin/request-form/incident-report/incident-report.component';
import { SuggestionBoxRFComponent } from 'app/components/admin/request-form/suggestion-box/suggestion-box.component';


import { DashboardComponent } from 'app/components/admin/dashboard/dashboard.component';
import { AdminBaseComponent } from 'app/components/base/admin-base/admin-base.component';
import { ApproveRequestComponent } from 'app/components/admin/pending-form/leave-request/approve-request/approve-request.component';
import { ApproveRequestOBComponent } from 'app/components/admin/pending-form/official-business/approve-request-ob/approve-request-ob.component';
import { ApproveRequestOTComponent } from 'app/components/admin/pending-form/request-for-ot/approve-request-ot/approve-request-ot.component';
import { ApproveRequestIRComponent } from 'app/components/admin/pending-form/incident-report/approve-request-ir/approve-request-ir.component';
import { UpdateUsersComponent } from 'app/components/admin/add-users/update-users/update-users.component';
import { AddUsersComponent } from 'app/components/admin/add-users/add-users.component';

//purchase forms
import { BookletsRequestPFComponent } from 'app/components/admin/pending-form/purchase/booklets/booklets-request.component';
import { BusinessCardRequestPFComponent } from 'app/components/admin/pending-form/purchase/business-card/business-card-request.component';
import { MarketingMaterialPFComponent } from 'app/components/admin/pending-form/purchase/marketing-material/marketing-material.component';
import { PurchaseRequestPFComponent } from 'app/components/admin/pending-form/purchase/purchase-request/purchase-request.component';
import { UniformRequestPFComponent } from 'app/components/admin/pending-form/purchase/uniform/uniform-request.component';
import { MaintenanceRequestPFComponent } from 'app/components/admin/pending-form/purchase/maintenance-request/maintenance-request.component';

import { ApproveRequestPRComponent } from 'app/components/admin/pending-form/purchase/purchase-request/approve-request/approve-request.component'
import { ApproveRequestURComponent } from 'app/components/admin/pending-form/purchase/uniform/approve-request/approve-request.component';
import { ApproveRequestBRComponent } from 'app/components/admin/pending-form/purchase/booklets/approve-request/approve-request.component';
import { ApproveRequestBCComponent } from 'app/components/admin/pending-form/purchase/business-card/approve-request/approve-request.component';
import { ApproveRequestMMComponent } from 'app/components/admin/pending-form/purchase/marketing-material/approve-request/approve-request.component';
import { ApproveRequestMRComponent } from 'app/components/admin/pending-form/purchase/maintenance-request/approve-request/approve-request.component';

import { RequestedFormsComponent } from 'app/components/admin/requested-forms/requested-forms.component';
import { AccountSettingsComponent } from 'app/components/account-settings/account-settings.component';
import { FAQsComponent } from 'app/common/faqs/faqs.component';
import { AnnouncementComponent } from 'app/common/announcement/announcement.component';
import { HomeComponent } from 'app/common/home/home.component';

// purchase request
import { BookletsRequestComponent } from 'app/components/user/purchase/booklets/booklets-request.component';
import { BusinessCardRequestComponent } from 'app/components/user/purchase/business-card/business-card-request.component';
import { PurchaseRequestComponent } from 'app/components/user/purchase/purchase-request/purchase-request.component';
import { UniformRequestComponent } from 'app/components/user/purchase/uniform/uniform-request.component';
import { MarketingMaterialComponent } from 'app/components/user/purchase/marketing-material/marketing-material.component';
import { MaintenanceRequestComponent } from 'app/components/user/purchase/maintenance-request/maintenance-request.component';

import { ChangeInfoRequestComponent } from 'app/components/admin/pending-form/change-info-request/change-info-request.component';
import { ApproveRequestCIComponent } from 'app/components/admin/pending-form/change-info-request/approve-request/approve-request.component';
import { PettyCashComponent } from 'app/components/user/petty-cash/petty-cash.component';


const adminRoutes: Routes = [

  { 
    path: '',
    component: AdminBaseComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
      { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
      { path: 'leave-request', component: LeaveRequestComponent, canActivate: [AuthGuard] },
      { path: 'request-ot', component: RequestForOTComponent, canActivate: [AuthGuard] },
      { path: 'official-business', component: OfficialBusinessComponent, canActivate: [AuthGuard] },
      { path: 'incident-report', component: IncidentReportComponent, canActivate: [AuthGuard] },
      { path: 'suggestion-box', component: SuggestionBoxComponent, canActivate: [AuthGuard] },
      { path: 'add-users', component: AddUsersComponent, canActivate: [AuthGuard] },
      
      { path: 'rf-leave-request', component: LeaveRequestRFComponent, canActivate: [AuthGuard] },
      { path: 'rf-request-ot', component: RequestForOTRFComponent, canActivate: [AuthGuard] },
      { path: 'rf-official-business', component: OfficialBusinessRFComponent, canActivate: [AuthGuard] },
      { path: 'rf-incident-report', component: IncidentReportRFComponent, canActivate: [AuthGuard] },
      { path: 'rf-suggestion-box', component: SuggestionBoxRFComponent, canActivate: [AuthGuard] },
      { path: 'rf-change-info', component: ChangeInfoRequestComponent, canActivate: [AuthGuard] },


      { path: 'pf-booklet-req', component: BookletsRequestPFComponent, canActivate: [AuthGuard] },
      { path: 'pf-businessCard-req', component: BusinessCardRequestPFComponent, canActivate: [AuthGuard] },
      { path: 'pf-purchase-req', component: PurchaseRequestPFComponent, canActivate: [AuthGuard] },
      { path: 'pf-uniform-req', component: UniformRequestPFComponent, canActivate: [AuthGuard] },
      { path: 'pf-marketing-material-req', component: MarketingMaterialPFComponent, canActivate: [AuthGuard] },
      { path: 'pf-maintenance-req', component: MaintenanceRequestPFComponent, canActivate: [AuthGuard] },

      { path: 'requested-forms', component: RequestedFormsComponent, canActivate: [AuthGuard] },
      { path: 'account-settings', component: AccountSettingsComponent, canActivate: [AuthGuard] },
      // modal
      { path: 'approve-req-leave', component: ApproveRequestComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-ob', component: ApproveRequestOBComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-ot', component: ApproveRequestOTComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-ir', component: ApproveRequestIRComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-ci', component: ApproveRequestCIComponent, canActivate: [AuthGuard] },
      { path: 'update-users', component: UpdateUsersComponent, canActivate: [AuthGuard] },

      //pr modal
      { path: 'approve-req-pr', component: ApproveRequestPRComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-ur', component: ApproveRequestURComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-bu', component: ApproveRequestBRComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-bc', component: ApproveRequestBCComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-mm', component: ApproveRequestMMComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-mr', component: ApproveRequestMRComponent, canActivate: [AuthGuard] },

      // purchase request
      { path: 'booklet-req', component: BookletsRequestComponent, canActivate: [AuthGuard] },
      { path: 'businessCard-req', component: BusinessCardRequestComponent, canActivate: [AuthGuard] },
      { path: 'purchase-req', component: PurchaseRequestComponent, canActivate: [AuthGuard] },
      { path: 'uniform-req', component: UniformRequestComponent, canActivate: [AuthGuard] },
      { path: 'marketing-material-req', component: MarketingMaterialComponent, canActivate: [AuthGuard] },
      { path: 'maintenance-req', component: MaintenanceRequestComponent, canActivate: [AuthGuard] },
      { path: 'maintenance-purchase-req', component: PurchaseRequestComponent, canActivate: [AuthGuard] },

      //FAQs
      { path: 'faqs', component: FAQsComponent, canActivate: [AuthGuard] },
      //pettyCash
      { path: 'petty-cash', component: PettyCashComponent, canActivate: [AuthGuard] },
      //Announcement
      { path: 'announcement', component: AnnouncementComponent, canActivate: [AuthGuard] },
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
