import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";

import { AuthGuard } from 'app/Services/auth.guard';

//Components
import { RequestFormComponent } from 'app/components/head/pending-form/request-form.component';
import { LeaveRequestComponent } from 'app/components/head/pending-form/leave-request/leave-request.component';
import { RequestForOTComponent } from 'app/components/head/pending-form/request-for-ot/request-for-ot.component';
import { OfficialBusinessComponent } from 'app/components/head/pending-form/official-business/official-business.component';
import { IncidentReportComponent } from 'app/components/head/pending-form/incident-report/incident-report.component';
import { SuggestionBoxComponent } from 'app/components/head/pending-form/suggestion-box/suggestion-box.component';

import { LeaveRequestRFComponent } from 'app/components/head/request-form/leave-request/leave-request.component'
import { RequestForOTRFComponent } from 'app/components/head/request-form/request-for-ot/request-for-ot.component';
import { OfficialBusinessRFComponent } from 'app/components/head/request-form/official-business/official-business.component';
import { IncidentReportRFComponent } from 'app/components/head/request-form/incident-report/incident-report.component';
import { SuggestionBoxRFComponent } from 'app/components/head/request-form/suggestion-box/suggestion-box.component';

import { RequestedFormsComponent } from 'app/components/head/requested-forms/requested-forms.component';

import { DashboardHeadComponent } from 'app/components/head/dashboard/dashboard.component';
import { HeadBaseComponent } from 'app/components/base/head-base/head-base.component';
import { ApproveRequestComponent } from 'app/components/head/pending-form/leave-request/approve-request/approve-request.component';
import { ApproveRequestOBComponent } from 'app/components/head/pending-form/official-business/approve-request-ob/approve-request-ob.component';
import { ApproveRequestOTComponent } from 'app/components/head/pending-form/request-for-ot/approve-request-ot/approve-request-ot.component';
import { ApproveRequestIRComponent } from 'app/components/head/pending-form/incident-report/approve-request-ir/approve-request-ir.component';

import { HeadAccountSettingsComponent } from 'app/components/head/dashboard/account-settings/account-settings.component';

//purchase pending
import { BookletsRequestPFComponent } from 'app/components/head/pending-form/purchase/booklets/booklets-request.component';
import { BusinessCardRequestPFComponent } from 'app/components/head/pending-form/purchase/business-card/business-card-request.component';
import { MarketingMaterialPFComponent } from 'app/components/head/pending-form/purchase/marketing-material/marketing-material.component';
import { PurchaseRequestPFComponent } from 'app/components/head/pending-form/purchase/purchase-request/purchase-request.component';
import { UniformRequestPFComponent } from 'app/components/head/pending-form/purchase/uniform/uniform-request.component';
import { MaintenanceRequestPFComponent } from 'app/components/head/pending-form/purchase/maintenance-request/maintenance-request.component';

//purchase pending modal
import { ApproveRequestPRComponent } from 'app/components/head/pending-form/purchase/purchase-request/approve-request/approve-request.component';
import { ApproveRequestURComponent } from 'app/components/head/pending-form/purchase/uniform/approve-request/approve-request.component';
import { ApproveRequestBRComponent } from 'app/components/head/pending-form/purchase/booklets/approve-request/approve-request.component';
import { ApproveRequestBCComponent } from 'app/components/head/pending-form/purchase/business-card/approve-request/approve-request.component';
import { ApproveRequestMMComponent } from 'app/components/head/pending-form/purchase/marketing-material/approve-request/approve-request.component';
import { ApproveRequestMRComponent } from 'app/components/head/pending-form/purchase/maintenance-request/approve-request/approve-request.component';


// purchase request
import { BookletsRequestComponent } from 'app/components/user/purchase/booklets/booklets-request.component';
import { BusinessCardRequestComponent } from 'app/components/user/purchase/business-card/business-card-request.component';
import { PurchaseRequestComponent } from 'app/components/user/purchase/purchase-request/purchase-request.component';
import { UniformRequestComponent } from 'app/components/user/purchase/uniform/uniform-request.component';
import { MarketingMaterialComponent } from 'app/components/user/purchase/marketing-material/marketing-material.component';
import { MaintenanceRequestComponent } from 'app/components/user/purchase/maintenance-request/maintenance-request.component';

import { FAQsComponent } from 'app/common/faqs/faqs.component';
import { HomeComponent } from 'app/common/home/home.component';
import { PettyCashComponent } from 'app/components/user/petty-cash/petty-cash.component';

const headRoutes: Routes = [

  { 
    path: '',
    component: HeadBaseComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
      { path: 'dashboard', component: DashboardHeadComponent, canActivate: [AuthGuard]},
      { path: 'request-form', component: RequestFormComponent, canActivate: [AuthGuard] },
      { path: 'leave-request', component: LeaveRequestComponent, canActivate: [AuthGuard] },
      { path: 'request-ot', component: RequestForOTComponent, canActivate: [AuthGuard] },
      { path: 'official-business', component: OfficialBusinessComponent, canActivate: [AuthGuard] },
      { path: 'incident-report', component: IncidentReportComponent, canActivate: [AuthGuard] },
      { path: 'suggestion-box', component: SuggestionBoxComponent, canActivate: [AuthGuard] },

      { path: 'rf-leave-request', component: LeaveRequestRFComponent, canActivate: [AuthGuard] },
      { path: 'rf-request-ot', component: RequestForOTRFComponent, canActivate: [AuthGuard] },
      { path: 'rf-official-business', component: OfficialBusinessRFComponent, canActivate: [AuthGuard] },
      { path: 'rf-incident-report', component: IncidentReportRFComponent, canActivate: [AuthGuard] },
      { path: 'rf-suggestion-box', component: SuggestionBoxRFComponent, canActivate: [AuthGuard] },

      { path: 'requested-forms', component: RequestedFormsComponent, canActivate: [AuthGuard] },
      { path: 'account-settings', component: HeadAccountSettingsComponent, canActivate: [AuthGuard] },
      // modal
      { path: 'approve-req-leave', component: ApproveRequestComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-ob', component: ApproveRequestOBComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-ot', component: ApproveRequestOTComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-ir', component: ApproveRequestIRComponent, canActivate: [AuthGuard] },

      { path: 'pf-booklet-req', component: BookletsRequestPFComponent, canActivate: [AuthGuard] },
      { path: 'pf-businessCard-req', component: BusinessCardRequestPFComponent, canActivate: [AuthGuard] },
      { path: 'pf-purchase-req', component: PurchaseRequestPFComponent, canActivate: [AuthGuard] },
      { path: 'pf-uniform-req', component: UniformRequestPFComponent, canActivate: [AuthGuard] },
      { path: 'pf-marketing-material-req', component: MarketingMaterialPFComponent, canActivate: [AuthGuard] },
      { path: 'pf-maintenance-req', component: MaintenanceRequestPFComponent, canActivate: [AuthGuard] },

      //pr modal
      { path: 'approve-req-pr', component: ApproveRequestPRComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-ur', component: ApproveRequestURComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-bu', component: ApproveRequestBRComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-bc', component: ApproveRequestBCComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-mm', component: ApproveRequestMMComponent, canActivate: [AuthGuard] },
      { path: 'approve-req-mr', component: ApproveRequestMRComponent, canActivate: [AuthGuard] },
      
      // purchase request
      { path: 'booklet-req', component: BookletsRequestComponent, canActivate: [AuthGuard] },
      { path: 'businessCard-req', component: BusinessCardRequestComponent, canActivate: [AuthGuard] },
      { path: 'purchase-req', component: PurchaseRequestComponent, canActivate: [AuthGuard] },
      { path: 'uniform-req', component: UniformRequestComponent, canActivate: [AuthGuard] },
      { path: 'marketing-material-req', component: MarketingMaterialComponent, canActivate: [AuthGuard] },
      { path: 'maintenance-req', component: MaintenanceRequestComponent, canActivate: [AuthGuard] },
      { path: 'maintenance-purchase-req', component: PurchaseRequestComponent, canActivate: [AuthGuard] },
      
      //FAQs
      { path: 'faqs', component: FAQsComponent, canActivate: [AuthGuard] },
      //pettyCash
      { path: 'petty-cash', component: PettyCashComponent, canActivate: [AuthGuard] },
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(headRoutes)],
  exports: [RouterModule]
})
export class HeadRoutingModule { }
