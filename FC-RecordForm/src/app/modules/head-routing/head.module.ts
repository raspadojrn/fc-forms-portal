// MODULES
import { NgModule } from '@angular/core';
import { CommonModule }   from '@angular/common';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HeadRoutingModule } from 'app/modules/head-routing/head-routing.module';
import { SharedModule } from 'app/modules/shared.module';

//For Material Design
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatStepperModule} from '@angular/material/stepper';
import {MatButtonModule} from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTabsModule} from '@angular/material/tabs';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatTooltipModule} from '@angular/material/tooltip';


// Service
import { RequestService } from 'app/Services/request.service';
import { AuthService } from 'app/Services/auth.service';
import { AuthGuard } from 'app/Services/auth.guard';


//Components
import { HeadBaseComponent } from 'app/components/base/head-base/head-base.component';

import { DashboardHeadComponent } from 'app/components/head/dashboard/dashboard.component';
import { RequestFormComponent } from 'app/components/head/pending-form/request-form.component';
import { LeaveRequestComponent } from 'app/components/head/pending-form/leave-request/leave-request.component';
import { RequestForOTComponent } from 'app/components/head/pending-form/request-for-ot/request-for-ot.component';
import { OfficialBusinessComponent } from 'app/components/head/pending-form/official-business/official-business.component';
import { IncidentReportComponent } from 'app/components/head/pending-form/incident-report/incident-report.component';
 import { SuggestionBoxComponent } from 'app/components/head/pending-form/suggestion-box/suggestion-box.component';

 import { LeaveRequestRFComponent } from 'app/components/head/request-form/leave-request/leave-request.component'
import { RequestForOTRFComponent } from 'app/components/head/request-form/request-for-ot/request-for-ot.component';
import { OfficialBusinessRFComponent } from 'app/components/head/request-form/official-business/official-business.component';
import { IncidentReportRFComponent } from 'app/components/head/request-form/incident-report/incident-report.component';
import { SuggestionBoxRFComponent } from  'app/components/head/request-form/suggestion-box/suggestion-box.component';

import { RequestedFormsComponent } from 'app/components/head/requested-forms/requested-forms.component';

import { ApproveRequestComponent } from 'app/components/head/pending-form/leave-request/approve-request/approve-request.component';
import { ApproveRequestOBComponent } from 'app/components/head/pending-form/official-business/approve-request-ob/approve-request-ob.component';
import { ApproveRequestOTComponent } from 'app/components/head/pending-form/request-for-ot/approve-request-ot/approve-request-ot.component';
import { ApproveRequestIRComponent } from 'app/components/head/pending-form/incident-report/approve-request-ir/approve-request-ir.component';

import { HeadAccountSettingsComponent } from 'app/components/head/dashboard/account-settings/account-settings.component';

import { BookletsRequestPFComponent } from 'app/components/head/pending-form/purchase/booklets/booklets-request.component';
import { BusinessCardRequestPFComponent } from 'app/components/head/pending-form/purchase/business-card/business-card-request.component';
import { MarketingMaterialPFComponent } from 'app/components/head/pending-form/purchase/marketing-material/marketing-material.component';
import { PurchaseRequestPFComponent } from 'app/components/head/pending-form/purchase/purchase-request/purchase-request.component';
import { UniformRequestPFComponent } from 'app/components/head/pending-form/purchase/uniform/uniform-request.component';
import { MaintenanceRequestPFComponent } from 'app/components/head/pending-form/purchase/maintenance-request/maintenance-request.component';

import { ApproveRequestPRComponent } from 'app/components/head/pending-form/purchase/purchase-request/approve-request/approve-request.component';
import { ApproveRequestURComponent } from 'app/components/head/pending-form/purchase/uniform/approve-request/approve-request.component';
import { ApproveRequestBRComponent } from 'app/components/head/pending-form/purchase/booklets/approve-request/approve-request.component';
import { ApproveRequestBCComponent } from 'app/components/head/pending-form/purchase/business-card/approve-request/approve-request.component';
import { ApproveRequestMMComponent } from 'app/components/head/pending-form/purchase/marketing-material/approve-request/approve-request.component';
import { ApproveRequestMRComponent } from 'app/components/head/pending-form/purchase/maintenance-request/approve-request/approve-request.component';



@NgModule({
  declarations: [
      
    // DashboardHeadComponent,
    HeadBaseComponent,
    RequestFormComponent,
    LeaveRequestComponent,
    RequestForOTComponent,
    OfficialBusinessComponent,
    IncidentReportComponent,
    SuggestionBoxComponent,
    ApproveRequestComponent,
    ApproveRequestOBComponent,
    ApproveRequestOTComponent,
    ApproveRequestIRComponent,
    HeadAccountSettingsComponent,

    //Request Forms
    LeaveRequestRFComponent,
    RequestForOTRFComponent,
    OfficialBusinessRFComponent,
    IncidentReportRFComponent,
    SuggestionBoxRFComponent,
    RequestedFormsComponent,
    
    //Pending forms Purchase Request
    BookletsRequestPFComponent,
    BusinessCardRequestPFComponent,
    MarketingMaterialPFComponent,
    PurchaseRequestPFComponent,
    UniformRequestPFComponent,
    MaintenanceRequestPFComponent,

    //PR Modal
    ApproveRequestPRComponent,
    ApproveRequestURComponent,
    ApproveRequestBRComponent,
    ApproveRequestBCComponent,
    ApproveRequestMMComponent,
    ApproveRequestMRComponent,

  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    HeadRoutingModule,
    HttpModule,
    SharedModule,
    RouterModule,
    
    //For Material Design
    MatMenuModule,
    MatToolbarModule,
    MatSidenavModule,
    MatDividerModule,
    MatListModule,
    MatIconModule,
    MatExpansionModule,
    MatStepperModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatTabsModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatTooltipModule
  ],
  providers: [RequestService, AuthService, AuthGuard],
})
export class HeadModule { }
