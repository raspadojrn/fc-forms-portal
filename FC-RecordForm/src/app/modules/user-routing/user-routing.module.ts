import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";

import { AuthGuard } from 'app/Services/auth.guard';

//Components

import { DashboardUserComponent } from 'app/components/user/dashboard/dashboard.component';

import { LeaveRequestComponent } from 'app/components/user/request-form/leave-request/leave-request.component';
import { RequestForOTComponent } from 'app/components/user/request-form/request-for-ot/request-for-ot.component';
import { OfficialBusinessComponent } from 'app/components/user/request-form/official-business/official-business.component';
import { IncidentReportComponent } from 'app/components/user/request-form/incident-report/incident-report.component';
import { SuggestionBoxComponent } from 'app/components/user/request-form/suggestion-box/suggestion-box.component';

import { UserBaseComponent } from 'app/components/base/user-base/user-base.component';

import { RequestedFormsComponent } from 'app/components/user/requested-forms/requested-forms.component';

// purchase request
import { BookletsRequestComponent } from 'app/components/user/purchase/booklets/booklets-request.component';
import { BusinessCardRequestComponent } from 'app/components/user/purchase/business-card/business-card-request.component';
import { PurchaseRequestComponent } from 'app/components/user/purchase/purchase-request/purchase-request.component';
import { UniformRequestComponent } from 'app/components/user/purchase/uniform/uniform-request.component';
import { MarketingMaterialComponent } from 'app/components/user/purchase/marketing-material/marketing-material.component';
import { MaintenanceRequestComponent } from 'app/components/user/purchase/maintenance-request/maintenance-request.component';

import { UserAccountSettingsComponent } from 'app/components/user/dashboard/account-settings/account-settings.component';
import { PettyCashComponent } from 'app/components/user/petty-cash/petty-cash.component';

import { FAQsComponent } from 'app/common/faqs/faqs.component';
import { HomeComponent } from 'app/common/home/home.component';

const userRoutes: Routes = [

  { 
    path: '',
    component: UserBaseComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
      { path: 'dashboard', component: DashboardUserComponent, canActivate: [AuthGuard]},
      { path: 'requested-forms', component: RequestedFormsComponent, canActivate: [AuthGuard] },
      { path: 'leave-request', component: LeaveRequestComponent, canActivate: [AuthGuard] },
      { path: 'request-ot', component: RequestForOTComponent, canActivate: [AuthGuard] },
      { path: 'official-business', component: OfficialBusinessComponent, canActivate: [AuthGuard] },
      { path: 'incident-report', component: IncidentReportComponent, canActivate: [AuthGuard] },
      { path: 'suggestion-box', component: SuggestionBoxComponent, canActivate: [AuthGuard] },

      { path: 'account-settings', component: UserAccountSettingsComponent, canActivate: [AuthGuard] },

      // purchase request
      { path: 'booklet-req', component: BookletsRequestComponent, canActivate: [AuthGuard] },
      { path: 'businessCard-req', component: BusinessCardRequestComponent, canActivate: [AuthGuard] },
      { path: 'purchase-req', component: PurchaseRequestComponent, canActivate: [AuthGuard] },
      { path: 'uniform-req', component: UniformRequestComponent, canActivate: [AuthGuard] },
      { path: 'marketing-material-req', component: MarketingMaterialComponent, canActivate: [AuthGuard] },
      { path: 'maintenance-req', component: MaintenanceRequestComponent, canActivate: [AuthGuard] },
      { path: 'maintenance-purchase-req', component: PurchaseRequestComponent, canActivate: [AuthGuard] },
      
      //FAQs
      { path: 'faqs', component: FAQsComponent, canActivate: [AuthGuard] },
      //pettyCash
      { path: 'petty-cash', component: PettyCashComponent, canActivate: [AuthGuard] },
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(userRoutes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
