// MODULES
import { NgModule } from '@angular/core';
import { CommonModule }   from '@angular/common';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserRoutingModule } from 'app/modules/user-routing/user-routing.module';


//For Material Design
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatStepperModule} from '@angular/material/stepper';
import {MatButtonModule} from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTabsModule} from '@angular/material/tabs';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatTooltipModule} from '@angular/material/tooltip';

// Service
import { RequestService } from 'app/Services/request.service';
import { AuthService } from 'app/Services/auth.service';
import { AuthGuard } from 'app/Services/auth.guard';
import { SharedModule } from 'app/modules/shared.module';

//Pipes

//Components
import { DashboardUserComponent } from 'app/components/user/dashboard/dashboard.component';
// import { LeaveRequestComponent } from 'app/components/user/request-form/leave-request/leave-request.component'
// import { RequestForOTComponent } from 'app/components/user/request-form/request-for-ot/request-for-ot.component';
// import { OfficialBusinessComponent } from 'app/components/user/request-form/official-business/official-business.component';
// import { IncidentReportComponent } from 'app/components/user/request-form/incident-report/incident-report.component';
import { SuggestionBoxComponent } from 'app/components/user/request-form/suggestion-box/suggestion-box.component';

import { UserBaseComponent } from 'app/components/base/user-base/user-base.component';
import { RequestedFormsComponent } from 'app/components/user/requested-forms/requested-forms.component';




import { UserAccountSettingsComponent } from 'app/components/user/dashboard/account-settings/account-settings.component';

@NgModule({
  declarations: [
    // AppComponent,
    // DashboardComponent,  
    // DashboardUserComponent,
    // RequestFormComponent,
    // LeaveRequestComponent,
    // RequestForOTComponent,
    // OfficialBusinessComponent,
    // IncidentReportComponent,
    SuggestionBoxComponent,
    // ShowErrorComponent,
    // LoginComponent,
    UserBaseComponent,
    RequestedFormsComponent,
    UserAccountSettingsComponent,

 
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    UserRoutingModule,
    HttpModule,
    SharedModule,
    RouterModule,
    

  //For Material Design
  MatMenuModule,
  MatToolbarModule,
  MatSidenavModule,
  MatDividerModule,
  MatListModule,
  MatIconModule,
  MatExpansionModule,
  MatStepperModule,
  MatButtonModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatProgressSpinnerModule,
  MatDialogModule,
  MatTabsModule,
  MatSelectModule,
  MatFormFieldModule,
  MatInputModule,
  MatRadioModule,
  MatTooltipModule,

  ],
  providers: [RequestService, AuthService, AuthGuard,],
})
export class UserModule { }
