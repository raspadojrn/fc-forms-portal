

//Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule  } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';


//For Material Design
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatStepperModule} from '@angular/material/stepper';
import {MatButtonModule} from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatTabsModule} from '@angular/material/tabs';

//Service
import { RequestService } from './Services/request.service';
import { AuthService } from './Services/auth.service';
import { AuthGuard } from './Services/auth.guard';
import { EnumsService } from './Services/common/enums.service';
import { FilterService } from './Services/common/filter.service';
import { PagerService } from 'app/Services/common/pager.service';
import { BaseService } from 'app/Services/base.service';
import { ApiBaseService } from './Services/api-base.service';

//Pipes
import { TimeFormat } from './common/pipes/time24to12.pipe';

//Components
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';


import { Error404Component } from './common/error-page/error404/error404.component';
import { LoginComponent } from './components/login/login.component';





@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    Error404Component,
    TimeFormat,

    
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    BrowserAnimationsModule,
    

    //For Material Design
    MatMenuModule,
    MatToolbarModule,
    MatSidenavModule,
    MatDividerModule,
    MatListModule,
    MatIconModule,
    MatExpansionModule,
    MatStepperModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatTabsModule,
  ],
  providers: [RequestService, AuthService, AuthGuard, EnumsService, FilterService, PagerService, ApiBaseService, BaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
