import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { AuthService } from 'app/Services/auth.service';
import { environment } from 'environments/environment';
import { Headers } from '@angular/http';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


@Injectable()
export class RequestService {

  public action : string;
  public id : any;
  private url = environment.apiBaseUrl;

  constructor(private http: HttpClient, private _auth : AuthService) { }


  
  // METHOD : GET
	// DETAILS : Get All Records
  getRequest(){

    let url = this.url + this.action;

    return this.http.get(url)
      .map((res) => this.extractData(res));
  }

    // METHOD : GET
    // DETAILS : Get Record Details by ID
    getRequestById(id: any)
    {

      let url = this.url + this.action+ id;
      return this.http.get(url).map((res) => this.extractData(res));
    }

    // METHOD : POST
    // DETAILS : Create new record.
    newRecord(model: any)
    {
      // this.action ="items";
      let url = this.url + this.action;

      return this.http
              .post(url, JSON.stringify(model), {headers: this.getUrlEncodedHeaders()} )
              .pipe()
              .map(this.extractData)
              .catch(this.HandleError);
    }


    // METHOD : PUT
    // DETAILS : Update existing record
    updateRecord(id: number , model: any )
    {

      // this.action ="items";
      let url = this.url + this.action + "/" + id;

      return this.http
              .put(url, JSON.stringify(model), { headers : this.getUrlEncodedHeaders() })
              .map(this.extractData)
              .catch(this.HandleError);
    }


    uploadImage(data : any): Observable<Response>
    {
  
        const input = new FormData();
        input.append("file", data);
      this.action ="MaintenanceRequests/upload";
      let url = this.url + this.action;
  
      return this.http
        .post(url, input, { headers:this.getUrlEncodedHeadersForFiles() })
        .map(this.extractData)
        .catch(this.HandleError);
    }

  private extractData(res) {

		let body = null;

		if (res != null) {
        if (res["_body"] != "" && res["_body"]) {
            body = res.json();
        }
        else	{
          body = res;
        }
		}
        return body;
  }

    private HandleError(error: Response | any){
      console.error(error.message || error);
      return Observable.throw(error.error);
    }

    protected getUrlEncodedHeaders(){

      const headers = new HttpHeaders()
        .set("Content-Type", "application/json")
        .set("Access-Control-Allow-Origin", "http://localhost:4200")
      return headers;
    }

    protected getUrlEncodedHeadersForFiles() {
      const headers = new HttpHeaders()
        .set('Authorization', 'Bearer ' + this._auth.token)
      return headers;
    }
    
}
