import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Http, Headers, Response } from "@angular/http";
import { Router } from '@angular/router'
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';


@Injectable()
export class AuthService {

  public token: string;
  public action : string;
  public id : any;
  private url = environment.apiBaseUrl;
  private baseUrl = environment.baseUrl;

  
  constructor(private http: Http, private router: Router) {
    var currentUser = JSON.parse(localStorage.getItem("currentUser"));
    

    if (currentUser) {
      this.token = currentUser.token;
    }
   }

  authenticateUser(credentials){
    let url = this.url + this.action;
    let loginDetails : boolean;
    let userType;
    return this.http
      .post(url, JSON.stringify(credentials), {headers: this.getUrlEncodedHeaders()})
      .map(res => {

        let response = res.json();
        console.log(response)
        if (response) {
          this.token = response.token;
       

          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem(
            "currentUser",
            JSON.stringify({
              id: response.id,
              empId: response.empid,
              userName: response.username,
              userType: response.access,
              accStatus : response.status,
              name: response. fullname,
              token: response.token,
      
            })
          );
      if(response.status == "ACTIVE"){
        switch(response.access.toLowerCase()){
          case "admin":
            this.router.navigateByUrl('admin/home'); 
            break;
          case "user":
            this.router.navigateByUrl('user/home'); 
            break;
          case "head":
            this.router.navigateByUrl('head/home'); 
            break;
          case "mgmt":
            this.router.navigateByUrl('mgmt/home'); 
            break;
          default:
            this.router.navigateByUrl('user/home'); 
            break;
        }
      }else{
        return "The Account is IN-ACTIVE";
      }

        } else {
        }
        return response.status;



      })
      // .catch(this.HandleError);
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    this.token = null;
    localStorage.removeItem("currentUser");
    
    // location.href = "/login";
    this.router.navigate(["login"]);
    // this.cookieService.deleteAll();
  }
  isLoggednIn() {
    return localStorage.getItem("currentUser") !== null;
  }

  isCorrectUserAccess(){
    let userAccess = JSON.parse(localStorage.getItem("currentUser")).userType

    if(window.location.href != environment.baseUrl + "login"){
      if( userAccess.toLowerCase() != window.location.pathname.split("/")[1]){
        return true
      }
    }
    return false
  }

  private extractData(res) {
		let body = null;
		if (res != null) {
        if (res["_body"] != "" && res["_body"]) {
            body = res.json();
        }
        else	{
          body = res;
        }
		}
        return body;
  }
  private HandleError(error: Response | any) {
    console.error(error.message || error);

    let err = null;
    if (error["_body"] != "") {
      err = error.json();
    }

    return Observable.throw(err);
  }

  protected getUrlEncodedHeaders() {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return headers;
  }
}
