import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { Headers } from '@angular/http';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class FilterService {

  public action : string;
  public id : any;
  private url = environment.apiBaseUrl;

  constructor(private http: HttpClient, private fb: FormBuilder) { }

  //Get Filtered data
  //"OrderBy"= Id
  //"OrderType" =(Ascending, Descending)
  //SearchBy "Status"=(Pending, Accept, Approved, Cancelled)
  //"ShowAll" = (true, false)
  //"currentPage"  = 1

  Criteria = this.fb.group({
    OrderBy: ['Id'],
    OrderType: ['Descending'],
    ShowAll: false,
    Status : null
  })
  
  GetfilterData(criteria: any)
  {
    // this.action ="items";
    let url = this.url + this.action;

    return this.http
            .post(url, JSON.stringify(criteria), {headers: this.getUrlEncodedHeaders()} )
            .pipe()
            .map(this.extractData)
            .catch(this.HandleError);
  }

  //only spcific data will show for specific user of head,MGmt
  //if approver is equal to id of head,mgmt the data will show
  filterData(data, tabIndex){
    let id = JSON.parse(localStorage.getItem("currentUser")).id
    let filtered : any =[]
    for( let key in data){
      if(data[key].approver != null){
        console.log( parseInt(data[key].approver.split(',')[tabIndex]) ,id)
        if(  parseInt(data[key].approver.split(',')[tabIndex]) == id )
        {
          filtered.push(data[key])          
        }
      }
    }
    console.log(filtered)  
    return filtered
  }

  private extractData(res) {

		let body = null;

		if (res != null) {
        if (res["_body"] != "" && res["_body"]) {
            body = res.json();
        }
        else	{
          body = res;
        }
		}
        return body;
  }

    private HandleError(error: Response | any){
      console.error(error.message || error);
      return Observable.throw(error.error);
    }

    protected getUrlEncodedHeaders(){

      const headers = new HttpHeaders()
        .set("Content-Type", "application/json")
        .set("Access-Control-Allow-Origin", "http://localhost:4200")
      return headers;
    }
}
