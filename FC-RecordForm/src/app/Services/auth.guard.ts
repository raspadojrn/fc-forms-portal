import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'app/Services/auth.service';
import {Router} from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate  {

  
  constructor(private auth: AuthService,
    private router: Router){
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      if(this.auth.isLoggednIn()){

        if(this.auth.isCorrectUserAccess()){      
          this.router.navigate(["login"]);
          return false;
        }
        return true;
               
      }else{
        console.log("login without token")
        this.router.navigate(["login"]);
        return false;
      }


  }
}
