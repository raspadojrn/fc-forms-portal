
import { Injectable } from '@angular/core';

import { FilterService } from './common/filter.service';
import { ApiBaseService } from 'app/Services/api-base.service';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class BaseService {

  public param : any;
  private services : Observable<any>[] = [];
  
  constructor(	private _apiBaseService : ApiBaseService,
                private _filterService : FilterService) {

    this.services["user"] = this._apiBaseService;
   }

  getService(serviceName : string): Object 
	{
		if(serviceName == "user")
		{
			this.services[serviceName].action = "tbusers/filter";
    }

    return this.services[serviceName];
  }
}
