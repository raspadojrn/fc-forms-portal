import { EnumsService } from 'app/Services/common/enums.service';

import { BaseService } from 'app/Services/base.service';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { PagerService } from 'app/Services/common/pager.service';

@Component({
  selector: 'app-pager',
  templateUrl: './pager.component.html',
  styleUrls: ['./pager.component.css']
})
export class PagerComponent implements OnInit {

  @Input() module : string;
  @Input() criteria : any= [];
  @Output() displayPageList : EventEmitter<any[]> = new EventEmitter<any[]>();

  pager: any = {};
  pagedItems: any[];
  listItems: any[];
  currentPage : number = 1;
  errorMessage : any;
  params : any;

  moduleService : any;
  private pagerModel : any[] = [];

  constructor(private _pagerService : PagerService,
              private _baseService : BaseService,
              private _commonViewService: EnumsService) {
     
               }

    ngOnInit() {
    this.loadPageRecord();   
  }
  loadnNull() {
    this.pagerModel["pageRecord"] = [];
    this.pagerModel["totalRecordMessage"] = `0 Records Found`;

    this.displayPageList.emit(this.pagerModel);
}
              
  loadPageRecord(page?: number) {
    // this.spinnerService.show();
  
    this.moduleService = this._baseService.getService(this.module);
    // this.moduleService.GetfilterData(this.criteria.value)

    this.moduleService.getList(this.criteria.value)
                    .subscribe(list => 
                        {
                            this.listItems  = list;

                            if(page != undefined)
                            {
                                this.pager.totalPages = undefined;
                                this.currentPage = 1;
                            }
                            this.setPage(this.currentPage);

                            // console.log("THISSS IS MODULE", this.listItems )
                            // this.spinnerService.hide();
                        },
    error =>{

      console.log('ERROR')
    });   
}      


setPage(page: number) {
       

  // console.log(this.listItems);
  page = this.listItems["currentPage"];
  this.currentPage = page;
  if (page < 1 || (page > this.listItems["totalPage"] && this.listItems["total"] == 0)) {
      return;
  } 

     
  // All records fetch before pagination.
  this.pagerModel["allRecords"] = this.listItems["list"];

  // get pager object from service
  this.pager = this._pagerService.getPager(this.listItems["total"], page);

  // get current page of items
  this.pagedItems = this.listItems["list"].slice(this.pager.startIndex, this.pager.endIndex + 1);

//    console.log(this.pager); 


  this.pagerModel["pageRecord"] = this.listItems["list"];
  this.pagerModel["totalRecordMessage"] = `${this.pager["totalItems"]} Records Found`;

  if (this.pagerModel["pageRecord"].length > 0) {
      this.pagerModel["pageRecordMessage"] = `Displaying ( ${this.pager["startIndex"] + 1} - ${this.pager["endIndex"] + 1} ) records.`;
  }
  else{
      this.pagerModel["pageRecordMessage"] = null;
  }

   this.pagerModel["errorMessage"] = null;

  // Return paginated list.
  this.displayPageList.emit(this.pagerModel);
}

filterPageWithParams(page : any) {
 
  
  
  console.log(page,this.criteria.value.currentPage = page)
  // params["currentPage"] = page;
  
  

  // this.spinnerService.show();
  this.moduleService = this._baseService.getService(this.module);
  this.moduleService.getList(this.criteria.value)
                  .subscribe(list => 
                      {
                  
                          this.listItems = list;
                          this.listItems["currentPage"] = page
                          // if (this.customizedList.indexOf(this.module) != - 1) {
                          //     this.listItems['list'] = this.formatNewList(list['list']);
                          // }
                          
                         this.setPage(list["currentPage"]);
                         console.log(list)
                          // this.spinnerService.hide();

                      },
  error =>{
      this.errorMessage = this._commonViewService.getErrors(error);
      this.pagerModel["errorMessage"] = this.errorMessage;
      this.displayPageList.emit(this.pagerModel);
      // this.spinnerService.hide();
  });  

}

}
