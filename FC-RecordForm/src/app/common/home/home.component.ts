import { Component, OnInit } from '@angular/core';
import { RequestService } from 'app/Services/request.service';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { CustomValidator } from 'app/models/validators/custom.validator';
import { EnumsService } from 'app/Services/common/enums.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title = "Home"
  empData :any = [];
  userType: any;
  constructor(private _requestService : RequestService, private fb: FormBuilder, private _enums : EnumsService) { }

  ngOnInit() {
    this.getEmpData();
  }

  getEmpData(){
    let user = JSON.parse(localStorage.getItem("currentUser"))
    this.userType = user.userType

    this._requestService.action = "Tblprofiles/"
    this._requestService.getRequestById(user.empId).subscribe(data => console.log(this.empData = data, this.userType))
  }

}
