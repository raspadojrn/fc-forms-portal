import { Component, OnInit, ViewChild } from '@angular/core';
import {MatAccordion} from '@angular/material/expansion';
import { RequestService } from 'app/Services/request.service';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.css']
})
export class FAQsComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;

  title = "Frequently Asked Questions(FAQs)"

  faqsData: any =[];
  constructor(private _requestService : RequestService) { }

  ngOnInit() {
    this._requestService.action = "faqs";
    this._requestService.getRequest().subscribe(data => console.log("FAQs",this.faqsData = data))
  }

}
